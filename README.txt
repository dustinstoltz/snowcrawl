Snowcrawl

Author:		Abe Gong
Created:	1/18/2011
Modified:	3/18/2011

=== Description ================================================================
Snowcrawl is a python library for directed webcrawls.  The main goal is to identify web pages or sites matching certain criteria, and build an index of those sites.  The software can also generates some useful byproducts: the network of edges linking pages, classification statistics for sites, and the index.html content of sites themselves.  See [link to paper] for an explanation of the methodology applied to the problem of constructing a representative sampling frame for political websites.

=== Dependencies ===============================================================

multiprocessing, csv, os, threading, time, re, urllib2, datetime

=== Architecture ===============================================================
Snowcrawl offers several different crawlers sharing a common API.

* SoloCrawler		executes using a single process.  Slow.  Simple.  Good for debugging.
* MultiCrawler		executes using a multiprocessing Pool.  Faster.  Still pretty simple.
* ServerCrawler		and
* ClientCrawler		execute a crawl together.  The server maintains a queue of urls to visit, and the clients visit and process them.

=== API: Functions =============================================================

The basic API has only two functions: a set of initializers and runUntilDone.

The initializer functions set up Crawler objects.  Each of the four types has its own arguments.

	SolCrawler()
	MultiCrawler(pool_size=None)
	ServerCrawler(server_port, server_pw, batch_size=100)
	ClientCrawler(server_ip, server_port, server_pw, pool_size=None)

* pool_size determines how many processes to start in multithreaded process pools.  If pool_size is omitted, both Multi and ClientCrawler default to cpu_count.  Note: since downloading files often has substantial latency, specifying more processes than processors is often much more efficient.

* server_ip, server_port and server_pw designate the ip, port and password for the server process manager. Port and password must be the same for the instances to connect and share information.  IP must also refer to the correct server address.  Make sure appropriate firewalls are turned off for this code to work.


runUntilDone( path, parameters, master_list={}, seed_list=[], current_wave=None, overwrite_existing_files=False )

Once your Crawler(s) are initialized, call runUntilDone to execute a crawl.  This is the workhorse function in the library.  For Solo, Multi, and ServerCrawler, it initializes the output files and state space for the crawl, then crawls sites in a series of waves.  In each wave, snowcrawl selects urls to crawl, processes the urls, updates the internal state space of the crawl, and saves output.  Pseudocode for this process is listed below.  (Note that in the client-server implementation, some of these steps may be performed asynchromously.)

	while( termination conditions are not met ) OR ( No urls remain to be processed ):
		Execute one wave:
			1. Choose urls to crawl -- unvisited urls from the master list
			2. Process the urls -- download, classify, etc.
			3. Process the results -- update master list with edge counts and visited status
			4. Save output

* path: the path to the folder for snowcrawl output.  If this folder does not already exist, snowcrawl will create it.  If it exists, but already contains snowCrawl output files, snowcrawl will terminate with an error message, unless overwrite_existing_files is set.

* parameters: a parameters object controlling most aspects of the crawl.  See below.

* master_list: a dictionary of tuples containing the starting state of the crawl. For instance, passing master_list={ 'www.google.com': (100,1), 'myhomepage.com' : (10,0) } would initialize master_list with two sites.  The crawler would start with the assumption that google was visited already with 100 total inlinks, and myhomepage.com is unvisited, with 10 inlinks.  This syntax can be useful for resuming crashed or halted crawls, or starting a snowcrawl with a "do not crawl" list.

* current_wave: an integer containing the wave number to start counting from.  If current_wave is omitted, it will default to 1.

* seed_list: a list of strings containing the urls to start the snowcrawl. When starting a new crawl from scratch, this is probably the option to use.  Passing seed_list=['foo.com', 'bar.org'] is equivalent to passing master_list={'foo.com':(0,0), 'bar.org':(0,0)}.  Note: either master_list or seed_list must contain a valid entry to begin a crawl.

* If overwrite_existing_files is True, then all output files in the path will be erased and recreated.  Be very careful with this option!

For ClientCrawler, runUntilDone accepts no arguments. It simply begins picking batches from the appropriate ServerCrawler queue until the server terminates the process.

=== API: State variables =======================================================

current_wave:	The number of waves processed so far
urls_remaining:	The number of urls remaining to be processed
urls_completed:	The number of urls that have been successfully processed.

=== API: Parameters ============================================================

Most aspects of snowcrawl are controlled through a parameters object passed to runUntilDone.  This object has the following variables.

Name			Type			Default	Description
wave_size		positive int	100		The maximum number of urls crawled in a given wave.
time_inc		positive float	1		Time increment in seconds between output to the screen.
time_limit		positive float	10		Maximum time in seconds spent attempting to process any given url.

path
save_states		boolean			True	Should snowCrawl generate snowcrawl_state.pkl during the crawl?
save_files		boolean			False	Should snowCrawl generate wave folders and save files found during the crawl?  (Note: if either save_edges or save_files is True, snowCrawl will generate wave[X]/ folders)
save_edges		boolean			False	Should snowCrawl generate wave folders and save edgelists found during the crawl?

csv_header		Array of string	[]		snowCrawl allows you to export site statistics to a csv file (snowcrawl_results.csv).  csv_header designates the header for those columns.
prioritize_urls	boolean			False	Should snowCrawl sort urls by links from previously kept files?

process_url_function					See the next section for details on this custom function.
decide_terminate_function				See the next section for details on this custom function.



=== API: Custom Functions ======================================================

Most of the flexibility and power of snowCrawl comes from the ability to write custom url-processing functions.  Snowcrawl's architecture allows for both object-orientation and multiprocessing across multiple computers.  This somewhat difficult task is accomplished by using a precise API allowing custom functions for processing urls and deciding when to terminate the classifier.

process_url_function( (url, params) )
	returns: (kept, text, eligible_urls, all_edges, statistics)
		kept			A boolean variable denoting whether the page met the crawl criteria.
		text			The text of the downloaded page.
		eligible_urls	A list of urls found within the crawled page. This list will be compared to the master list, and crawled in future waves.  For directed crawls, eligible_urls should be [] when kept is False.
		all_edges		A list of all urls found within the crawled page. If the save_edges flag is set, this list will be converted to an edgelist and saved.
		statistics		A list of values to export to snowcrawl_results.csv

Important!  Values returned from a custom process_url_function must conform exactly to these specifications or snowcrawl will probably crash!

The params object does not contain url-specific parameters.  Instead, it contains general objects for all url processing.  This allows for object-orientation, while still allowing multiprocessing and client-server stuff.  For example, if the processor uses pre-compiled regular expressions or a classifier, those objects can be passed through the params object.

Note: Snowcrawl uses interruptable threads.  If a url is taking too long to process, the thread will be terminated and null values (e.g. 0, '', []) returned.


decide_terminate_function( self )
	returns: Boolean

If decide_terminate_function returns 1, the crawler will terminate after the current wave.  Most decide_terminate_functions will probably be based on snowcrawl's state variables(current_wave, urls_remaining, urls_completed), but other possibilities (e.g. based on time) can also be implemented.




=== Output =====================================================================

* snowcrawl_params.xml
	Saves the crawl parameters

* snowcrawl_seedlist.txt
	Saves the initial list of seed sites

* snowcrawl_state.pkl
	Saves the current state of the crawl.  So that if the crawl is interrupted, it can be resumed without problems.

* snowcrawl_progress.csv
	Tracks the progress of the crawl, wave by wave. These variables are tracked: wave, total, completed, remaining, time. Total, completed, and remaining refer to the number of urls in the master list.

* snowcrawl_results.csv
	wave				The wave in which the site was crawled.
	siteid				The site's id within the wave
	url					The site url (acts as a unique id)
	filename			The file where the url was stored. Set to '.' if the file was not saved
	timed_out			1 if the thread timed out; 0 if processing succeeded
	kept				1 is the url met the criteria; 0 otherwise.
	...					Additional columns can be added using csv_header in the Parameters class, and returning matching statistics from a process_url_function.

* files/wave[X]/[filename]
	Document text saved using the save_files flag.  Documents are saved in separate wave folders.

* edges/snowcrawl_edgelist[X].txt
	Edgelists saved using the save_edges flag.  Each edge list stores only the outlinks found in the current wave.  Together all the edgelist files can be used to reconstruct the full network explored in the snowcrawl.

* states/snowcrawl_state[X].txt
	Wave-by-wave master lists saved using the save_edges flag.  Each master list builds on the previous lists.  This is probably overkill for most applications.


=== Examples ===================================================================

* test1.py runs a small, simple SoloCrawler with all the options turned on. All sites are included in the snowball sample.

* test2.py runs a simple MultiCrawler with all the options turned on. The first 100,000 sites including the word "scrapbook" are included.  This is a good place to begin for moderate-sized crawls.

* test3.py runs a client-server architecture, guided by a full-fledged text classifier.  It runs using a localhost port, so client and server must be run from the same machine.  To run the demo, execute test3_server.py and test3_client.py (you can launch them in either order).  Note that these scripts require the test3_processing.py support library, which itself depends on the porter.py module, test3_stopwords.txt, and test3_classifier.csv files.


Note: These demos will overwrite the results directories when you run them.  You may want to change the names (e.g "results1-bk") before running.
Note: Don't expect the crawl results to be exactly the same as the demos.  Sites go up and down, and the day-to-day content may change.  All of these things can---and should---affect snowcrawl's results.



=== Style Guide ==============================================================
http://code.google.com/p/soc/wiki/PythonStyleGuide#naming
