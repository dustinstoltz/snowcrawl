<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us">
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Flickr - Photo Sharing</title>
        <meta name="keywords" content="photography, digital photography, cameraphones, camera, hobby photography, photo, photos, digital camera, compactflash, smartmedia, cameras, canon, nikon, olympus, fujifilm, video">
        <meta name="description" content="Flickr is almost certainly the best online photo management and sharing application in the world. Show off your favorite photos and videos to the world, securely and privately show content to your friends and family, or blog the photos and videos you take with a cameraphone.">
		<script type="text/javascript">
		  // for .htc reference
		  var _images_root = 'http://l.yimg.com/g/images';
		</script>
		<!--[if LT IE 7]>
        <style type="text/css">
                .trans_png {
            behavior: url('/javascript/pngbehavior.htc');
            border:0;
        }
        </style>
		<![endif]-->
        <link href="http://l.yimg.com/g/css/c_home_new.css.v99532.17" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" type="image/ico" href="http://l.yimg.com/g/favicon.ico">
	<meta name="viewport" content="width=950" />
	<link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Flickr" />
</head>
<body>
	
<div id="beacon"><img src="http://geo.yahoo.com/f?s=792600119&t=a1376b5755b9175704a7d02f0baba429&fl_ev=0&lang=en&intl=us" width="0" height="0" alt="" /></div>

			<img src="http://geo.yahoo.com/p?s=792600119&t=1204882655&position=top" style="position:absolute;top:0;right:0" alt="">
	<div class="wrap" id="top" role="banner">
	<div class="wrap-inner">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<div id="featured-image">
						<h1 id="logo"><img src="http://l.yimg.com/g/images/flickr-logo-stacked-en-us.png.v3" class="trans_png" alt="Flickr" width="150" height="75"></h1>
						<a href="/photos/lynhana/416152814/?f=hp" class="image_link" title="Photo detail page"><img src="http://l.yimg.com/g/images/home_photo_lyn.jpg" alt="-" width="500" height="333"></a>
						<cite id="featured-image-caption"><a href="/creativecommons/" class="image_link"><img src="http://l.yimg.com/g/images/home_cc.png" width="16" height="16" alt="Creative Commons License" class="trans_png"></a> By <a href="/photos/lynhana/">+lyn </a></cite>
					</div>
				</td>
				<td>
					<div id="featured-intro">
						<p id="featured-signin">
							<a id="head-signin-link" href="/signin/">Sign In</a>
						</p>
						<div id="featured-create-account">
							<a id="signup-link" href="/signup/?f=hp" class="sbab image_link"><strong>Create Your Account</strong></a>
							<p>Only takes a moment with your Yahoo!, Google or Facebook ID</p>
						</div>
						
						<div style="position:relative;">
							<img src="http://l.yimg.com/g/images/en-us/home_tagline.png.v9" class="trans_png"  width="400" height="90" alt="Share your photos. Watch the world.">
							
														<div id="vid_andvideos"><img src="http://l.yimg.com/g/images/en-us/video/rosette_andvideo.png.v1" width="50" height="51" class="trans_png" alt="And Video" /></div>
													</div>
						
						<div id="featured-search">
							<form action="/search/" method="get" role="search"><label for="search" class="obscured">Search</label><input type="text" id="search" name="q" size="20" value=""> <input type="hidden" name="f" value="hp" /><input type="submit" value="SEARCH" class="Butt"></form>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="wrap" role="main">
	<div class="wrap" id="strip">
		<div class="wrap-inner">
			<h2 class="obscured">Statistics</h2>
			<ul>
								<li class="first"><strong>5,762</strong> uploads in <a href="/photos/">the last minute</a> <b>&#183;</b></li>
												<li><strong>160,129</strong> things tagged with <a href="/photos/tags/morning/clusters/">morning</a> <b>&#183;</b></li>
												<li><strong>4.5 million</strong> things <a href="/map/">geotagged</a> this month <b>&#183;</b></li>
								<li class="tour"><a href="/tour/?f=hp">Take the tour</a></li>
			</ul>
		</div>
	</div>

	<div class="wrap" id="promo-ad">
		<div class="wrap-inner">
			<h2 class="obscured">Features</h2>
			<div class="left">
				<a href="/tour/share/?f=hp" class="image_link"><img src="http://l.yimg.com/g/images/en-us/home_share.png.v1"  id="hp_share" alt="Share & stay in touch" height="166" width="235"></a>
				<a href="/tour/upload/?f=hp" class="image_link"><img src="http://l.yimg.com/g/images/en-us/home_upload.png.v1"  id="hp_up" alt="Upload & organize" height="153" width="239"></a>
				<br><br>
												<a href="/tour/makestuff/?f=hp" class="image_link"><img src="http://l.yimg.com/g/images/en-us/home_make.jpg" id="hp_make" alt="Make stuff!" height="139" width="146"></a>
												<a href="/explore/?f=hp" class="image_link"><img src="http://l.yimg.com/g/images/en-us/home_explore.png.v1" id="hp_explore" alt="Explore..." height="139" width="288"></a>
			</div>
			<div class="ad">
				<iframe src="http://adjax.flickr.yahoo.com/ads/792600119/LREC/" width="302" height="265" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			</div>
		</div>
	</div>

	<div class="wrap" id="tour">
		<div class="wrap-inner">
			<h2 class="obscured">Tour Flickr</h2>
			<table border="0">
				<tr>
					<td width="220"><a href="/tour/?f=hp" class="image_link sbab"><strong style="padding-left:40px; padding-right:40px;">Take the Tour</strong></a></td>
					<td width="600">
						<p>
							Explore <a href="http://blog.flickr.com/">Flickr Blog</a>, the <a href="/map/">World Map</a>, <a href="/cameras/">Camera Finder</a> or interesting uploads from <a href="/explore/?f=hp">the last 7 days</a>.
						</p>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="wrap" id="foot" role="contentinfo">
        <div class="wrap-inner">
                <div id="yahoo_co">
			<a href="http://www.yahoo.com/">
				<span class="yahoo-logo-from">from</span>
				<img src="http://l.yimg.com/g/images/home_yahoo_grey.gif.v3" width="56" height="11" alt="Flickr, a Yahoo! Company">
			</a>
		</div> 
		
		<p class="LanguageSelector" id="LanguageSelector">

                        <span class="choose">Choose a language:</span>
                                
	<span>
		<a href="/change_language.gne?lang=zh-hk&magic_cookie=632356d4eeefd1ef57229a8a81e73992" class="image_link" id="lang_zh-hk">繁體中文</a>		<b>&#183;</b>
		<a href="/change_language.gne?lang=de-de&magic_cookie=632356d4eeefd1ef57229a8a81e73992" >Deutsch</a>
		<b>&#183;</b>
		<a href="/change_language.gne?lang=en-us&magic_cookie=632356d4eeefd1ef57229a8a81e73992" class="selected">English</a>
		<b>&#183;</b>
		<a href="/change_language.gne?lang=es-us&magic_cookie=632356d4eeefd1ef57229a8a81e73992" >Espa&#241;ol</a>
		<b>&#183;</b>
		<a href="/change_language.gne?lang=fr-fr&magic_cookie=632356d4eeefd1ef57229a8a81e73992" >Fran&#231;ais</a>
		<b>&#183;</b>
		<a href="/change_language.gne?lang=ko-kr&magic_cookie=632356d4eeefd1ef57229a8a81e73992" class="image_link" id="lang_ko-kr">한글</a>		<b>&#183;</b>
		<a href="/change_language.gne?lang=it-it&magic_cookie=632356d4eeefd1ef57229a8a81e73992" >Italiano</a>
		<b>&#183;</b>
		<a href="/change_language.gne?lang=pt-br&magic_cookie=632356d4eeefd1ef57229a8a81e73992" >Portugu&#234;s</a>
				<b>&#183;</b>
		<a href="/change_language.gne?lang=vn-vn&magic_cookie=632356d4eeefd1ef57229a8a81e73992" >Tiếng Việt</a>
						<b>&#183;</b>
		<a href="/change_language.gne?lang=id-id&magic_cookie=632356d4eeefd1ef57229a8a81e73992" >Bahasa Indonesia</a>
				
			</span>
	
	</p>
		<ul>
			<li><a href="http://blog.flickr.com/">Flickr Blog</a></li>
			<li>&#183; <a href="/about/">About Flickr</a></li>
			<li>&#183; <a href="/terms.gne" onClick="window.open('/terms.gne','TOC','status=yes,scrollbars=yes,resizable=yes,width=600,height=480'); return false">Terms of Service</a></li>
			<li>&#183; <a href="/privacy_policy.gne">Your Privacy</a></li>
			<li>&#183; <a href="http://info.yahoo.com/relevantads" target="_blank">About Our Ads</a></li>			<li>&#183; <a href="http://docs.yahoo.com/info/copyright/copyright.html">Copyright/IP Policy</a></li>
			<li>&#183; <a href="/guidelines.gne">Community Guidelines</a></li>
			<li>&#183; <a id="ft-report-abuse" href="/report_abuse.gne">Report Abuse</a></li>
		</ul>
		<p id="copyright">
			Copyright &copy; 2011 Yahoo! Inc. All rights reserved.
		</p>
	</div>
</div>
			<img src="http://geo.yahoo.com/p?s=792600119&t=181947159&position=bottom"  style="position:absolute;top:0;right:0" alt="">
	


</body>
<script type="text/javascript">	
var _enable_popup_login = true;


(function(){function h(){m.l("/fragment.gne?name=social-auth-fragment");b.setTimeout(h,1E4)}var c=window.navigator;(c&&c.userAgent).match(/MSIE\s(6[^;]*)/);var b=window;c=document;var m={_msxml_progid:["MSXML2.XMLHTTP.6.0","MSXML3.XMLHTTP","Microsoft.XMLHTTP","MSXML2.XMLHTTP.3.0"],l:function(a){if(this.req=this.c()){this.req.open("GET",a,true);var e=this;this.req.onreadystatechange=function(){if(e.req.readyState==4&&e.req.status==200){var d=e.req.responseText;if(d=="1,1"||d=="1,0")b.location.reload()}};
this.req.send(null)}},c:function(){var a;try{a=new XMLHttpRequest}catch(e){for(var d=0,j=this._msxml_progid.length;d<j;++d)try{a=new ActiveXObject(this._msxml_progid[d]);break}catch(p){}}finally{return a}}};if(b._enable_popup_login){var g=false,f=false,i,n=function(a){var e=a+"?popup=1&redir=/photo_grease_postlogin.gne?d="+b.location;try{i=b.open(e,"newWindow","width=650,height=650,resizable=1,scrollbars=1,location=yes")}catch(d){return true}try{i.focus&&i.focus()}catch(j){}b.setTimeout(function(){if(!g)b.location=
a+"?popup=0&redir="+encodeURIComponent("/photo_grease_postlogin.gne?d="+b.location+"&notpopup=1")},2E3);return false},o=c.getElementById("head-signin-link"),l=function(a){f=true;if(!a)a=b.event;a.cancelBubble=true;a.returnValue=false;a.stopPropagation&&a.stopPropagation();a.preventDefault&&a.preventDefault();n("/signin");b.setTimeout(k,20);return false};c.getElementById("signup-link").onclick=l;o.onclick=l;var k=function(){var a;if(document.cookie.match(/cookie_session=[^;]+/gi)){
a=true}else{a=false}if(a){f=false;h()}else b.setTimeout(k,10)};c.onfocusin=function(){if(g){f=false;h()}};b.onfocus=function(){if(g){f=false;h()}};b.onblur=function(){if(f)g=true};c.onfocusout=function(){if(f)g=true}}})();

</script>
</html>