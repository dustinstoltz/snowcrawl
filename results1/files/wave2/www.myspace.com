
<!DOCTYPE html>
<html class="noJS en-US">
<!-- Splash -->
<head><title>
	Myspace | Social Entertainment
</title>
	<script>_start = +new Date,_gaq=[['_setAccount', 'UA-6293770-1'],['_trackPageview']];(function(o,e,s,g,p,j){e.className=e.className.replace('noJS','hasJS');for(j in window)o[j]=1;g.src=p+('https:'==p?'//ssl':'//www')+'.google-analytics.com/ga.js';g.type='text/javascript';g.async=true;s.parentNode.insertBefore(g,s);window.delta=function(){var d=[],i;for(i in window)if(o[i]!==1)d.push(i);return d;};})({},document.documentElement,document.getElementsByTagName('script')[0],document.createElement('script'),document.location.protocol);</script>
	<meta name="keywords" content="myspace, social entertainment, social network, social media" /><meta http-equiv="expires" content="0" /><meta http-equiv="Pragma" content="no-cache" /><meta name="description" content="Myspace is the leading social entertainment destination powered by the passion of fans. Music, movies, celebs, TV, and games made social." /><meta name="fragment" content="!" />
    <meta name="verify-v1" content="7YGMEkLzqZwwDvnyumG9QeSJq7JgzCye9uyzdjF5j0s=" />
	<meta property="fb:app_id" content="109417482436806" />
    <link rel="search" type="application/opensearchdescription+xml" href="http://cms.myspacecdn.com/cms/api/opensearch_people.xml"
        title="MySpace People" />
    <link rel="search" type="application/opensearchdescription+xml" href="http://cms.myspacecdn.com/cms/api/opensearch_videos.xml"
        title="MySpace Videos" />
    <link rel="search" type="application/opensearchdescription+xml" href="http://cms.myspacecdn.com/cms/api/opensearch_images.xml"
        title="MySpace Images" />
<link rel="canonical" href="http://www.myspace.com/" />
<link rel="stylesheet" type="text/css" href="http://x.myspacecdn.com/modules/common/static/css/futuraglobal_hydxtnen.css" />
<link rel="stylesheet" type="text/css" href="http://x.myspacecdn.com/modules/common/static/css/futura/iconsbase_slebgyuj.css" />
<link rel="stylesheet" type="text/css" href="http://x.myspacecdn.com/modules/common/static/css/futura/icons_yt73kuqc.css" />

<link rel="stylesheet" type="text/css" href="http://x.myspacecdn.com/modules/splash/static/css/splashv2bundle_xbkmdpii.css" />
</head>
<body class="layout_1_2_1">
	

	<header id="globalHeader">
		<nav id="globalNav">
			<ul>
				
<li class="logo">
<h2>	
	<a id="msStaticLogo" class="MSIcon MSLogo" href="/">
		Myspace
	</a>
</h2>	
		
<div id="flashLogoContainer">
	<a href="/">
	<span id="logoInTrigger">	
	Myspace	
	</span>
	</a>
	<div id="msFlashLogos">
	</div>
	<div id="leftLogoTrigger"></div>
	<div id="rightLogoTrigger"></div>	
	<div id="bottomLogoTrigger"></div>	
</div>
</li>
                
	<li><a id="headerSignUp" href="https://www.myspace.com/signup" class="glue signup">Sign up</a></li>
	<li class="loginButton"><a id="NLLogin" href="https://www.myspace.com/auth/form" class="loginPopupButton">Login</a></li>


<li  id="HeaderNavLinks">
	<ul class="group">	
		<li><a id="NLFriends" href="/browse/people">Friends</a></li>
		
		<li><a id="NLMusic" href="/music">Music</a></li>
		<li><a id="NLTopics" href="/everything">Topics</a></li>
		<li><a id="NLVideo" href="/video">Video</a></li>
		<li><a id="NLGames" href="/games">Games</a></li>		
	</ul>
</li>
				

<li id="headerSearch" class="search">
	<form id="MSSearchForm" action="/search/people" method="get"><fieldset><input id="MSSearchInput" name="q" type="text" placeholder="Search People" autocomplete="off" data-searchtype="People"></input><button type="submit" class="glue utility iconOnly"><span class="MSIcon searchIconDark"></span></button></fieldset></form>	
</li>
			</ul>
			<span class="MSIcon loadingIcon spacewayLoader">Loading</span>
		</nav>
	</header>
	
		<div class="wrap">
			
			<div id="leaderboard">
			
			</div>
			
			<div class="container">
				
				<article>
					
    <div class="layout"><section class="row row0 rowPath0 rowDepth0" id="row0"><section class="column column0 columnPath0_0 columnDepth1 firstColumn lastColumn allowDrop" id="col0_0"><article class="module module1 columnModule0 odd HeroUnitModule" id="module-1" data-mt="1059737436" data-area="Splash">
<div class="wrapper">
<section class="content moduleBody">
<div id="marketing">
                <div id="swf"><a href="http://www.myspace.com/music/blog/2011/04/new-music-tuesday-april-5-2011/"  onclick="MySpace.CMS.track('aeb441fa-8435-4438-bec6-6d1126f2603b','131379')">
<img src="http://cms.myspacecdn.com/cms/x/11/15/Kills_hero_v1.jpg" alt="New Music Tuesday: The Kills" width="960" height="250" />
</a></div>
                <div id="ad"><div id="tkn_medrec" class="ad_container medrec" style="height:250px;"><iframe width="300" height="250" marginwidth="0" marginheight="0" HSPACE="0" VSPACE="0" frameborder="0" scrolling="no" allowTransparency="true" src="about:blank"></iframe></div>
</div>
</div></section></div>
</article>
<section class="columnEnd"></section></section><section class="rowEnd"></section></section><section class="row row1 rowPath1 rowDepth0" id="row1"><section class="column column0 columnPath1_0 columnDepth1 firstColumn allowDrop" id="col1_0"><article class="module module2 columnModule0 odd InfoGraphicModule" id="module-2" data-mt="1564706570" data-area="Splash">
<div class="wrapper">
<section class="content moduleBody">
<div class="infographic">
	<h4>
		Myspace is all your favorite music, movies, celebs, and TV
	</h4><img alt="Follow, Get Latest, and Share!" src="http://cms.myspacecdn.com/cms/x/11/8/Infographic.jpg" /><ul>
		<li class="first">1. Follow</li><li class="second">2. Get the latest</li><li class="third">3. Share</li>
	</ul>
</div></section></div>
</article>
<section class="columnEnd"></section></section><section class="column column1 columnPath1_1 columnDepth1 lastColumn allowDrop" id="col1_1"><article class="module module3 columnModule0 odd SignUpV2Module draggable" id="module-3" data-mt="504435417" data-area="Splash">
<div class="wrapper">
<section class="content moduleBody">

<div class="splashSignUp">
	<p class="submit">
		<a href="https://www.myspace.com/signup" class="glue signup">
			Sign up free</a>
	</p>
	<p class="login">
		<strong>
			Already a member?</strong> <a class="glue noButtonPrimary loginPopupButton"
				href="https://www.myspace.com/auth/form">
				Login</a>
	</p>
	

<div class="fboMashup">
    <p class="divider">
        <strong>OR</strong>
    </p>
    <p class="facebookImport">
        <a class="btnFacebook" href="https://graph.facebook.com/oauth/authorize?client_id=8744a0ccdce1491c4474dacf75dc2d12&redirect_uri=http%3A%2F%2Fwww.myspace.com%2Ffbocallback&scope=email,offline_access,user_about_me,user_birthday,user_likes,publish_stream&display=popup" target="_blank">
            <img src="http://x.myspacecdn.com/modules/splash/static/img/fb_logo.gif" />
            <span>Connect with Facebook</span>
        </a>
    </p>
    <p class="facepile">
        
    </p>
    <div class="learnmoretext" style="display:none;">
        To help save you time setting up your profile (and give you more of the things you love), we can pull your interests (like movies, music, and celebs) and public profile info (name, email, gender, birth date, and photo) from Facebook and automatically add and follow them on your Myspace profile.  Don't worry, you'll be able to edit it later!
    </div>
</div>
</div>
</section></div>
</article>
<section class="columnEnd"></section></section><section class="rowEnd"></section></section><section class="row row2 rowPath2 rowDepth0" id="row2"><section class="column column0 columnPath2_0 columnDepth1 firstColumn lastColumn allowDrop" id="col2_0"><article class="module module4 columnModule0 odd ContentBlockModule" id="module-4" data-mt="-345183400" data-area="Common">
<div class="wrapper">
<section class="content moduleBody">
<ul>
	<li class="columnSpan1"><div class="musicColumn">
		<h5>
			Music
		</h5><ul>
			<li><div class="splashMedia">
				<aside>
					<a href="http://www.myspace.com/music/blog/2011/04/new-music-tuesday-april-5-2011"><img alt="The Kills" src="http://cms.myspacecdn.com/Music_Futura/editorial/box_kills.png" /></a>
				</aside><section>
					<a class="title" href="http://www.myspace.com/music/blog/2011/04/new-music-tuesday-april-5-2011">The Kills</a><p class="noPad"><a class="subtitle" href="http://www.myspace.com/music/blog/2011/04/new-music-tuesday-april-5-2011">New Music Tuesday</a></p><p></p><a class="textlink" href="http://www.myspace.com/music/blog/2011/04/new-music-tuesday-april-5-2011">Read Now</a><p class="desc">Check out new music including The Kills’ ‘Blood Pressures.’</p>
				</section>
			</div></li><li><div class="splashMedia">
				<aside>
					<a href="http://www.myspace.com/music/blog/2011/04/ten-best-sophomore-albums"><img alt="Top 10: Sophomore Albums" src="http://cms.myspacecdn.com/Music_Futura/editorial/box_kurt.png" /></a>
				</aside><section>
					<a class="title" href="http://www.myspace.com/music/blog/2011/04/ten-best-sophomore-albums">Top 10: Sophomore Albums</a><p class="noPad"><a class="subtitle" href="http://www.myspace.com/music/blog/2011/04/ten-best-sophomore-albums">New Music Tuesday</a></p><p></p><a class="textlink" href="http://www.myspace.com/music/blog/2011/04/ten-best-sophomore-albums">Read Now</a><p class="desc">Metallica to Eminem, we revisit some of the best second efforts.</p>
				</section>
			</div></li><li><div class="splashMedia">
				<aside>
					<a href="http://www.myspace.com/music/blog/2011/04/whos-popping-scattered-trees"><img alt="Popping: Scattered Trees" src="http://cms.myspacecdn.com/Music_Futura/editorial/box_trees.png" /></a>
				</aside><section>
					<a class="title" href="http://www.myspace.com/music/blog/2011/04/whos-popping-scattered-trees">Popping: Scattered Trees</a><p class="noPad"><a class="subtitle" href="http://www.myspace.com/music/blog/2011/04/whos-popping-scattered-trees">New Music Tuesday</a></p><p></p><a class="textlink" href="http://www.myspace.com/music/blog/2011/04/whos-popping-scattered-trees">Read Now</a><p class="desc">A Chicago band rallies around its bereaved leader.</p>
				</section>
			</div></li>
		</ul>
	</div></li><li class="columnSpan1"><div class="curatorsColumn">
		<h5>
			Curators
		</h5><ul>
			<li><div class="splashMedia">
				<aside>
					<a href="http://www.myspace.com/comicbooks"><img alt="Comic Books with Keoni" src="http://cms.myspacecdn.com/cms/x/11/15/040511-Keoni-124.jpg" /></a>
				</aside><section>
					<a class="title" href="http://www.myspace.com/comicbooks">Comic Books with Keoni</a><p>243,348 Friends</p><a class="textlink" href="http://www.myspace.com/comicbooks">+ Friend</a><p class="desc">Keep up with the latest superhero action.</p>
				</section>
			</div></li><li><div class="splashMedia">
				<aside>
					<a href="http://www.myspace.com/lol"><img alt="LOL with Claire" src="http://cms.myspacecdn.com/cms/x/11/15/040511-Claire-124.jpg" /></a>
				</aside><section>
					<a class="title" href="http://www.myspace.com/lol">LOL with Claire</a><p>313,316 Friends</p><a class="textlink" href="http://www.myspace.com/lol">+ Friend</a><p class="desc">Learn some random knowledge while getting a good laugh in.</p>
				</section>
			</div></li><li><div class="splashMedia">
				<aside>
					<a href="http://www.myspace.com/vj/stream"><img alt="Music Videos with Ned" src="http://cms.myspacecdn.com/cms/x/11/15/040511-NedATL-124.jpg" /></a>
				</aside><section>
					<a class="title" href="http://www.myspace.com/vj/stream">Music Videos with Ned</a><p>196,207 Friends</p><a class="textlink" href="http://www.myspace.com/vj/stream">+ Friend</a><p class="desc">It's Tuesday, which means new music. Peep some vids.</p>
				</section>
			</div></li>
		</ul>
	</div></li><li class="columnSpan1"><div class="topicsColumn">
		<h5>
			Topics
		</h5><ul>
			<li><div class="splashMedia">
				<aside>
					<a href="http://www.myspace.com/everything/uconn-huskies"><img alt="March Madness" src="http://cms.myspacecdn.com/cms/x/11/15/040511-UConn-124.jpg" /></a>
				</aside><section>
					<a class="title" href="http://www.myspace.com/everything/uconn-huskies">March Madness</a><p></p><a class="textlink" href="http://www.myspace.com/everything/uconn-huskies">+ Follow</a><p class="desc">Congratulations to UConn. NCAA Men's Basketball champs!</p>
				</section>
			</div></li><li><div class="splashMedia">
				<aside>
					<a href="http://www.myspace.com/everything/scream-4"><img alt="Scream 4" src="http://cms.myspacecdn.com/cms/x/11/15/040511-Scream-124.jpg" /></a>
				</aside><section>
					<a class="title" href="http://www.myspace.com/everything/scream-4">Scream 4</a><p></p><a class="textlink" href="http://www.myspace.com/everything/scream-4">+ Follow</a><p class="desc">Ask the cast of 'Scream 4' anything you want!</p>
				</section>
			</div></li><li><div class="fimServeAd">
				<div id="tkn_adspecial3" class="ad_container adspecial3" style="height:124px;"><iframe width="300" height="124" marginwidth="0" marginheight="0" HSPACE="0" VSPACE="0" frameborder="0" scrolling="no" allowTransparency="true" src="about:blank"></iframe></div>

			</div></li>
		</ul>
	</div></li>
</ul></section></div>
</article>
<article class="module module5 columnModule1 even ContentLinksModule" id="module-5" data-mt="564355113" data-area="Common">
<div class="wrapper">
<section class="content moduleBody">
<div class="linksColumn">
	<h5>
		<a href="http://www.myspace.com/music/">More in Music</a>
	</h5><ul>
		<li><a href="http://www.myspace.com/music">Music Home</a></li><li><a href="http://www.myspace.com/music/my-music">My Music</a></li><li><a href="http://www.myspace.com/music/videos">Music Videos</a></li><li><a href="http://www.myspace.com/music/charts">Charts</a></li><li><a href="http://www.myspace.com/music/new-releases">New Releases</a></li><li><a href="http://www.myspace.com/music/featured-playlists">Featured Playlists</a></li><li><a href="http://www.myspace.com/music/blog">Music News</a></li><li><a href="http://www.myspace.com/karaoke">Karaoke</a></li><li><a href="http://www.myspace.com/events/Browse/PopularShows">Shows</a></li><li><a href="http://www.myspace.com/music/artists">Artists</a></li>
	</ul>
</div><div class="linksColumn">
	<h5>
		<a href="http://www.myspace.com/guide/curators/">More in Curators</a>
	</h5><ul>
		<li><a href="http://www.myspace.com/baseball">Baseball with Joshua</a></li><li><a href="http://www.myspace.com/basketball">Basketball with Barney</a></li><li><a href="http://www.myspace.com/marquismovies">Movies with Marquis</a></li><li><a href="http://www.myspace.com/vinyltoys">Vinyl Toys with TB</a></li><li><a href="http://www.myspace.com/adrienne">Urban Culture with Adrienne</a></li><li><a href="http://www.myspace.com/meme">Memes with Amber</a></li><li><a href="http://www.myspace.com/celebrityfashion">Fashion with Seojie</a></li><li><a href="http://www.myspace.com/thewtf">WTF with Redux</a></li><li><a href="http://www.myspace.com/realitytv">Reality with Nikki</a></li><li><a href="http://www.myspace.com/photog">Photography with Paris</a></li>
	</ul>
</div><div class="linksColumn">
	<h5>
		<a href="http://www.myspace.com/everything/">More in Topics</a>
	</h5><ul>
		<li><a href="http://www.myspace.com/everything/2011-ncaa-mens-division-i-basketball-tournament">March Madness</a></li><li><a href="http://www.myspace.com/everything/britney-spears">Britney Spears</a></li><li><a href="http://www.myspace.com/everything/blake-lively">Blake Lively</a></li><li><a href="http://www.myspace.com/everything/lady-gaga">Lady Gaga</a></li><li><a href="http://www.myspace.com/everything/nicole-snooki-polizzi">Snooki</a></li><li><a href="http://www.myspace.com/everything/lindsay-lohan">Lindsay Lohan</a></li><li><a href="http://www.myspace.com/everything/miley-cyrus">Miley Cyrus</a></li><li><a href="http://www.myspace.com/everything/vanessa-hudgens">Vanessa Hudgens</a></li><li><a href="http://www.myspace.com/everything/teen-mom">Teen Mom</a></li><li><a href="http://www.myspace.com/everything/insidious">Insidious</a></li>
	</ul>
</div></section></div>
</article>
<article class="module module6 columnModule2 odd AdContainerModule draggable" id="module-6" data-mt="-1729082449" data-area="Common">
<div class="wrapper">
<section class="content moduleBody">
<div id="tkn_leaderboard2" class="ad_container leaderboard2" style="height:90px;"><iframe width="728" height="90" marginwidth="0" marginheight="0" HSPACE="0" VSPACE="0" frameborder="0" scrolling="no" allowTransparency="true" src="about:blank"></iframe></div>
	</section></div>
</article>
<section class="columnEnd"></section></section><section class="rowEnd"></section></section></div>
	<div id="Div1" style="height: 1px; width: 1px;">
		<div id="tkn_adspecial1" class="ad_container adspecial1" style="height:1px;"><iframe width="1" height="1" marginwidth="0" marginheight="0" HSPACE="0" VSPACE="0" frameborder="0" scrolling="no" allowTransparency="true" src="about:blank"></iframe></div>

	</div>
	

				</article>
				<div class="footershim">
				</div>
			</div>			
		</div>
	
	

<form action="https://www.myspace.com/auth/login" method="post" class="signin popupForm shownone"  id="loginPopup">
   <fieldset>
		
		<input type="hidden" name="formLocation" value="popupForm" />
        <input type="hidden" name="hash" value="MIGcBgkrBgEEAYI3WAOggY4wgYsGCisGAQQBgjdYAwGgfTB7AgMCAAECAmYDAgIAwAQIKs9rXpKNHycEEGcaW7BJsStdtpI2JyXhTuQEUGvqYOflHwZEeMKeueQwAqdleSRRJZfnHxxg%2bsaM7hFy59Ihs%2fqE5Njbr8SyPCjqqz7%2fxHqu%2bXidS74pWc%2fd4WIicZJJQAWJ5w6w0KDJ0vNB" />
        <input type="hidden" name="SMSVerifiedCookieToken" />
		<input type="hidden" name="NextPage" value="" />
		<input type="hidden" name="js" value="-1" />
		<input type="hidden" name="fbtoken" value="" />
		
            <h2>Login</h2>
        

        <p class="email">
			<label for="emailPopup">Email</label>
			
            <input type="text" id="emailPopup" class="emailInput" name="Email" value="" tabindex="1" />
			
		</p>

        <p class="password">
			<label for="passwordPopup">Password</label>
            
            <input type="password" id="passwordPopup" class="passwordInput" name="Password" tabindex="2" />
			
		</p>
        <p class="check">
            <input type="checkbox" id="rememberPopup" checked="checked" class="rememberUser" name="Remember" tabindex="3" />
            <label for="rememberPopup">Keep me logged in</label> 
		</p>
		<div class="captchaBlock"></div>
        <p class="submit right">
			
            <a href="https://www.myspace.com/auth/resetpassword/" class="forgotPassword">Forgot password?</a>
			
            <button name="loginBtn" type="submit" class="glue primary" tabindex="5">Login</button></p>
        
        <p class="right needAccount">Need an account? <a href="https://www.myspace.com/signup">Sign up</a>
            </p>
       
		
       <div id="SMSVerifyObj"></div>
  </fieldset>
</form>

<iframe id="loginIframe" name="loginIframe"></iframe>

	

<footer id="pageFooter">
        
            <ul class="first">
                <li class=" checkerLightBG">
                    <h3>
                        Around Myspace</h3>
                </li>
                <li><a href="/music">
                    Music</a></li>
                <li><a href="/video">
                    Video</a></li>
                <li><a href="/everything/movies">
                    Movies</a></li><li><a href="/everything/television">
                    Television</a></li><li><a href="/everything/celebrities">
                    Celebrity</a></li>
				<li><a href="/events">Events</a></li>
				<li><a href="/threads/ ">Threads</a></li>
				<li><a href="/themes">Themes</a></li>
            </ul>
        
        
        
        
		  <ul>
      <li class=" checkerLightBG">
            <h3>
                    What's Trending
            </h3>
      </li>
                  <li>
	          <a href="http://www.myspace.com/justinbieber" onclick="MySpace.CMS.track('f8869db6-2028-4ccf-b9e7-971c94afaead','114978')">Justin Bieber</a>
            </li>
                   <li>
	          <a href="http://www.myspace.com/wizkhalifa" onclick="MySpace.CMS.track('f8869db6-2028-4ccf-b9e7-971c94afaead','131237')">Wiz Khalifa</a>
            </li>
                   <li>
	          <a href="http://www.myspace.com/arcadefireofficial" onclick="MySpace.CMS.track('f8869db6-2028-4ccf-b9e7-971c94afaead','131381')">Arcade Fire</a>
            </li>
                   <li>
	          <a href="http://www.myspace.com/katyperry" onclick="MySpace.CMS.track('f8869db6-2028-4ccf-b9e7-971c94afaead','131380')">Katy Perry</a>
            </li>
                   <li>
	          <a href="http://www.myspace.com/guide/curators" onclick="MySpace.CMS.track('f8869db6-2028-4ccf-b9e7-971c94afaead','128712')">Curators</a>
            </li>
                   <li>
	          <a href="http://www.myspace.com/ladygaga" onclick="MySpace.CMS.track('f8869db6-2028-4ccf-b9e7-971c94afaead','114982')">Lady Gaga</a>
            </li>
                   <li>
	          <a href="http://www.myspace.com/eminem" onclick="MySpace.CMS.track('f8869db6-2028-4ccf-b9e7-971c94afaead','114984')">Eminem</a>
            </li>
                   <li>
	          <a href="http://www.myspace.com/kimsaprincess" onclick="MySpace.CMS.track('f8869db6-2028-4ccf-b9e7-971c94afaead','130270')">Kim Kardashian</a>
            </li>
       </ul>


        <ul>
            <li class=" checkerLightBG">
                <h3>
                    Also on Myspace</h3>
            </li>
            <li><a href="/guide/im">
                IM</a></li>
            <li><a href="/guide/mobile">
                Mobile</a></li>
            <li><a href="/themes">
                Themes</a></li>
            <li><a href="/guide/myspace_layouts">
                Myspace Layouts</a></li>
            <li><a href="/guide/sync">
                Sync</a></li>
            <li><a href="/karaoke">
                Karaoke</a></li>
        </ul>
        <ul>
            <li class=" checkerLightBG">
                <h3>
                    IGN</h3>
            </li>
            <li><a href="http://www.ign.com/">
                Video Games</a></li>
            <li><a href="http://www.ign.com/index/reviews.html">
                Game Reviews</a></li>
            <li><a href="http://cheats.ign.com/">
                Video Game Cheats</a></li>        
            <li><a href="http://www.ign.com/videos">
                Game Videos</a></li> 
            <li><a href="http://www.direct2drive.com/">
                Download Video Games</a></li> 
		</ul>
        <ul>
            <li class=" checkerLightBG">
                <h3>
                    Join Myspace</h3>
            </li>
            <li><a href="https://www.myspace.com/signup">
                Sign up</a></li>
            <li><a href="/careers">
                Careers</a></li>
            <li><a href="http://developer.myspace.com">
                Developers</a></li>
            <li><a href="/Help/Terms">
                Terms</a></li>
			<li><a href="/karaoke/termsofuse/main">
                Karaoke Terms</a></li>
            <li><a href="/Help/Privacy">
                Privacy Policy</a></li>
			<li><a href="/Help/AboutUs">
                About us</a></li>
        </ul>
        <ul class="last">
            <li class=" checkerLightBG">
                <h3>
                    Get Help</h3>
            </li>
            <li><a href="/help">
                Help & FAQs</a></li>
            <li><a href="/help/safety">
                Safety Tips</a></li>
			<li><a class="gapReportAbuse" href="/help/reportabuse?AbuseType=Profile&reportedUserId=0&ProfileContentID=0&Httpreferrer=http%3A%2F%2Fwww.myspace.com%2FModules%2FSplash%2FPages%2FIndex.aspx%2F0">Report Abuse</a></li> 
			<li><a href="/sitemap">
                Sitemap</a></li>
			
			<li><a href="/Help/Privacy">Privacy Policy</a></li>
			<li><a href="/pages/privacysettings">Learn more</a></li>
			<li><a href="https://www.myads.com/myspace/login.html?pr=d3oatdQsvADw%2BazsdI%2Fi0A%3D%3D&adv=gf.1">
                Advertisers</a></li>
            <li><a href="/pressroom">
                Press Room</a></li>
        </ul>
        <div class="moduleFooter checkerLightBG">
            Copyright &copy; 2003-2011 Myspace Inc.
            All Rights Reserved
			<span class="moduleFooterRight">
			<strong>Change Country:</strong>
			<a href="/international">  United States (English) </a>
			</span>
		</div>
</footer>

	


<footer id="siteFooter">
	<ul class="footertools group">
		
        

<li class="imTray displayNone">
	<div class="footerButton"><span id="buddyCount">0 IM Friends</span><span class="MSIcon IMIconWhite"></span></div>
	<section class="tray">
    <div class="imHeader" >
		<div id='userImg' class='vcard notification'>
			<a class='msProfileLink'><img  data-src="" alt="" class='profileimage photo' data-friendid="" /></a>
    		 <div class="settingDD">
				<ul >
					<li class="moreOptions" title='More Options' id='morelinks'>
					<span> &nbsp;&nbsp;▼</span>
					<div class="status">Online</div>
					<div class="Drop">
						<ul >
							<li><a id="goOffline">Go Offline for IM</a></li>
							<li><a id="generalsettings">IM Settings</a></li>
							<li><a id="editFriends" >IM Friends</a> </li>
							<li><a id="editBlockList" >IM Block List</a></li>
							<li><a id="settingshelp" href="#">Help</a></li>
						</ul>
					</div>
					</li>
				</ul>
			</div>
		</div>
		<button type="button" id="wideIM" class="MSIcon rightArrowIcon"></button>
		<button type="button" class="MSIcon minusCircleIconDarkWhite"></button> 
     </div>
     <div class="settingDD" id="settings">
         <div style="border:none;">
            <div class="settingsPanel"> 
            <div class="settingsPanelItem header" >General Options</div>
             <div id="settingsPanelSubSoundHeader" class="settingsPanelItem subheader">Play a sound when:</div>		        
                <div class="settingsPanelItem"><input id="cbSoundIM" type="checkbox" />I receive a new IM</div>
		        <div class="settingsPanelItem"><input id="cbSoundFriend" type="checkbox" />A friend comes online</div>

            <div id="privacysettingsdiv" style="display:block;">
		        <div class="settingsPanelItem subheader" style="background:none;" >I'm available for IM to:</div>
				
                <div class="settingsPanelItem"><input id="rbPrivacyEveryone" type="radio" name="privacy"/><label for="rbPrivacyEveryone" id="lblPrivacyEveryone">Everyone</label></div>
		        <div class="settingsPanelItem"><input id="rbPrivacyALL" type="radio" name="privacy"/><label for="rbPrivacyALL">Friends</label></div>
		        <div class="settingsPanelItem"><input id="rbPrivacyIM" type="radio" name="privacy"/><label for="rbPrivacyIM">Only IM Friends</label></div>
		        <div class="settingsPanelItem button" ><input type="button"  value="Cancel" class="glue utility small" id="cancelSettingsButton"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="button"  value="Save" class="glue primary" id="saveSettingsButton"/></div>
		    </div>
            </div>
            </div>
        </div>
    
    
        <ul class="chatList">
			<li class="accordion accordion-trayActive">
                <h3 id="onlineGroup">Online Friends<span>▲</span></h3>
                   <span id='clearSearch' class="MSIcon searchIconDark"></span>
                   <span class="search"><input id="searchList" type="text" /></span>
                    <div class="list">
						<ul class="friendsOnline groupList" ></ul>
						<h3 id="idleGroup">Idle Friends<span>▼</span></h3>
						<ul class="friendsIdle" style="overflow:hidden;"></ul>
					</div>
			</li>
		</ul>
	</section>
</li>

<li class="onlineTray displayNone">	 
    <ul  id="chatCarousel">
       <li style="display:none;"><span class="MSIcon leftArrowIcon"></span></li>
       <li style="display:none;"><span class="MSIcon rigthArrowIcon"></span></li>
    </ul>
</li>

<li class="newlyOnlineTray displayNone" >	
    <ul></ul>
</li>





        

<li class="imToast">
    <ul style="">
    
     
    </ul>
</li>
		<li class="privacyTray" id="privacyTray" style="float:right; visibility:hidden; width:0;"></li>
	</ul>
    
</footer>


	
<script>
//<![CDATA[
function namespace(parent,name,extender){if(typeof parent==='string'){var parts=parent.split('.');extender=name;parent=window;for(var i=0;i<parts.length;++i)parent=namespace.ns(parent,parts[i]);if(extender)for(var k in extender)parent[k]=extender[k];return parent}return namespace.ns(parent,name,extender)}
namespace.ns=function(p,n,x){var a=p[n];if(!a){var tn=p.__typeName;tn=(tn?tn+'.':'')+n;eval('p[n]=a=function '+tn.replace(/[\W]/g,'$')+'(name,extender){return (typeof name!=="string")?namespace.ns(p,n,name):namespace.ns(a,name,extender);}');
a.__namespace=a.__typeName=tn;a.getName=function(){return tn}}if(x)for(var k in x)a[k]=x[k];var w=namespace.watch,l=w.length,i=0;if(tn && l)for(i=0;i<l;i++)w[i](a);return a};
namespace.watch = [];
namespace('MySpace', {StaticContentBase:'http://x.myspacecdn.com'})('UI');
namespace('MySpaceRes',{"Global":{"AddMessage":"Add a message","NeededRequestConfirmation":"{0} will have to confirm your request.","ORBig":"OR","PrivacyEmailAddress":"\"What\u0027s my email address?\"","PrivacyLastName":"\"What\u0027s my last name?\"","PrivacyMessage":"Verify how well you know {0} to send this request.","RequestSentConfirmation":"Once confirmed, you and {0} will be friends.","RequestSentTitle":"Request Sent!","SendRequestTitle":"Send a friend request to {0}","PendingRequestExists":"You already have a pending friend request for {0}.","ErrorMessageOnFaliure":"We’re sorry, an error occurred while processing your friend request.  Please try again.","CannotAddSelf":"You cannot friend yourself.","IncorrectInputs":"The email or last name you entered is incorrect. Please re-enter and try again.","RequiredPrivacyInputs":"You must enter an email address or last name.","RequiredEmailAddress":"You must enter an email address.","InvalidVerificationCode":"Security code does not match. Please try again.","PeopleYouMayKnow":"People You May Know","ShowMore":"Show more","AddFriend":"+ Friend","MaxEmailLimit":"Sorry, that email address is too long.  Try again!","MaxLastNameLimit":"Sorry, that last name is too long.  Try again!","MaxMessageCharLimit":"Easy there!  Your message exceeds our max character limits.","Friend":"Friend","PleaseLogin":"Please login or sign up","NeedMyspaceAccount":"You need a Myspace account to complete this action.","LoginToMyspace":"Login to Myspace","SignupFree":"Sign up free","Add":"Add","Message":"Message","BlockUser":"Block User","BlockUser_Description":"Are you sure you want to block this user?","RemoveFriend":"Remove Friend","RemoveFriend_Description":"Are you sure you want to remove this friend?","ErrorMessageRequest":"We’re sorry, an error occurred while processing your request.  Please try again.","FriendToChatDescription":"You need to be friends with {0} in order to chat.","InvalidEmailOrPassword":"Invalid email and/or password. Please try again","ForYourSecurity":"For your security, please fill in the info below"},"Comment":{"DeleteThisComment":"Delete this comment","RemoveThisComment":"Remove this comment and all replies from the activity stream and the original page.","Delete":"Delete","MarkAsSpamLowerS":"Mark as spam","HelpUsCleanTheHouse":"Help us clean the house by marking spam and \u003ca target=\u0027_blank\u0027 href=\u0027{0}\u0027\u003ereporting abuse\u003c/a\u003e when you spot something offensive.","BlockUser":"Block user","BlockThisUserQ":"Block this user?","BlockThisPersonFromContacting":"Block this person from contacting you (you can \u003ca target=\u0027_blank\u0027 href=\u0027{0}\u0027\u003eunblock\u003c/a\u003e later).","HelpUsCleanTheHouseByMarkAsSpam":"Help us clean the house by marking spam when you spot something offensive.","Post":"Post","JustNow":"just now","PostACommentEllipsis":"Post a comment...","PostAReplyEllipsis":"Post a reply...","Reply":"Reply","ThisCommentWillPostToXsProfile":"This comment will post to {0}\u0027s profile","Commentback":"Comment Back"},"SiteSearch":{"People":"People","Music":"Music","Videos":"Videos","Photos":"Photos","Games":"Games","Events":"Events","MySpace":"Myspace","Web":"Web","In":"in"},"Common":{"Global_Add_Friend_Tip":"Add user to friends","Yes":"Yes","No":"No","Ok":"Ok","Attention":"Attention","PleaseWait":"Please wait...","PageXofY":"Page {0} of {1}"},"SplashPage":{"AccountLocked1":"Click Here","AccountLocked2":"This account has been locked out for 15 minutes due to excessive failed login attempts. Please try again or","AccountLocked3":"to enter a different e-mail and password.","LoadingAltText":"loading..."},"Header":{"Cancel":"Cancel / Cancelación","Continue":"Continue / Continuar"},"ProfileDisplayV3":{"ChangePhoto":"Change Photo"},"RichTextEditorGeneral":{"AddPhoto":"Add a Photo","AddVideo":"Add a Video"},"Photos":{"ChooseAlbumLabel":"Choose Album","PhotoUpdatedSuccessfullyNotificationMsg":"Your photo was updated successfully!","ProfilePicSuccessfullyCreatedMsg":"Nice, your new profile photo has been saved.","Crop":"Crop","MakeProfilePicture":"Make Profile Picture","ServiceNotAvailableMsg":"Service is inaccessible. Try again later"},"VideosPage":{"ChooseVideo":"Choose a Video","MyVideos":"My Videos","MyUploads":"My Uploads","MySubscriptions":"My Subscriptions","MyFavorites":"My Favorites"},"MySpaceTV":{"Futura_VideoCharts_Title":"Video Charts","AV_ChartAllTime":"All Time","AV_ChartMonth":"Month","AV_ChartWeek":"Week","AV_ChartDay":"Day","AV_MyVideosUploads":"Uploads","AV_MyVideosFavorites":"Favorites","AV_MyVideosSubscriptions":"Subscriptions","AV_PlayerBy":"By"},"Signup":{"UploadPhoto":"Upload a photo","TakePhoto":"Take a photo","ChooseImageDescription":"You can upload an image or use a webcam."},"HomeDisplayV3":{"SuperPost_UploadPhotoError":"There was a problem uploading your photo.","SuperPost_PhotoFileType":"Your file was not uploaded. Please select an image file with one of the following formats: .jpg, .gif, .bmp, .tiff, .png.","SuperPost_PhotoFileSize":"Your photo is too large to upload. Please select a file under 5 MB."},"UploadResource":{"CameraActivity":"Looking for camera activity.","Cancel":"Cancel","Capture":"Capture","ChooseDevice":"Choose the device that allows you to see a picture in this window.","Error":" Error","Keep":"Keep","MsgCamActivity":"Looking for camera activity.","MsgNoCamFound":"Sorry, no camera was found. In order to take a photo, you will need to install a camera.","Msg_NoCam":"Please make sure that your camera is active and not in use by any other application.","OK":"OK","Photo":" {0} Photo","Retake":"Retake","RetakePic":"Retake the photo","SaveFlashSettings":"Click \u0027Allow\u0027 and \u0027Close\u0027 to save your settings.","TakePicture":"Take Picture","UploadError1":" Error uploading file","UploadError2":" There was an error uploading","Uploading":" Uploading","UploadPic":"Upload your photo to Myspace","UploadToMySpace":"Upload your photo to Myspace"}});
MySpaceClientContext = MySpace.ClientContext = {"UserId":-1,"DisplayFriendId":0,"IsLoggedIn":false,"FunctionalContext":"Splash","UserType":1};
MySpaceClientContext.PreferredCulture = 'en-US';
MySpace.ClientIntMaintenanceConfigs = {};
MySpace.ClientMaintenanceConfigs = {"asyncphotobrowsecaching":0,"msplinksconvertall":0,"photocommentsajax":0,"inf_ind_newvideocomment":0,"musicjv_amazoninplace":0,"enablevideocategorydropdown":0,"applications_userapppreferences":0,"inf_ind_unreadim":0,"webimtestclient":0,"applications_showafterloaduhp":0,"applications_showafterloadprofiles":0,"applications_enablev2settingspopup":0,"musicjv_allmyplaylists":0,"inf_ind_newclassified":0,"webimclientrichtexteditor":0,"mdpcontaineruseopencanvas":0,"inf_ind_newautotag":0,"applications_enableopenidpopuphandling":0,"applications_removeonappprofile":0,"friendselectorinstatusmooddialog":0,"musicjv_debug":0,"photoquickpostmessaging":0,"mdp_enable_homeappnavdragdrop":0,"musicjv_stoprefresh_medrec":0,"mystuffautos":0,"mystuffreportabusepost":0,"mystuffuploadphotos":0,"dwbeaconclientv2":0,"dwbeaconpymk":0,"dwbeaconmailv2":0,"mdp_rpcsetauthtoken":0,"mmt_usebigpipeuniveralid":0,"eventrouterrelayclient":0,"eventrouterdatarelayclient":0,"signup_addrecommendationv2_deafultselectall":0,"appnotification_useosmltemplatename":0,"webimofflineindicatorsupport":0,"musicjv_mymusicv2_newscroll":0,"signup_strengthmeter":0,"dwbeaconphotossites":0,"testvalidationofkey":0,"core_indicatorcounters":0,"topicsfriendstabfollowerssimple":0,"reshareallactivities":0,"stream_threecolumn":0,"enableinfluencer":0,"photosort":0,"stream_inlinereduxplayer":0};
(function(w,n,p){function a(o,n,v){o[n]?p.apply(o[n],v):o[n]=v;}n('MySpace.LoginPrompt',{"LoginUrl":"/redirector","SignupUrl":"https://www.myspace.com/signup"});n('MySpace.MediaSelector.JSFiles',{"Cropper":"http://js.myspacecdn.com/modules/common/static/js/jquery/cropperbundle_ftcbjdlp.js","MediaSelector":"http://js.myspacecdn.com/modules/common/static/js/jquery/mediaselectordialogbundle_umz9xci9.js"});n('MySpace.UI.Common.photoUploadConfig',{"uploadUrl":"http://upload.myspace.com/modules/upload/services/upload.ashx?upload_source=WebStream&output_type=HTTP","photoUploaderUrl":"http://lads.myspace.com/superpost/MediaUploader_sp.swf","photoUploadHash":"MIGcBgkrBgEEAYI3WAOggY4wgYsGCisGAQQBgjdYAwGgfTB7AgMCAAECAmYDAgIAwAQIdfl2afbjrZ4EEKUKsE%2fdL5fU2ocxq64ZBngEUJNk2O54MYjyeph9jgptdaN7OUzzZxdW5YAtLzCkdzQw%2beLQJ4%2b%2bNAx9oDcwq8rZO7ixf6WYWvp3AlPJ0Ld5UDOopgy45EhIJ26UZTGKrBRN","culture":"en-US","unixTime":1302016308,"uploadToken":"NS7JgEC3PeM8b%2fGAkK6ADLN%2bf4JEJIW0Wk%2bfD7fqcieW0K93cn9GJS0GN33YrqZ8lCISPC%2bS7QZnQZ2iS1zeLg%3d%3d","moduleHash":"MIGcBgkrBgEEAYI3WAOggY4wgYsGCisGAQQBgjdYAwGgfTB7AgMCAAECAmYDAgIAwAQI%2fLSP42lqDvEEEBFPN9rYpoy%2bMTz16WN3JBsEUOGV5mGv8ATqidUOHQmQPIiSqijmQD67HZvHSJ3tdn6OlVvMAR3EJAfBg2fqFtDNOkrD44P7lN0j8zUr4bimsRGlF5xvLBSxrRNvksNhzoq8"});n('MySpace.UI.DeferredScript',{"ValidatorJSPath":"http://js.myspacecdn.com/modules/common/static/js/jquery/validations_sjdd5qb8.js"});n('MySpace.UI.Pages').Area="Splash";n('MySpace.UI.Pages').HashMashter="MIGcBgkrBgEEAYI3WAOggY4wgYsGCisGAQQBgjdYAwGgfTB7AgMCAAECAmYDAgIAwAQI7rpz%2fk5qVAIEEKcqFn63smfFKEqliu%2bxV18EUMTgeTApMuJ%2fihKLV0%2bIjBlx5Vlm6anjtd7yVyaT9MRliVj3uLmN%2bYS10T95GOa487UOpbOmdtU3LjGX5pLFCSLsXhK7FNNexfSENAiPJ3dd";n('MySpace.UI.Splash',{"SignUpURL":"/signup"});n('SMSVerification',{"FieldName":"SMSVerifiedCookieToken","SwfUrl":"http://lads.myspace.com/SMSToken/smsToken.swf"});})(window,namespace,[].push);
//]]>
</script>

	
	
<script type="text/javascript">
	namespace('MySpace.Ads', {Account:{},BandType:{},PageParams:"rnd=819709192"});

</script><script type="text/javascript" src="http://js.myspacecdn.com/modules/common/static/js/atlas/msglobal_phwq1-x2.js"></script>
<script type="text/javascript" src="http://cms.myspacecdn.com/cms/js/ad_wrapper0177.js"></script>

<script type="text/javascript">
	sdc_wrapper("tkn_medrec", "/MySpaceHomepage/Index-SiteHome,10000000", "x14");
</script>
<script type="text/javascript">
	sdc_wrapper("tkn_leaderboard2", "/MySpaceHomepage/LaederboardOnSplash,10000001", "Frame1");
</script>
<script type="text/javascript">
	sdc_wrapper("tkn_adspecial3", "/MySpaceHomepage/Index-SiteHome,10000000", "300x124");
</script>
<script type="text/javascript">
	sdc_wrapper("tkn_adspecial1", "/MySpaceHomepage/Index-SiteHome,10000000", "x77");
</script>
	<script type="text/javascript" src="http://js.myspacecdn.com/modules/splash/static/js/splashv2bundle_rzr-txrd.js"></script>

<script type="text/javascript">
	
	var _comscore = _comscore || [];
	_comscore.push ( {c1: "2", c2: "4000002" });
	( function() {
		var s = document.getElementById("comscoreTag");
		if (s) s.parentNode.removeChild(s);
		var el = document.getElementsByTagName("script")[0];
		s = document.createElement("script"), 
		s.async = true;
		s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
		s.setAttribute("id", "comscoreTag");
		el.parentNode.insertBefore(s, el);
	})();
</script>
<script type="text/javascript">
	MySpace.BeaconData={"dsid":"2","dsv":"1","rd":"","rqs":"","refpg":"","rpf":"","d":"www.myspace.com","qs":"","pf":"Splash","fa":"","pgnm":"/","cip":"2379464017","pc":"en-US","pid":"6760772068367795828","pidf":"1","ABtd":"0","t":"1302016308653","ct":"1302016308653","ci":"Ann Arbor","st":"MI","co":"US","dmac":"505","uff":"0","uatv":"ua=Python-urllib/2.6","sip":"170660426","uid":"-2","pggd":"8317332b-7e75-4e31-9b6f-ec4896bd7f81","prid":"-1","ili":"0","at":"-1","cfv":"0:0:0","cef":"0","sliu":"0","pref":"0","kvp":"bt=0&pidv=2&sn=els2mwebnet4932&page=%2f&ff=t&restqs=&"};
MySpace.BeaconAddress="http://b.myspace.com/~myspace/beacon/b.ashx?";
MySpace.Beacon.sn="els2mwebnet4932";
MySpace.Beacon.ff="t";

if (MySpace.Beacon) MySpace.Beacon.SendPageBeacon();

</script>
	
	
				<noscript>
					<img src="http://b.scorecardresearch.com/p?c1=2&c2=4000002&cv=2.0&cj=1" />
				</noscript>
	
	
	<script>jQuery.ready()</script>
</body>
</html>