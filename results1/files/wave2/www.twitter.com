<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <script type="text/javascript">
//<![CDATA[
(function(g){var a=location.href.split("#!")[1];if(a){window.location.hash = "";g.location.pathname = g.HBR = a.replace(/^([^/])/,"/$1");}})(window);
//]]>
</script>
  <script type="text/javascript">
//<![CDATA[
if(!window.HBR){var matches,url,path,domain;url=document.location.toString();try{domain=url.match(/https?:\/\/[^\/]+/);if(matches=url.match(/(.+?)#(.+)/)){url=matches[1];path=matches[2];if(path){var arr=path.split(/\?/);path=arr[0];var params=arr[1];path=path.replace(/^\//,"");var redirect_url=[domain,path].join("/");if(params){redirect_url=[redirect_url,params].join("?")}document.location=redirect_url}}}catch(err){};}
//]]>
</script>
  <script type="text/javascript">
//<![CDATA[
var page={};var onCondition=function(D,C,A,B){D=D;A=A?Math.min(A,5):5;B=B||100;if(D()){C()}else{if(A>1){setTimeout(function(){onCondition(D,C,A-1,B)},B)}}};
//]]>
</script>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta content="en-us" http-equiv="Content-Language" />
<meta content="Twitter is without a doubt the best way to share and discover what is happening right now." name="description" />
<meta content="no" http-equiv="imagetoolbar" />
<meta content="width = 780" name="viewport" />
<meta content="4FTTxY4uvo0RZTMQqIyhh18HsepyJOctQ+XTOu1zsfE=" name="verify-v1" />
<meta content="1" name="page" />
<meta content="NOODP" name="robots" />
<meta content="n" name="session-loggedin" />
  <title id="page_title">Twitter</title>
  <link href="http://a1.twimg.com/a/1301951652/images/twitter_57.png" rel="apple-touch-icon" />
<link href="/oexchange.xrd" rel="http://oexchange.org/spec/0.8/rel/related-target" type="application/xrd+xml" />
<link href="http://a1.twimg.com/a/1301951652/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link href="http://a0.twimg.com/a/1301951652/stylesheets/fronts.css?1301953494" media="screen" rel="stylesheet" type="text/css" />
  
  </head>

<body class="timeline firefox-windows front" id="front">
  <script type="text/javascript">
//<![CDATA[
document.domain = 'twitter.com';function fn(){document.write = "";window.top.location = window.self.location;setTimeout(function(){document.body.innerHTML = '';},0);window.self.onload = function(evt){document.body.innerHTML = '';};}if(window.top !== window.self){try{if(window.top.location.host){}else{fn();}}catch(e){fn();}}
//]]>
</script>
  
  <div id="header">
  <div class="background">
    <div class="inner">
      <div id="logo-search">
        <a href="/" id="logo"><img alt="Twitter" height="55" src="http://a0.twimg.com/a/1301951652/images/fronts/logo_withbird_home.png" width="224" /></a>
        <form action="/search" id="home_search" method="post"><div style="margin:0;padding:0"><input name="authenticity_token" type="hidden" value="1acc05004885b1a75298c88975c2dfd316a028bc" /></div>
          <p>
            <input accesskey="/" class="round-left" id="searchform_q" name="q" size="30" tabindex="8" type="text" /><input class="submit round-right" id="searchform_submit" name="commit" tabindex="9" type="submit" value="Search" />
          </p>
        </form>
        <p id="tag">
          The best way to discover what&rsquo;s new in your world.
        </p>
      </div>
      <div id="topnav">
        <div id="signin-c">
          <div id="signin_controls">
  <span id="have_an_account">
    Have an account?<a href="/login" class="signin" tabindex="3"><span>Sign in</span></a></span>
  <div id="signin_menu" class="common-form standard-form offscreen">
  
    <form method="post" id="signin" action="https://twitter.com/sessions">
  
  <input id="authenticity_token" name="authenticity_token" type="hidden" value="1acc05004885b1a75298c88975c2dfd316a028bc" />  <input id="return_to_ssl" name="return_to_ssl" type="hidden" value="false" />
  <input id="redirect_after_login" name="redirect_after_login" type="hidden" value="/" />  <p class="textbox">
    <label for="username">Username or email</label>
    <input type="text" id="username" name="session[username_or_email]" value="" title="username" tabindex="4"/>
  </p>

  <p class="textbox">
    <label for="password">Password</label>
    <input type="password" id="password" name="session[password]" value="" title="password" tabindex="5"/>
  </p>

  <p class="remember">
    <input type="submit" id="signin_submit" value="Sign in" tabindex="7"/>
    <input type="checkbox" id="remember" name="remember_me" value="1" tabindex="6"/>
    <label for="remember">Remember me</label>
  </p>

  <p class="forgot">
    <a href="/account/resend_password" id="resend_password_link">Forgot password?</a>
  </p>

  <p class="forgot-username">
    <a href="/account/resend_password" id="forgot_username_link" title="If you remember your password, try logging in with your email">Forgot username?</a>
  </p>
    <p class="complete">
      <a href="/account/complete" id="account_complete_link">Already using Twitter on your phone?</a>
    </p>
  <input type="hidden" name="q" id="signin_q" value=""/>
  </form>
</div>

</div>


        </div>
      </div>
      <div class="overlay">
  <div class="newuser">
    <h2>New to Twitter?</h2>
    <p>Easy, free, and instant updates. Get access to the information that interests you most.</p>
    <p id="signup-btn"><a href="/signup" id="signup_submit" tabindex="10"><span>Sign Up &rsaquo;</span></a></p>
    
      <h2>Discover Twitter</h2>
      <p><a href="http://discover.twitter.com/?autoplay=true">Find out how</a> some of your favorite people use Twitter.</p>
      <p><a href="http://discover.twitter.com/?autoplay=true" id="video-thumb"><img alt="Discover-video" height="140" src="http://a3.twimg.com/a/1301951652/images/fronts/discover-video.png" width="200" /></a></p>
    
  </div>
</div>

    </div>
  </div>
  
    

<div id="trends">
  
  <div class="inner">
      <ul class="trendscontent">
        
          
              <li class="trend-label">Trending topics</li>
          
          <li>
            <a href="http://twitter.com/search?q=RIP+Kurt+Cobain" class="search_link" name="RIP Kurt Cobain" rel="nofollow">RIP Kurt Cobain</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Epsilon" class="search_link" name="Epsilon" rel="nofollow">Epsilon</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Raquel+Tibol" class="search_link" name="Raquel Tibol" rel="nofollow">Raquel Tibol</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Copa+Am%C3%A9rica" class="search_link" name="Copa América" rel="nofollow">Copa América</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Makled" class="search_link" name="Makled" rel="nofollow">Makled</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Javier+Sicilia" class="search_link" name="Javier Sicilia" rel="nofollow">Javier Sicilia</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Benz" class="search_link" name="Benz" rel="nofollow">Benz</a>
            
            <em class="description"></em>
          </li>
        
          
              <li class="trend-label">Trending topics</li>
          
          <li>
            <a href="http://twitter.com/search?q=Toya" class="search_link" name="Toya" rel="nofollow">Toya</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Klaus" class="search_link" name="Klaus" rel="nofollow">Klaus</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=God" class="search_link" name="God" rel="nofollow">God</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Rise+%26+Grind" class="search_link" name="Rise &amp; Grind" rel="nofollow">Rise &amp; Grind</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=SAOS+CABE" class="search_link" name="SAOS CABE" rel="nofollow">SAOS CABE</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=LAB+FISIKA" class="search_link" name="LAB FISIKA" rel="nofollow">LAB FISIKA</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=ICE+MAGNUM" class="search_link" name="ICE MAGNUM" rel="nofollow">ICE MAGNUM</a>
            
            <em class="description"></em>
          </li>
        
          
              <li class="trend-label">Trending topics</li>
          
          <li>
            <a href="http://twitter.com/search?q=Laurent+Gbagbo" class="search_link" name="Laurent Gbagbo" rel="nofollow">Laurent Gbagbo</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Kitty+Purry" class="search_link" name="Kitty Purry" rel="nofollow">Kitty Purry</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Tupac+Back" class="search_link" name="Tupac Back" rel="nofollow">Tupac Back</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=One+Shining+Moment" class="search_link" name="One Shining Moment" rel="nofollow">One Shining Moment</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Mister+Cee" class="search_link" name="Mister Cee" rel="nofollow">Mister Cee</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=TAYLOR+WON" class="search_link" name="TAYLOR WON" rel="nofollow">TAYLOR WON</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Liam+Hemsworth" class="search_link" name="Liam Hemsworth" rel="nofollow">Liam Hemsworth</a>
            
            <em class="description"></em>
          </li>
        
          
          <li>
            <a href="http://twitter.com/search?q=Dennis+Rodman" class="search_link" name="Dennis Rodman" rel="nofollow">Dennis Rodman</a>
            
            <em class="description"></em>
          </li>
        
      </ul>
  </div>
  <span class="fade fade-left">&nbsp;</span><span class="fade fade-right">&nbsp;</span>
  
</div>


  
</div>

  <ul id="accessibility" class="offscreen">
  <li><a href="#content" accesskey="0">Skip past navigation</a></li>
  <li>On a mobile phone? Check out <a href="http://m.twitter.com/">m.twitter.com</a>!</li>
  <li><a href="#footer" accesskey="2">Skip to navigation</a></li>
    <li><a href="#signin">Skip to sign in form</a></li>
</ul>


  <div class="content-wrapper">
    <div id="content" class="homepage">
      

<div class="section clearfix">
  <div class="aside">
    
        <div class="trenddesc" style="display:none">
    <span class="trending">Trending Now</span>

    <div id="trend"></div>

    <div id="trend_description"><p></p></div>

    <div id="what_the_trend">Source: <span>What the Trend?</span></div>
  </div>
  <div class="current-trends" style="display:none">
    <h3>Top Trending Topics</h3>
    <ul>
      
        <li><a href="http://twitter.com/search?q=%23whycantyoujust" class="search_link" name="#whycantyoujust" rel="nofollow">#whycantyoujust</a></li>
      
        <li><a href="http://twitter.com/search?q=%23ilovewhen" class="search_link" name="#ilovewhen" rel="nofollow">#ilovewhen</a></li>
      
        <li><a href="http://twitter.com/search?q=%23sincewebeinghonest" class="search_link" name="#sincewebeinghonest" rel="nofollow">#sincewebeinghonest</a></li>
      
        <li><a href="http://twitter.com/search?q=RIP+Kurt+Cobain" class="search_link" name="RIP Kurt Cobain" rel="nofollow">RIP Kurt Cobain</a></li>
      
        <li><a href="http://twitter.com/search?q=Epsilon" class="search_link" name="Epsilon" rel="nofollow">Epsilon</a></li>
      
        <li><a href="http://twitter.com/search?q=Raquel+Tibol" class="search_link" name="Raquel Tibol" rel="nofollow">Raquel Tibol</a></li>
      
        <li><a href="http://twitter.com/search?q=Copa+Am%C3%A9rica" class="search_link" name="Copa América" rel="nofollow">Copa América</a></li>
      
        <li><a href="http://twitter.com/search?q=Benz" class="search_link" name="Benz" rel="nofollow">Benz</a></li>
      
        <li><a href="http://twitter.com/search?q=Makled" class="search_link" name="Makled" rel="nofollow">Makled</a></li>
      
        <li><a href="http://twitter.com/search?q=Klaus" class="search_link" name="Klaus" rel="nofollow">Klaus</a></li>
      
    </ul>
  </div>

    
    <div class="search-tip" style="display:none">
      <h3>Search tip</h3>
      <div>Use <em>source:</em> immediately before a particular Twitter source (like a desktop or mobile app) to find tweets posted via that client. Example: <a href="/search?q=weather+source%3Atwitterfeed">weather source:tweetie</a> will find tweets containing "weather" and entered via Tweetie.</div>
    </div>
    
      
        <div id="sources">
          <div class="inside">
            <h2>See who&rsquo;s here</h2>
            <ul>
              <li>
  <a href="/jtimberlake" class="avatar-sm" hreflang="en" id="icon_jtimberlake" rel="jtimberlake" target="_blank"><img alt="" border="0" height="48" src="http://a2.twimg.com/profile_images/1184225775/rehearsals-121_twitter_-_small__normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_jtimberlake">
    <div class="hc-profile">
      <a href="/jtimberlake" class="avatar" hreflang="en" target="_blank"><img alt="jtimberlake" border="0" height="48" src="http://a2.twimg.com/profile_images/1184225775/rehearsals-121_twitter_-_small__normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Justin Timberlake </strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/jtimberlake">jtimberlake</a></span>
      <span class="hc-location">Memphis, TN | in Music</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">Follow @SOTSK , get educated and support all of the people on the ground helping others get clean water #worldwaterday</div>
        <div class="hc-meta">7:25 PM Mar 22nd</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/CHOW" class="avatar-sm" hreflang="en" id="icon_CHOW" rel="CHOW" target="_blank"><img alt="" border="0" height="48" src="http://a1.twimg.com/profile_images/1145414573/chow-icon_FB-TWITTER_normal.png" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_CHOW">
    <div class="hc-profile">
      <a href="/CHOW" class="avatar" hreflang="en" target="_blank"><img alt="CHOW" border="0" height="48" src="http://a1.twimg.com/profile_images/1145414573/chow-icon_FB-TWITTER_normal.png" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>CHOW.com</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/CHOW">CHOW</a></span>
      <span class="hc-location">San Francisco | in Food & Drink</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">RT @TheTalk_CBS: Talk @Chow: Soup Nights! Soup is comfort food that keeps on giving. Get #recipe here: http://bit.ly/hsgLub</div>
        <div class="hc-meta">about 15 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/AJArabic" class="avatar-sm" hreflang="en" id="icon_AJArabic" rel="AJArabic" target="_blank"><img alt="" border="0" height="48" src="http://a3.twimg.com/profile_images/22356772/AJILogo_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_AJArabic">
    <div class="hc-profile">
      <a href="/AJArabic" class="avatar" hreflang="en" target="_blank"><img alt="AJArabic" border="0" height="48" src="http://a3.twimg.com/profile_images/22356772/AJILogo_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Al Jazeera Arabic</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/AJArabic">AJArabic</a></span>
      <span class="hc-location">Qatar | in Voices in Egypt</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">معارك طاحنة حول البريقة وتراجع الثوار: قال مراسل الجزيرة إن المعارك لا زالت متواصلة حول مدينة البريقة النفطية شر... http://bit.ly/hJ7TAG</div>
        <div class="hc-meta">about 3 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/RoomtoRead" class="avatar-sm" hreflang="en" id="icon_RoomtoRead" rel="RoomtoRead" target="_blank"><img alt="" border="0" height="48" src="http://a2.twimg.com/profile_images/601329413/Twitter_logo_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_RoomtoRead">
    <div class="hc-profile">
      <a href="/RoomtoRead" class="avatar" hreflang="en" target="_blank"><img alt="RoomtoRead" border="0" height="48" src="http://a2.twimg.com/profile_images/601329413/Twitter_logo_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Room to Read</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/RoomtoRead">RoomtoRead</a></span>
      <span class="hc-location">San Francisco, CA | in Books</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">Remember typewriters? Great list curated by @flavorpill: Famous Authors And Their Typewriters. http://bit.ly/eHoJfz #literacy</div>
        <div class="hc-meta">about 21 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/Kiva" class="avatar-sm" hreflang="en" id="icon_Kiva" rel="Kiva" target="_blank"><img alt="" border="0" height="48" src="http://a1.twimg.com/profile_images/267682233/kiva_k_medium_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_Kiva">
    <div class="hc-profile">
      <a href="/Kiva" class="avatar" hreflang="en" target="_blank"><img alt="Kiva" border="0" height="48" src="http://a1.twimg.com/profile_images/267682233/kiva_k_medium_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Kiva</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/Kiva">Kiva</a></span>
      <span class="hc-location">San Francisco, CA | in Charity</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">RT @nytimesfixes: Can the average citizen change the world? The explosion of online crowdsourcing offers a number of ways. http://nyti.m ...</div>
        <div class="hc-meta">about 13 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/twitter_fr" class="avatar-sm" hreflang="fr" id="icon_twitter_fr" rel="twitter_fr" target="_blank"><img alt="" border="0" height="48" src="http://a3.twimg.com/profile_images/1168748308/support-france_normal.png" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_twitter_fr">
    <div class="hc-profile">
      <a href="/twitter_fr" class="avatar" hreflang="fr" target="_blank"><img alt="twitter_fr" border="0" height="48" src="http://a3.twimg.com/profile_images/1168748308/support-france_normal.png" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Twitter Français</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/twitter_fr">twitter_fr</a></span>
      <span class="hc-location">San Francisco CA | in Twitter</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">RT @bayrou: Après coup, je trouve qu'on peut s'exprimer, même en 140 signes. Ça oblige à la simplicité, à un sourire direct, à un coup d ...</div>
        <div class="hc-meta">2:13 PM Mar 31st</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/scottmccloud" class="avatar-sm" hreflang="en" id="icon_scottmccloud" rel="scottmccloud" target="_blank"><img alt="" border="0" height="48" src="http://a3.twimg.com/profile_images/556926220/selfport-small_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_scottmccloud">
    <div class="hc-profile">
      <a href="/scottmccloud" class="avatar" hreflang="en" target="_blank"><img alt="scottmccloud" border="0" height="48" src="http://a3.twimg.com/profile_images/556926220/selfport-small_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Scott McCloud</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/scottmccloud">scottmccloud</a></span>
      <span class="hc-location">So. California | in Art & Design</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">Earlier realized I'd been about 3 blocks from the Geppi Museum -- as my cab headed off to the airport. Oh well!</div>
        <div class="hc-meta">about 19 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/keithlaw" class="avatar-sm" hreflang="en" id="icon_keithlaw" rel="keithlaw" target="_blank"><img alt="" border="0" height="48" src="http://a2.twimg.com/profile_images/64868094/applepie_normal.JPG" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_keithlaw">
    <div class="hc-profile">
      <a href="/keithlaw" class="avatar" hreflang="en" target="_blank"><img alt="keithlaw" border="0" height="48" src="http://a2.twimg.com/profile_images/64868094/applepie_normal.JPG" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>keithlaw</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/keithlaw">keithlaw</a></span>
      <span class="hc-location">Arizona. | in Sports</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">I'm convinced the ads for this alleged &quot;Arthur&quot; remake are just a ploy to get us all to go back and watch the original again</div>
        <div class="hc-meta">about 10 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/chadfowler" class="avatar-sm" hreflang="en" id="icon_chadfowler" rel="chadfowler" target="_blank"><img alt="" border="0" height="48" src="http://a1.twimg.com/profile_images/1288309752/image_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_chadfowler">
    <div class="hc-profile">
      <a href="/chadfowler" class="avatar" hreflang="en" target="_blank"><img alt="chadfowler" border="0" height="48" src="http://a1.twimg.com/profile_images/1288309752/image_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Chad Fowler</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/chadfowler">chadfowler</a></span>
      <span class="hc-location">Boulder, CO | in Technology</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">@glv Oh, nice location</div>
        <div class="hc-meta">about 6 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/amazondeals" class="avatar-sm" hreflang="en" id="icon_amazondeals" rel="amazondeals" target="_blank"><img alt="" border="0" height="48" src="http://a3.twimg.com/profile_images/68908343/Gold_Box_plain_normal.PNG" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_amazondeals">
    <div class="hc-profile">
      <a href="/amazondeals" class="avatar" hreflang="en" target="_blank"><img alt="amazondeals" border="0" height="48" src="http://a3.twimg.com/profile_images/68908343/Gold_Box_plain_normal.PNG" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Amazon.com Deals</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/amazondeals">amazondeals</a></span>
      <span class="hc-location">Seattle, Washington | in Deals</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">Lightning Deal! $129.00 - Sanyo High Definition Camcorder and 14 MP Camera w/12x Optical Zoom http://amzn.to/GoldboxDeals</div>
        <div class="hc-meta">about 2 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/andrewmcdonald4" class="avatar-sm" hreflang="en" id="icon_andrewmcdonald4" rel="andrewmcdonald4" target="_blank"><img alt="" border="0" height="48" src="http://a3.twimg.com/profile_images/1075631635/4_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_andrewmcdonald4">
    <div class="hc-profile">
      <a href="/andrewmcdonald4" class="avatar" hreflang="en" target="_blank"><img alt="andrewmcdonald4" border="0" height="48" src="http://a3.twimg.com/profile_images/1075631635/4_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Andrew McDonald</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/andrewmcdonald4">andrewmcdonald4</a></span>
      <span class="hc-location">in Staff Picks:Cricket WC</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">@Paulnico199 haha the changes rooms look smaller in the picture! they have not shrunk have they?</div>
        <div class="hc-meta">7:21 AM Apr 4th</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/nytimestravel" class="avatar-sm" hreflang="en" id="icon_nytimestravel" rel="nytimestravel" target="_blank"><img alt="" border="0" height="48" src="http://a1.twimg.com/profile_images/278433794/travel_75_twitter_normal.gif" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_nytimestravel">
    <div class="hc-profile">
      <a href="/nytimestravel" class="avatar" hreflang="en" target="_blank"><img alt="nytimestravel" border="0" height="48" src="http://a1.twimg.com/profile_images/278433794/travel_75_twitter_normal.gif" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>NYTimes Travel</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/nytimestravel">nytimestravel</a></span>
      <span class="hc-location">New York, NY | in Travel</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">In Transit: On Prague's Stages, the Best in Czech Dance http://nyti.ms/he1a07</div>
        <div class="hc-meta">about 4 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/USGS" class="avatar-sm" hreflang="en" id="icon_USGS" rel="USGS" target="_blank"><img alt="" border="0" height="48" src="http://a3.twimg.com/profile_images/1178479766/usgs_icon_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_USGS">
    <div class="hc-profile">
      <a href="/USGS" class="avatar" hreflang="en" target="_blank"><img alt="USGS" border="0" height="48" src="http://a3.twimg.com/profile_images/1178479766/usgs_icon_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>USGS</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/USGS">USGS</a></span>
      <span class="hc-location">Reston, VA | in Science</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">[PUB] Distance to Nearest Road in the Conterminous United States: http://go.usa.gov/2zB</div>
        <div class="hc-meta">about 18 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/CraigyFerg" class="avatar-sm" hreflang="en" id="icon_CraigyFerg" rel="CraigyFerg" target="_blank"><img alt="" border="0" height="48" src="http://a2.twimg.com/profile_images/1239953020/image_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_CraigyFerg">
    <div class="hc-profile">
      <a href="/CraigyFerg" class="avatar" hreflang="en" target="_blank"><img alt="CraigyFerg" border="0" height="48" src="http://a2.twimg.com/profile_images/1239953020/image_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Craig Ferguson</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/CraigyFerg">CraigyFerg</a></span>
      <span class="hc-location">Los Angeles, CA | in Funny</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">http://yfrog.com/gyt02lkj Flew a 1936 Waco over LA yesterday. Shocked that my thumb looks so penisy in this picture. #butsmaller</div>
        <div class="hc-meta">7:33 AM Apr 2nd</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/iD_magazine" class="avatar-sm" hreflang="en" id="icon_iD_magazine" rel="iD_magazine" target="_blank"><img alt="" border="0" height="48" src="http://a0.twimg.com/profile_images/566486521/i-D_Classic_Star1_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_iD_magazine">
    <div class="hc-profile">
      <a href="/iD_magazine" class="avatar" hreflang="en" target="_blank"><img alt="iD_magazine" border="0" height="48" src="http://a0.twimg.com/profile_images/566486521/i-D_Classic_Star1_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>i-D magazine</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/iD_magazine">iD_magazine</a></span>
      <span class="hc-location">London | in Fashion</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">A touch of Nike, a dollop of Karl, a dash of @ColetteParis and a fistful of football, mix together, what have you got? http://bit.ly/ftp7BV</div>
        <div class="hc-meta">13 minutes ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/LIFE" class="avatar-sm" hreflang="en" id="icon_LIFE" rel="LIFE" target="_blank"><img alt="" border="0" height="48" src="http://a2.twimg.com/profile_images/210227369/twitter-life_normal.gif" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_LIFE">
    <div class="hc-profile">
      <a href="/LIFE" class="avatar" hreflang="en" target="_blank"><img alt="LIFE" border="0" height="48" src="http://a2.twimg.com/profile_images/210227369/twitter-life_normal.gif" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>LIFE.com</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/LIFE">LIFE</a></span>
      <span class="hc-location">New York, NY | in News</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">12 Grisly, Unsolved Murder Cases - http://on.life.com/hoOlRu</div>
        <div class="hc-meta">6 minutes ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/TIMEHealthland" class="avatar-sm" hreflang="en" id="icon_TIMEHealthland" rel="TIMEHealthland" target="_blank"><img alt="" border="0" height="48" src="http://a1.twimg.com/profile_images/1121118757/health-75x75_healthland_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_TIMEHealthland">
    <div class="hc-profile">
      <a href="/TIMEHealthland" class="avatar" hreflang="en" target="_blank"><img alt="TIMEHealthland" border="0" height="48" src="http://a1.twimg.com/profile_images/1121118757/health-75x75_healthland_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>TIMEHealthland</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/TIMEHealthland">TIMEHealthland</a></span>
      <span class="hc-location">in Health</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">Breast cancer survivors: how gaining weight ups your risk of recurrence | http://ti.me/dF1fXF</div>
        <div class="hc-meta">34 minutes ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/jimcramer" class="avatar-sm" hreflang="en" id="icon_jimcramer" rel="jimcramer" target="_blank"><img alt="" border="0" height="48" src="http://a0.twimg.com/profile_images/52064189/cramer_normal.gif" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_jimcramer">
    <div class="hc-profile">
      <a href="/jimcramer" class="avatar" hreflang="en" target="_blank"><img alt="jimcramer" border="0" height="48" src="http://a0.twimg.com/profile_images/52064189/cramer_normal.gif" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Jim Cramer</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/jimcramer">jimcramer</a></span>
      <span class="hc-location">New York City | in Business</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">Top Takeover Stocks  - http://ow.ly/4trsb</div>
        <div class="hc-meta">36 minutes ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/BillCosby" class="avatar-sm" hreflang="en" id="icon_BillCosby" rel="BillCosby" target="_blank"><img alt="" border="0" height="48" src="http://a1.twimg.com/profile_images/306284528/Bill_CosbyTwitter_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_BillCosby">
    <div class="hc-profile">
      <a href="/BillCosby" class="avatar" hreflang="en" target="_blank"><img alt="BillCosby" border="0" height="48" src="http://a1.twimg.com/profile_images/306284528/Bill_CosbyTwitter_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Bill Cosby</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/BillCosby">BillCosby</a></span>
      <span class="hc-location">Los Angeles | in Entertainment</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">Try a pudding pop &amp; BC49. It is on my smartphone app too http://bit.ly/i1FK3d RT @LivingLightsOn Followed @BillCosby because, hey why not?</div>
        <div class="hc-meta">11:09 AM Mar 28th</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>
<li>
  <a href="/arjunbasu" class="avatar-sm" hreflang="en" id="icon_arjunbasu" rel="arjunbasu" target="_blank"><img alt="" border="0" height="48" src="http://a0.twimg.com/profile_images/70988003/Arjun_01_normal.jpg" style="vertical-align:middle" width="48" /></a>

  <div class="hc" id="hc_arjunbasu">
    <div class="hc-profile">
      <a href="/arjunbasu" class="avatar" hreflang="en" target="_blank"><img alt="arjunbasu" border="0" height="48" src="http://a0.twimg.com/profile_images/70988003/Arjun_01_normal.jpg" style="vertical-align:middle" width="48" /></a>
      <div class="hc-name"><strong>Arjun Basu</strong><span class="hc-verified">&nbsp;</span></div>
      <span class="hc-username">@<a href="/arjunbasu">arjunbasu</a></span>
      <span class="hc-location">Montreal | in Staff Picks</span>
    </div>
    
      <div class="hc-tweet">
        <div class="hc-label">Recently tweeted:</div>
        <div class="hc-tweet-text">He caught his parents in the yard and his mother put down the hose and said, You did not see this, and afterward the kid felt like an adult.</div>
        <div class="hc-meta">about 18 hours ago</div>
      </div>
    
    <div class="hc-pointer">&nbsp;</div>
  </div>
</li>

            </ul>
            <div class="explained">Friends and industry peers you know. Celebrities you watch. Businesses you frequent. Find them all on Twitter.</div>
          </div>
        </div>
      
    
  </div>
  <div class="article">
    <div id="tweets" data="{timeout:8000}">
      <h2>Top Tweets <a id="view_all_top_tweets" href="toptweets/favorites" tabindex="11">View all</a></h2>
      
        <script src="http://a1.twimg.com/a/1301951652/javascripts/widgets/widget.js?1301337106" type="text/javascript"></script>
<script type="text/javascript">
  //height was 202 to match 5 lines of sources
  new TWTR.Widget({
    version: 2,
    type: 'faves',
    rpp: 40,
    interval: 6000,
    title: 'Noteworthy tweets',
    subject: '',
    width: 'auto',
    height: '270',
    theme: {
      shell: {
        background: '#ffffff',
        color: '#333333'
      },
      tweets: {
        background: '#ffffff',
        color: '#333333',
        links: '#2276bb'
      }
    },
    features: {
      scrollbar: false,
      loop: true,
      live: true,
      hashtags: true,
      timestamp: true,
      avatars: true,
      behavior: 'preloaded'
    }
  }).render().setUser("toptweets").start();
</script>

      
    </div>
    <div id="results" style="display: none">
      <h2 id="timeline_heading">Realtime results for <span></span></h2>
              <div data="&#123;&quot;timeline&quot;:&#123;&quot;max_refresh_size&quot;:40,&quot;delay&quot;:30,&quot;decay&quot;:1.5,&quot;max_delay&quot;:300&#125;,&quot;search&quot;:&#123;&quot;delay&quot;:20,&quot;decay&quot;:1.25,&quot;max_delay&quot;:180&#125;&#125;" id="new_results_notification"><a accesskey="n" class="minor-notification" id="results_update" style="display:none;"></a></div>
      
      <div class="no-results"></div>
      <ol id="timeline" class="statuses"></ol>
      <div id="pagination"></div>
    </div>
  </div>
</div>


    </div>
  </div>

  <div class="footer-wrapper">
    
  <div id="footer" class="round wide">
      <h3 class="offscreen">Footer</h3>
      
      
  <form action="/sessions/change_locale" id="lf" method="post" style="display:none;"><div style="margin:0;padding:0"><input name="authenticity_token" type="hidden" value="1acc05004885b1a75298c88975c2dfd316a028bc" /></div>
  <input type="hidden" name="lang"  id="lang" value="en"/>
</form>
<ul class="language-select"><li>Language:</li><li><a href="#" class="locale">English</a>&nbsp;<a href="#" class="arrow">&#9660;</a></li></ul>
<ol class="language-menu round">
  <li id="it">Italian - Italiano</li>
  <li id="es">Spanish - Español</li>
  <li id="en">English</li>
  <li id="ko">Korean - 한국어</li>
  <li id="fr">French - français</li>
  <li id="de">German - Deutsch</li>
  <li id="ja">Japanese - 日本語</li>
</ol>

      <ul class="footer-nav">
          <li class="first">&copy; 2011 Twitter</li>
          <li><a href="/about">About Us</a></li>
          <li><a href="/about/contact">Contact</a></li>
          <li><a href="http://blog.twitter.com">Blog</a></li>
          <li><a href="http://status.twitter.com">Status</a></li>
                      <li><a href="/about/resources">Resources</a></li>
                    <li><a href="http://dev.twitter.com/">API</a></li>
                      <li><a href="http://twitter.com/business">Business</a></li>
                    <li><a href="http://support.twitter.com">Help</a></li>
          <li><a href="/jobs">Jobs</a></li>
          <li><a href="/tos">Terms</a></li>
          <li><a href="/privacy">Privacy</a></li>
      </ul>
  </div>


  </div>
  
  <div class="trendtip">
  <div class="trendtip-content">
    <div>Trending right now:</div>
    <a class="trendtip-trend"></a>
    <div class="trendtip-why">
      Why?
      <span class="trendtip-desc"></span>
      <span class="trendtip-source">Source: <span>What the Trend?</span></span>
    </div>
  </div>
  <div class="trendtip-pointer">&nbsp;</div>
</div>


  
  <div id="notifications"></div>
  
  


  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js" type="text/javascript"></script>
<script src="http://a2.twimg.com/a/1301951652/javascripts/fronts.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
  page.summizeSearchUrl = 'http://search.twitter.com/search';
  page.query = '';
  page.prettyQuery = '';
  page.locale = 'en';
  
    page.showSS = true;
  

//]]>
</script>
<script type="text/javascript">
//<![CDATA[

      $( function () {
        
  setTimeout(function() { $(".twtr-widget").append($("<div></div>").attr("class", "twtr-fade")); }, 0);

  var tabIndex = $("[tabindex]:last").attr("tabIndex");
  var setTabIndex = function() {
    $(this).attr("tabindex", ++tabIndex);
  }
  $(".footer-nav a").each(setTabIndex);
  $(".language-select a").each(setTabIndex);
  
(function(){function b(){var c=location.href.split("#!")[1];if(c){window.location.hash = "";window.location.pathname = c.replace(/^([^/])/,"/$1");}else return true}var a="onhashchange"in window;if(!a&&window.setAttribute){window.setAttribute("onhashchange","return;");a=typeof window.onhashchange==="function"}if(a)$(window).bind("hashchange",b);else{var d=function(){b()&&setTimeout(d,250)};setTimeout(d,250)}}());
  $('#signin_menu').isSigninMenu();

      new FrontPage().init();
    
      $('#newbar .hashtag').isSearchLink(SEARCH_CALLBACKS.hashtagLink);
    
  
      });
    
//]]>
</script>
      <!-- BEGIN google analytics -->

  <script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  </script>

  <script type="text/javascript">

    try {
      var pageTracker = _gat._getTracker("UA-30775-6");
      pageTracker._setDomainName("twitter.com");
            pageTracker._setVar('Not Logged In');
            pageTracker._setVar('lang: en');
            pageTracker._initData();
                    
          pageTracker._trackPageview('/');
                  } catch(err) { }

  </script>

  <!-- END google analytics -->



</body>
</html>