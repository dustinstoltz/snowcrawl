<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--     _
  __   _|_|_ __ ___   ___  ___
  \ \ / / | '_ ' _ \ / _ \/ _ \
   \ V /| | | | | | |  __/ |_| |
    \_/ |_|_| |_| |_|\___|\___/
               you know, for videos
-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Vimeo, Video Sharing For You</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=1024,maximum-scale=1.0" />

        <meta name="description" content="Vimeo is a respectful community of creative people who are passionate about sharing the videos they make. We provide the best tools and highest quality video in the universe." />

        <link rel="stylesheet" type="text/css" media="all" href="http://a.vimeocdn.com/combine.php?type=css&amp;version=d76ac&amp;files=global,lightbox,land,forage,embed,share,moo_rainbow&amp;ssl=0" />
    <link rel="stylesheet" type="text/css" media="all" href="http://a.vimeocdn.com/p/1.1.2/css/player.core.opt.css?d76ac" />

<!--[if gte IE 7]>
    <link rel="stylesheet" type="text/css" href="http://a.vimeocdn.com/css/ie.css?d76ac" />
<![endif]-->

<!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="http://a.vimeocdn.com/css/ie6.css?d76ac" />
    <script type="text/javascript" src="http://a.vimeocdn.com/js/ie6.js?d76ac"></script>
<![endif]-->

<script type="text/javascript" src="http://a.vimeocdn.com/combine.php?type=js&amp;version=d76ac&amp;files=mootools.v1.11_jsmin,mootools_ext,lab,land,forage,share,moo_rainbow,embed,paginator&amp;ssl=0"></script>
    <script type="text/javascript" src="http://a.vimeocdn.com/p/1.1.2/js/player.core.opt.js?d76ac"></script>

        
            </head>
    <body>
        <input type="hidden" id="xsrft" class="xsrft" name="token" value="bedd5883e47b41c6499b00a1ec773524" />

        <img src="http://a.vimeocdn.com/images/smoke.png" id="lightbox_wrapper" class="lightbox_wrapper" style="display:none;" alt="" />
        <div id="lightbox"></div>

        <div id="outer_wrap">
    <div id="inner_wrap">
        <div id="everything">
            <div id="top">
                <div id="logo"><a href="/"><img src="http://a.vimeocdn.com/images/logo_vimeo_land.png" alt="Vimeo" /></a></div>
                <div id="newmenudo">
    <div id="capright" class="menudo_image"></div>
    <ul id="nav" class="grandpappy">
        <li class="firstborn search" id="menudo_search_subtier">
            <div class="rounded_input">
                <div class="contain menudo_image">
                    <form action="/videos/" method="get" onsubmit="menudo_search(); return false;">
                        <input name="search" id="menudo_search_field" type="text" onblur="menudo_search_blur()" onclick="menudo_search_click()" value="Search Videos" class="field"  autocomplete="off" maxlength="50" />
                        <input type="submit" value="" onclick="menudo_search(); return false;" class="button" />
                    </form>
                </div>
            </div>
            <ul class="dotted favoritechild menudo_subtier">
                <li id="menudo_search_videos" class="first selected first">
                    <a href="javascript:void(0)" onclick="menudo_search_change('Videos')">Search Videos</a>
                    <div class="left_shoulder menudo_image"></div>
                    <div class="right_shoulder menudo_image"></div>
                </li>
                <li id="menudo_search_people" class=""><a href="javascript:void(0)" onclick="menudo_search_change('People')">Search People</a></li>
                <li id="menudo_search_groups" class=""><a href="javascript:void(0)" onclick="menudo_search_change('Groups')">Search Groups</a></li>
                <li id="menudo_search_channels" class=""><a href="javascript:void(0)" onclick="menudo_search_change('Channels')">Search Channels</a></li>
                <li id="menudo_search_forums" class="last"><a href="javascript:void(0)" onclick="menudo_search_change('Forums')">Search Forums</a></li>
                <li style="margin-top: 0px; background: transparent;">
                    <div class="cheek_left menudo_image"></div>
                    <div class="cheek_fill"></div>
                    <div class="cheek_right menudo_image"></div>
                </li>
            </ul>
                    </li>
        <li class="firstborn help">
            <a class="label" href="/help">Help</a>
            <ul class="dotted favoritechild">
                <li class="first">
                    <div class="left_shoulder menudo_image"></div>
                    <div class="right_shoulder menudo_image"></div>
                    <a href="/help">Help Center</a>
                </li>
                <li><a href="/help/basics">Vimeo Basics</a></li>
                <li><a href="/guidelines">Community Guidelines</a></li>
                <li><a href="/forums">Community Forums</a></li>
                <li class="last"><a href="/api">Developers</a></li>
                <li style="margin-top: 0px; background: transparent;">
                    <div class="cheek_left menudo_image"></div>
                    <div class="cheek_fill"></div>
                    <div class="cheek_right menudo_image"></div>
                </li>
            </ul>
        </li>
        <li class="firstborn explore" id="menudo_explore">
            <a href="/explore" class="label">Explore</a>
            <ul class="favoritechild dotted">
                <li class="first">
                    <a href="/videoschool">Video School<span class="new">New!</span></a>
                    <div class="left_shoulder menudo_image"></div>
                    <div class="right_shoulder menudo_image"></div>
                </li>
                <li class="first"><a href="/categories">Categories</a></li>
                <li><a href="/groups">Groups</a></li>
                <li><a href="/channels">Channels</a></li>
                <li><a href="/hd">HD Videos</a></li>
                <li><a href="/staffpicks">Staff Picks</a></li>
                <li class="last"><a href="/forum:vimeo_projects">Projects</a></li>
                <li style="margin-top: 0px; background: transparent;">
                    <div class="cheek_left menudo_image"></div>
                    <div class="cheek_fill"></div>
                    <div class="cheek_right menudo_image"></div>
                </li>
            </ul>
        </li>
                    <li class="firstborn login">
                                    <a class="label" href="/log_in" rel="nofollow">Log In</a>
                            </li>
            <li class="firstborn join">
                <a href="/join" title="Join Vimeo"><div class="joinimage loggedout"></div></a>
            </li>
            </ul>
    </div>            </div>

                            <div id="cap" class="native"></div>
            
            <div id="main">
                <div id="header">
                    <h1>Welcome, you're new, aren't you?</h1>
                    <div id="intro">
                        Vimeo is a respectful community of creative people who are passionate about sharing the videos they make. We provide the best tools and highest quality video in the universe. See for yourself and <a href="/join" rel="nofollow">Join today!</a>
                        <img src="http://a.vimeocdn.com/images/white940.gif" alt="" id="wgo_topline" />
                        <img src="http://a.vimeocdn.com/images/white940.gif" alt="" id="wgo_bottomline" />
                    </div>
                </div>

                <div class="columns">
                    <div class="column" id="columnA">

                        <div id="tabs">
                            <ul>
                                <li class="active">
                                    <div id="videos" class="softcorner native tab" style="background-color: #f4f4ee; -moz-border-radius-topleft: 10px; -webkit-border-top-left-radius: 10px; border-top-left-radius: 10px; -moz-border-radius-topright: 10px; -webkit-border-top-right-radius: 10px; border-top-right-radius: 10px; " onclick="tab_stream(this);">
	Videos we like</div>                                </li>
                                <li>
                                    <div id="explore" class="softcorner native tab" style="background-color: #f4f4ee; -moz-border-radius-topleft: 10px; -webkit-border-top-left-radius: 10px; border-top-left-radius: 10px; -moz-border-radius-topright: 10px; -webkit-border-top-right-radius: 10px; border-top-right-radius: 10px; " onclick="tab_stream(this);">
	Explore</div>                                </li>
                                <li>
                                    <div id="activity" class="softcorner native tab" style="background-color: #f4f4ee; -moz-border-radius-topleft: 10px; -webkit-border-top-left-radius: 10px; border-top-left-radius: 10px; -moz-border-radius-topright: 10px; -webkit-border-top-right-radius: 10px; border-top-right-radius: 10px; " onclick="tab_stream(this);">
	Right now</div>                                </li>
                            </ul>

                            <div class="clear"></div>
                        </div>

                                                <div id="content_wrapper" class="softcorner native" style="background-color: #e6e6dc; -moz-border-radius-topright: 15px; -webkit-border-top-right-radius: 15px; border-top-right-radius: 15px; -moz-border-radius-bottomright: 15px; -webkit-border-bottom-right-radius: 15px; border-bottom-right-radius: 15px; -moz-border-radius-bottomleft: 15px; -webkit-border-bottom-left-radius: 15px; border-bottom-left-radius: 15px; " >
	<div id="stream_loading" style="display:none;">Loading...</div>

                            <div id="videos_content">
                                                                <div id="videos_meat" class="softcorner native" style="background-color: #ffffff; -moz-border-radius: 10px; -webkit-border-radius: 10px; border-radius: 10px; " >
	<div class="vimeo_holder" style="position:relative;width:560px;height:312px">
            <style>
            .vimeo_holder div, .vimeo_holder dl, .vimeo_holder dt, .vimeo_holder dd, .vimeo_holder ul, .vimeo_holder ol, .vimeo_holder li, .vimeo_holder h1, .vimeo_holder h2, .vimeo_holder h3, .vimeo_holder h4, .vimeo_holder h5, .vimeo_holder h6, .vimeo_holder pre, .vimeo_holder form, .vimeo_holder fieldset, .vimeo_holder input, .vimeo_holder textarea, .vimeo_holder p, .vimeo_holder blockquote, .vimeo_holder th, .vimeo_holder td{margin:0;padding:0;}
            .vimeo_holder div, .vimeo_holder aside, .vimeo_holder header {background:transparent;}
            .vimeo_holder table{border-collapse:collapse;border-spacing:0;}
            .vimeo_holder fieldset, .vimeo_holder img{border:0;}
            .vimeo_holder address, .vimeo_holder caption, .vimeo_holder cite, .vimeo_holder code, .vimeo_holder dfn, .vimeo_holder em, .vimeo_holder strong, .vimeo_holder th, .vimeo_holder var{font-style:normal;font-weight:normal;}
            .vimeo_holder ol, .vimeo_holder ul {list-style:none;}
            .vimeo_holder caption, .vimeo_holder th {text-align:left;}
            .vimeo_holder q:before, .vimeo_holder q:after{content:'';}
            .vimeo_holder abbr, .vimeo_holder acronym {border:0;}
            .vimeo_holder * {line-height: normal;}
        </style>
        <!--[if lt IE 9]><style>.a.d .aa {display: block;}.a.d .bd {background: #000;filter: alpha(opacity='70');}</style><![endif]--><style>.f {overflow: hidden;padding: 0;margin: 0;width: 100%;height: 100%;background: transparent;}.f > div {width: 100%;height: 100%;}.f .r .an,.f .r .c {visibility: hidden !important;}.f .r .j,.f .r .i,.f .r .c {opacity: 0;filter:alpha(opacity=0);}.f .aa {position: absolute;left: 0;top: 0;z-index: 1;width: 100%;height: 100%;background-position: 50% 50%;background-repeat: no-repeat;background-color: #000;-webkit-background-size: 100% auto;-moz-background-size: 100% auto;background-size: 100% auto;}.bk.r .aa + div {position: absolute;z-index: 2;opacity: 1 !important;}.f .w,.f .v {background: #000;}.f .w .aa {-webkit-background-size: auto 100% !important;-moz-background-size: auto 100% !important;background-size: auto 100% !important;}.f .v .aa {-webkit-background-size: 100% auto !important;-moz-background-size: 100% auto !important;background-size: 100% auto !important;}.f object {z-index: 2;position: absolute;}body.z {overflow: hidden;}body.z .f .c {max-width: 60%;margin-left: -30%;left: 50%;}.bf {display: none !important;}.ag {opacity: 0 !important;filter: alpha(opacity='0');}.ba {visibility: hidden !important;}</style><div id="player_21823642_63394367" class="f player"><style>.a aside button.y.bp:after {content: 'Remove';}.a aside button.y:after {content: 'Later';}</style><div class="r"><div class="aa bf"></div><div class="an"><header class="j"><div class="u"><a href="http://vimeo.com/user814889"><img width="60" height="60" src="http://b.vimeocdn.com/ps/145/075/1450756_75.jpg" /></a></div><hgroup><h1><a href="http://vimeo.com/21823642" class="bs">Sermon on the Mound</a></h1><br /><h4>from&nbsp;<a href="http://vimeo.com/user814889" class="bs">phos pictures</a></h4></hgroup></header><aside class="i"><button class="cg ap"><canvas width="30" height="20"></canvas>Like</button><button class="y ap"><canvas width="30" height="20"></canvas></button><button class="bq ap"><canvas width="30" height="20"></canvas>Share</button><button class="aq ap"><canvas width="30" height="20"></canvas>Embed</button></aside></div><div class="c"><button class="al ap"><canvas width="20" height="20"></canvas></button><div class="h ap"><div class="m"><div class="l"><canvas class="cc" height="8"></canvas><div class="cb" data-min="0" data-max="1"></div><div class="be" data-min="0" data-max="1"></div><div class="ae"><div class="cp"><span>02:35</span><div class="br cr"></div><div class="br"></div></div></div></div><div class="aw"><canvas width="34" height="15"></canvas></div><div class="aj on"><button>HD</button></div><div class="q"><canvas width="12" height="12"></canvas></div></div></div></div><div class="e bf"></div><div class="bd bf"></div></div></div></div>
                                    <div id="clips">
                                        <ul class="videos">
				<li class="top">
			<a href="/21919856"><div class="thumbnail"><img src="http://b.vimeocdn.com/ts/141/306/141306637_200.jpg" width="125" height="94" alt="" /></div></a>
			<div class="digest">
				<h3><a href="/21919856">Incendium</a></h3>
				<div class="byline">by <a href="/dannycooke">Danny&nbsp;Cooke</a> 1 day ago</div>
				<div class="description">Fire-breathing short, filmed at Torquay Pavilion Gardens, Devon, UK.

Filmed with Canon 7D DSLR using Sigma 30mm f1.4, Canon 50mm f1.4,&hellip;</div>
				<div class="stat"><span class="likes"><strong>182</strong> likes</span></div>
			</div>

			<div class="clear"></div>
		</li>
					<li>
			<a href="/21904658"><div class="thumbnail"><img src="http://b.vimeocdn.com/ts/141/203/141203344_200.jpg" width="125" height="94" alt="" /></div></a>
			<div class="digest">
				<h3><a href="/21904658">Jamais Vu!</a></h3>
				<div class="byline">by <a href="/eeseitz">Evan&nbsp;Seitz</a> 1 day ago</div>
				<div class="description">ja·mais vu
Etymology: Fr, never seen
:  the phenomenon of experiencing a situation that one recognises but that nonetheless seems very&hellip;</div>
				<div class="stat"><span class="likes"><strong>57</strong> likes</span></div>
			</div>

			<div class="clear"></div>
		</li>
					<li>
			<a href="/21746459"><div class="thumbnail"><img src="http://b.vimeocdn.com/ts/140/103/140103233_200.jpg" width="125" height="94" alt="" /></div></a>
			<div class="digest">
				<h3><a href="/21746459">Today Only</a></h3>
				<div class="byline">by <a href="/user1104526">Toby&nbsp;Jackman</a> 5 days ago</div>
				<div class="description">My Graduation film from the NFTS 2009.


Director			        Toby Jackman

Writer				Mahalia Rimmer
				
Producer                 		Alexander&hellip;</div>
				<div class="stat"><span class="likes"><strong>153</strong> likes</span></div>
			</div>

			<div class="clear"></div>
		</li>
					<li>
			<a href="/21443752"><div class="thumbnail"><img src="http://b.vimeocdn.com/ts/137/933/137933005_200.jpg" width="125" height="94" alt="" /></div></a>
			<div class="digest">
				<h3><a href="/21443752">Das Pop: The Game</a></h3>
				<div class="byline">by <a href="/bigactive">Big&nbsp;Active</a> 12 days ago</div>
				<div class="description">Director: Will Sweeney

Lead Animators: Daniel Britt, Joseph Pelling

Animators: Andy Baker, Tom Bunker, Nicos Livesey

Character Design,&hellip;</div>
				<div class="stat"><span class="likes"><strong>224</strong> likes</span></div>
			</div>

			<div class="clear"></div>
		</li>
					<li>
			<a href="/21864555"><div class="thumbnail"><img src="http://b.vimeocdn.com/ts/140/932/140932789_200.jpg" width="125" height="94" alt="" /></div></a>
			<div class="digest">
				<h3><a href="/21864555">A Tiny Day in the Jackson Hole Backcountry</a></h3>
				<div class="byline">by <a href="/user1037039">Tristan&nbsp;Greszko</a> 2 days ago</div>
				<div class="description">This mountain is like nothing you have skied before! Spend a day riding the tiny tram and shredding miniature backcountry lines in&hellip;</div>
				<div class="stat"><span class="likes"><strong>745</strong> likes</span></div>
			</div>

			<div class="clear"></div>
		</li>
					<li>
			<a href="/21362582"><div class="thumbnail"><img src="http://b.vimeocdn.com/ts/137/369/137369914_200.jpg" width="125" height="94" alt="" /></div></a>
			<div class="digest">
				<h3><a href="/21362582">Tune for Two (2011)</a></h3>
				<div class="byline">by <a href="/user6344579">alfa&nbsp;primo</a> 14 days ago</div>
				<div class="description">An execution takes an unexpected turn...</div>
				<div class="stat"><span class="likes"><strong>1763</strong> likes</span></div>
			</div>

			<div class="clear"></div>
		</li>
					<li>
			<a href="/20953938"><div class="thumbnail"><img src="http://b.vimeocdn.com/ts/134/428/134428936_200.jpg" width="125" height="94" alt="" /></div></a>
			<div class="digest">
				<h3><a href="/20953938">Kenton Slash Demon - Daemon</a></h3>
				<div class="byline">by <a href="/darkmatters">Dark&nbsp;Matters</a> 24 days ago</div>
				<div class="description">On their quest for creating classic computer effects without the computer, Dark Matters continues the analogue feel from the "Matter"&hellip;</div>
				<div class="stat"><span class="likes"><strong>596</strong> likes</span></div>
			</div>

			<div class="clear"></div>
		</li>
					<li>
			<a href="/21823642"><div class="thumbnail"><img src="http://b.vimeocdn.com/ts/140/641/140641704_200.jpg" width="125" height="94" alt="" /></div></a>
			<div class="digest">
				<h3><a href="/21823642">Sermon on the Mound</a></h3>
				<div class="byline">by <a href="/user814889">phos&nbsp;pictures</a> 4 days ago</div>
				<div class="description">Inspired by a conversation between Darren Rouanzoin  and Eliot Rausch and 36 hours in Los Angeles. 


Directed / Edited: Eliot Rausch
www.eliotrausch.com
Director&hellip;</div>
				<div class="stat"><span class="likes"><strong>688</strong> likes</span></div>
			</div>

			<div class="clear"></div>
		</li>
										
</ul>

 
<div class="pagination">
	<ul>
			<li class="selected">1</li>
	

			
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(2); return false;" >2</a></li>
				
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(3); return false;" >3</a></li>
				
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(4); return false;" >4</a></li>
				
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(5); return false;" >5</a></li>
				
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(6); return false;" >6</a></li>
				
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(7); return false;" >7</a></li>
				
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(8); return false;" >8</a></li>
				
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(9); return false;" >9</a></li>
				
		
					<li><a href="javascript:void(0)" onclick="page_home_videos(10); return false;" >10</a></li>
				
		
			<li class="dots">...</li>
			

			<li><a href="javascript:void(0)" onclick="page_home_videos(38); return false;" >38</a></li>
		
		<li class="arrow"><a href="javascript:void(0)" onclick="page_home_videos(2); return false;" ><img src="http://a.vimeocdn.com/images/page_arrow_next_on.gif" alt="next" /></a></li>		
		</ul>
	<div class="clear"></div>
</div>

                                    </div></div>                            </div>

                            <div id="explore_content" style="display:none;"></div>
                            <div id="activity_content" style="display:none;"></div></div>                    </div>

                    <div class="column" id="columnB">
                        <a href="/join" rel="nofollow"><img src="http://a.vimeocdn.com/images/btn_signup.png" alt="Sign up for Vimeo" class="signup" width="300" height="50" /></a>

                        
    <div class="nippleBox ad_box">
	<div class="bar" style="background-color: #4ebaff;">
		<h4>Advertisement</h4>	</div>
	<div class="nipple" style="border-top-color: #4ebaff;"></div>
	<div class="content">
		<div class="ad" id="dfp-ad-1" style="width:300px; height:250px;">
            <iframe src="http://ad.doubleclick.net/adi/5480.iac.vimeo/home_logged_out;clipid=;tile=1;sz=300x250;s=vm;ord=1016316408?" width="300" height="250" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no">
            <script language="JavaScript" src="http://ad.doubleclick.net/adj/5480.iac.vimeo/home_logged_out;clipid=;tile=1;sz=300x250;s=vm;ord=1016316408?" type="text/javascript"></script>
            <noscript><a href="http://ad.doubleclick.net/jump/5480.iac.vimeo/home_logged_out;clipid=;tile=1;sz=300x250;s=vm;ord=1016316408?" target="_blank"><img src="http://ad.doubleclick.net/ad/5480.iac.vimeo/home_logged_out;clipid=;tile=1;sz=300x250;s=vm;ord=1016316408?"></a></noscript>
            </iframe>

    </div>	</div>
</div>
                                                <div class="nippleBox arrogantSunflower">
	<div class="bar">
		<h4>Staff Blog</h4>	</div>
	<div class="nipple"></div>
	<div class="content">
		<div id="blog" class="blog">
                            <h3><a href="/blog:399">Introducing the VOICETRON 2000</a></h3>
                            <p>Ladies and Gentleman,<br />
<br />
Navigating Vimeo videos just got a whole lot easier! <br />
<br />
It is with immense pleasure that I present to you the future of Vimeo software: VOICETRON 2000. Now I know what you're thinking, "VOICETRON 2000? That name sounds so dated."  And you would be correct. Vimeo has been developing this technology since 1993 using the latest in DMHRA technology. Development of this incredible project was the primary cause for the delay in&hellip;</p>
                            <div class="digest">
                                <a href="/andrew"><img  src="http://b.vimeocdn.com/ps/924/924543_30.jpg" alt="" title="Andrew Pile" width="30" height="30" class="portrait" /></a>                                <div class="byline">by <a href="/andrew">Andrew Pile</a> 4 days ago</div>
                                <div class="clear"></div>
                            </div>
                            <p class="see_more"><a href="/blog">See more from the Staff Blog</a></p>
                        </div>	</div>
</div>
                        <div class="nippleBox arousedBaboon">
	<div class="bar">
		<h4>Explore Vimeo</h4>	</div>
	<div class="nipple"></div>
	<div class="content">
		<div class="products">
        <ul class="dottedlist">
            <li class="first">
                <a href="/plus" id="product_plus">
                    <div class="home_icon"></div>
                    <h3>Vimeo Plus</h3>
                    <p>An affordable way for you to take your Vimeo experience to the next level.</p>
                </a>
                <div class="clear"></div>
            </li>
            <li>
                <a href="/videoschool" id="product_vvs">
                    <div class="home_icon"></div>
                    <h3>Video School</h3>
                    <p>A fun place for anyone to learn how to make better videos.</p>
                </a>
                <div class="clear"></div>
            </li>
            <li>
                <a href="/categories" id="product_categories">
                    <div class="home_icon"></div>
                    <h3>Categories</h3>
                    <p>An ever-changing catalog of Vimeo's great content and active communities.</p>
                </a>
                <div class="clear"></div>
            </li>
            <li>
                <a href="/groups" id="product_groups">
                    <div class="home_icon"></div>
                    <h3>Groups</h3>
                    <p>Share your videos with communities of people who have similar interests.</p>
                </a>
                <div class="clear"></div>
            </li>
            <li>
                <a href="/channels" id="product_channels">
                    <div class="home_icon"></div>
                    <h3>Channels</h3>
                    <p>A simple and beautiful way for anyone to showcase and watch videos.</p>
                </a>
                <div class="clear"></div>
            </li>
            <li>
                <a href="/forum:vimeo_projects" id="product_projects">
                    <div class="home_icon"></div>
                    <h3>Projects</h3>
                    <p>Participate in fun Vimeo projects with other members or start your own.</p>
                </a>
                <div class="clear"></div>
            </li>
            <li class="last">
                <a href="/hd" id="product_hd">
                    <div class="home_icon"></div>
                    <h3>High Def</h3>
                    <p>It's true, Vimeo supports HD video in full 1280x720 resolution! See the HD Channel.</p>
                </a>
                <div class="clear"></div>
            </li>
        </ul>
    </div>	</div>
</div>                    </div>

                    <div class="clear"></div>
                </div>

                <div id="footer">
                    <div class="legal_container">
                        TM + &copy;2011 Vimeo, LLC. All rights reserved. <a href="/about">About</a> / <a href="/site_map">Site Map</a> / <a href="/guidelines">Community Guidelines</a> / <a href="/blog">Blog</a> / <a href="/terms" rel="nofollow">Terms of Service</a> / <a href="/privacy" rel="nofollow">Privacy Statement</a> / <a href="/videoschool">Video School</a> / <a href="/help">Help Center</a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
document.addEvent('domready', function() {
    var iframes = $E('iframe', 'blog');
    if (iframes) {
        iframes.setStyles('width:300px;height:200px');
    }
});
</script>
        <script type="text/javascript">
    var vimeo_startup = {
        domain: '.vimeo.com',
        vimeo_url: 'vimeo.com'
    };
</script>

<script type="text/javascript" src="http://a.vimeocdn.com/js/global.js?d76ac"></script>
<script type="text/javascript">
                        
                    var cur_search_type = 'Videos';
                                    var player21823642_63394367,player21823642_63394367_element = document.getElementById('player_21823642_63394367'),clip21823642_63394367 = {config:{"request":{"cached_timestamp":1302015186,"source":"cache","signature":"d1f384ea88d901ed7b260a9ca381c055","timestamp":1302016309,"referrer":null,"vimeo_url":"vimeo.com","player_url":"player.vimeo.com","cdn_url":"a.vimeocdn.com","cookie_domain":".vimeo.com"},"video":{"id":21823642,"title":"Sermon on the Mound","width":1280,"height":720,"duration":155,"url":"http:\/\/vimeo.com\/21823642","thumbnail":"http:\/\/b.vimeocdn.com\/ts\/140\/641\/140641704_640.jpg","embed_code":"<iframe src=\"http:\/\/player.vimeo.com\/video\/21823642\" width=\"400\" height=\"225\" frameborder=\"0\"><\/iframe><p><a href=\"http:\/\/vimeo.com\/21823642\">Sermon on the Mound<\/a> from <a href=\"http:\/\/vimeo.com\/user814889\">phos pictures<\/a> on <a href=\"http:\/\/vimeo.com\">Vimeo<\/a>.<\/p>","hd":1,"owner":{"name":"phos pictures","portrait":"http:\/\/b.vimeocdn.com\/ps\/145\/075\/1450756_75.jpg","url":"http:\/\/vimeo.com\/user814889"},"stats":{"comments":59,"likes":684,"plays":29767},"files":{"h264":["hd","sd","mobile"]}},"security":{"embed_permission":"public","hd_embed":1,"privacy":"anybody"},"analytics":{"ga_account":"UA-76641-35"},"user":{"liked":0,"watch_later":0,"preferences":{"hd":1,"scaling":1,"volume":100,"html":2},"status":{"mod":0,"logged_in":0,"owner":0}},"embed":{"alt_color":"c0c0c0","color":"00adef","autoplay":0,"loop":0,"pause_info":1,"on_site":1,"api":{"on":0,"player_id":null,"version":1},"toggle":{"badge":0,"byline":1,"portrait":1,"title":1,"byline_badge":0,"embed":1,"hd":1,"like":1,"scaling":1,"share":1,"watch_later":1,"playbar":1,"fullscreen":1,"logo":0,"volume":1},"outro":{"type":"nothing"}}},assets: {"path":"http:\/\/a.vimeocdn.com\/p\/1.1.2\/js\/","ming":{"touch":["player.touch.js?d76ac"],"mobile":["player.mobile.js?d76ac"],"desktop":["player.desktop.js?d76ac"]},"moogaloop":["swfobject.v2.2.js"],"cache":1},uniqueId: '21823642_63394367',options: {ming: {html_path: "http://player.vimeo.com/assets/html_opt",css_path: "http://player.vimeo.com/assets/css_opt",version: '1.1.2'},moogaloop: {force_embed: 0,force_info: 1,ref_id: 'player21823642_63394367',swfs: {cover: {desktop: "http:\/\/a.vimeocdn.com\/p\/flash\/moogalover\/1.1.3\/moogalover.swf?v=1.0.0",mobile: "http:\/\/a.vimeocdn.com\/p\/flash\/moogalover\/1.1.3\/moogalover.swf?v=1.0.0"},player: {desktop: "http:\/\/a.vimeocdn.com\/p\/flash\/moogaloop\/5.1.11\/moogaloop.swf?v=1.0.0",desktop_sv: "http:\/\/a.vimeocdn.com\/p\/flash\/moogaloop\/5.1.11\/moogaloop.swf?v=1.0.0",mobile: "http:\/\/a.vimeocdn.com\/p\/flash\/moogaloop\/5.1.11\/moogaloop.swf?v=1.0.0",mobile_sv: "http:\/\/a.vimeocdn.com\/p\/flash\/moogaloop\/5.1.11\/moogaloop.swf?v=1.0.0"}},versions: {player: '5.1.11',cover: '1.1.3',js: '1.1.2'}},noplayer: {html_path: "http://player.vimeo.com/assets/html_opt",css_path: "http://player.vimeo.com/assets/css_opt"}},thumbs: {1280: 'http://b.vimeocdn.com/ts/140/641/140641704_640.jpg',940: 'http://b.vimeocdn.com/ts/140/641/140641704_640.jpg',640: 'http://b.vimeocdn.com/ts/140/641/140641704_640.jpg'}};Player.checkRatio(player21823642_63394367_element.getElementsByTagName('div')[0],player21823642_63394367_element.getElementsByTagName('div')[0].getElementsByTagName('div')[0],clip21823642_63394367.config);function initPlayer21823642_63394367() {player21823642_63394367 = new Player(player21823642_63394367_element,clip21823642_63394367,Player.init.loader);}Player.init.queue.push(initPlayer21823642_63394367);if (!Player.init.loaderAdded) {window.addEvent('domready', function() {Player.init.loader();});Player.init.loaderAdded = true;}            
    EventCenter.fire('domready');
</script>

        <script type="text/javascript">
    var _qoptions = {
        qacct: 'p-53jJe1KAP5ehs'
    };

    var tracker = null;
    function trigger_pageview(url) {
        if(tracker) {
            tracker._trackPageview(url);
        }
    }

    $LAB
    .script("http://edge.quantserve.com/quant.js")
    .script("http://www.google-analytics.com/ga.js")
    .wait(function() {
        tracker = _gat._getTracker('UA-76641-8');
        tracker._setDomainName('.vimeo.com');

                    tracker._setLocalRemoteServerMode();
            tracker._setLocalGifPath('http://utmtrk.vimeo.com/__utm.gif');
        
        tracker._initData();

                    tracker._setVar('logged_out');
        
                    tracker._trackPageview();
        
            });
</script>
    </body>
</html>