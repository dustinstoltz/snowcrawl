<!DOCTYPE html>





<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Google Code</title>
<script type="text/javascript"><!--
(function(){function a(){this.t={};this.tick=function(c){this.t[c]=(new Date).getTime()};this.tick("start")}var b=new a;window.jstiming={Timer:a,load:b};if(window.external&&window.external.pageT)window.jstiming.pt=window.external.pageT;})();

var _tocPath_ = null;
var codesite_token = null;
var logged_in_user_email = null;
//--></script>
<link href="/css/codesite.pack.04102009.css" type="text/css" rel="stylesheet">
<script src="/js/codesite_head.pack.04102009.js" type="text/javascript"></script>
<script type="text/javascript">CODESITE_CSITimer['load'].tick('bhs');</script>
<link rel="search" type="application/opensearchdescription+xml" title="Google Code" href="/osd.xml">

    <link rel="stylesheet" href="/css/homepage_local_extensions.css" type="text/css" media="screen">
    <!--[if IE]>
    <style type="text/css">
      #news li {height:auto; display:block;}
    </style>
    <![endif]-->
    <meta name="description" content="Google's official developer site.  Featuring APIs, developer tools and technical resources.">
    <meta name="verify-v1" content="3asBM6UzbjYQ71+DNA57fdL+f7WB/w5ybuQFutAOH/A=">
  </head>

  <body>
    <div id="gb">
 <span>
  
   
    <a href="#"
       id="projects-dropdown" onclick="return false;"
       ><span style="text-decoration:underline">My favorites</span> <span style="font-size:.75em;">&#9660;</span></a>
    |
  
    <a id="lang-dropdown" href="/" onclick="return false;"><span style="text-decoration:underline">English</span> <span style="font-size:.75em;">&#9660;</span></a>
  
   
    | <a href="https://www.google.com/accounts/Login?continue=http%3A%2F%2Fcode.google.com%2F&amp;followup=http%3A%2F%2Fcode.google.com%2F" onclick="CODESITE_click('/gb/sc/signin');"><span style="style:text-decoration:underline">Sign in</span></a>
   
  
 </span>
</div>

<div class="gbh" style="left:0px;"></div>
<div class="gbh" style="right:0px;"></div>

<div id="gc-container">
<a id="top"></a>
<div id="skipto">
  
  
</div>

<div id="gc-header">
  <div id="logo"><a href="/">
  
  
     <img src="/images/code_logo.gif" height="40" width="167" alt="Google Code" style="border:0;margin:3px 0 0 0;">
  
  
  </a></div>
  <div id="search">
    <div id="searchForm">
      <form id="cse" action="http://www.google.com/cse" accept-charset="utf-8" onsubmit="executeGSearch(document.getElementById('gsearchInput').value); return false;">
      <noscript>
      <input type="hidden" name="cref" value="http://code.google.com/cse/googlecode-context.xml">
      </noscript>
      <div id="gsc-search-box">
        <input id="gsearchInput" type="text" name="q" maxlength="2048" class="gsc-input" autocomplete="off" title="Google Code Search" style="width:345px">
        <div id="cs-searchresults" onclick="event.cancelBubble = true;"></div>
        <input title="Search" id="gsearchButton" class="gsc-search-button" name="sa" value="Search" type="submit">
        <div class="greytext">e.g. "adwords" or "open source"</div>
      </div>
      </form>
    </div> <!-- end searchForm -->
  </div> <!-- end search -->




</div> <!-- end gc-header -->


<div id="codesiteContent">

<a id="gc-topnav-anchor"></a>
<div id="gc-topnav">
  <h1 style="padding:0 0 0 6px;"></h1>
  <ul class="gc-topnav-tabs">
<li>&nbsp;</li>
  </ul>
</div> <!-- end gc-topnav -->


    <!-- Google Code Content -->
    <div id="gc-home">
      <div class="g-section g-tpl-225">
        <div class="g-unit g-first">
            <h2>Featured Products</h2>
            <ul class="devprod">
            <li class="android"><a href="http://developer.android.com/" onclick="_gaq.push(['siteTracker._trackEvent','Outbound Links','Android',this.href]);">Android</a></li>
            <li class="appengine"><a href="/appengine/">App Engine</a></li>
            <li class="marketplace"><a href="/googleapps/marketplace/">Google Apps Marketplace</a></li>
            <li class="gwt"><a href="/webtoolkit/">Google Web Toolkit</a></li>
            <li class="projecthosting"><a href="/projecthosting/">Project Hosting</a><br></li>
            <li class="moreproducts"><a href="/more/" onclick="_gaq.push(['siteTracker._trackEvent','More Links','More Products']);">More products &raquo;</a></li>
            </ul>
            <h2>Developer Resources</h2>
            <ul class="devprod">
            <li class="apis"><a href="/more/" onclick="_gaq.push(['siteTracker._trackEvent','More Links','APIs Tools']);">APIs &amp; Tools</a></li>
            <li class="apisconsole"><a href="https://code.google.com/apis/console/">APIs Console</a></li>
            <li class="apisexplorer"><a href="https://code.google.com/apis/explorer/">APIs Explorer</a></li>
            <li class="playground"><a href="http://code.google.com/apis/ajax/playground/">Code Playground</a></li>
            <li class="mobile"><a href="/mobile/">Mobile</a></li>
            <li class="opensource"><a href="/opensource/">Open Source Programs</a></li>
            <li class="events"><a href="/events/">Events</a></li>
            <li class="advocates"><a href="/team/">Developer Advocates</a></li>
            </ul>
        </div>
        <div class="g-unit">
          <div class="g-section g-tpl-67-33">
            <div class="g-unit g-first">
               <h2>
               <a href="http://feeds2.feedburner.com/GoogleCodeNews" title="Subscribe" class="subscribe">
               <img src="images/cleardot.gif" style="border:0px none">
               </a> News
               </h2>
              

<ul id="news">

 <li>
 
 <h3><a href="http://feedproxy.google.com/~r/YoutubeApiBlog/~3/8xajLv_O6lE/man-vs-machine-curating-with-youtube.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">Man vs. Machine: Curating with YouTube APIs</a></h3>
 <span class="blog-name">YouTube API Blog</span> - April  4, 2011
 <p class="blog-entry-summary">
If you are reading this, chances are you are building an app which includes video. Given that over 35 hours of video is uploaded to YouTube every minute...
 </p>
 </li>

 <li>
 
 <h3><a href="http://feedproxy.google.com/~r/blogspot/NWLT/~3/olcY09ANcL0/gwtgpe-23-cloud-connecting-eclipse.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">GWT/GPE 2.3: Cloud Connecting Eclipse</a></h3>
 <span class="blog-name">Google Web Toolkit Blog</span> - April  4, 2011
 <p class="blog-entry-summary">
We’re not going to be shy about this, the GWT team loves tools and APIs that make developers&#39; lives better. That’s why we’re happy to have reached an...
 </p>
 </li>

 <li>
 
 <div class="screenshot">
 <a href="http://feedproxy.google.com/~r/blogspot/Dcni/~3/Dnryvkb5tRY/new-test-suite-for-rich-text-editing.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">
 <img class="entry_image" src="http://3.bp.blogspot.com/-w55fZCj4sqk/TZZsTrSQu0I/AAAAAAAAANM/zvToZwK3jak/s72-c/rolandsteiner.png" />
 </a>
 </div>
 
 <h3><a href="http://feedproxy.google.com/~r/blogspot/Dcni/~3/Dnryvkb5tRY/new-test-suite-for-rich-text-editing.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">A new test suite for rich text editing</a></h3>
 <span class="blog-name">Google Code Blog</span> - April  4, 2011
 <p class="blog-entry-summary">
This post is by Roland Steiner of the Chrome Team. Roland works mainly on layout and rendering for Chrome and WebKit. An Austrian native, Roland has ventured far...
 </p>
 </li>

 <li>
 
 <h3><a href="http://feedproxy.google.com/~r/GoogleOpenSourceBlog/~3/2E8bYVwNeEs/dos-and-donts-of-google-summer-of-code.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">The DOs and DON’Ts of Google Summer of Code: Organization Administrator Edition</a></h3>
 <span class="blog-name">Google Open Source Blog</span> - April  4, 2011
 <p class="blog-entry-summary">
This is the second in a series of three posts on DOs and DON’Ts for Google Summer of Code students, mentors, and organization administrators. The first post dealt...
 </p>
 </li>

 <li>
 
 <h3><a href="http://blog.chromium.org/2011/04/new-experimental-apis-for-chrome.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">New experimental APIs for Chrome extensions</a></h3>
 <span class="blog-name">Chromium Blog</span> - April  4, 2011
 <p class="blog-entry-summary">
In the latest Chrome beta release, we made available two new experimental extension APIs : the Web Navigation and Proxy Extension APIs. They are the first in a...
 </p>
 </li>

 <li>
 
 <h3><a href="http://feedproxy.google.com/~r/blogspot/hsDu/~3/PIIpAGfFw70/io-ticket-contest.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">The IO Ticket Contest</a></h3>
 <span class="blog-name">Android Developers Blog</span> - April  1, 2011
 <p class="blog-entry-summary">
When Google I/O sold out so fast, were kicking around ideas for how to get some of our ticket reserve into the hands of our favorite people: Dedicated...
 </p>
 </li>

 <li>
 
 <div class="screenshot">
 <a href="http://feedproxy.google.com/~r/GoogleAppsDeveloperBlog/~3/RBCkbAgaeuc/grading-made-easy-using-apps-script.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">
 <img class="entry_image" src="http://3.bp.blogspot.com/-hz8vqZjIDqU/TZYQZ0I70PI/AAAAAAAAAIk/2qhKcb69VL4/s72-c/image00.png" />
 </a>
 </div>
 
 <h3><a href="http://feedproxy.google.com/~r/GoogleAppsDeveloperBlog/~3/RBCkbAgaeuc/grading-made-easy-using-apps-script.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">Grading Made Easy using Apps Script</a></h3>
 <span class="blog-name">Google Apps Developer Blog</span> - April  1, 2011
 <p class="blog-entry-summary">
Editor’s Note: Guest author Dave Abouav is a Google employee who is a part-time instructor for a night class in physics. He created Flubaroo as a 20% time...
 </p>
 </li>

 <li>
 
 <div class="screenshot">
 <a href="http://feedproxy.google.com/~r/blogspot/Dcni/~3/M3JZOps9Hv8/sessions-updated-for-google-io-2011.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">
 <img class="entry_image" src="http://4.bp.blogspot.com/-cJpDTgxu5xE/TZUD9dsT9oI/AAAAAAAAAM4/9SNwkp1OYbk/s72-c/IMG_4296.JPG" />
 </a>
 </div>
 
 <h3><a href="http://feedproxy.google.com/~r/blogspot/Dcni/~3/M3JZOps9Hv8/sessions-updated-for-google-io-2011.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">Sessions updated for Google I/O 2011!</a></h3>
 <span class="blog-name">Google Code Blog</span> - April  1, 2011
 <p class="blog-entry-summary">
This post is by Monica Tran. Monica comes to Google by way of the Developer Marketing team, primarily focused on Google I/O and our international Developer Days. As...
 </p>
 </li>

 <li>
 
 <h3><a href="http://blog.chromium.org/2011/04/taking-chrome-to-lite-speeds.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">Taking Chrome to Lite speeds</a></h3>
 <span class="blog-name">Chromium Blog</span> - April  1, 2011
 <p class="blog-entry-summary">
When we created Chrome, we focused on speed, simplicity, and security as its hallmark traits. Today, we’re proud to announce a new extension for Chrome, called ChromeLite...
 </p>
 </li>

 <li>
 
 <h3><a href="http://feedproxy.google.com/~r/YoutubeApiBlog/~3/Uc0rzWO2UZA/deprecating-equine-frame-embedding.html" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links', 'NewsFeed', this.href]);">Deprecating Equine-Frame Embedding</a></h3>
 <span class="blog-name">YouTube API Blog</span> - April  1, 2011
 <p class="blog-entry-summary">
Although equine-frame embedding of YouTube moving pictures was launched with great huzzabulloo a few short months ago, we regret that we can no longer recommend this horsey practice...
 </p>
 </li>

</ul>

<!-- Time generated: 2011-04-04T13:32:39 (1301949159) -->


            </div>
            <div class="g-unit">
              <h2>Announcement</h2>
              
<div>
<a style="border:0;" href="http://www.google-melange.com/gsoc/program/home/google/gsoc2011" onclick="_gaq.push(['siteTracker._trackEvent','Outbound Links','Announcement',this.href]);"><img alt="GSOC 2011" src="/images/gsoc2011.png"></a>
<h4>Get ready for Google Summer of Code</h4>
<p>University students, <a href="http://www.google-melange.com/gsoc/profile/student/google/gsoc2011">register
and apply now</a> to take part in <a href="http://www.google-melange.com/gsoc/program/home/google/gsoc2011">Google
Summer of Code 2011</a>!  Application deadline: <a href="http://www.timeanddate.com/worldclock/fixedtime.html?month=4&day=8&year=2011&hour=19&min=0&sec=0&p1=0">
April 8th, 19:00 UTC</a>.</p>
</div>


              <br>
              <h2><a href="http://gdata.youtube.com/feeds/base/users/GoogleDevelopers/uploads?alt=rss&amp;v=2&amp;orderby=published&amp;client=ytapi-youtube-profile" title="Subscribe" class="subscribe"><img src="images/cleardot.gif" style="border:0px none"></a>
              Videos</h2>
              

<div id="gc-codevideo">
 <div class="gc-techtalk">

 <div class="g-video">
 <div class="screenshot">
 <a href="http://www.youtube.com/watch?v=gK72pcu3cpk" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links','Video',this.href]);"><img width="96" src="http://i.ytimg.com/vi/gK72pcu3cpk/1.jpg" alt="HTML5 video accessibility and the WebVTT file format - Audio Described"/></a>
 </div>
 <div class="info">
 <div class="name">
 <a title="HTML5 video accessibility and ..." href="http://www.youtube.com/watch?v=gK72pcu3cpk" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links','Video',this.href]);">HTML5 video accessibility and ...</a>
 </div>
 <div class="author videodateformat"></div>
 </div>
 </div>

 <div class="g-video">
 <div class="screenshot">
 <a href="http://www.youtube.com/watch?v=RoYvr-KXvLs" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links','Video',this.href]);"><img width="96" src="http://i.ytimg.com/vi/RoYvr-KXvLs/1.jpg" alt="HTML5 video accessibility and the WebVTT file format"/></a>
 </div>
 <div class="info">
 <div class="name">
 <a title="HTML5 video accessibility and ..." href="http://www.youtube.com/watch?v=RoYvr-KXvLs" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links','Video',this.href]);">HTML5 video accessibility and ...</a>
 </div>
 <div class="author videodateformat"></div>
 </div>
 </div>

 <div class="g-video">
 <div class="screenshot">
 <a href="http://www.youtube.com/watch?v=ITFUMI8XCyU" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links','Video',this.href]);"><img width="96" src="http://i.ytimg.com/vi/ITFUMI8XCyU/1.jpg" alt="Google Technology User Group Geo Care Package Introduction"/></a>
 </div>
 <div class="info">
 <div class="name">
 <a title="Google Technology User Group G..." href="http://www.youtube.com/watch?v=ITFUMI8XCyU" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links','Video',this.href]);">Google Technology User Group G...</a>
 </div>
 <div class="author videodateformat"></div>
 </div>
 </div>

 <div class="g-video">
 <div class="screenshot">
 <a href="http://www.youtube.com/watch?v=hNbFkLuRk5Y" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links','Video',this.href]);"><img width="96" src="http://i.ytimg.com/vi/hNbFkLuRk5Y/1.jpg" alt="SImulating Markers with Tile Layers"/></a>
 </div>
 <div class="info">
 <div class="name">
 <a title="SImulating Markers with Tile L..." href="http://www.youtube.com/watch?v=hNbFkLuRk5Y" onclick="_gaq.push(['siteTracker._trackEvent', 'Outbound Links','Video',this.href]);">SImulating Markers with Tile L...</a>
 </div>
 <div class="author videodateformat"></div>
 </div>
 </div>

 </div>
</div>

<!-- Time generated: 2011-03-31T17:35:26 (1301618126) -->


              <div style="clear:both"></div>
              <a href="http://www.youtube.com/GoogleDevelopers" onclick="_gaq.push(['siteTracker._trackEvent','Outbound Links','Video',this.href]);">More videos &raquo;</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Google Code Content -->

    </div> <!-- end codesite content -->


<div id="gc-footer" dir="ltr">
  <div class="text">
    
    ©2011 Google -
    <a href="/">Code Home</a> -
    <a href="/terms.html">Terms of Service</a> -
    <a href="/privacy.html">Privacy Policy</a> -
    <a href="/more/">Site Directory</a>
    <br> <br>
    Google Code offered in:
    <a href="/intl/en/">English</a> -
    <a href="/intl/es/">Español</a> -
    <a href="/intl/ja/">日本語</a> -
    <a href="/intl/ko/">한국어</a> -
    <a href="/intl/pt-BR/">Português</a> -
    <a href="/intl/ru/">Pусский</a> -
    <a href="/intl/zh-CN/">中文(简体)</a> -
    <a href="/intl/zh-TW/">中文(繁體)</a>
  </div>
</div><!-- end gc-footer -->

</div><!-- end gc-container -->

<script type="text/javascript">CODESITE_CSITimer['load'].tick('ats');</script>
<script src="/js/codesite_tail.pack.04102009.js" type="text/javascript"></script>




<script type="text/javascript">
var _gaq = _gaq || [];

_gaq.push(


    ['siteTracker._setAccount', 'UA-18071-1'],
    ['siteTracker._setDomainName', 'code.google.com'],
    ['siteTracker._setCookiePath', window.location.pathname.substring(0,
        window.location.pathname.lastIndexOf('/') + 1)],
    ['siteTracker._trackPageview']
);
(function() {
  var ga = document.createElement('script');

  ga.type = 'text/javascript';
  ga.async = true;
  ga.src = 'http://www.google-analytics.com/ga.js';
  (document.getElementsByTagName('head')[0] ||
   document.getElementsByTagName('body')[0]).appendChild(ga);
 })();
</script>



  </body>
</html>
