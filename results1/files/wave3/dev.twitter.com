<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Create cool applications! | dev.twitter.com</title>
  <meta content="Twitter API developer resources." name="description" />
  <link href="/images/dev/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link href="http://a1.twimg.com/a/1301951652/stylesheets/dev/console.css?1301337105" media="screen" rel="stylesheet" type="text/css" />  <link href="http://a3.twimg.com/a/1301951652/stylesheets/dev/syntax_highlighting.css?1301337105" media="screen" rel="stylesheet" type="text/css" />  <link href="http://a0.twimg.com/a/1301951652/stylesheets/dev/dev.css?1301337105" media="screen" rel="stylesheet" type="text/css" />
  <link href="http://a0.twimg.com/a/1301951652/stylesheets/dev/anywhere.css?1301337105" media="screen" rel="stylesheet" type="text/css" />
  <link href="http://a1.twimg.com/a/1301951652/stylesheets/buttons.css?1301337105" media="screen" rel="stylesheet" type="text/css" />
  <link href="http://a1.twimg.com/a/1301951652/stylesheets/dialog.css?1301337105" media="screen" rel="stylesheet" type="text/css" />
  <script src="http://a3.twimg.com/a/1301951652/javascripts/dev/jquery.min.js?1301337105" type="text/javascript"></script>  <script src="http://a0.twimg.com/a/1301951652/javascripts/dev/jquery.form.js?1301337105" type="text/javascript"></script>  <script src="http://a1.twimg.com/a/1301951652/javascripts/dev/jquery.ui.js?1301337105" type="text/javascript"></script>  <script src="http://a2.twimg.com/a/1301951652/javascripts/dev/jquery.autocomplete.js?1301337105" type="text/javascript"></script>  <script src="http://a2.twimg.com/a/1301951652/javascripts/dev/json2.js?1301337105" type="text/javascript"></script>  <script src="http://a3.twimg.com/a/1301951652/javascripts/dev/hurl.js?1301337105" type="text/javascript"></script>  <script src="http://a2.twimg.com/a/1301951652/javascripts/dev/hurl.headers.js?1301337105" type="text/javascript"></script>  <script src="http://a1.twimg.com/a/1301951652/javascripts/base.js?1301953494" type="text/javascript"></script>  <script src="http://a0.twimg.com/a/1301951652/javascripts/dialog.js?1301337105" type="text/javascript"></script>  <script src="http://a3.twimg.com/a/1301951652/javascripts/dev/dev.js?1301337105" type="text/javascript"></script>  <script src="http://a2.twimg.com/a/1301951652/javascripts/dev/toc.js?1301337105" type="text/javascript"></script>  
</head>
<body>
  <div id="container">
    <div id="header">
      <div id="logo"><a href="http://dev.twitter.com/">dev@twitter</a></div>
      <div id="nav">
        <ul>
          <li class="first">
                          <a href="http://dev.twitter.com/start">Begin</a>
                      </li>
          <li><a href="/doc">Documentation</a></li>
          <li><a href="http://dev.twitter.com/discuss">Discussions</a></li>
                      <li class="last"><a href="http://dev.twitter.com/login">Sign in</a></li>
                  </ul>
        <form id="search" action="/search" method="get">
           <input type="text" name="query" value="" class="q" />
           <input type="submit" id="q" value="Search" class="btn"/>
         </form>
      </div>
    </div>
          <div id="announcements">
        <div class="more">
  <h2><a href="/pages/intents">Introducing Web Intents</h2>
</div>

<p>
  <img src="//si0.twimg.com/images/dev/bookmark.png" class="bookmark" alt="Attention!" />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="important"><a href="/pages/intents">March 30, 2011</a></span> Bring interactivity to Tweets that you display on the web with <a href="/pages/intents">Web Intents</a>.
&nbsp;&nbsp;&nbsp;<a href="#" onclick="$('#announcements').hide();">x</a>
</p>

<div class="more">
  <p>Find out everything about Web Intents <a href="/pages/intents">here</a>.</p>
</div>
      </div>
        <div id="content">
            
      
<div class="row clearfix" id="home">
  <div class="t-100">
    <div class="box prominent clearfix">
      <div class="intro">
        <span class="birds"></span>
        <h1>Create cool applications that integrate with Twitter.</h1>
      </div>
    </div>
    <div class="actions">
      <a href="http://dev.twitter.com/start" class="first"><strong></strong><b><em>Get Started</em><span>Explore all of Twitter, from your own timeline and mentions.</span></b></a>
      <a href="http://dev.twitter.com/apps/new" class="second"><strong></strong><b><em>Register an app</em><span>Register an application to start using the Twitter API. It’s easy.</span></b></a>
      <a href="http://dev.twitter.com/discuss" class="third"><strong></strong><b><em>Discuss</em><span>Get in touch with the Twitter API team and the community of API developers.</span></b></a>
    </div>
  </div>
</div>
<div class="row clearfix">
  <div class="box featured anywherehome">
  <div class="t-100">
          <!-- begin included content -->
<style>
p.preview img.screenshot { margin: -60px 0 0 436px !important;}
</style>
<p class="preview">
  <a href="http://twitter.com/tweetbutton"><img src="//si0.twimg.com/images/dev/cms/tweet-button/share-on-twitter.jpg" alt="" class="screenshot"/></a>
  <a href="http://twitter.com/tweetbutton">
    <img src="//si0.twimg.com/images/dev/cms/tweet-button/tweet-button-title.png" alt="Tweet Button" />
  </a>
  <span class="desc">Make it easy for users to share your website with Twitter.</span>
  <a href="http://twitter.com/tweetbutton" class="learn">Learn more</a>
</p>
<!-- end included content -->
        </div>
  </div>
</div>
    </div>
    <div id="footer">
      <ul>
        <li class="first">&copy; 2011 Twitter</li>
        <li><a href="http://twitter.com/about#about">About Us</a></li>
        <li><a href="http://twitter.com/about#contact">Contact</a></li>
        <li><a href="http://blog.twitter.com">Blog</a></li>
        <li><a href="http://status.twitter.com">Status</a></li>
        <li><a href="/status">API Status</a></li>
        <li><a href="http://twitter.com/about/resources">Resources</a></li>
        <li><a href="http://business.twitter.com/twitter101">Business</a></li>
        <li><a href="http://support.twitter.com">Help</a></li>
        <li><a href="http://twitter.com/jobs">Jobs</a></li>
        <li><a href="http://twitter.com/tos">Terms</a></li>
        <li><a href="http://twitter.com/privacy">Privacy</a></li>
        <li><a href="http://twitter.com/twitterapi">@TwitterAPI</a></li>
      </ul>
    </div>
  </div>
  <script type="text/javascript">
    
  </script>
  <script type="text/javascript">
  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  </script>
  <script type="text/javascript">
  try {
  var pageTracker = _gat._getTracker("UA-30775-16");
  pageTracker._trackPageview();
  } catch(err) {}</script>
</body>
</html>
