<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html>
<!--Template: info_homepage Package Version: 1.3.0--><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Begin meta tags --><meta CONTENT-LANGUAGE="EN">
<!-- End meta tags --><title>Corporate Information</title>
<!-- Begin css --><link rel="stylesheet" href="http://l.yimg.com/a/lib/cmrc/base.css" type="text/css">
<link rel="stylesheet" type="text/css" href="http://l.yimg.com/a/lib/common/fonts_200502080901.css">
<link href="http://l.yimg.com/a/lib/hlp/info/ycompanyinfo_20090211.css" rel="stylesheet" type="text/css">
<!-- End css -->
</head>
<body>
<!--start masthead--><center>
<!--begin ad served masthead--><!-- spaceId: 967400423 -->      <style type="text/css">
      .yzq_x { position:absolute; }
      </style><!-- SpaceID=0 robot -->
<!--end ad served masthead-->
</center>
<!--end masthead--><div id="yciwrap">
<div id="ycicontainer" class="ycictner">
<!--Top graphic--><div><img src="http://l.yimg.com/a/i/us/info/ci/img_ci_01.jpg"></div>
<!--Begin left side blocks--><div id="ycileft"><ul id="main-center">
<li><h3><a href="http://info.yahoo.com/privacy/us/yahoo/details.html" class="bb-url">Privacy Policy</a><cite>Respect user information...</cite>
</h3></li>
<li><h3><a href="http://security.yahoo.com/" class="bb-url">Security Center </a><cite>Protect yourself online...</cite>
</h3></li>
<li><h3><a href="http://info.yahoo.com/legal/us/yahoo/utos/utos-173.html" class="bb-url"> Terms </a><cite>Yahoo! Terms of Service...</cite>
</h3></li>
<li><h3><a href="http://info.yahoo.com/copyright/details.html" class="bb-url">Copyright/IP Policy </a><cite>Notice and takedown policies...</cite>
</h3></li>
<li><h3><a href="http://help.yahoo.com/l/us/yahoo/helpcentral" class="bb-url">Help Central </a><cite>Assistance for specific sites...</cite>
</h3></li>
<li><h3><a href="http://family.yahoo.com/" class="bb-url"> Family Accounts</a><cite>Online safety for parents and kids...</cite>
</h3></li>
</ul></div>
<!--ycileft--><!--End left side blocks--><!--Begin middle section--><div id="ycimiddle">
<div class="corpinfobox">
<div class="tp1"></div>
<div class="bd1">
<div id="ycicorpinfo" class="ycictner">
<h2>Corporate Information</h2>
<div class="corpinfo-lf">
<!--Begin new category--><p><a href="http://join.yahoo.com/">Employment Opportunities</a></p>
<ul>
<li><a href="http://careers.yahoo.com/">Find your big job!</a></li>
<li><a href="http://careers.yahoo.com/life.html">Yahoo! Life</a></li>
</ul>
<!--End new category--><!--Begin new category--><p>Business Opportunities</p>
<ul>
<li><a href="http://advertising.yahoo.com">Advertising Opportunities</a></li>
<li><a href="http://search.yahoo.com/info/submit.html">Submit Your Site</a></li>
<li><a href="http://advertising.yahoo.com/affiliate_programs/">Affiliate Programs</a></li>
</ul>
<!--End new category--><!--Begin new category--><p>More About Yahoo!</p>
<ul>
<li><a href="http://docs.yahoo.com/info/values">Our Company Values</a></li>
<li><a href="http://forgood.yahoo.com">Yahoo! for Good</a></li>
<li><a href="http://pressroom.yahoo.net/pr/ycorp/company-address.aspx">Our Address</a></li>
<li><a href="http://yodel.yahoo.com/subscribe">Yahoo! Blogs</a></li>
<li><a href="http://companystore.yahoo.com">Company Store</a></li>
</ul>
<!--End new category--></div><div class="corpinfo-rt"><!--Begin new category--><p><a href="http://investor.yahoo.net/">Investor Relations</a></p>
<ul>
<li><a href="http://investor.yahoo.net/results.cfm">Quarterly Earnings</a></li>
<li><a href="http://investor.yahoo.net/releases.cfm">Financial Press Releases</a></li>
<li><a href="http://investor.yahoo.net/documents.cfm">Corporate Governance</a></li>
</ul>
<!--End new category--><!--Begin new category--><p><a href="http://pressroom.yahoo.com/">News Center</a></p>
<ul>
<li><a href="http://pressroom.yahoo.net/pr/ycorp/overview.aspx">Company Overview</a></li>
<li><a href="http://pressroom.yahoo.net/pr/ycorp/news.aspx">Press Releases</a></li>
<li><a href="http://pressroom.yahoo.net/pr/ycorp/management.aspx">Management Team</a></li>
<li><a href="http://pressroom.yahoo.net/pr/ycorp/permissions.aspx">Permission Requests</a></li>
</ul>
<!--End new category--><!--Begin new category--><p><a href="http://surveylink.yahoo.com/wix/p1484643.aspx?sourcekey=fp">Yahoo! User Research</a></p>
<ul><li><a href="http://surveylink.yahoo.com/wix/p1484643.aspx?sourcekey=fp">Participate in research</a></li></ul>
<!--End new category--></div><div class="corpinfo-rt"></div>
</div>
<!--ycicorpinfo-->
</div>
<!--bd1--><div class="bm1"></div>
</div>
<!--corpinfobox-->
</div>
<!--ycimiddle--><!--End middle section--><!--Begin right side blocks--><div id="yciright">
<!-- Begin Top Questions --><div class="promobox"><a href="http://pressroom.yahoo.com/" class="bb-url">
<img src="http://info.yahoo.com/center/us/yahoo/vision.jpg" alt="Our Purpose" class="bb-image" border="0" />
</a></div>
<!-- End Top Questions --><!--Begin More Help Links--><div class="promobox"><a href="http://careers.yahoo.com/" class="bb-url">
<img src="http://l.yimg.com/a/i/us/info/ci/thinkbigbutton.jpg" alt="Think Big Think Purple" class="bb-image" border="0" />
</a></div>
<!--End More Help Links--><div class="promobox"><a href="http://ycorpblog.com/" class="bb-url">
<img src="http://l.yimg.com/a/i/us/info/ci/img_yblog_01.gif" alt="OUR COMPANY" class="bb-image" border="0" />
</a></div>
</div>
<!--yciright--><!--End right side blocks-->
</div>
<!--ycicontainer--><!--Begin footer--><div id="footer">
<!--Begin static footer html--><center><dir_att_footer_html><div style="font-family:Arial, Helvetica, sans-serif; font-size:10px; text-align:center; ">

Copyright © 2008 Yahoo! Inc. All rights reserved. | <a class="uline" href="http://us.ard.yahoo.com/SIG=151jmqduh/M=289534.9722465.10435768.9412057/D=co_info/S=967400420:FOOT/Y=YAHOO/EXP=1213135851/L=_swJftFJq2Zf2fCnfwCMuhZ1RZNQY0hO38sAAxEK/B=Y7MUA0LaX.I-/J=1213128651202383/A=4184031/R=0/SIG=11lp7krrc/*http://docs.yahoo.com/info/copyright/copyright.html">Copyright/IP Policy</a> | <a class="uline" target="_top" href="http://us.ard.yahoo.com/SIG=151jmqduh/M=289534.9722465.10435768.9412057/D=co_info/S=967400420:FOOT/Y=YAHOO/EXP=1213135851/L=_swJftFJq2Zf2fCnfwCMuhZ1RZNQY0hO38sAAxEK/B=Y7MUA0LaX.I-/J=1213128651202383/A=4184031/R=1/SIG=1136qnvkg/*http://docs.yahoo.com/info/terms/">Terms of Service</a> | <a class="uline" target="_top" href="http://us.ard.yahoo.com/SIG=151jmqduh/M=289534.9722465.10435768.9412057/D=co_info/S=967400420:FOOT/Y=YAHOO/EXP=1213135851/L=_swJftFJq2Zf2fCnfwCMuhZ1RZNQY0hO38sAAxEK/B=Y7MUA0LaX.I-/J=1213128651202383/A=4184031/R=2/SIG=10neqg5hf/*http://help.yahoo.com">Help</a><br/>NOTICE: We collect personal information on this site. To learn more about how we use your information, see our <a class="uline" target="_top" href="http://us.ard.yahoo.com/SIG=151jmqduh/M=289534.9722465.10435768.9412057/D=co_info/S=967400420:FOOT/Y=YAHOO/EXP=1213135851/L=_swJftFJq2Zf2fCnfwCMuhZ1RZNQY0hO38sAAxEK/B=Y7MUA0LaX.I-/J=1213128651202383/A=4184031/R=3/SIG=1163rhhok/*http://privacy.yahoo.com/privacy/us/">Privacy Policy</a><br></br>



</div>
</dir_att_footer_html></center><!--End static footer html-->
</div>
<!--footer--><!--End footer-->
</div>
<!--yciwrap-->
</body>
</html>
<!-- w1.help.re1.yahoo.com uncompressed/chunked Tue Apr  5 08:11:53 PDT 2011 -->
