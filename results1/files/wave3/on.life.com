<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="nojs">
<!-- MIME TYPE Guidlines and references: http://hixie.ch/advocacy/xhtml -->
<head>
<title>bitly | Basic | a simple URL shortener</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="IE=8" http-equiv="X-UA-Compatible">
<meta name="keywords" content="bitly, awesome, url shortener" />
<meta name="description" content="bitly, a simple url shortener" />
<script type="text/javascript">
// remove nojs from html
try{ document.getElementsByTagName("html")[0].className = ""; }
catch (ex) {}
</script>


<link rel="stylesheet" href="/s/v313/css/bitly_compressed.css" type="text/css" charset="utf-8" />


<!--[if lte IE 8]>
<link rel="stylesheet" href="/s/v313/css/IE.css" type="text/css" charset="utf-8" />
<![endif]-->
<!--[if lte IE 6]>
<link rel="stylesheet" href="/s/v313/css/IE6.css" type="text/css" charset="utf-8" />
<![endif]-->

<link rel="icon" type="image/png" href="/s/v313/graphics/favicon.png" />

<style type="text/css">
/* temp until i put sticky footer on every page */
html:before, #middle:before { /* Opera and IE8 "redraw" bug fix */
content:"";
float:left;
height:100%;
margin-top:-999em;
}
* html #wrap { /* IE6 workaround */
height:100%;
}
</style>

<script type="text/javascript">
// chartbeat
var _sf_startpt=(new Date()).getTime();
var _sf_async_config={uid: 30, sections: 'default', pingServer: 'pdev.chartbeat.net', domain: 'bit.ly' };

_sf_async_config['sections'] = "home,home_unauthenticated";

</script>

</head>
<body class="unauth_home">
<div class="ext_bitly_chrome_promo_delay">
</div>


<div id="external_container">
<div id="container">
<!-- Put Content Here -->
<div id="top">




</div> <!-- end #top -->
<div id="middle">


<!-- Sorry, you are not logged -->
<div id="middle_inner">
<div id="header">
<a id="logo" href="/">bit.ly</a>
</div>
<div id="middleLevelContainer1">
<div id="mainSearchContainer" class="">
<div id="mainSearchContainerInner" class="mainUnAuthShortenContainerInner">
<div class="formActionContainer">
<form action="/" method="POST" name="shortenUrl" id="unAuthShortenForm">
<div class="shortenUnAuthBox clearfix">
<div id="mainUnAuthShortenContainer" class="inputBoxContainer">
<input tabindex="1" id="shortenUnAuthContainer" type="text" name="url" value="" />
</div>
<input type="submit" class="submitButtonBackground white_button" id="unAuthShortenButton" name="searchButton" value="Shorten" />
<a href="/a/sign_in" id="sign_in">Sign In</a>
<form action="" method="get">
<input type="hidden" name="_xsrf" value="2554a6af47454d5c9d588683ca62941f"/>
</form>
</div>
</form>
</div>
</div>
</div>
<div class="shortenedBitlyListListBox last_shorten ">
<div class="linkCapsule_link clearfix">
<strong class="shortened_url">Shortened links</strong>
<strong class="realtime_stats">Real-time stats</strong>
<strong class="long_link">Long link</strong>
</div>
<ul class="shortened_results_list">

</ul>
</div>


<div class="resultsContainer">
<div id="historyHeadline"></div>
<div id="weeklySparkLines"></div>
<div id="results">
<div id="preloaderContainer"><img src="/s/v313/graphics/64_64_blue_preloader.gif" alt="" border="0" width="64" height="64" /></div>
</div> <!-- end #resultsContainer -->
</div>


<p id="signup_promo" class="clearfix">
Bitly helps you share, track, and analyze your links.
<a id="sign_up" href="/a/sign_up">Sign Up »</a>
</p>
</div>
</div>

</div> <!-- END #middle -->
<div id="bottom">

</div> <!-- end #bottom -->
</div> <!-- end #container -->
</div> <!-- end #external_container -->


<div id="footer">

<div class="footerLinksContainer clearfix">
<div class="footerColumn">
<h2>bitly</h2>
<ul>
<li><a href="/pages/help">Help</a></li>
<li><a href="/pages/tools">Tools</a></li>
<li><a href="/pages/partners">Partners</a></li>
<li><a href="http://bit.ly/apidocs">API</a></li>

<li><a href="/a/sign_up">Sign Up</a></li>


</ul>
</div>
<div class="footerColumn">
<h2>bitlyverse</h2>
<ul>
<li><a href="http://bitly.pro/">bitly.Pro</a></li>
<li><a href="http://labs.bit.ly/">labs.bit.ly</a></li>
<li><a href="http://bitly.tv/">bitly.tv</a></li>
<li><a href="http://j.mp/">j.mp</a></li>
</ul>
</div>
<div class="footerColumn">
<h2>Company</h2>
<ul>
<li><a href="/pages/about">About</a></li>
<li><a href="http://blog.bit.ly">Blog</a></li>
<li><a href="/pages/jobs">Jobs</a></li>
<li><a href="/pages/privacy">Privacy</a></li>
<li><a href="/pages/terms-of-service">Terms of Service</a></li>
</ul>
</div>
<div class="footerColumn">
<h2>Community</h2>
<ul>
<li><a href="http://feedback.bit.ly/">Feedback</a></li>
<li><a href="mailto:support+sitelink@bit.ly">Contact</a></li>
<li><a href="/a/report_spam">Report Abuse</a></li>
</ul>
</div>
<div class="bitlyTrademarkContainer">&copy; 2011 bitly <span class="super">TM</span></div>
</div>

<form action="" method="get">
<input type="hidden" name="_xsrf" value="2554a6af47454d5c9d588683ca62941f"/>
</form>
</div>



<!--[if IE]><script language="javascript" type="text/javascript" src="/s/v313/js/ie-hacks/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" src="/s/v313/js/bitly_compressed.js"></script>
<script type="text/javascript" src="/s/v313/js/i18n/en_US.js"></script>



<script type="text/javascript">
//<!--
if(!window.console) window.console={};
if(!window.console.log) window.console.log = function(){};
var pageTracker;
// turn caching off for all AJAX - fix IE issue with stale data
$.ajaxSetup({
cache : false,
traditional : true,
timeout : 12000,
beforeSend : function(XMLHttpRequest) {
XMLHttpRequest.setRequestHeader("X-XSRFToken", $.cookie.get("_xsrf"));
}
});
$(function() {
var $bod = $(document.body).errorMessenger().eventTracker();
// TODO: move these to a .js file
$bod.bind('addTwitterViaOAuth', function(e, data) {
var completion_redirect = document.location.pathname,
url = '/a/add_twitter_account';
if(data && data.share_text) {
completion_redirect += '?s=' + encodeURIComponent( data.share_text );
}
url += '?rd=' + encodeURIComponent(completion_redirect);
top.location.href = url;
}).bind('addFacebookViaConnect', function(e,data) {
var completion_redirect = document.location.pathname,
url = '/a/add_facebook_account';
if(data && data.share_text) {
completion_redirect += '?s=' + encodeURIComponent( data.share_text );
}
url += '?rd=' + encodeURIComponent(completion_redirect);
top.location.href = url;
});

/* only trigger for production */
var protcl = document.location.protocol
_google_analitcies_url = (("https:" == protcl ) ? "https://ssl." : "http://www.") + "google-analytics.com/ga.js",
_charbeat_url = (("https:" == protcl) ? "https://s3.amazonaws.com/" : "http://") + "static.chartbeat.com/js/chartbeat.js";
window._sf_endpt=(new Date()).getTime();
getJS(_google_analitcies_url, function(){try {
pageTracker = _gat._getTracker("UA-1223636-11");
pageTracker._trackPageview();
} catch(err) {}
});
getJS(_charbeat_url, function() {
// chart beat loaded
});

BITLY.data.xsrf = '<input type="hidden" name="_xsrf" value="2554a6af47454d5c9d588683ca62941f"/>';
BITLY.data.host = "bit.ly";
BITLY.data.current_user = '';
BITLY.els.search_box = new BITLY.DefaultTextbox("#q");
BITLY.els.search_box.els.form.historySuggest({
searchBox: BITLY.els.search_box.el,
searchContainer: BITLY.els.search_box.els.form.find("fieldset")
});
//TODO: actually handle the case where there is a search box but no history box
BITLY.Eventer.delegate({
evt: "submit",
el: BITLY.els.search_box.els.form,
del: BITLY.els.search_box.els.form,
cb: function(e) {
if (!BITLY.data.instant_activated) {
e.preventDefault();
var action = BITLY.els.search_box.els.form.attr("action");
if(location.href.indexOf(action) > -1) {
location.hash = BITLY.data.cur_search = BITLY.els.search_box.val();
$(document.body).trigger("refreshHistory", {page: 1, search_term:BITLY.els.search_box.val()});
}
else {
location.href=action+"#"+BITLY.els.search_box.val();
}
}
return false;
},
scope: this
});
$("#header .user_link").bind("click", function(e) {
e.preventDefault();
e.stopPropagation();
var el=$(this), container = $("#loginContainer");
if(container.hasClass("active")) {
container.removeClass("active");
$("body").unbind("click.hide_user_dropdown");
}
else {
container.addClass("active");
$("body").bind("click.hide_user_dropdown", function(e) {
$("#header .user_link").click();
});
}
});
jQuery(function() {
if(navigator.userAgent.toLowerCase().indexOf('chrome') > -1 && !jQuery.cookie.get("no_cp")) {
setTimeout(function() {
jQuery(".ext_bitly_chrome_promo_delay").html('<p>Check out the <a href="http://chrome.bit.ly/">bitly chrome extension</a> - It\'s the best way to <a href="http://chrome.bit.ly/">shorten, share and track your links</a>!</p><a class="close" href="#">x</a><a class="install rounded" href="http://chrome.bit.ly/">Install</a>').slideDown();
jQuery(".ext_bitly_chrome_promo_delay a.close").bind("click", function(e) {
e.preventDefault();
jQuery.cookie.write("no_cp", 1, 365);
jQuery(".ext_bitly_chrome_promo_delay").slideUp(function() { jQuery(this).remove(); });
$(document.body).trigger('track', { 'chrome_ext_promo' : 'Chrome extension promo closed' } )
});
}, 2000);
}
});
// document.ready?
})
//-->
</script>


<script type="text/javascript">
$(function($){
new BITLY.SignInDropdown("#sign_in", "#mainSearchContainer");
new BITLY.SignUpPanel("#sign_up", "#middle_inner", "#middleLevelContainer1");
$('#results').history( "", 1, { host_name: "bit.ly", showToggle:false, 'rows':'one_row', params : { perpage : 10 }, use_link_capsules: true } )
.totalClicks()
.pagination()
$("#unAuthShortenForm").bind("submit", function(e) {
e.preventDefault();
if(BITLY.data.unauth_shorten_box.el.val().indexOf(BITLY.data.unauth_shorten_box.defaultText) === 0) {
BITLY.data.unauth_shorten_box.el.val($.trim(BITLY.data.unauth_shorten_box.el.val().replace(BITLY.data.unauth_shorten_box.defaultText, "")));
}
var submit_options = {
url: "/data/shorten",
type: "POST",
dataType: "json",
traditional: "true",
success: function(data) {
if(data && data.data) {
if(data.status_code==200) {
$("#middleLevelContainer1").trigger("shortenComplete", data.data);
$(".last_shorten").show();
BITLY.data.shortened_hashes = BITLY.data.shortened_hashes || {};
BITLY.data.short_url = data.data.url;
BITLY.data.shortened_hashes[BITLY.data.short_url.split("/")[3]] = 1;
//$(document.body).trigger('refreshHistory');
$("#shortenUnAuthContainer").val(BITLY.data.short_url).focus().select();
}
else {
var error_msg = $._("An error occurred shortening that link");
switch(data.status_txt) {
case "INVALID_URI":
error_msg = $._("Unable to shorten that link. It is not a valid url.");
break;
case "ALREADY_A_BITLY_LINK":
error_msg = $._("That is already a %(sitename)s link").replace("%(sitename)s", "bitly");
break;
}
$(document.body).trigger("errorMessage", {text:error_msg});
}
}
},
error: function() {
var error_msg = "An error occurred shortening that link";
$(document.body).trigger("errorMessage", {text:error_msg});
}
}
$("#unAuthShortenForm").ajaxSubmit(submit_options);
});
$("#middleLevelContainer1").linkCapsule('.shortened_results_list', {
capsule_container: '.shortenedBitlyListListBox',
block_customization: true
})
BITLY.data.unauth_shorten_box = new BITLY.DefaultTextbox("#shortenUnAuthContainer", "Shorten Links Here");
BITLY.data.unauth_shorten_box.el.focus();

});
</script>

<script type="text/javascript">
//<![CDATA[
  $(function(){
            var short_url = BITLY.data.short_url = "";
            if(short_url === "") return;

            // conditional js...
            
            ZeroClipboard.setMoviePath("/s/v313/flash/zeroclipboard/ZeroClipboard.swf");
            var el = 

            $(".last_shorten li.linkCapsule_link").linkCapsule_Copy(short_url);
            $(".last_shorten li.linkCapsule_link").find("a.copy_button").bind("click.short_circuit", function(e) { e.preventDefault(); });

            $(".last_shorten").unbind('click.customize');
            $(".last_shorten").bind('click.customize', function(e) {
                if($(e.target).is("a.customize_button")) {
                    e.preventDefault();
                    $(document.body).trigger('errorMessage', {
                      text :  $._("Please sign in or sign up to customize a url")
                    });
                }
            });


            }); 
//]]>
</script>
</body>
</html>
