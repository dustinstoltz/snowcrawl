<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Ow.ly - Shorten urls, share files and track visits - Owly</title>
	<meta name="keywords" content="Ow.ly, Shorten urls, share files, track visits, twitter file sharing, twitter image sharing, tweet comments" />
	<meta name="description" content="Ow.ly - Shorten urls, share files and track visits." />
						<link href="http://static.ow.ly/4-9-10/css/owly_shorten.css" type="text/css" rel="stylesheet" />
				<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="http://static.ow.ly/4-9-10/css/ie6.css" />
	<![endif]-->	
	<script type="text/javascript">
	var owly = new Object();
	owly.c = new Object();
	owly.c.rootUrl = "http://ow.ly";
	owly.c.photoRootUrl = "http://static.ow.ly/photos";
	owly.c.swfUrl = "http://static.ow.ly/4-9-10/swf";
</script>

			<script type="text/javascript" src="http://static.ow.ly/4-9-10/js/owly.js"></script>
		 

	 <script type="text/javascript">
		 var RecaptchaOptions = { theme : 'white' };
	 </script>
	 
    <link rel="shortcut icon" href="http://static.ow.ly/4-9-10/images/favicon.ico" />
    <link rel="apple-touch-icon" href="http://static.ow.ly/4-9-10/images/apple-touch-icon.png"/>
</head>

<body>


<div id="statusContainer" style="display: none;">
	<div class="y-n">
    	<span class="cn-3">
            <span class="tl"><!--  --></span>
            <span class="tc"><!--  --></span>
            <span class="tr"><!--  --></span>
            <span class="ml"><!--  --></span>
            <span class="mr"><!--  --></span>
            <span class="bl"><!--  --></span>
            <span class="bc"><!--  --></span>
            <span class="br"><!--  --></span>
        </span>
        <span class="_statusMsgContent"></span>
    </div>
</div>

<div id="headerWrapper">
	<div id="globalLogin">
					<a href="#" id="btnLogin"><span class="hidden">Sign in with Twitter</span></a>
			</div>
	<div id="header">
		<h1 class="logo"><a href="http://ow.ly">Owly</a></h1>
	</div>
</div>	

<div id="page">

	<div class="box">

		
		<h5>Shorten URL</h5>
		
				
		<form class="shrink-form" action="http://ow.ly/url/shorten-url" method="post">
			<div class="ipt c-r"><input class="link" type="text" name="url" value="http://" /></div>
			<div class="sbt"><input class="submit" type="submit" name="form_submit" value="Shrink It" src="http://static.ow.ly/4-9-10/images/btn_shrink.gif" /></div>
			<span class="formError"></span>
	
		<br/>
		<br/>		
					<script type="text/javascript" src="http://api.recaptcha.net/challenge?k=6LcLMwwAAAAAAKoAf-zylfZbfqCrAIppjVOkpgiY"></script>

	<noscript>
  		<iframe src="http://api.recaptcha.net/noscript?k=6LcLMwwAAAAAAKoAf-zylfZbfqCrAIppjVOkpgiY" height="300" width="500" frameborder="0"></iframe><br/>
  		<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
  		<input type="hidden" name="recaptcha_response_field" value="manual_challenge"/>
	</noscript>
				
		</form>
		
					<div class="uploadOr"><div><span>or</span></div></div>
			<a id="loginToUpload" class="_loginToUpload" href="#"><img src="http://static.ow.ly/4-9-10/images/new/login_to_upload.png" /></a>
			</div>
	
	
	<div id="response-message"></div>
	<div class="apiList">
		<h3><a href="#" onclick="$('#form-container').slideToggle(); return false;"><strong>Developer API</strong> Get on the list now!</a></h3>
		<div id="form-container" style="display: none">
		
			<form name="newsletterForm" action="javascript:;" method="post">
				<label for="signupEmail">Email Address:</label>
				<div class="ipt"><input id="signupEmail" type="text" name="signupEmail" value="" /></div>
				<label for="signupCompany">Company Name:</label>
				<div class="ipt"><input id="signupCompany" type="text" name="signupCompany" value="" /></div>
				<label for="signupUrl">Company URL:</label>
				<div class="ipt"><input id="signupUrl" type="text" name="signupUrl" value="" /></div>
				<input id="signupType" type="hidden" name="signupType" value="API" />
				<div class="sbt"><input type="submit" onclick="submitEmail()" name="form_submit" value="Submit" /></div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

		        		
    });
</script>
<!-- close main wrapping div... no content below this area -->

<div id="footer">
	<a class="logo-hs" target="_blank" href="http://hootsuite.com/">Hootsuite</a>
</div>

<div id="bottomMargin"></div>

<div id="userMessage">
</div>

<script type="text/javascript">
	$(document).ready(function() {
	    		     // popauth
	        var p = new popauth("twitter", '/ajax/twitter/get-auth-url', function(e, ot, ov) { 
		        $.getJSON('/ajax/twitter/get-access-token?ot='+ot+'&ov='+ov, function(data) {
					if (data.success) {
						setTimeout(function(){ window.location = window.location; }, 0.1);	// refresh
					} else {
						displayUserMessage("Login failed, please try again");
					}
		        }); 
			});
			// bind button
			$("#btnLogin").bind("click", function(e) {
				e.preventDefault();
				p.start();	// start auth
			});

			// popauth upload
			// @TODO: refactor
			
			var pupload = new popauth("twitter", '/ajax/twitter/get-auth-url', function(e, ot, ov) { 
		        $.getJSON('/ajax/twitter/get-access-token?ot='+ot+'&ov='+ov, function(data) {			        
					if (data.success) {
						setTimeout(function(){ window.location = 'http://ow.ly/upload'; }, 0.1);	// refresh
					} else {
						displayUserMessage("Login failed, please try again");
					}
		        }); 
			});
			// bind button
			$("#loginToUpload").bind("click", function(e) {
				e.preventDefault();
				pupload.start();	// start auth
			});
	    	});
</script>

	<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		try {
		var pageTracker = _gat._getTracker("UA-51223-22");
		pageTracker._trackPageview();
		} catch(err) {}
	</script>

</body>
</html>