<!DOCTYPE html>
<html lang="en-us">
  <head>
    <title>Twitter for Business</title>

    <link href="http://a1.twimg.com/a/1301951652/images/twitter_57.png" rel="apple-touch-icon" />
<link href="/oexchange.xrd" rel="http://oexchange.org/spec/0.8/rel/related-target" type="application/xrd+xml" />
<link href="http://a1.twimg.com/a/1301951652/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="twitter, business, small, advertise, advertising, guide, learn, case studies, how to, promoted products, promoted tweets, promoted accounts, promoted trends, sponsored tweets, who to follow"/>
    <meta name="description" content="The official Twitter for Business site: How to get started, optimize and advertise on Twitter" />

    <link href="http://a1.twimg.com/a/1301951652/stylesheets/business/compiled/business.css?1301337105" media="screen, projection, print" rel="stylesheet" type="text/css" />
    <link href="http://a1.twimg.com/a/1301951652/stylesheets/business/compiled/print.css?1301337105" media="print" rel="stylesheet" type="text/css" />
    <link href="http://a1.twimg.com/a/1301951652/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <script src="http://a2.twimg.com/a/1301951652/javascripts/lib/jquery/1.4.2/jquery.min.js?1301337105" type="text/javascript"></script>
    <script src="http://a2.twimg.com/a/1301951652/javascripts/lib/jquery.tipsy.min.js?1301337105" type="text/javascript"></script>
    <script src="http://a1.twimg.com/a/1301951652/javascripts/layout_newtwitter.js?1301337105" type="text/javascript"></script>
    <!-- @anywhere -->
    
    <script src="http://platform.twitter.com/anywhere.js?id=Ol7dHlO1gvJ4XEKQVRGemA&v=1"></script>
    

    <script type="text/javascript">
      twttr.anywhere(function(twitter) {
        twitter.hovercards();
      });
    </script>
  </head>

  <body>

    <div id="top-stuff">
            <link href="http://a2.twimg.com/a/1301951652/stylesheets/layout_newtwitter_topbar.css?1301337105" media="screen" rel="stylesheet" type="text/css" />


<div class="topbar" id="corpBar">
  <div class="topbar-bg"></div>
  <div class="topbar-inner">
    <div class="fixed-container clearfix">
      
        <a href="http://business.twitter.com/" title="Twitter / Home" accesskey="1" id="logo">Twitter</a>
      

      <form method="post" id="sign_out_form" action="/sessions/destroy" style="display:none;">
        <input name="authenticity_token" value="6190d708da518c0c20ed4c619e87fb5dd1be8744" type="hidden"/>
      </form>

      <ul class="nav secondary-nav">
        <li class="account">
          
          <ul class="menu-dropdown">
            <li><a href="http://twitter.com/settings/account" accesskey="s" id="settings_link">Settings</a></li>
            <li><a href="http://twitter.com/invitations/find_on_twitter" accesskey="=" id="find_people_link">Who to Follow</a></li>
            <li><a href="http://support.twitter.com" accesskey="?" id="help_link">Help</a></li>
            <li><a href="http://twitter.com/logout" accesskey="l" id="sign_out_link" onclick="document.getElementById('sign_out_form').submit(); return false;">Sign out</a></li>
                    </ul>
        </li>
      </ul>

    </div> <!-- /fixed-container -->
  </div> <!-- /topbar-inner -->
</div>


    </div>

    <div class="fixed-container">
      <div id="content">
      
        <div id="jumbotron">
  <h1>Twitter for Business</h1>
  <p>Whatever the size of your organization, make the most of Twitter.</p>
</div>

<div id="businessOverview">
  <div class="row clearfix">
    <div class="one-third column">
      <a href="/basics/what-is-twitter">
        <img alt="Learn the Basics" class="reflection" src="http://a1.twimg.com/a/1301951652/images/business/twitter-abcs.png" />
      </a>
      <h2><a href="/basics/what-is-twitter">Learn the Basics</a></h2>
      <ul>
        <li><a href="/basics/what-is-twitter">What is Twitter?</a></li>
        <li><a href="/basics/glossary">Twitter Glossary</a></li>
        <li><a href="/basics/best-practices">Best Practices</a></li>
        <li><a href="/basics/mobile">Twitter on the Go</a></li>
      </ul>
    </div>
    <div class="one-third column">
      <a href="/optimize/community-growing">
        <img alt="Use Twitter more" class="reflection" src="http://a2.twimg.com/a/1301951652/images/business/twitter-equalizer.png" />
      </a>
      <h2><a href="/optimize/community-growing">Optimize Your Activity</a></h2>
      <ul>
        <li><a href="/optimize/community-growing">Community Growing</a></li>
        <li><a href="/optimize/case-studies">Case Studies</a></li>
        <li><a href="/optimize/resources">Resources and Widgets</a></li>
        <li><a href="/optimize/api">API Integration</a></li>
      </ul>
    </div>
    <div class="one-third column">
      <a href="/advertise/start">
        <img alt="Start Advertising" src="http://a0.twimg.com/a/1301951652/images/business/promoted-billboard.png" style="top: 4px; left: 19px;" />
      </a>
      <h2><a href="/advertise/start">Start Advertising</a></h2>
      <ul>
        <li><a href="/advertise/promoted-tweets">Promoted Tweets</a></li>
        <li><a href="/advertise/promoted-trends">Promoted Trends</a></li>
        <li><a href="/advertise/promoted-accounts">Promoted Accounts</a></li>
        <li><a href="/advertise/analytics">Analytics</a></li>
      </ul>
    </div>
  </div>
</div>

      
      </div>

      <div id="footer">
        <div class="twivider"></div>
        <p>
          <a href="/">Business Home</a> <span class="bullet">&middot;</span> Follow @TwitterBusiness <span class="bullet">&middot;</span> Visit the <a href="http://support.twitter.com/business" target="_blank">Business Support site</a> <span class="bullet">&middot;</span> <a href="/advertise/start">Advertise</a>
        </p>
        <p>
          <a href="/about">About Us</a> <span class="bullet">&middot;</span>
          <a href="http://support.twitter.com">Help</a> <span class="bullet">&middot;</span>
          <a href="http://blog.twitter.com">Blog</a> <span class="bullet">&middot;</span>
          <a href="http://status.twitter.com">Status</a> <span class="bullet">&middot;</span>
          <a href="/tos">Terms</a> <span class="bullet">&middot;</span>
          <a href="/privacy">Privacy</a> <span class="bullet">&middot;</span>
          <a href="/about/resources">Resources</a> <span class="bullet">&middot;</span>
          <a href="http://dev.twitter.com/">API</a> <span class="bullet">&middot;</span>
          <a href="/jobs">Jobs</a>        </p>
        <p>
          &copy; 2011 Twitter
        </p>
      </div>
    </div>

        <!-- BEGIN google analytics -->

  <script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  </script>

  <script type="text/javascript">

    try {
      var pageTracker = _gat._getTracker("UA-30775-6");
      pageTracker._setDomainName("twitter.com");
            pageTracker._setVar('Not Logged In');
            pageTracker._setVar('lang: en');
            pageTracker._initData();
                              pageTracker._trackPageview();
                  } catch(err) { }

  </script>

  <!-- END google analytics -->



  </body>
</html>
