<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html lang="en-US" id="front" class="pageCobrand-NONE pageLang-en-US">
<head>
	<title>What&#039;s Buzzing? You Tell Us! Top News and More - Yahoo! Buzz</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=8">
	<meta name="robots" content="noydir">
	<meta name="description" value="Yahoo! Buzz surfaces the Web's most remarkable stories (top news, videos, photos, and blogs), determined by people like you. Discover, vote, comment and submit!">
	<meta name="keywords" value="yahoo, buzz, yahoo buzz, news, business, entertainment, health, images, lifestyle, politics, sci/tech, seen on yahoo.com, sports, travel feed, u.s. news, video, world news, daily news, local news, breaking news, hot news, top news, most emailed news, most viewed news, newspaper, social news, social content, buzz up, buzz down, vote, vote up, vote down, comment, comments, top buzz, upcoming buzz, up & coming, top searches, top stories, first buzzer, buzz updates, yahoo buzz updates, buzz profile, buzz profiles, yahoo buzz profile, yahoo buzz profiles, buzzing now, buzzing">
	<meta name="generator" value="orion_front-1.124.8.324">
	<link rel="search" type="application/opensearchdescription+xml" title="Yahoo! Buzz" href="http://buzz.yahoo.com/plugins/buzz_search.xml">
	<link rel="alternate" type="application/rss+xml" href="http://d.yimg.com/ds/rss/V1/top10/all">

<link rel="stylesheet" type="text/css" href="http://l.yimg.com/ds/combo?yui/2.7.0/build/reset-fonts-grids/reset-fonts-grids.css&orion/1.0.15.59/css/orion.css&orion/1.0.15.59/css/welcome.css&orion/1.0.15.59/css/overlay.css&orion/1.0.15.59/css/header.css&orion/1.0.15.59/css/activity_messaging.css&orion/1.0.15.59/css/nav.css&orion/1.0.15.59/css/categorylist.css&orion/1.0.15.59/css/articlelist.css&orion/1.0.15.59/css/messagebar.css&orion/1.0.15.59/css/nagbar.css&orion/1.0.15.59/css/vote.css&orion/1.0.15.59/css/whatsthis.css&orion/1.0.15.59/css/related_articles.css&orion/1.0.15.59/css/pagination.css&orion/1.0.15.59/css/sidebar.css&orion/1.0.15.59/css/coreid.css&orion/1.0.15.59/css/top_searches.css&orion/1.0.15.59/css/topStories.css&orion/1.0.15.59/css/localbuzz.css&orion/1.0.15.59/css/vitality.css&orion/1.0.15.59/css/about_buzz.css&orion/1.0.15.59/css/buzzlogMini.css&orion/1.0.15.59/css/sidebarFooter.css&orion/1.0.15.59/css/footer.css&orion/1.0.15.59/css/container.css">
<link rel="stylesheet" type="text/css" href="http://l.yimg.com/kx/ds/css/disclosure-1.0.css">
<style>.updates-disclosure{ padding:0px; margin:0px; }.updates-disclosure .hd{ zoom:1; }
</style>
<script type="text/javascript">
	ORION_USER_YID="";	
</script>
<script type="text/javascript" src="http://yui.yahooapis.com/3.1.1/build/yui/yui.js"></script>
</head>
 
 
<body>

<div id="body">
	<div id="doc" class="yui-t6">
		<div id="hd">
		<link type="text/css" rel="stylesheet" href="http://l.yimg.com/zz/combo?kx/ucs/uh/css/168/yunivhead-min.css" /><style>
#yUnivHead {background:transparent;}
#yUnivHead #yuhead-bd {padding-bottom:0;}
#yUnivHead .yucs-trending-anim .yucs-trending-anim-fade {background-color: #ebf0f6;}
</style><div id="yUnivHead" class="yucs-en-US">    <a href="#yuhead-search" class="yucs-skipto-search yucs-activate">Skip to search.</a>        <div id="yuhead-hd" class="yuhead-clearfix">        <div id="yuhead-mepanel-cont">            <ul id="yuhead-mepanel"><li class="yuhead-me yuhead-nodivide yuhead-nopad">   <a class="yuhead-signup" href="https://edit.yahoo.com/config/eval_register?.src=ybz&.intl=us&.done=http://buzz.yahoo.com/">    <em>New User?</em> Register</a></li><li class="yuhead-me"><a href="https://login.yahoo.com/config/login?.src=ybz&.intl=us&.done=http://buzz.yahoo.com/"><em>Sign In</em></a></li><li class="yuhead-me">
	<a href="http://help.yahoo.com/l/us/yahoo/buzz/">
		Help
	</a>
</li></ul>        </div>        <div id="yuhead-promo"><style>/* panels */.yucs-sethp .yucs-sethp-panel {font-size:x-small;border:1px solid #4333BC;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:5px;background:#fff;width:360px;padding:10px 10px;position:absolute;z-index:100;margin-top:6px;}.yucs-sethp .pnt{background-position:-29px 0;background-repeat:no-repeat;height:6px;width:11px;position:absolute;left:50%;z-index:100;}.yucs-sethp .yucs-sethp-panel .yucs-sethp-panel-logo {background-position:0 -16px;background-repeat:no-repeat;display:block;font-size:0;height:37px;line-height:0;width:40px;text-indent:-1000px;overflow:hidden;float:left;}.yucs-sethp .myyhpbg {background-image:url(http://l.yimg.com/a/i/us/uh/bt2/myyhp_sprite_v1.gif);}.yucs-sethp .yucs-sethp-panel ol {width:300px;display:block;float:left;margin:0 0 10px;list-style:decimal inside;padding:0 0 0 10px;text-align:left;}.yucs-sethp .yucs-sethp-panel .hr {clear:both;text-align:center;}.yucs-sethp .yucs-sethp-panel .hr a {color:#000;}.yucs-sethp .hide {display:none;}</style><div class="yucs-sethp yucs-activate" data-prop="buzz" data-detectscript="http://www.yahoo.com/includes/hdhpdetect.php"><a href="http://us.lrd.yahoo.com/_ylc=X3oDMTFnNzFiMTJoBHRtX2RtZWNoA1RleHQgTGluawR0bV9sbmsDVTExMzA1NTYEdG1fbmV0A1lhaG9vIQ--/SIG=112cgufir/**http%3A//www.yahoo.com/%3Fmkt=3" target="_top" rel="nofollow">Make Y! My Homepage<abbr title="Yahoo!"></abbr> </a><div class="myyhpbg pnt hide">&nbsp;</div><div class="yucs-sethp-panel shdw hide"><div class="bd"><a title="Yahoo!" href="http://www.yahoo.com" class="yucs-sethp-panel-logo myyhpbg">Yahoo!</a><ol class="yuheadhpinstr"><li>Drag the "Y!" and drop it onto the "Home" icon.</li><li>Select "Yes" from the pop up window.</li><li>Nothing, you're done.</li></ol><div class="hr"><p>If this didn't work for you see <a href="http://us.lrd.yahoo.com/_ylc=X3oDMTFnNzFiMTJoBHRtX2RtZWNoA1RleHQgTGluawR0bV9sbmsDVTExMzA1NTYEdG1fbmV0A1lhaG9vIQ--/SIG=112cgufir/**http%3A//www.yahoo.com/%3Fmkt=3"class="yuheadshps yuheadshpdetails">detailed instructions</a></p><p><a href="javascript:void(0);" class="yucs-sethp-panel-close">Close this window</a></p></div></div></div></div></div>        <div id="yuhead-com-links-cont">            <ul id="yuhead-com-links">                                <li class="yuhead-com-link-item">    <a class="sp yuhead-ico-mail" href="http://mail.yahoo.com?.intl=us" rel="nofollow" target="_top">Mail</a></li><li class="yuhead-com-link-item">    <a href="http://my.yahoo.com"     rel="nofollow"     target="_top">   My Y!    </a></li><li id="yuhead-com-home"><a class="sp yuhead-ico-home" href="http://www.yahoo.com" rel="nofollow" target="_top">Yahoo!</a></li>            </ul>        </div>    </div>    <div id="yuhead-bd" class="yuhead-clearfix">        <!-- Add a classname of "small" to the h2 for the fixed variant in the JS layer forthe fixed header. Theming overrides are:#yUnivHead .yuhead-logo h2 {background-position:0px -120px;}#yUnivHead .yuhead-logo h2.small {background-position:0px -360px;}--><div class="yuhead-logo">   <style>      .yuhead-logo h2{         background-repeat:no-repeat;         width:194px;         height:58px;         background-image:url(http://l.yimg.com/a/i/brand/purplelogo/uh/us/20/buzz/buzz.png);         _background-image:url(http://l.yimg.com/a/i/brand/purplelogo/uh/us/20/buzz/buzz-ffffff.gif);      }      .yuhead-logo h2.small{         background-position:0px -240px;      }      .yuhead-logo a{         display:block;         position:absolute;         width:194px;         height:58px;      }      .yuhead-logo div.yuhead-comarketing {        font-size:93%;        width:194px;      white-space:nowrap;      text-align:right;      }      .yuhead-logo div.yuhead-comarketing a {        display:inline;        position:relative;      }   </style>   <h2>      <a href="http://buzz.yahoo.com/"       target="_top">         Yahoo! Buzz      </a>   </h2>   <!-- comarketing component --></div>        <div id="yuhead-search">        <div id="yuhead-sform-cont" class="yuhead-s-web yuhead-search-form">        <form role="search" class="yucs-search yucs-activate" action="http://search.yahoo.com/search" method="get"><table role="presentation">   <tbody role="presentation"><tr><td class="yucs-form-input" role="presentation"> <label>     <span>Search</span>                 <input autocomplete="off" type="text" class="sp yuhead-ico-mglass yuhead-search-hint yucs-search-hint-color yucs-search-field" name="p" value="Search" data-sh="Search" data-satype="rich" data-gosurl="" data-pubid="" /> </label></td><td NOWRAP class="yucs-form-btn" role="presentation"><div class="yucs-btn-wrap">    <button class="yucs-sweb-btn" type="submit">Search Web</button></div>            </td>         </tr>        </tbody></table><input type="hidden" id="fr" name="fr" value="orion" /></form>            </div>    </div>    </div></div><script charset="utf-8" type="text/javascript" src="http://l.yimg.com/zz/combo?kx/ucs/common/js/1/setup-min.js&kx/ucs/common/js/110/jsonp-super-cached-min.js&kx/ucs/search/js/88/search-min.js&kx/ucs/menu_utils/js/105/menu_utils-min.js&kx/ucs/username/js/14/user_menu-min.js&kx/ucs/help/js/13/help_menu-min.js&kx/ucs/sts/js/1/skip-min.js&kx/ucs/homepage/js/41/homepage-min.js&kx/ucs/trending_searches/js/130/trending_searches_anim-min.js"></script>   <div id="top_container_gradient">
		<span class="cTL"><!-- --></span>
		<span class="cTR"><!-- --></span>
		<span class="middle"><!-- --></span>		
	</div> 
	<div id="top_container">		
					<div class="hd" id="nav_container"> 
			
			
						<div id="top_nav_container">
							<ul>
								<li class="selected_tab"><a href="/"><u></u><b></b>Top Buzz</a></li>
																	<li class=""><a href="/buzzlog"><u></u><b></b>The Buzz Log</a></li>
																<li class=""><a href="/activity"><u></u><b></b>My Buzz Activity</a></li>
															</ul>						
							
						</div>
					</div>					
			</div>
		</div>
		<div id="bd">
			<div id="yui-main">
				<div class="yui-b hfeed">
					<div id="activity_messaging" class="mod messagebar hidden"></div>

		<div class="view-by clearfix">
		<ul class="filterlist clearfix">
			 
					 
						<li id="cat-more" class="category-more last"><span class="label">Categories:</span></li>
			<li id="category-list" class="categoryList">
			 
				<ul class="catFirst">
				<li id="cat-first" class="cat-first"><span><u></u><b></b>All</span></li>			
				<ul class="cat-list"> 
					<li class="cat_selected"><a class="allowDefaultBehavior" href="/">All</a></li><li><a class="allowDefaultBehavior" href="/articles/business/12/1/topstories/popular">Business</a></li><li><a class="allowDefaultBehavior" href="/articles/entertainment/12/1/topstories/popular">Entertainment</a></li><li><a class="allowDefaultBehavior" href="/articles/health/12/1/topstories/popular">Health</a></li><li><a class="allowDefaultBehavior" href="/articles/images/12/1/topstories/popular">Images</a></li><li><a class="allowDefaultBehavior" href="/articles/lifestyle/12/1/topstories/popular">Lifestyle</a></li><li><a class="allowDefaultBehavior" href="/articles/politics/12/1/topstories/popular">Politics</a></li><li><a class="allowDefaultBehavior" href="/articles/science/12/1/topstories/popular">Sci/Tech</a></li><li><a class="allowDefaultBehavior" href="/articles/sports/12/1/topstories/popular">Sports</a></li><li><a class="allowDefaultBehavior" href="/articles/news/12/1/topstories/popular">U.S. News</a></li><li><a class="allowDefaultBehavior" href="/articles/video/12/1/topstories/popular">Video</a></li><li><a class="allowDefaultBehavior" href="/articles/world/12/1/topstories/popular">World</a></li>				</ul>
				</ul>					
			</li>		 
					<form id="filter" action="" method="get" class="">
			<li class="viewfilter">View by:</li>
			<li id="viewby-list" class="viewbyList">				 
				<ul class="viewFirst">
				<li id="view-first" class="view-first"><span><u></u><b></b>12 hrs</span></li>			 	<ul class="view-list"> 	 
				<li><a class="allowDefaultBehavior" href="/articles/all/6/1/topstories/popular">6 hrs</a></li><li class="view_selected"><a class="allowDefaultBehavior" href="/">12 hrs</a></li><li><a class="allowDefaultBehavior" href="/articles/all/24/1/topstories/popular">24 hrs</a></li><li><a class="allowDefaultBehavior" href="/articles/all/168/1/topstories/popular">7 days</a></li><li><a class="allowDefaultBehavior" href="/articles/all/720/1/topstories/popular">30 days</a></li><li><a class="allowDefaultBehavior" href="/articles/all/24/1/topstories/vote">Vote Count</a></li><li><a class="allowDefaultBehavior" href="/articles/all/24/1/topstories/recent">Most Recent</a></li>					 
			 	</ul>
			</ul>					
			</li>
			</form> 	
				<li id="buzz-search">
	<form name="search" action="/search" method="get">
		  
		 <input class="text" type="text" id="search-terms" name="p" value='Search Top Buzz' onfocus="if(this.value=='Search Top Buzz'){this.value='';this.style.color='#000';} else {this.style.color='#000';}"> 		
		 <div class="search_btn">
		 <button id="top_nav_search_button" type="submit" value="Go">Go</button>
		 <u></u>
		 </div>
		 
	</form>
	 
	</li>
				</ul>
		</div>
		<div class="mod articleListContainer" id="articleListContainer">
	<div class="content">
				<div class="bd">
			
	
	
			<div articleid="1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6" id="articleItem-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.businesspundit.com/10-most-profitable-low-budget-movies-of-all-time/" rel="nofollow" trackurl="/track-click">10 Most Profitable Low Budget Movies of All Time</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://businesspundit.com" target="_blank" rel="nofollow">businesspundit.com</a> - <p>Made Popular: 4 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6/10-Most-Profitable-Low-Budget-Movies-of-All-Time" class="allowDefaultBehavior articleSummaryAnchor">In the movie world you don’t necessarily need big budgets to get big returns, as this list proves beyond all doubt. Big budget flicks might seem like a sure thing, but audiences decide with their feet.</a>
									<a href="/article/1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6/10-Most-Profitable-Low-Budget-Movies-of-All-Time" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6/10-Most-Profitable-Low-Budget-Movies-of-All-Time" class="allowDefaultBehavior">Comments (2)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/CQUQHZZHQR6B7MTMQIWFXWWWJI">garywineheart</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6">
						<a href="/article/1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6/10-Most-Profitable-Low-Budget-Movies-of-All-Time" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6"
					name="voteform-down-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6">106&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6"
										name="voteform-up-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:a7b3195a9ec1b82001e4130895cf8d7f:8b71503a0f1a074ca8776dd3be2bada6" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff" id="articleItem-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.womenhealthline.com/sexually-transmitted-diseases-lesbian-health-challenges/" rel="nofollow" trackurl="/track-click">Sexually Transmitted Diseases & Lesbian Health Challenges</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://womenhealthline.com" target="_blank" rel="nofollow">womenhealthline.com</a> - <p>Made Popular: 2 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff/Sexually-Transmitted-Diseases--Lesbian-Health-Challenges" class="allowDefaultBehavior articleSummaryAnchor">Lesbian females are at risk of contracting several of the analogous sexually transmitted diseases (STDs) as heterosexual females. Lesbians could be transmitting STDs between one another via skin-on-skin touch, mucosal contacts, fluids of the vagina&hellip;</a>
									<a href="/article/1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff/Sexually-Transmitted-Diseases--Lesbian-Health-Challenges" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff/Sexually-Transmitted-Diseases--Lesbian-Health-Challenges" class="allowDefaultBehavior">Comments (7)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/XVSVT3A5IE55FL2OUIEPF3V4IE">qucxjoz rppyt</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff">
						<a href="/article/1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff/Sexually-Transmitted-Diseases--Lesbian-Health-Challenges" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff"
					name="voteform-down-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff">140&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff"
										name="voteform-up-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:ec8b8288a5241db9b69af7da60fc8f74:2584219fc97aa61257ce2abb575a96ff" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3" id="articleItem-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.livemusicguide.com/blog/columns/great-combacks-in-music.html" rel="nofollow" trackurl="/track-click">Greatest Music Comebacks In History</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://livemusicguide.com" target="_blank" rel="nofollow">livemusicguide.com</a> - <p>Made Popular: 4 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3/Greatest-Music-Comebacks-In-History" class="allowDefaultBehavior articleSummaryAnchor">Don t call it a comeback, they ve been here for years. But even music s most influential artists have succumb to their share of career setbacks.</a>
									<a href="/article/1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3/Greatest-Music-Comebacks-In-History" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3/Greatest-Music-Comebacks-In-History" class="allowDefaultBehavior">Comments (1)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/AIGEEVJL6QITT7OJCINMHPFUO4">Dale Davis</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3">
						<a href="/article/1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3/Greatest-Music-Comebacks-In-History" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3"
					name="voteform-down-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3">135&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3"
										name="voteform-up-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:a4060f4d58cb5e7d291122e5ac780cdf:9b9450d568ebe9289c48e723556bfda3" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e" id="articleItem-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://topics.dirwell.com/tech/graphically-intensive-top-10-ipad-2-games-enjoy-a-console-like-experience.html" rel="nofollow" trackurl="/track-click">Graphically Intensive Top 10 iPad 2 Games: Enjoy A Console-Like Experience!</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://topics.dirwell.com" target="_blank" rel="nofollow">topics.dirwell.com</a> - <p>Made Popular: 2 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e/Graphically-Intensive-Top-10-iPad-2-Games-Enjoy-A-Console-Like-Experience" class="allowDefaultBehavior articleSummaryAnchor">Apple iPad 2 has bridged the gap between consoles and portable gaming, leading to iPad 2 games showcasing more graphical power and overall performance.</a>
									<a href="/article/1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e/Graphically-Intensive-Top-10-iPad-2-Games-Enjoy-A-Console-Like-Experience" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e/Graphically-Intensive-Top-10-iPad-2-Games-Enjoy-A-Console-Like-Experience" class="allowDefaultBehavior">Comments (2)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/S5GMUY3XNU42QJYRA5C6HI6YTY">Anees</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e">
						<a href="/article/1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e/Graphically-Intensive-Top-10-iPad-2-Games-Enjoy-A-Console-Like-Experience" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e"
					name="voteform-down-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e">134&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e"
										name="voteform-up-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:f39ddbe274b86485e46ee3da9f1033ca:010e368d25658c1b09f34dbea241ff7e" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87" id="articleItem-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.inspiringmothers.net/featured/teenage-pregnancy-facts.html" rel="nofollow" trackurl="/track-click">Teenage pregnancy facts</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://inspiringmothers.net" target="_blank" rel="nofollow">inspiringmothers.net</a> - <p>Made Popular: 2 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87/Teenage-pregnancy-facts" class="allowDefaultBehavior articleSummaryAnchor">Teenage pregnancy is a difficult, but a true fact. It is high time that we elders teach our teens to avoid stepping into such difficult time. Most of the teenage girls becoming pregnant without marriage, which is the first reason to stop it.</a>
									<a href="/article/1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87/Teenage-pregnancy-facts" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87/Teenage-pregnancy-facts" class="allowDefaultBehavior">Comments (4)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/FMWOED74DVSPYD7H2ULRWWXDXE">Don Bosco</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87">
						<a href="/article/1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87/Teenage-pregnancy-facts" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87"
					name="voteform-down-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87">121&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87"
										name="voteform-up-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:a2e118fa6b3f3e27bdcb8ca6047c8b55:ed96cf54a63d0d9cd18ed8f7d898fa87" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3" id="articleItem-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://shopping.yahoo.com/articles/yshoppingarticles/573/10-fashion-trends-with-staying-power/" rel="nofollow" trackurl="/track-click">10 Fashion Trends with Staying Power</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="/publisher/yahooshoppin_855/shoppingyahoocom">shopping.yahoo.com</a> - <p>Made Popular: 10 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3/10-Fashion-Trends-with-Staying-Power" class="allowDefaultBehavior articleSummaryAnchor">10 Fashion Trends with Staying Power</a>
									<a href="/article/1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3/10-Fashion-Trends-with-Staying-Power" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3/10-Fashion-Trends-with-Staying-Power" class="allowDefaultBehavior">Comments (2)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/BEGQVG7NU2B3UB737C56TAUMIM">azg</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3">
						<a href="/article/1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3/10-Fashion-Trends-with-Staying-Power" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3"
					name="voteform-down-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3">92&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3"
										name="voteform-up-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:yahooshoppin_855:5ae91de0e3f1868071315df7aec0cad3" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8" id="articleItem-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.autoslug.com/honda-brio-review.html" rel="nofollow" trackurl="/track-click">Honda BRIO – Review</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://autoslug.com" target="_blank" rel="nofollow">autoslug.com</a> - <p>Made Popular: 2 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8/Honda-BRIO--Review" class="allowDefaultBehavior articleSummaryAnchor">The Honda BRIO will certainly raise the competition to the next level because Honda is now looking forward to new eco-cars that have the best design, packaging and fuel efficiency in Thailand.</a>
									<a href="/article/1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8/Honda-BRIO--Review" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8/Honda-BRIO--Review" class="allowDefaultBehavior">Comments (4)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/BBYYQESAGHROZJ7PZFNTESSOBM">Adnan</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8">
						<a href="/article/1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8/Honda-BRIO--Review" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8"
					name="voteform-down-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8">133&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8"
										name="voteform-up-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:47caac821f8aba11cfeed585f5741f5d:00b1f81b309a9284637e3961e8cf88a8" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571" id="articleItem-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://blog.kosherdietplans.com/a-sunday-stroll-in-the-big-apple/" rel="nofollow" trackurl="/track-click">Taking a Sunday Stroll in the Big Apple</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://blog.kosherdietplans.com" target="_blank" rel="nofollow">blog.kosherdietplans.com</a> - <p>Made Popular: 2 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571/Taking-a-Sunday-Stroll-in-the-Big-Apple" class="allowDefaultBehavior articleSummaryAnchor">taking a look at a stroll through New York City</a>
									<a href="/article/1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571/Taking-a-Sunday-Stroll-in-the-Big-Apple" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571/Taking-a-Sunday-Stroll-in-the-Big-Apple" class="allowDefaultBehavior">Comments (4)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/D4BEKZVX44X5IKNMWGEFSSKO5E">Jasper As</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571">
						<a href="/article/1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571/Taking-a-Sunday-Stroll-in-the-Big-Apple" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571"
					name="voteform-down-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571">125&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571"
										name="voteform-up-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:adf5a7109d8fd26c318b851e92b24eff:ab8693b714004868d5477a279a5cd571" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34" id="articleItem-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.indiandrives.com/honda-city-has-got-a-strong-competition.html" rel="nofollow" trackurl="/track-click">Honda City has got a strong competition</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://indiandrives.com" target="_blank" rel="nofollow">indiandrives.com</a> - <p>Made Popular: 2 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34/Honda-City-has-got-a-strong-competition" class="allowDefaultBehavior articleSummaryAnchor">Finally it seems that the reign of the Honda City as the number 1 selling sedan in its category is over. Well literally so and we would say rather over. The strong contender to the number 1 throne happens to be Volkswagen’s new sedan, the Volkswagen&hellip;</a>
									<a href="/article/1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34/Honda-City-has-got-a-strong-competition" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34/Honda-City-has-got-a-strong-competition" class="allowDefaultBehavior">Comments (2)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/WQ3ZGJMPYFHODU7G4DJJEM7TQE">pratik j</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34">
						<a href="/article/1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34/Honda-City-has-got-a-strong-competition" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34"
					name="voteform-down-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34">109&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34"
										name="voteform-up-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:22eeb7ca09dec275dcf4dd06c6605ad8:f0ed25b952d3156f03fb278273946d34" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c" id="articleItem-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.biblehealth.com/latest-news/coronary-heart-diseases-share-ancient-links.html" rel="nofollow" trackurl="/track-click">Coronary Heart Diseases Share Ancient Links</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://biblehealth.com" target="_blank" rel="nofollow">biblehealth.com</a> - <p>Made Popular: 2 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c/Coronary-Heart-Diseases-Share-Ancient-Links" class="allowDefaultBehavior articleSummaryAnchor">If you are one of those who think that coronary heart diseases comes under the modern kind of diseases then better revert your judgment. According to researchers, it has been recently discovered that over 50 mummies from Egypt have shown ancient&hellip;</a>
									<a href="/article/1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c/Coronary-Heart-Diseases-Share-Ancient-Links" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c/Coronary-Heart-Diseases-Share-Ancient-Links" class="allowDefaultBehavior">Comments (1)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/WQ3ZGJMPYFHODU7G4DJJEM7TQE">pratik j</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c">
						<a href="/article/1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c/Coronary-Heart-Diseases-Share-Ancient-Links" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c"
					name="voteform-down-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c">104&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c"
										name="voteform-up-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:0f2d2fec9adbb3ca5c438951adc430db:3f6f20fd728217879865119e3647f33c" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107" id="articleItem-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.colorcoat-online.com/blog/index.php/2011/04/opulence-and-power-through-architecture-top-20-tallest-buildings-in-the-world/" rel="nofollow" trackurl="/track-click">Top 20 Tallest Buildings in the World</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="http://colorcoat-online.com" target="_blank" rel="nofollow">colorcoat-online.com</a> - <p>Made Popular: 4 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107/Top-20-Tallest-Buildings-in-the-World" class="allowDefaultBehavior articleSummaryAnchor">The world is filled with so many beautiful architectural wonders, that it is high time to encompass a list featuring the top 20 highest buildings in descending order.</a>
									<a href="/article/1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107/Top-20-Tallest-Buildings-in-the-World" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107/Top-20-Tallest-Buildings-in-the-World" class="allowDefaultBehavior">Comments (3)</a>

															<p>| First buzzed by: anonymous</p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107">
						<a href="/article/1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107/Top-20-Tallest-Buildings-in-the-World" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107"
					name="voteform-down-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107">122&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107"
										name="voteform-up-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:5d7a3953ca5e8b587a6e8eae2e69fdd5:b33e8cfca257f33e6b75eff802e9b107" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357" id="articleItem-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.limelife.com/blog-entry/Charlie-Sheen-I-Want-to-Go-Back-to-CBS/126839.html" rel="nofollow" trackurl="/track-click">Charlie Sheen: I Want to Go Back to CBS</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="/publisher/limelife358/LimeLife">LimeLife</a> - <p>Made Popular: 4 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357/Charlie-Sheen-I-Want-to-Go-Back-to-CBS" class="allowDefaultBehavior articleSummaryAnchor">Sheen s answer to the lukewarm tour response.
We reported Monday morning that Charlie Sheen had seen very mixed reviews after his weekend kicking off the Violent Torpedo of Truth Tour. Turns out Sheen knows there s not a lot he can do without a&hellip;</a>
									<a href="/article/1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357/Charlie-Sheen-I-Want-to-Go-Back-to-CBS" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357/Charlie-Sheen-I-Want-to-Go-Back-to-CBS" class="allowDefaultBehavior">Comments (7)</a>

															<p>| First buzzed by: anonymous</p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357">
						<a href="/article/1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357/Charlie-Sheen-I-Want-to-Go-Back-to-CBS" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357"
					name="voteform-down-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357">9&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357"
										name="voteform-up-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:ada7d7a126367ab6da824a509de426a7:3f0050ab62ed10dca06a3b7c633c3357" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d" id="articleItem-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://www.limelife.com/blog-entry/Robert-Pattinson-Furious-Over-Leaked-Breaking-Dawn-Pics/126855.html" rel="nofollow" trackurl="/track-click">Robert Pattinson Furious Over Leaked &#39;Breaking Dawn&#39; Pics</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="/publisher/limelife358/LimeLife">LimeLife</a> - <p>Made Popular: 4 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d/Robert-Pattinson-Furious-Over-Leaked-Breaking-Dawn-Pics" class="allowDefaultBehavior articleSummaryAnchor">I m being genuinely serious, Twilight fans. 
Robert Pattinson is calling upon the dark, unholy power of his Twilight horde to smite his enemies. The actor is incensed over the recent leak of Breaking Dawn photos featuring him and Kristen Stewart in&hellip;</a>
									<a href="/article/1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d/Robert-Pattinson-Furious-Over-Leaked-Breaking-Dawn-Pics" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d/Robert-Pattinson-Furious-Over-Leaked-Breaking-Dawn-Pics" class="allowDefaultBehavior">Comments (2)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/FVNG3H6DR6WSNPC2ATL6TDTEMA">Jeffrey R</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d">
						<a href="/article/1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d/Robert-Pattinson-Furious-Over-Leaked-Breaking-Dawn-Pics" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d"
					name="voteform-down-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d">4&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d"
										name="voteform-up-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:ada7d7a126367ab6da824a509de426a7:a97deebe569a7d83dcafde801a759d0d" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:usatoday:516d595010a8c881cc4d9cdc4307d8f8" id="articleItem-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://content.usatoday.com/communities/campusrivalry/post/2011/04/shaka-smart-nc-state-vcu-ncaa-tournament-final-four/1?csp=34" rel="nofollow" trackurl="/track-click">Shaka Smart agrees to new deal to remain at VCU</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="/publisher/usatoday/USATODAYcom">USATODAY.com</a> - <p>Made Popular: 12 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:usatoday:516d595010a8c881cc4d9cdc4307d8f8/Shaka-Smart-agrees-to-new-deal-to-remain-at-VCU" class="allowDefaultBehavior articleSummaryAnchor">Virginia Commonwealth coach Shaka Smart, who has burst onto the scene after leading the Rams to the Final Four in just his second season on the job, signed a new deal with VCU on Monday to ensure he would remain at the school.</a>
									<a href="/article/1:usatoday:516d595010a8c881cc4d9cdc4307d8f8/Shaka-Smart-agrees-to-new-deal-to-remain-at-VCU" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:usatoday:516d595010a8c881cc4d9cdc4307d8f8/Shaka-Smart-agrees-to-new-deal-to-remain-at-VCU" class="allowDefaultBehavior">Comments (1)</a>

															<p>| First buzzed by: <a class="allowDefaultBehavior" href="/activity/u/ALSGA3PIEL7XRK7E5PV2ZNV63E">Andrew Kuchel</a></p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8">
						<a href="/article/1:usatoday:516d595010a8c881cc4d9cdc4307d8f8/Shaka-Smart-agrees-to-new-deal-to-remain-at-VCU" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8"
					name="voteform-down-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8">31&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8"
										name="voteform-up-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:usatoday:516d595010a8c881cc4d9cdc4307d8f8" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
					<div articleid="1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8" id="articleItem-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8" class="articleItem">
				<dl class="articleItemDL">
					<dt class="articleHeadlineContainer">
												<a class="allowDefaultBehavior trackclick" target="_blank" href="http://news.yahoo.com/s/ap/20110405/ap_on_re_us/us_terrorism_trial" rel="nofollow" trackurl="/track-click">Holder: 9/11 suspects to face military tribunals      (AP)</a>
						<div class="articleVotedDownIcon">Buzzed Down</div>
					</dt>
					<dd class="articleAttributionContainer">
						<a class="allowDefaultBehavior org" href="/publisher/y_news/Yahoo-News">Yahoo! News</a> - <p>Made Popular: 10 hours ago</p>
					</dd>
					<dd class="articleSummaryContainer hasNoThumbnail">
															<a href="/article/1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8/Holder-911-suspects-to-face-military-tribunals-AP" class="allowDefaultBehavior articleSummaryAnchor">AP - Yielding to political opposition, the Obama administration gave up Monday on trying avowed 9/11 mastermind Khalid Sheikh Mohammed and four alleged henchmen in civilian federal court in New York and will prosecute them instead before military&hellip;</a>
									<a href="/article/1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8/Holder-911-suspects-to-face-military-tribunals-AP" class="allowDefaultBehavior">»&nbsp;See&nbsp;more</a>
													</dd>
					<dd class="articleBottomContainer">
						<a href="/article/1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8/Holder-911-suspects-to-face-military-tribunals-AP" class="allowDefaultBehavior">Comments (41)</a>

															<p>| First buzzed by: anonymous</p>
															<div class="buzzDownContainer hidden" id="voteDownCommentsContainer-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8">
						<a href="/article/1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8/Holder-911-suspects-to-face-military-tribunals-AP" class="buzzDownCommentsContainer allowDefaultBehavior">Leave a comment!</a>
					</div>
						
									<div class="buzzDownContainer hoverHidden" id="voteDownFormContainer-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8">
														<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					id="voteform-down-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8"
					name="voteform-down-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8"
					onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteform-down-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8','tl','bl');"
					class="itemVoteForm allowDefaultBehavior">


					<button type="submit" class="voteDownButton allowDefaultBehavior">Buzz&nbsp;Down</button>
				</form>
												</div>
													</dd>
				</dl>
				<div class="buzzUpContainer">
							<dl>
			<dt class="hoverHidden" id="voteCountContainer-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8">42&nbsp;Votes</dt>
								<dd id="votedUpContainer-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8" class="votedUpContainer hidden">
						Buzzed Up!					</dd>
					<dd id="votedDownContainer-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8" class="votedDownContainer hidden">
						Buzzed Down					</dd>
					<dd id="voteUpFormContainer-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8" class="voteUpFormContainer " >
															<form method="get" action="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
										id="voteform-up-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8"
										name="voteform-up-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8"
										onclick="return YAHOO.Orion.Nagbar.showSigninNagbar('voteUpButton-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8','tl','bl');"
										class="itemVoteForm allowDefaultBehavior">
										<button type="submit" id="voteUpButton-1:50cd1a9a183758039b0841aa738c3f0b:0ee0ea993dccdded76055b0326f8e7b8" class="voteUpButton allowDefaultBehavior"></button>
									</form>
								
					</dd>
						</dl>	
					</div>
			</div>
				</div>
		<div class="ft">
				<div class="pagination available">
		<p><strong>1 - 15</strong> of 17</p><ul>
		<li class="first"><strong>1</strong></li>
		<li><a href="/articles/all/12/2/topstories/popular" class="allowDefaultBehavior">2</a></li>
		<li class="last"><a href="/articles/all/12/2/topstories/popular" class="next allowDefaultBehavior">» Next</a></li></ul>	</div>
		</div>
	</div>
</div>


				</div>
			</div>
			<div class="yui-b" id="sidebar">
	<div class="content" id="sidebar_inner_container">
		<span class="cTL"><!-- --></span>
		<span class="cTR"><!-- --></span>
		<span class="cBL"><!-- --></span>
		<span class="cBR"><!-- --></span>
		<div class="hd"><!-- --></div>
		<div class="bd">
			    <div class="coreid mod clearfix">
        <span class="x1"><span class="x1a"></span></span>
        <span class="x2"><span class="x2a"></span></span>
        <div class="content">
            <div class="hd"><!-- --></div>
            <div class="bd">
                        <div class="signedout">
                    <h3>Welcome to Yahoo! Buzz</h3>
                    <ul>
						<li><a href="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F">Sign In</a></li>
						<li class="last">New User? <a href="https://edit.yahoo.com/registration?.intl=us&new=1&.src=ybz&.done=http%3A%2F%2Fbuzz.yahoo.com">Sign Up</a></li>
					</ul>
					<div class="signedOutWhatIsText">
						<strong>What is Yahoo Buzz?</strong>&nbsp;It's the place where you can talk about and shape the best stories people are reading right now.
					</div>
                </div>            </div>
            <div class="ft">
<!-- -->            </div>
        </div>
    </div>
    <div class="mod" id="submitstory">
        <span class="x1"><span class="x1a"></span></span>
        <span class="x2"><span class="x2a"></span></span>
        <div class="content">
            <div class="hd">
                <h3>Tell Us What's Buzzing</h3>
            </div>
            <div class="bd">
				<p>Influence the stories and images people see.  What is buzzing in your world?</p>
				<p><a href="/submit/">&raquo; Submit</a></p>
            </div>
            <div class="ft"><!-- --></div>
        </div>
    </div>
    <div class="mod clearfix last" id="vitality">
        <span class="x1"><span class="x1a"></span></span>
        <span class="x2"><span class="x2a"></span></span>
        <div class="content">
            <div class="hd">
                <h3>Buzz Updates</h3>
                            </div>
            <div class="bd signedout" id="yui-tabview">
                <ul class="yui-nav clearfix">
                    <li class="selected">
                        <a href="#everyone">Everyone</a>
                    </li>
                    <li class="first last">
                        <a href="#myconnections">My Connections</a>
                    </li>
                </ul>
                <div class="yui-content">
                    <dl class="everyone">
                                                    <div class="msg msg-small promo">
							    <span class="tl"><!-- --></span>
								<span class="tr"><!-- --></span>
							    <span class="bl"><!-- --></span>
								<span class="br"><!-- --></span>
                                <p><a href="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F">Sign In</a> to view Buzz updates from your connections!<span>New User? | <a href="https://edit.yahoo.com/registration?.intl=us&new=1&.src=ybz&.done=http%3A%2F%2Fbuzz.yahoo.com%2F">Sign Up</a></span></p>
                            </div>
                            <div class="updates">
<div class="item">
    <dt><a href="/activity/u/6665EUZ2A7EIGMAZHSYEYME2DE"><img src="http://l.yimg.com/a/i/identity2/profile_48e.png" alt="" class="avatar" width="32" height="32"></a></dt>
    <dd>
		<a href="/activity/u/6665EUZ2A7EIGMAZHSYEYME2DE" class="nickname">Cynthia</a> left a comment - <span class="time">21 min ago</span><div class="headline"><a href="http://us.rd.yahoo.com//Updates/buzz/comment/SIG=1cg27n1gu/*http%3A//us.lrd.yahoo.com/_ylc=X3oDMTVwYTZ0ZTZnBElfYWd1aWQDNjY2NUVVWjJBN0VJR01BWkhTWUVZTUUyREUESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5idXp6LnljYS52aXRhbGl0eS5jbGllbnQucHJvZARJX3VjbnR4A2dsb2JhbARJX3VzcmMDYnV6egRJX3VzdWlkAzFlMjBkODZjMTcyOWNjMzhjNzVmMjIxMzE4MzE5ZGNhBElfdXR5cGUDY29tbWVudARfUwMyMDIzNDM1MjYx/SIG=12p77d9tu/**http%3A//buzz.yahoo.com/article/1%3Amilwaukee_jou37%3Abab4e23ed0d2f157cb0fde272a692694" class="headline">No degree, little experience pays off big</a><em> - <a class="org" href="/publisher/milwaukee_jou37">Milwaukee Journal Sentinel</a></em></div>		<p class="comment">&#8220;Jay Davis, you&#39;re on!

A DWI conviction and a room-temperature IQ? Sounds like TeaBagger- and &hellip;&#8221;</p>	</dd>
</div>
<div class="item">
    <dt><a href="/activity/u/DJTHWIC7EUJFCXKYNP2STT5AUU"><img src="http://l.yimg.com/a/i/identity2/profile_48e.png" alt="" class="avatar" width="32" height="32"></a></dt>
    <dd>
		<a href="/activity/u/DJTHWIC7EUJFCXKYNP2STT5AUU" class="nickname">Margaret Ota</a> left a comment - <span class="time">43 min ago</span><div class="headline"><a href="http://us.rd.yahoo.com//Updates/buzz/comment/SIG=1d1ccch8h/*http%3A//us.lrd.yahoo.com/_ylc=X3oDMTVwNTBwZWIzBElfYWd1aWQDREpUSFdJQzdFVUpGQ1hLWU5QMlNUVDVBVVUESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5idXp6LnljYS52aXRhbGl0eS5jbGllbnQucHJvZARJX3VjbnR4A2dsb2JhbARJX3VzcmMDYnV6egRJX3VzdWlkAzkzOTg1YzY1ODM1NDA0ZDM0YzE1MGRmOGIzNGIwODA0BElfdXR5cGUDY29tbWVudARfUwMyMDIzNDM1MjYx/SIG=13a3ntbdp/**http%3A//buzz.yahoo.com/article/1%3Aada7d7a126367ab6da824a509de426a7%3A3f0050ab62ed10dca06a3b7c633c3357" class="headline">Charlie Sheen: I Want to Go Back to CBS</a><em> - <a class="org" href="/publisher/limelife358">LimeLife</a></em></div>		<p class="comment">&#8220;Charlie Sheen is a mentally ill person, who should go someplace and get well, and then, &hellip;&#8221;</p>	</dd>
</div>
<div class="item">
    <dt><a href="/activity/u/DGVYDUFZWFKGXJOT5ZVGAAKMAI"><img src="http://avatars.zenfs.com/users/1B1Ekt_hHAAEDqOJ0HJBy8slSmfoG.medium.png" alt="" class="avatar" width="32" height="32"></a></dt>
    <dd>
		<a href="/activity/u/DGVYDUFZWFKGXJOT5ZVGAAKMAI" class="nickname">X</a> left a comment - <span class="time">1 hour ago</span><div class="headline"><a href="http://us.rd.yahoo.com//Updates/buzz/comment/SIG=1d1iiv6oq/*http%3A//us.lrd.yahoo.com/_ylc=X3oDMTVwbXFyZGtpBElfYWd1aWQDREdWWURVRlpXRktHWEpPVDVaVkdBQUtNQUkESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5idXp6LnljYS52aXRhbGl0eS5jbGllbnQucHJvZARJX3VjbnR4A2dsb2JhbARJX3VzcmMDYnV6egRJX3VzdWlkA2U0NzY0ZGY3NTQzMjNmZmNmM2EyYmVkZjZkYWUxMjljBElfdXR5cGUDY29tbWVudARfUwMyMDIzNDM1MjYx/SIG=13abctu2t/**http%3A//buzz.yahoo.com/article/1%3A50cd1a9a183758039b0841aa738c3f0b%3A0ee0ea993dccdded76055b0326f8e7b8" class="headline">Holder: 9/11 suspects to face military tribunals      (AP)</a><em> - <a class="org" href="/publisher/y_news">Yahoo! News</a></em></div>		<p class="comment">&#8220;THE WORST FAILED PRESIDENT and DEMON-cRAT Administration in the History of this Great Nation . . . &hellip;&#8221;</p>	</dd>
</div>
<div class="item">
    <dt><a href="/activity/u/6NK626ZUEDMJQWH72ZG4XQM7SQ"><img src="http://l.yimg.com/a/i/identity2/profile_48d.png" alt="" class="avatar" width="32" height="32"></a></dt>
    <dd>
		<a href="/activity/u/6NK626ZUEDMJQWH72ZG4XQM7SQ" class="nickname">Jerry</a> left a comment - <span class="time">1 hour ago</span><div class="headline"><a href="http://us.rd.yahoo.com//Updates/buzz/comment/SIG=1d11u5ckp/*http%3A//us.lrd.yahoo.com/_ylc=X3oDMTVwcjUxbWRiBElfYWd1aWQDNk5LNjI2WlVFRE1KUVdINzJaRzRYUU03U1EESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5idXp6LnljYS52aXRhbGl0eS5jbGllbnQucHJvZARJX3VjbnR4A2dsb2JhbARJX3VzcmMDYnV6egRJX3VzdWlkAzI5OTU1ZTdmYjRkMDE1ZDljOWU2NGNiZmU3YzU0NTU3BElfdXR5cGUDY29tbWVudARfUwMyMDIzNDM1MjYx/SIG=13abctu2t/**http%3A//buzz.yahoo.com/article/1%3A50cd1a9a183758039b0841aa738c3f0b%3A0ee0ea993dccdded76055b0326f8e7b8" class="headline">Holder: 9/11 suspects to face military tribunals      (AP)</a><em> - <a class="org" href="/publisher/y_news">Yahoo! News</a></em></div>		<p class="comment">&#8220;These racists should be impeached for wasting our money on bs like this.&#8221;</p>	</dd>
</div>
<div class="item">
    <dt><a href="/activity/u/6FI6YCRLHQVDUGQLKKBW45UOQU"><img src="http://a323.yahoofs.com/coreid/4d02bc31i1cfbzws109ac4/ScDdEiYieKclXh.RTAeTKFZ44ug-/1/tn48.jpeg?ciAw8eOBBQRFvjvi" alt="" class="avatar" width="32" height="32"></a></dt>
    <dd>
		<a href="/activity/u/6FI6YCRLHQVDUGQLKKBW45UOQU" class="nickname">OSatan</a> left a comment - <span class="time">2 hours ago</span><div class="headline"><a href="http://us.rd.yahoo.com//Updates/buzz/comment/SIG=1c92qbp8k/*http%3A//us.lrd.yahoo.com/_ylc=X3oDMTVwdDdrb3JzBElfYWd1aWQDNkZJNllDUkxIUVZEVUdRTEtLQlc0NVVPUVUESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5idXp6LnljYS52aXRhbGl0eS5jbGllbnQucHJvZARJX3VjbnR4A2dsb2JhbARJX3VzcmMDYnV6egRJX3VzdWlkAzkzNGJmYTNiMWNmZTE5ZWU2MTA5MzA4ZDUwYzQ5OTkwBElfdXR5cGUDY29tbWVudARfUwMyMDIzNDM1MjYx/SIG=12ir3ftkh/**http%3A//buzz.yahoo.com/article/1%3Ausatoday%3Aa830b48cd4d49034dea77527ab55bddc" class="headline">GOP takes aim at Obama s re-election bid</a><em> - <a class="org" href="/publisher/usatoday">USATODAY.com</a></em></div>		<p class="comment">&#8220;Hey Thug.. bring it on !!!  You wouldn&#39;t be the first negro to find the bottom of &quot;the well&quot;.&#8221;</p>	</dd>
</div>
</div>
						<div class="ft"></div>
                    </dl>
                	<dl class="myconnections yui-hidden">
                                                <div class="msg msg-large promo">
						    <span class="tl"><!-- --></span>
							<span class="tr"><!-- --></span>
						    <span class="bl"><!-- --></span>
							<span class="br"><!-- --></span>
                            <div class="hd"></div>
                            <div class="bd">
                                <span class="icon"></span>
                                <p><a href="http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F">Sign In</a> to see what your connections are buzzing!</p>
                                <p>New User? | <a href="https://edit.yahoo.com/registration?.intl=us&new=1&.src=ybz&.done=http%3A%2F%2Fbuzz.yahoo.com%2F">Sign Up</a></p>
                            </div>
                            <div class="ft"></div>
                        </div>
                                            </dl>                    
				</div>
            </div>
        	<div class="ft">
			    <ul class="clearfix">
                    <li class="first"><a href="http://profiles.yahoo.com/settings/updates/">Manage updates</a></li>
					<li class="last"><a href="/updates">&raquo; View all Buzz Updates</a></li>
                </ul>
			</div>
		</div>
    </div>
		</div>
		<div class="ft"><!-- --></div>
	</div>
</div>
		</div>
		<div id="ft">
						<div id="bottom_corner_container">
				<span class="cBL"><!-- --></span>
				<span class="cBR"><!-- --></span>
			</div>
			<div id="buzz_footer">
				<span class="cTL"><!-- --></span>
				<span class="cTR"><!-- --></span>
				<span class="cBL"><!-- --></span>
				<span class="cBR"><!-- --></span>
				<div class="content">
					<div class="hd"></div>
					<div class="bd">
															<ul class="first">
									<li class="first">Top Searches:&nbsp;</li><li><a href="/search?p=Elizabeth%20Taylor&amp;filter=168&amp;srch_in=topstories">Elizabeth Taylor</a></li> <li> | </li> <li><a href="/search?p=Pawn%20Stars&amp;filter=168&amp;srch_in=topstories">Pawn Stars</a></li> <li> | </li> <li><a href="/search?p=Libya&amp;filter=168&amp;srch_in=topstories">Libya</a></li> <li> | </li> <li><a href="/search?p=Michael%20Jackson&amp;filter=168&amp;srch_in=topstories">Michael Jackson</a></li> <li> | </li> <li><a href="/search?p=China&amp;filter=168&amp;srch_in=topstories">China</a></li> <li> | </li> <li><a href="/search?p=Michael%20Wilding&amp;filter=168&amp;srch_in=topstories">Michael Wilding</a></li> <li> | </li> <li><a href="/search?p=Obama&amp;filter=168&amp;srch_in=topstories">Obama</a></li> <li> | </li> <li><a href="/search?p=Dez%20Bryant&amp;filter=168&amp;srch_in=topstories">Dez Bryant</a></li> <li> | </li> <li><a href="/search?p=Larry%20Fortensky&amp;filter=168&amp;srch_in=topstories">Larry Fortensky</a></li>									</ul>
																<ul class="last">
									<li class="first">Buzz Categories:&nbsp;</li><li><a href="/articles/business/12/1/topstories/popular">Business</a></li> <li> | </li> <li><a href="/articles/entertainment/12/1/topstories/popular">Entertainment</a></li> <li> | </li> <li><a href="/articles/health/12/1/topstories/popular">Health</a></li> <li> | </li> <li><a href="/articles/images/12/1/topstories/popular">Images</a></li> <li> | </li> <li><a href="/articles/lifestyle/12/1/topstories/popular">Lifestyle</a></li> <li> | </li> <li><a href="/articles/politics/12/1/topstories/popular">Politics</a></li> <li> | </li> <li><a href="/articles/science/12/1/topstories/popular">Sci/Tech</a></li> <li> | </li> <li><a href="/articles/sports/12/1/topstories/popular">Sports</a></li> <li> | </li> <li><a href="/articles/news/12/1/topstories/popular">U.S. News</a></li> <li> | </li> <li><a href="/articles/video/12/1/topstories/popular">Video</a></li> <li> | </li> <li><a href="/articles/world/12/1/topstories/popular">World</a></li> <li> | </li> 									</ul>
															<ul id="intlbuzzcountries">						
							<li class="first">Yahoo! Buzz Worldwide:</li>
							<li><a href="http://au.buzz.yahoo.com/">Australia</a></li> <li> | </li> <li><a href="http://br.buzz.yahoo.com/">Brazil</a></li> <li> | </li> <li><a href="http://fr.buzz.yahoo.com/">France</a></li> <li> | </li> <li><a href="http://in.buzz.yahoo.com/">India</a></li> <li> | </li> <li><a href="http://mx.buzz.yahoo.com/">Mexico</a></li> <li> | </li> <li><a href="http://uk.buzz.yahoo.com/">UK & Ireland</a></li> <li> | </li> <li><a href="http://buzz.yahoo.com/">United States</a></li><li class="rss"><a href="http://d.yimg.com/ds/rss/V1/top10/all"><span class="icon"></span>RSS Feed</a></li>							</ul>	
					</div>
					<div class="ft"></div>
				</div>
			</div>
						<ul class="primary">
		<li class="first"><a href="/about/" rel="nofollow">About Buzz</a></li>
				<li><a href="/submit/" rel="nofollow">Submit A Story</a></li>
				<li><a href="/publishers/" rel="nofollow">Publish Your Content</a></li>
		<li><a href="/publisher/directory" rel="nofollow">Publisher Directory</a></li>
				<li><a href="http://fe.shortcuts.search.yahoo.com/widget/buzz.php" rel="nofollow">Widgets</a></li>
		<li><a href="/i/" rel="nofollow">iPhone</a></li>
				
		<li><a href="http://feedback.help.yahoo.com/feedback.php?.src=BUZZ&.done=http%3A%2F%2Fbuzz.yahoo.com%2F" rel="nofollow">Feedback</a></li>
				<li class="last"><a href="http://help.yahoo.com/l/us/yahoo/buzz/" rel="nofollow">Buzz Help</a></li>
	</ul>
	
		</div>
	</div>
</div>


<script type="text/javascript" src="http://l.yimg.com/ds/combo?yui/2.7.0/build/yahoo-dom-event/yahoo-dom-event.js&yui/2.7.0/build/selector/selector-min.js&yui/2.7.0/build/connection/connection-min.js&yui/2.7.0/build/json/json-min.js&yui/2.7.0/build/animation/animation-min.js&yui/2.7.0/build/datasource/datasource-min.js&yui/2.7.0/build/autocomplete/autocomplete-min.js&yui/2.7.0/build/cookie/cookie-min.js&yui/2.7.0/build/imageloader/imageloader-min.js&yui/2.7.0/build/element/element-min.js&yui/2.7.0/build/tabview/tabview-min.js&yui/2.7.0/build/container/container-min.js&orion/1.0.15.59/js/main.js&orion/1.0.15.59/js/eventdelegation.js&orion/1.0.15.59/js/layer.js&orion/1.0.15.59/js/messagebar.js&orion/1.0.15.59/js/view-by.js&orion/1.0.15.59/js/categorylist-more-menu.js&orion/1.0.15.59/js/nagbar.js&orion/1.0.15.59/js/whatsthis.js&orion/1.0.15.59/js/ylc_1.9.js&orion/1.0.15.59/js/ult.js&orion/1.0.15.59/js/articlelist.js&orion/1.0.15.59/js/beacon.js&orion/1.0.15.59/js/coreid.js&orion/1.0.15.59/js/vitality.js&orion/1.0.15.59/js/localbuzz.js&orion/1.0.15.59/js/nagbarPopup.js&orion/1.0.15.59/js/more-topics.js"></script>
<script type="text/javascript">
YAHOO.namespace("Orion").SpaceID = "1171750003";

YAHOO.namespace("Orion.Refresh").setAutoRefresh = function() {
	window.location.reload();
};

(function() {
	var refreshTimer = setTimeout(YAHOO.Orion.Refresh.setAutoRefresh, 900000);
})();

if(typeof YAHOO !== "undefined" && typeof YAHOO.Buzz !== "undefined" && typeof YAHOO.Buzz.Main !== "undefined" && typeof YAHOO.Buzz.Main.setSidebarHeight !== "undefined") {	
	if(typeof YAHOO.util !== "undefined" && typeof YAHOO.util.Event !== "undefined") {
		YAHOO.util.Event.onDOMReady(YAHOO.Buzz.Main.setSidebarHeight);	
	} else {
		window.setTimeout(YAHOO.Buzz.Main.setSidebarHeight, 250);
	}
}

YAHOO.namespace("Orion.Templates")["welcome"]="<div id=\"modal\"> <div class=\"overlay-decorator\" id=\"welcome-overlay-decorator\"><\/div> <div class=\"overlay-wrap\" id=\"welcome-overlay-wrap\"> <div class=\"overlay\"> <div class=\"dialog-decorator\"><\/div> <div class=\"dialog-wrap\"> <div class=\"dialog share\"> <span class=\"x1\"><span class=\"x1a\"><\/span><\/span> <span class=\"x2\"><span class=\"x2a\"><\/span><\/span> <div class=\"content\"> <div class=\"hd\"> <h2><span>Welcome to Yahoo! Buzz<span class=\"sup\">&#174;<\/span>!<\/span><\/h2> <\/div> <div class=\"bd\"> <div class=\"welcome\"> <div class=\"header\"> <p>Your vote just helped influence what millions will see on Yahoo! Buzz. Thanks for your contribution and keep on buzzing!<\/p> <p><em>Check out your Buzz settings:<\/em><\/p> <\/div> <div class=\"messaging\"> <ul> <li> <div>@@sharing@@<\/div> <div><a href=\"http:\/\/profiles.yahoo.com\/settings\/updates?.done=http%3A%2F%2Fbuzz.yahoo.com%2F%3Fcid%3D1\">Change This<\/a><\/div> <\/li> <li> <div>You currently appear as <em>&quot;&quot;<\/em> on Yahoo! Buzz.<\/div> <div><a href=\"http:\/\/profiles.yahoo.com\/edit\/basicinfo?.done=http%3A%2F%2Fbuzz.yahoo.com%2F%3Fcid%3D1\">Edit Display Name<\/a>&nbsp;|&nbsp;<a href=\"http:\/\/help.yahoo.com\/l\/us\/yahoo\/buzz\/additional\/additional6.html\">Learn More<\/a><\/div> <\/li> <\/ul> <\/div> <div class=\"actions inputfield\"> <input id=\"closeWelcome\" type=\"submit\" class=\"button\" value=\"Close\"> <\/div> <\/div> <\/div> <div class=\"ft\"><\/div> <\/div> <\/div> <\/div> <\/div> <\/div> <\/div>";

YAHOO.namespace("Orion.Welcome").shared_activity = "You <em>are<\/em> sharing your Buzz activity with users on Buzz and other Yahoo! services. This refers to past and future Buzz activity, including voting on the iPhone version of Buzz.";
YAHOO.namespace("Orion.Welcome").not_shared_activity = "You <em>are not<\/em> sharing your Buzz activity with users on Buzz and other Yahoo! services. This refers to past and future Buzz activity, including voting on the iPhone version of Buzz.";


YAHOO.util.Event.addListener('top_nav_search_button', 'click', function(e) {
	YAHOO.util.Event.stopEvent(e);
	var val =  YAHOO.lang.trim(YAHOO.util.Dom.get("search-terms").value);
        var defaultSrch = "Search Top Buzz" ;
        if (val == "" || val == defaultSrch) return;
	var target = YAHOO.util.Event.getTarget(e);
	var form = YAHOO.util.Dom.getAncestorByTagName(target, 'form');
	form.submit();
});

YAHOO.namespace("Orion.Templates")["messagebar_activity"]="<div> <span class=\"x1\"><span class=\"x1a\"><\/span><\/span> <span class=\"x2\"><span class=\"x2a\"><\/span><\/span> <span class=\"x3\"><span class=\"x3a\"><\/span><\/span> <div class=\"content\"> <div class=\"hd\"><\/div> <div class=\"bd\">@@message@@<\/div> <div class=\"ft\"><span>Close<\/span><span class=\"icon\"><\/span><\/div> <\/div> <\/div>";

YAHOO.namespace("Orion.Messaging").activity_success_message = "Thanks! You will see your changes on Buzz shortly.";
YAHOO.namespace("Orion.Messaging").activity_error_message = "Sorry, we're having some technical difficulties.  Please try again later.";
YAHOO.namespace("Orion.Messaging").activity_warning_message = "Please visit <a href=\"http:\/\/profiles.yahoo.com\/settings\/updates?.done=http%3A%2F%2Fbuzz.yahoo.com%2F%3Fcid%3D1\">Yahoo! Updates<\/a> to make your Buzz activity public.";
YAHOO.util.Event.delegateBySelector('#no_activity .ft span', 'click', YAHOO.Orion.Messaging.close, { node : "no_activity", template : "messagebar", hide : false });
YAHOO.util.Event.delegateBySelector('#activity_messaging .ft span', 'click', YAHOO.Orion.Messaging.close, {node : "activity_messaging", template : "messagebar_activity", hide : true });


YAHOO.namespace("Orion.ULT").Section = "articlelist";
YAHOO.util.Event.addListener(YAHOO.util.Dom.getElementsByClassName('articleItem'), 'mouseover', function() {
	YAHOO.util.Dom.addClass(this, 'articleItem_hover');
});
YAHOO.util.Event.addListener(YAHOO.util.Dom.getElementsByClassName('articleItem'), 'mouseout', function() {
	YAHOO.util.Dom.removeClass(this, 'articleItem_hover');
});

YAHOO.namespace("Orion.Templates")["nagbar"]="<div class=\"messagebar nagbar mod\"> <span class=\"x1\"><span class=\"x1a\"><\/span><\/span> <span class=\"x2\"><span class=\"x2a\"><\/span><\/span> <span class=\"x3\"><span class=\"x3a\"><\/span><\/span> <div class=\"content\"> <div class=\"hd\"> <h2>Please sign in to vote!<\/h2> <span>Signing in ensures votes are counted accurately and prevents system abuse. <\/span> <\/div> <div class=\"bd\"> <a class=\"confirm\" href=\"http:\/\/login.yahoo.com\/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2Fcollectpendingvotes%2F%40%40pending_vote_key%40%40%2Fhttp%25253A%25252F%25252Fbuzz.yahoo.com%25252F\">Sign In<\/a><span class=\"new\">New User?<\/span> <a class=\"signup\" href=\"https:\/\/edit.yahoo.com\/registration?.intl=us&new=1&.src=ybz&.done=http%3A%2F%2Fbuzz.yahoo.com%2Fcollectpendingvotes%2F@@pending_vote_key@@%2Fhttp%25253A%25252F%25252Fbuzz.yahoo.com%25252F\">Sign Up<\/a> <\/div> <div class=\"ft\"> <a class=\"close\" href=\"\/discardpendingvotes\/@@pending_vote_key@@\/http%253A%252F%252Fbuzz.yahoo.com%252F\">Close<\/a> <\/div> <\/div> <\/div>";

		YAHOO.Orion.Nagbar.setSigninNagbarHTML('\
								<div class="signinNagbarLeft">\
									<i><!-- --></i><strong>{title}</strong>\
									{body}\
								</div>\
								<div class="signinNagbarRight">\
									<a href="{signInURL}" class="signin">Sign In</a>\
									New User?\
									<a href="{signUpURL}">Sign Up</a>\
								</div>\
							');
	

	YAHOO.Orion.Nagbar.setSigninNagbarStrings(
					"Please sign in to vote!",
					"Signing in ensures votes are counted accurately and prevents system abuse.",
					"http://login.yahoo.com/?.pd=c%3DhYw09vWp2e4FXlpTB9bd0rU-&.src=ybz&.intl=us&.done=http%3A%2F%2Fbuzz.yahoo.com%2F",
					"https://edit.yahoo.com/registration?.intl=us&new=1&.src=ybz&.done=http%3A%2F%2Fbuzz.yahoo.com%2F"
					);
	
YAHOO.namespace("Orion.Templates")["whatsthis_featured_on_y"]="<div class=\"mod bubble\"> <span class=\"x1\"><span class=\"x1a\"><\/span><\/span> <span class=\"x2\"><span class=\"x2a\"><\/span><\/span> <span class=\"x3\"><span class=\"x3a\"><\/span><\/span> <div class=\"content\"> <div class=\"hd\"><\/div> <div class=\"bd\">This story got so much buzz, it reached the Yahoo! homepage!<\/div> <div class=\"ft\"><\/div> <\/div> <\/div>";
YAHOO.namespace("Orion.Templates")["vote_state_voted_up"]="<div class=\"vote\"><div> <h4>@@vote_count@@<\/h4> <em>Buzzed Up!<\/em> <p class=\"action\"><a href=\"@@ult-share@@\" class=\"email\" rel=\"email\">Send<\/a><\/p><\/div><\/div>";
YAHOO.namespace("Orion.Templates")["vote_state_voted_down"]="<div class=\"vote\"><div> <h4>@@vote_count@@<\/h4> <em>Buzzed Down<\/em> <\/div><\/div>";
YAHOO.namespace("Orion.Templates")["vote_state_not_voted"]="<div class=\"vote\"><div> <h4>@@vote_count@@<\/h4> <form method=\"post\" action=\"@@ult-vote-action@@\" name=\"voteform-up-@@URN@@\" class=\"voteform\"> <input type=\"hidden\" name=\".done\" value=\"http:\/\/buzz.yahoo.com\/article\/@@URN@@\/HEADLINE\"> <input type=\"hidden\" name=\"from\" value=\"buzz\"> <input type=\"hidden\" name=\"assettype\" value=\"article\"> <input type=\"hidden\" name=\"asseturn\" value=\"@@URN@@\"> <input type=\"hidden\" name=\".crumb\" value=\"\"> <input type=\"hidden\" name=\"spaceid\" value=\"1171750003\"><input type=\"hidden\" name=\"getrelated\" value=\"3\"><input type=\"hidden\" name=\"votetype\" value=\"1\"><button type=\"submit\" class=\"vote-up\">Buzz Up<\/button><\/form> <form method=\"post\" action=\"@@ult-vote-action@@\" name=\"voteform-up-@@URN@@\" class=\"voteform\"> <input type=\"hidden\" name=\".done\" value=\"http:\/\/buzz.yahoo.com\/article\/@@URN@@\/HEADLINE\"> <input type=\"hidden\" name=\"from\" value=\"buzz\"> <input type=\"hidden\" name=\"assettype\" value=\"article\"> <input type=\"hidden\" name=\"asseturn\" value=\"@@URN@@\"> <input type=\"hidden\" name=\".crumb\" value=\"\"> <input type=\"hidden\" name=\"spaceid\" value=\"1171750003\"><input type=\"hidden\" name=\"getrelated\" value=\"3\"><input type=\"hidden\" name=\"votetype\" value=\"0\"><button type=\"submit\" class=\"vote-down\">Buzz&nbsp;Down<\/button><\/form><\/div><\/div>";
</script>
<script type="text/javascript" src="http://l.yimg.com/kx/ds/js/disclosure-1.0.js"></script><!-- SpaceID=0 robot -->
</body>
</html>
<!-- fe5.buzz.re1.yahoo.com uncompressed/chunked Tue Apr  5 08:12:13 PDT 2011 -->
