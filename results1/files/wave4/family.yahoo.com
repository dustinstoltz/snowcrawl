<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>Yahoo! Safely</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>    
    <meta name="description" content="Welcome to Yahoo! Safely. Learn how to make smart and safer choices online and get advice on using Yahoo! products safely" />
    <meta name="keywords" content="Yahoo! Safely, using Yahoo! products safely, Yahoo! product guide safey, tips on safety online, online safety tips for parents, safety tips for teens" />        
    
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://safely.yahoo.com/"/>
    <meta name="y_key" content="80db14aed3667d7f" />

	<link type="text/css" rel="stylesheet" href="http://l.yimg.com/ep/cx/p/safely/style-min-7188.css"/>
    <link type="text/css" rel="stylesheet" href="http://l.yimg.com/ep/cx/p/safely/print-min-4824.css" media="print"/>
    <script type="text/javascript" src="http://l.yimg.com/ep/cx/p/safely/safely-min-5569.js"></script>                
  </head>
  <body>        
      <div id="wrapper">                      
          <!-- Include Top Nav PHP file -->
          <link type='text/css' rel='stylesheet' href='http://l.yimg.com/zz/combo?kx/ucs/uh/css/168/yunivhead-min.css' /><style>body {min-height:1000px;}
div#wrapper div#yUnivHead {*overflow:visible;}
#yUnivHead.yucs-it-it ul#yuhead-useroptions {*width:15em;}
#yUnivHead.yucs-it-it #yuhead-hd ul li.yucs-menu li.last-child {*width:15em;}</style><div id="yUnivHead" class="yucs-en-us">    <a href="#yuhead-search" class="yucs-skipto-search yucs-activate">Skip to search.</a>        <div id="yuhead-hd" class="yuhead-clearfix">        <div id="yuhead-mepanel-cont">            <ul id="yuhead-mepanel"><li class="yuhead-me yuhead-nodivide yuhead-nopad">   <a class="yuhead-signup" href="https://edit.yahoo.com/config/eval_register?.src=safely&.intl=us&.done=http://safely.yahoo.com/">    <em>New User?</em> Register</a></li><li class="yuhead-me"><a href="https://login.yahoo.com/config/login?.src=safely&.intl=us&.done=http://safely.yahoo.com/"><em>Sign In</em></a></li><li class="yuhead-me">	<a href="http://help.yahoo.com/l/us/yahoo/helpcentral/">		Help	</a></li></ul>        </div>        <div id="yuhead-promo"><style>/* panels */.yucs-sethp .yucs-sethp-panel {font-size:x-small;border:1px solid #4333BC;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:5px;background:#fff;width:360px;padding:10px 10px;position:absolute;z-index:100;margin-top:6px;}.yucs-sethp .pnt{background-position:-29px 0;background-repeat:no-repeat;height:6px;width:11px;position:absolute;left:50%;z-index:100;}.yucs-sethp .yucs-sethp-panel .yucs-sethp-panel-logo {background-position:0 -16px;background-repeat:no-repeat;display:block;font-size:0;height:37px;line-height:0;width:40px;text-indent:-1000px;overflow:hidden;float:left;}.yucs-sethp .myyhpbg {background-image:url(http://l.yimg.com/a/i/us/uh/bt2/myyhp_sprite_v1.gif);}.yucs-sethp .yucs-sethp-panel ol {width:300px;display:block;float:left;margin:0 0 10px;list-style:decimal inside;padding:0 0 0 10px;text-align:left;}.yucs-sethp .yucs-sethp-panel .hr {clear:both;text-align:center;}.yucs-sethp .yucs-sethp-panel .hr a {color:#000;}.yucs-sethp .hide {display:none;}</style><div class="yucs-sethp yucs-activate" data-prop="safely" data-detectscript="http://www.yahoo.com/includes/hdhpdetect.php"><a href="http://us.lrd.yahoo.com/_ylc=X3oDMTFnNzFiMTJoBHRtX2RtZWNoA1RleHQgTGluawR0bV9sbmsDVTExMzA1NTYEdG1fbmV0A1lhaG9vIQ--/SIG=112cgufir/**http%3A//www.yahoo.com/%3Fmkt=3" target="_top" rel="nofollow">Make Y! My Homepage<abbr title="Yahoo!"></abbr></a><div class="myyhpbg pnt hide">&nbsp;</div><div class="yucs-sethp-panel shdw hide"><div class="bd"><a title="Yahoo!" href="http://www.yahoo.com" class="yucs-sethp-panel-logo myyhpbg">Yahoo!</a><ol class="yuheadhpinstr"><li>Drag the "Y!" and drop it onto the "Home" icon.</li><li>Select "Yes" from the pop up window.</li><li>Nothing, you're done.</li></ol><div class="hr"><p>If this didn't work for you see <a href="http://us.lrd.yahoo.com/_ylc=X3oDMTFnNzFiMTJoBHRtX2RtZWNoA1RleHQgTGluawR0bV9sbmsDVTExMzA1NTYEdG1fbmV0A1lhaG9vIQ--/SIG=112cgufir/**http%3A//www.yahoo.com/%3Fmkt=3"class="yuheadshps yuheadshpdetails"target="_top">detailed instructions</a></p><p><a href="javascript:void(0);" class="yucs-sethp-panel-close">Close this window</a></p></div></div></div></div></div>        <div id="yuhead-com-links-cont">            <ul id="yuhead-com-links">                                <li class="yuhead-com-link-item">    <a class="sp yuhead-ico-mail" href="http://mail.yahoo.com?.intl=us" rel="nofollow" target="_top">Mail</a></li><li class="yuhead-com-link-item">    <a href="http://my.yahoo.com"     rel="nofollow"     target="_top">   My Y!    </a></li><li id="yuhead-com-home"><a class="sp yuhead-ico-home" href="http://www.yahoo.com" rel="nofollow" target="_top">Yahoo!</a></li>            </ul>        </div>    </div>    <div id="yuhead-bd" class="yuhead-clearfix">        <!-- Add a classname of "small" to the h2 for the fixed variant in the JS layer forthe fixed header. Theming overrides are:#yUnivHead .yuhead-logo h2 {background-position:0px -120px;}#yUnivHead .yuhead-logo h2.small {background-position:0px -360px;}--><div class="yuhead-logo">   <style>      .yuhead-logo h2{         background-repeat:no-repeat;         width:236px;         height:34px;         background-image:url(http://l.yimg.com/a/i/brand/purplelogo/uh/us/20/safely/us_safely.png);         _background-image:url(http://l.yimg.com/a/i/brand/purplelogo/uh/us/20/safely/us_safely-ffffff.gif);      }      .yuhead-logo h2.small{         background-position:0px -240px;      }      .yuhead-logo a{         display:block;         position:absolute;         width:236px;         height:34px;      }      .yuhead-logo div.yuhead-comarketing {        font-size:93%;        width:236px;      white-space:nowrap;      text-align:right;      }      .yuhead-logo div.yuhead-comarketing a {        display:inline;        position:relative;      }   </style>   <h2>      <a href="http://safely.yahoo.com"       target="_top">         Yahoo! Safely      </a>   </h2>   <!-- comarketing component --></div>        <div id="yuhead-search">        <div id="yuhead-sform-cont" class="yuhead-s-web yuhead-search-form">        <form role="search" class="yucs-search yucs-activate" action="http://search.yahoo.com/search" method="get"><table role="presentation">   <tbody role="presentation"><tr><td class="yucs-form-input" role="presentation"> <label>     <span>Search</span>                 <input autocomplete="off" type="text" class="sp yuhead-ico-mglass yuhead-search-hint yucs-search-hint-color yucs-search-field" name="p" value="Search" data-sh="Search" data-satype="rich" data-gosurl="" data-pubid="" /> </label></td><td NOWRAP class="yucs-form-btn" role="presentation"><div class="yucs-btn-wrap">    <button class="yucs-sweb-btn" type="submit">Search Web</button></div>            </td>         </tr>        </tbody></table><input type="hidden" id="fr" name="fr" value="ush-safely" /></form>            </div>    </div>    </div>    </div><script type="text/javascript" charset="utf-8"> YUI().use('node','event','event-mouseenter','substitute','oop','node-focusmanager','node','event','node','event','event-custom', function(Y) {});</script><script charset='utf-8' type='text/javascript' src='http://l.yimg.com/zz/combo?kx/ucs/common/js/1/setup-min.js&kx/ucs/sts/js/1/skip-min.js&kx/ucs/menu_utils/js/111/menu_utils-min.js&kx/ucs/username/js/19/user_menu-min.js&kx/ucs/help/js/19/help_menu-min.js&kx/ucs/utility_link/js/3/utility_menu-min.js&kx/ucs/homepage/js/41/homepage-min.js&kx/ucs/common/js/1/setup-min.js&kx/ucs/search/js/162/search_v1-min.js'></script>
<div class="hd">
  <a name="top_page"></a>
  <div id="safely-nav">        
      <ul id="hdmenu" class="en-US">
          <li class="active"><a href="/" id="nav-home">Home</a></li>
                    <li ><a href="/expert-advice/" id="nav-expert-advice">Expert Advice</a></li>
                    <li ><a href="/yahoo-products/" id="nav-yahoo-products">Yahoo! Products</a></li>
          <li ><a href="/parents/" id="nav-parents">Parents</a></li>
          <li ><a href="/teens/" id="nav-teens">Teens</a></li>
          <li ><a href="/safety-tips/" id="nav-safely-tips">Safety Tips</a></li>
          <li ><a href="/faq/" id="nav-faq">FAQ</a></li>
      </ul>
  </div><!-- end: nav-yahoo-safely -->
</div><!-- end: hd -->

          <div class="bd">
			<div id="ylogoprint"><img src="http://l.yimg.com/ep/cx/p/safely/images/yahoosafely_logo-2692.gif" width="230" alt="" height="26"></div>
              <div class="bd-cont">
                  <div id="home">
                  <div id="bdtitdesc-cont">
                            <h2 id="bdtitle">Welcome to Yahoo! Safely</h2>
                            <p id="bddesc">Learn how to make smart and safer choices online and get advice on using Yahoo! products safely.
</p>
                  </div><!-- end: hdtitdesc-cont -->
                  <div id="safely-slide-show">
                      <div id="pict-container">
                          <ol id="slide-show-cont">
                                                            
                                <li class="slide-show-pict first-pic">
                                      <img src="http://l.yimg.com/a/i/us/policy/safely/photos/swm_intlwomen.jpg" alt="" />                                  
                                      <div class="description-cont">
                                        <h3>People around the world are sharing what matters. Safely.</h3>
                                        <p>Chime in, learn smart ways to share, and see How Good Grows on Yahoo!.
</p>
                                        <a href="http://howgoodgrows.safely.yahoo.com/"><span>Start Now<sup class="arrows">&nbsp;</sup></span></a>
                                      </div>
                                </li>                            
                                                            
                                <li class="slide-show-pict">
                                      <img src="http://l.yimg.com/a/i/us/i/safely/ysafely_landing_banner_930x236_smartphone.jpg" alt="" />                                  
                                      <div class="description-cont">
                                        <h3>Are they being smart with their smartphone?</h3>
                                        <p>Show them how to stay safer on their mobile device.
</p>
                                        <a href="/safety-tips/mobile-safety-tips-2/"><span>Make These Tips a Habit<sup class="arrows">&nbsp;</sup></span></a>
                                      </div>
                                </li>                            
                                                            
                                <li class="slide-show-pict">
                                      <img src="http://l.yimg.com/a/i/us/i/safely/ysafely_landing_banner_930x236_pusharound.jpg" alt="" />                                  
                                      <div class="description-cont">
                                        <h3>Don’t let kids get pushed around on the Web</h3>
                                        <p>Recognize cyberbullying and learn how to prevent it.
</p>
                                        <a href="/parents/tips-for-parents-to-help-prevent-cyberbullying-2/"><span>Here’s What to Know<sup class="arrows">&nbsp;</sup></span></a>
                                      </div>
                                </li>                            
                                                            
                                <li class="slide-show-pict">
                                      <img src="http://l.yimg.com/a/i/us/i/safely/ysafely_landing_banner_930x236_Password.jpg" alt="" />                                  
                                      <div class="description-cont">
                                        <h3>When they set their password, make sure they play hard to get.</h3>
                                        <p>Sure, you can be creative with your online ID, but making your ID safe is crucial.
</p>
                                        <a href="http://safely.yahoo.com/safety-tips/choosing-a-secure-online-id-2/"><span>Ace Your Online ID<sup class="arrows">&nbsp;</sup></span></a>
                                      </div>
                                </li>                            
                                                            
                                <li class="slide-show-pict">
                                      <img src="http://l.yimg.com/a/i/us/i/safely/ysafely_landing_banner_930x236_tweetsmart.jpg" alt="" />                                  
                                      <div class="description-cont">
                                        <h3>Get some Tweet Smarts</h3>
                                        <p>What you need to know to guide your children online.
</p>
                                        <a href="/parents/social-networking-tips-2/"><span>Tips by Age Group<sup class="arrows">&nbsp;</sup></span></a>
                                      </div>
                                </li>                            
                                                      </ol>
                      <div id="slide-show-nav">
                              <ul>
                                                                    <li >
                                        <a class="pager-item item-selected" tabindex="0" href="#1"></a>
                                    </li>
                                                                    <li >
                                        <a class="pager-item" tabindex="1" href="#2"></a>
                                    </li>
                                                                    <li >
                                        <a class="pager-item" tabindex="2" href="#3"></a>
                                    </li>
                                                                    <li >
                                        <a class="pager-item" tabindex="3" href="#4"></a>
                                    </li>
                                                                    <li class="last">
                                        <a class="pager-item" tabindex="4" href="#5"></a>
                                    </li>
                                                                  
                              </ul><!-- end: yui-carousel-content -->
                              
                      </div><!-- end: slide-show-nav -->
                      <div id="slide-show-btn">
                        <span class="first-button" id="prev-button"></span>
                        <span class="next-button" id="next-button"></span>
                      </div>
                      </div><!-- end: pict-container -->
                      
                  </div><!-- end: safely-carousel -->
                  
                                    
                  <div id="bdhero-cont">
                      <div id="expert-advise">
                          <div class="expert-advice-cont">
                              <div class="pm-cont">
                                                                  
                                  <h3 class="pmtitle">Family Pledge</h3>
                                  <div class="pm-cont-p">
                                    <p class="pmdescription"> <p>Step up to online safety together.</p>
</p>
                                  </div><!-- end: pm-cont-p -->
                              </div><!-- end: pm-cont -->
                              <a href="/parents/family-online-and-mobile-device-agreement/" class="pmcall">Take the Family Pledge &raquo;</a>
                          </div><!-- end: expert-advice-cont -->
                      </div><!-- end: promotional-modules-container -->
                      
                      <div id="social-networking-tips">
                          <div class="social-networking-tips-cont">
                              <div class="pm-cont">
                                                                  
                                  <h3 class="pmtitle">Yahoo! Pulse</h3>
                                  <div class="pm-cont-p">
                                    <p class="pmdescription"> <p>Get updates from your friends and family, and stay safe.</p>
</p>
                                  </div><!-- end: pm-cont-p -->
                              </div><!-- end: pm-cont -->
                              <a href="/yahoo-products/yahoo-pulse-safety-guide/" class="pmcall">Read the facts &raquo;</a>
                          </div><!-- end: social-networking-tips-cont -->
                      </div><!-- end: social-networking-tips-container -->
                      
                      <div id="family-pledge">
                          <div class="family-pledge-cont">
                              <div class="pm-cont">
                                                                  
                                  <h3 class="pmtitle">Strong Passwords</h3>
                                  <div class="pm-cont-p">
                                    <p class="pmdescription"> Your dog's name is not a secure password.
</p>
                                  </div><!-- end: pm-cont-p -->
                              </div><!-- end: pm-cont -->
                              <a href="/safety-tips/tips-for-strong-secure-passwords-2/" class="pmcall">Find out what is &raquo;</a>
                          </div><!-- end: family-pledge-cont -->
                      </div><!-- end: family-pledge-container -->
                  </div>
              </div>
              </div>
              <div class="btn"></div>
            </div><!-- end: bd -->

            <!-- Include Footer Nav PHP file -->
            <div class="ft">
    <div class="ft-cont">
        <h4 id="fttitle">Yahoo! Safely Around the World</h4>
        <ul id="ftcountries">
            <li class="ar"><a href="http://ar.safely.yahoo.com" target="_blank">Argentina</a></li>
            <li class="aa"><a href="http://asia.safely.yahoo.com" target="_blank">Asia</a></li>
            <li class="au"><a href="http://au.safely.yahoo.com" target="_blank">Australia</a></li>
            <li class="br"><a href="http://br.safely.yahoo.com " target="_blank">Brasil</a></li>
            <li class="ca"><a href="http://ca.safely.yahoo.com" target="_blank">Canada</a></li>
            <li class="cl"><a href="http://cl.safely.yahoo.com" target="_blank">Chile</a></li>
            <li class="co"><a href="http://co.safely.yahoo.com" target="_blank">Colombia</a></li>
            <li class="de"><a href="http://de.safely.yahoo.com" target="_blank">Deutschland</a></li>
            <li class="es"><a href="http://es.safely.yahoo.com" target="_blank">España</a></li>
            <li class="fr"><a href="http://fr.safely.yahoo.com" target="_blank">France</a></li>
            <li class="hk"><a href="http://hk.safely.yahoo.com" target="_blank">香港 </a></li>
            <li class="in"><a href="http://in.safely.yahoo.com" target="_blank">India</a></li>
            <li class="id"><a href="http://id.safely.yahoo.com" target="_blank">Indonesia</a></li>
            <li class="it"><a href="http://it.safely.yahoo.com/" target="_blank">Italia</a></li>
            <li class="my"><a href="http://malaysia.safely.yahoo.com" target="_blank">Malaysia</a></li>
            <li class="mx"><a href="http://mx.safely.yahoo.com" target="_blank">México</a></li>
            <li class="mea"><a href="http://esafe.yahoo.com" target="_blank">الشرق الأوسط وإفريقيا</a></li>
            <li class="nz"><a href="http://nz.safely.yahoo.com" target="_blank">New Zealand</a></li>
            <li class="pe"><a href="http://pe.safely.yahoo.com" target="_blank">Perú</a></li>
            <li class="ph"><a href="http://ph.safely.yahoo.com" target="_blank">Philippines</a></li>
            <li class="qu"><a href="http://fr-ca.safely.yahoo.com" target="_blank">Québec</a></li>
            <li class="sg"><a href="http://sg.safely.yahoo.com" target="_blank">Singapore</a></li>
            <li class="kr"><a href="http://kr.safely.yahoo.com" target="_blank">한국</a></li>
            <li class="tw"><a href="http://tw.safely.yahoo.com" target="_blank">台灣</a></li>
            <li class="th"><a href="http://th.safely.yahoo.com" target="_blank">ประเทศไทย</a></li>
            <li class="uk"><a href="http://uk.safely.yahoo.com/" target="_blank">United Kingdom</a></li>
            <li class="us"><a href="http://safely.yahoo.com/" target="_blank">United States</a></li>
            <li class="e1"><a href="http://espanol.safely.yahoo.com">en&nbsp;Espa&ntilde;ol</a></li>
            <li class="ve"><a href="http://ve.safely.yahoo.com">Venezuela</a></li>
            <li class="vn"><a href="http://vn.safely.yahoo.com">Việt Nam</a></li>
        </ul><!-- end: ftcountries -->
        <div class="info">
            <p>Copyright &copy; 2011 Yahoo! Inc. All rights reserved.</p>
            <ul>
                <li class="first"><a href="http://info.yahoo.com/copyright/us/details.html">Copyright/IP Policy</a></li>
                <li><a href="http://info.yahoo.com/legal/us/yahoo/utos/">Terms of Service</a></li>
                <li><a href="http://info.yahoo.com/privacy/us/yahoo/">Privacy Policy</a></li>                                
                <li><a href="http://help.yahoo.com/l/us/yahoo/helpcentral/">Help</a></li>                
            </ul>
        </div><!-- end: info -->
    </div>
    <div class="btn"></div>
</div>
        </div><!--end: wrapper-->
        <script> 
            var keys = {
                A_pn:'Yahoo! Safely',
                A_id:'',
                A_pt:'HOMEPAGE'
            }; 
            var conf = {
                    spaceid:1350252393,                      
                    keys:keys,
                    tracked_mods:{
                        'expert-advise'         :'Promotional Module 1',
                        'social-networking-tips':'Promotional Module 2',
                        'family-pledge'         :'Promotional Module 3'
                    },
                    lt_attr:'text',
                    ywa:{ 
                        project_id:1000726271362                         
                    },
					client_only:1 					
            }; 
            var ins = new YAHOO.i13n.Track(conf); 
            ins.init(); 
        </script>         
  </body>      
</html><!-- CACHED --><!-- fe4.cx.bf1.yahoo.com uncompressed/chunked Tue Apr  5 15:12:12 UTC 2011 -->
