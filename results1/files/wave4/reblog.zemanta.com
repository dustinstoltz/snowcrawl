<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Zemanta Reblog discontinued</title>
	<meta name="robots" content="noindex,nofollow" />
	<meta http-equiv="pragma" content="no-cache" />
	<link href="css/style.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="js/download.js"></script>
</head>
<body>
<div id="header">
<a href="http://www.zemanta.com"><img src="img/zemanta-logo.png" alt="Zemanta" /></a>
</div>
<div id="content">
	<h1>Reblog has been discontinued</h1>
	<p>Unfortunately Zemanta Reblog has never got used as widely as we expected, therefore its support has been discontinued.</p>

	<div class="downloadbutton">
		<a class="download_button firefox" href="http://www.zemanta.com/download/">
			<span class="main_title">Free Download</span>
			<span class="subtitle">Firefox Extension</span>
		</a>
		<a class="demo" href="http://www.zemanta.com/demo/"><span>Free Demo</span></a>
	</div>
</div>
<div id="footer">
<p>The names and logos for Zemanta are registered trademarks of Zemanta, Ltd.</p> <p>&copy;2006-2010 Zemanta, Ltd. All rights reserved.</p>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-1933864-6");
pageTracker._trackPageview();
} catch(err) {}
</script>
</body>
</html>
