

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>
			Yahoo! Security Center: simple tips and solutions for computer security and online safety
		</title>
		<meta name="description" content="Fast answers, tips, and tools to help you stay safer online and protect your computer from viruses, spyware, and identity theft">
		<meta name="keywords" content="yahoo, computer, online, internet, security, safety, protection,Symantec, Norton, virus, spam, spyware, phishing, identity theft, privacy, free scan, free virus scan, answers, solutions, tips">
		<link rel="stylesheet" type="text/css" href="http://us.js2.yimg.com/us.js.yimg.com/lib/common/css/reset_2.0.5.css">
		<link rel="stylesheet" type="text/css" href="http://us.js2.yimg.com/us.js.yimg.com/lib/common/css/fonts_2.0.5.css">
		<link rel="stylesheet" type="text/css" href="http://us.js2.yimg.com/us.js.yimg.com/lib/common/css/grids_2.0.5.css">
		<link rel="stylesheet" type="text/css" href="http://us.js2.yimg.com/us.js.yimg.com/lib/seccntr/security_1.css">
		<style type="text/css">
input#p {height:24px;}
		#ft_shadow {margin-top:-3px;}
		#srch_m #prbsrch {margin-bottom:3px; padding:3px 2px 1px 2px;}
		#nested1 { height:255px; }
		#nested2 { height:100px; }
		#nested2 .nested_btm_lnk {margin-top:30px;}
		#bd1 { width:750px;height:195px; float:center; background:#469df2;margin:0px;padding:0; }
		#flashcontent {width:750px;height:197px;margin:0;padding:0; }
		#content {float:left;width:530px;margin:0;padding:0;}
		#content p {margin:10px 0;}
		.para_title {font:bold 100% arial; }
		#content ul {list-style:disc;margin-left:15px;}
		#botleft { float:left;width:560px;background:#ebeff1;margin:0 0 0 10px;}

		.bcnav {font:normal 77% arial;margin: 5px 0px;}

		#botleft .pri_dn_top, #botleft .pri_dn_mid, #botleft .pri_dn_bot {float:left; }
		#botleft .pri_dn_top {padding-top:8px;}
		.pagehdr {color:#545454; padding:0;margin:0;}

		.leftcol, .rightcol {float:left; width:250px; margin:0;padding:0; }
		.leftcol {border-right:1px solid #ccc;margin-right:10px; padding:0 15px 0 0;}
		.rightcol {padding:0;margin:0;}
		#bd2 .yui-u {margin-left:0px; }
		</style><!--[if gte IE 7]>
<style type="text/css">
#ygma #ygmahelp {width:12em;float:right;margin:0 5px 0 0;}
#frmblock {margin-right:5px; }
#nested2 .nested_btm_lnk {margin-top:0px;}
</style>
<![endif]-->

		<script type="text/javascript">
var spaceid = 565000002;
		</script>
		<script type="text/javascript" src="http://us.js2.yimg.com/us.js.yimg.com/ult/ylc_1.7.js">
</script>
		<script type="text/javascript" src="http://us.js2.yimg.com/us.js.yimg.com/lib/seccntr/scanmypc_1.js">
</script>
	</head>
	<body id="securitysite">
		<div id="doc">
			<div id="hd1" class="head">
				<!--  start: dynamically included  page header -->
				<!-- SpaceID=0 robot -->

			</div>
			<div id="bd1">
				<!-- start: your content here -->
				<!-- SWFObject embed by Geoff Stearns geoff@deconcept.com http://blog.deconcept.com/swfobject/ -->
				<script type="text/javascript" src="/flash/swfobject.js">
</script>
				<div id="flashcontent">
					<img id="noflash" src="http://us.i1.yimg.com/us.yimg.com/i/us/seccntr/gr/no_flash.gif" width="559" height="195" alt="Yahoo! Security Center" usemap="#no_flash_map" name="noflash"><map name="no_flash_map" id="no_flash_map">
						<area shape="rect" alt="Browse All Topics" coords="0,0,559,151" href="/all_topics.html">
						<area shape="rect" alt="Browse All Topics" coords="428,164,551,188" href="/all_topics.html">
						<area shape="rect" alt="Protect Your Kids" coords="291,164,414,188" href="/article_kids.html">
						<area shape="rect" alt="Protect Your Privacy" coords="154,164,277,188" href="/article_privacy.html">
						<area shape="rect" alt="Protect Your PC" coords="17,164,140,188" href="/article_pc.html">
					</map>
				</div><script type="text/javascript">
// <![CDATA[

							if(typeof(SWFObject) == "function") {
							var so = new SWFObject("/flash/ysecurity.swf", "ysecurity", "560", "195", "7", "#469df2");
							so.addParam("wmode", "transparent");
							so.addVariable("contentURL", "/flash/flashcontent.xml");
							so.write("flashcontent");
							}

							// ]]>
				</script> <!-- <img id="noflash" src="http://us.i1.yimg.com/us.yimg.com/i/us/seccntr/gr/flash_kid3.gif" width="559" height="195"> -->
				 <!-- end: your content here -->
			</div>
			<div id="ft_shadow">
			
				<img src="http://us.i1.yimg.com/us.yimg.com/i/us/seccntr/gr/flash_nav_shadow_750.gif" width="750" height="3"> 
			</div> 
			<div id="bd2" class="yui-t4">
				<!-- start: bottom primary column -->
				<div id="yui-main">
					<div class="yui-b">
						<!-- start: stack grids here -->
						<div class="yui-u">
							<div id="botleft">
								
								<p class="bcnav">
									<a href="index.html">Security Home</a> &gt; All Topics
								</p>
								<div class="pri_dn_top">
									<!-- only one module on sec page(s) -->
									<div class="pri_dn_mid no_nested">
										<h1 class="pagehdr">
											All Topics
										</h1>
										<div id="content">
											<div class="leftcol">
												<p class="para_title">Computer Security</p>
                <ul>
                <li><a href="/article_pc.html">Protect Your PC</a></li>
                <li><a href="/article.html?aid=2006102605">Protecting your computer makes good sense</a></li>
                <li><a href="/article.html?aid=2006102505">Do I need to back up my computer?</a></li>
                </ul>
<p class="moreart"><a  href="topic_full.html?topic=Computer Security">See All</a></p>
<p class="para_title">Email</p>
                <ul>
                <li><a href="/article.html?aid=2006102502">How can I recognize a phishing email?</a></li>
                <li><a href="/article.html?aid=2006102506">How do I report a phishing email or web site?</a></li>
                <li><a href="/article_pc.html">Protect Your PC</a></li>
                </ul>
<p class="moreart"><a  href="topic_full.html?topic=Email">See All</a></p>
<p class="para_title">Passwords</p>
                <ul>
                <li><a href="/article.html?aid=2006102509">How do I choose my password?</a></li>
                <li><a href="/article.html?aid=2006102510">How do I safeguard my password?</a></li>
                <li><a href="/article.html?aid=2006102507">What is a sign-in seal?</a></li>
                </ul>
<p class="moreart"><a  href="topic_full.html?topic=Passwords">See All</a></p>
<p class="para_title">Phishing</p>
                <ul>
                <li><a href="/article.html?aid=2006111501">What is phishing?</a></li>
                <li><a href="/article.html?aid=2006102506">How do I report a phishing email or web site?</a></li>
                <li><a href="/article.html?aid=2006102503">How can I identify a phishing web site?</a></li>
                </ul>
<p class="moreart"><a  href="topic_full.html?topic=Phishing">See All</a></p>

											</div>
											<div class="rightcol">
												<p class="para_title">Privacy</p>
                <ul>
                <li><a href="/article_privacy.html">Protect Your Privacy</a></li>
                <li><a href="/article.html?aid=2006102606">Why should I worry about my privacy on the Internet?</a></li>
                <li><a href="/article.html?aid=2006102601">Online Privacy Resources</a></li>
                </ul>
<p class="moreart"><a  href="topic_full.html?topic=Privacy">See All</a></p>
<p class="para_title">Kids and Family Safety</p>
                <ul>
                <li><a href="/article_kids.html">Protect Your Kids</a></li>
                <li><a href="/article.html?aid=2006102501">Can I keep my kids safe when they surf the Internet?</a></li>
                <li><a href="/article.html?aid=2006102504">How can my kids communicate safely online?</a></li>
                </ul>
<p class="moreart"><a  href="topic_full.html?topic=Kids and Family Safety">See All</a></p>
<p class="para_title">Viruses and Spyware</p>
                <ul>
                <li><a href="/article.html?aid=2006102508">How do I get rid of pop-ups, viruses, adware, or spyware?</a></li>
                <li><a href="/article.html?aid=2006102605">Protecting your computer makes good sense</a></li>
                <li><a href="/article_pc.html">Protect Your PC</a></li>
                </ul>
<p class="moreart"><a  href="topic_full.html?topic=Viruses and Spyware">See All</a></p>
<p class="para_title">Your Security on Yahoo!</p>
                <ul>
                <li><a href="/article.html?aid=2006120501">Reporting security issues to Yahoo!</a></li>
                <li><a href="/article.html?aid=2006102506">How do I report a phishing email or web site?</a></li>
                <li><a href="/article.html?aid=2006102604">Using shared computers</a></li>
                </ul>
<p class="moreart"><a  href="topic_full.html?topic=Your Security on Yahoo!">See All</a></p>

											</div><!-- end: rightcol -->
										</div>
									</div>
									<div class="pri_dn_bot"></div>
								</div><!-- end: pri_dn_top -->
							</div><!-- end: botleft -->
						</div><!-- end: stack grids here -->
					</div>
				</div><!-- end: bottom primary column  -->
				<!-- start: bottom secondary column  -->
				<div class="yui-b">
					<!-- start: your content here -->
					<div id="sec_dn">
						<div class="sec_top">
							<div class="sec_mid">
								<p id="othrprds">
									<a name="othrprds" href="http://us.lrd.yahoo.com/_ylc=X3oDMTBjNmdiN29uBF9TAzU2NTAwMDAwMg--/SIG=110v39g9c/**http%3A//downloads.yahoo.com/" id="othrprds">Keep your Yahoo! software updated</a>
								</p>
							</div>
							<div class="sec_bot"></div>
						</div><!--
			<div class="sec_top"><div class="sec_mid"><h2>Security Reference</h2><a class="strong" href="#" id="glossary_open">Glossary</a></div><div class="sec_bot"></div></div>
			-->
						<div class="sec_top">
							<div class="sec_mid">
								<h2>Featured Topics</h2> 
		<ul >
                <li><a href="/article.html?aid=2006102603">Help! I think I've been phished!</a></li>
                <li><a href="/article.html?aid=2010110401">How can I use public networks?</a></li>
                <li><a href="/article.html?aid=2006102507">What is a sign-in seal?</a></li>
                <li><a href="/article.html?aid=2006102508">How do I get rid of pop-ups, viruses, adware, or spyware?</a></li>
                </ul>
		<br />
                <div align="right"><a href="/all_topics.html">See All</a></div>

							</div>
							<div class="sec_bot"></div><br>
							<a href="http://us.lrd.yahoo.com/_ylc=X3oDMTBwbTRibmhmBF9TAzk3NjI0NTc5BF9zAzEzNTAyNTAwMTE-/SIG=116p58i41/**http%3A//info.yahoo.com/relevantads"><img src="flash/ads_privacy_171x171.jpg" width="171" height="171"></a>
						</div><div id="glossary">
				<div id="hdr"><h2>Glossary</h2><div id="close"><a href="javascript:;" onClick="hideitemdetail()"><img src="http://us.i1.yimg.com/us.yimg.com/i/us/seccntr/gr/close.gif" width="43" height="13"></a></div></div>
					<div id="glossary_nav">
						<span><a href="#" onClick="glos_show_hide('a');">A</a></span><span><a href="#" onClick="glos_show_hide('b');">B</a></span><span><a href="#" onClick="glos_show_hide('c');">C</a></span><span><a href="#" onClick="glos_show_hide('d');">D</a></span><span><a href="#" onClick="glos_show_hide('e');">E</a></span><span><a href="#" onClick="glos_show_hide('f');">F</a></span><span>G</span><span><a href="#" onClick="glos_show_hide('h');">H</a></span><span>I</span><span>J</span><span><a href="#" onClick="glos_show_hide('k');">K</a></span><span>L</span><span><a href="#" onClick="glos_show_hide('m');">M</a></span><span>N</span><span>O</span><span><a href="#" onClick="glos_show_hide('p');">P</a></span><span>Q</span><span>R</span><span><a href="#" onClick="glos_show_hide('s');">S</a></span><span><a href="#" onClick="glos_show_hide('t');">T</a></span><span><a href="#" onClick="glos_show_hide('u');">U</a></span><span><a href="#" onClick="glos_show_hide('v');">V</a></span><span><a href="#" onClick="glos_show_hide('w');">W</a></span><span>X</span><span>Y</span><span><a href="#" onClick="glos_show_hide('z');">Z</a></span>
					<img id='glossary_beacon' width='1' height='1' src='#' />
					</div>
				<span class="glossary_body" id="glos_a">
					<p class="title">adware</p>	
					<p class="desc">A program that typically displays advertising through pop-up or pop-under windows as you surf the Web. Adware is often hidden alongside other programs, and you may unknowingly install it when you download a program from the Internet or install software from disks. You usually need a specialized anti-adware or anti-spyware program to remove adware from your computer.</p>
					<p class="title">anonymity</p>	
					<p class="desc">Inability to identify a person from known information.</p>
					<p class="title">anti-spam</p>
					<p class="desc">A program that filters spam in an email inbox and moves it to a bulk or spam folder, where it can be deleted.</p>
					<p class="title">anti-spyware</p>
					<p class="desc">A program that finds and removes spyware. Some anti-spyware programs can also find and remove other malware, like keyloggers, Trojan horses, worms, and more.</p>
					<p class="title">anti-virus</p>
					<p class="desc">A program that is designed to identify, prevent, and eliminate viruses and other malicious software.</p>
					<p class="title">attacker</p>
					<p class="desc">A person who intentionally attempts to defeat a system.</p>
				</span>
				<span class="glossary_body" id="glos_b">
				  <p class="title">bulk folder</p>	
					<p class="desc">A folder in some email programs that is used to hold email identified as spam.</p>
				</span>
				<span class="glossary_body" id="glos_c">
					<p class="title">case-sensitive</p>
					<p class="desc">Distinguishing between uppercase (or capital) letters and lowercase (or small) letters. Yahoo! passwords are case-sensitive, which means that a capital A is different from a lowercase a. So when you enter your password, make sure to type it with the correct capitalization.</p>
					<p class="title">cookie</p>	
					<p class="desc">A small amount of data, often including an anonymous unique identifier, that is sent to your browser from a web site's computers and stored on your computer's hard disk. Web sites use cookies to "remember" details about you, such as your user name or site preferences, in order to personalize your experience on that web site. Your browser transmits information back to the site each time you view that site until the cookie expires. Yahoo!, for example, uses cookies to remember your personal preferences, allowing us to display customized content on pages such as My Yahoo!. </p>
				</span>
				<span class="glossary_body" id="glos_d">
					<p class="title">download</p>
					<p class="desc">The transfer of a copy of program or file from a network to a single computer.</p>
				</span>
				<span class="glossary_body" id="glos_e">
					<p class="title">email header</p>	
					<p class="desc">Part of an email message that describes the path that the email took to go from the sender to the recipient. Email headers are generally hidden, but can be displayed if necessary. If you report spam or phishing emails to Yahoo!, you'll be asked to include the email headers to help identify the source of the email.</p>
					<p class="title">encryption</p>	
					<p class="desc">The process of converting data or other information into code so that unauthorized people cannot access it.</p>
				</span>
				<span class="glossary_body" id="glos_f">
					<p class="title">firewall</p>
				  <p class="desc">Hardware or a program that prevents unauthorized users from accessing a computer network or that monitors the transfer of information to and from a network. A personal firewall is a program that filters traffic to or from a single computer. Many operating systems (such as Microsoft Windows XP and Mac OS X) include firewall protection.</p> 
					<p class="desc">A computer firewall gets its name from the fireproof wall in buildings that acts as a barrier to prevent the spread of fire.</p>
					<p class="title">freeware</p>	
					<p class="desc">Software (or programs) available for free, usually over the Internet. These programs can be sources of hidden spyware and adware.</p>
				</span>
				<span class="glossary_body" id="glos_h">
					<p class="title">hacker</p>
					<p class="desc">A person who uses programming skills to gain illegal access to a computer, network, or file.</p>
					<p class="title">header</p>	
					<p class="desc">Another name for an email header.</p>
					<p class="title">hijacker</p>	
					<p class="desc">A malicious program that takes control of a browser and may redirect it to a fraudulent site for the purpose of committing identity theft or fraud.</p>
					<p class="title">hoax</p>
					<p class="desc">Something meant to deceive or trick. Hoaxes involving threats to computers usually arrive in an email and contain bogus warnings designed to frighten or mislead you. Unsuspecting recipients may forward the email to friends and colleagues, spreading the hoax.</p>
				</span>
				<span class="glossary_body" id="glos_k">
					<p class="title">keylogger</p>
					<p class="desc">Software (or a program) that secretly tracks and records all activities on a computer, including keystrokes, web sites visited, and potentially more. The information captured is transmitted back to a third party, who can then use the information to access online accounts and sensitive personal and financial information.</p>
				</span>
				<span class="glossary_body" id="glos_m">
					<p class="title">mail header</p>
					<p class="desc">Another name for an email header.</p>
					<p class="title">malware</p>
					<p class="desc">Software designed to infiltrate or damage a computer without the owner's knowledge. Malware is a general category of software that includes viruses, worms, Trojan horses, spyware, adware, and other malicious software.</p>
				</span>
				<span class="glossary_body" id="glos_p">
					<p class="title">phishing</p>
					<p class="desc">An attempt to steal passwords and private account information through fake web sites and emails that look like those of trusted companies. A phishing web site or email can look identical to the real thing, so it can be hard to tell that it's fake. Phishing schemes can also use instant messages, typically when an account is compromised. In this case, the fraudster sends phishing messages to the contacts in the account's Messenger or friend list.</p>
					<p class="title">pop-under</p>
					<p class="desc">A form of online advertising designed to attract viewers to a web site or to capture email addresses. This type of ad "pops under" the current web page in a new window and isn't seen until the browser window is closed, making it more difficult to determine which web site opened it.</p>
					<p class="title">pop-up</p>
					<p class="desc">A form of online advertising designed to attract viewers to a web site or to capture email addresses. This type of ad "pops up" in a new window, covering all or part of the current web page.</p>
					<p class="title">pop-up blocker</p>
					<p class="desc">A program designed to prevent pop-ups and pop-unders.</p>
					<p class="title">pretexting</p>
					<p class="desc">Using false pretenses (such as a false identify or name) to get personal information, which may be used to fraudulently obtain credit or assets.</p>
				</span>
				<span class="glossary_body" id="glos_s">
					<p class="title">shareware</p>
					<p class="desc">Copyrighted software (or programs) available for free on a trial basis. Usually you'll be asked to pay a fee if you want to continue using the software after the trial period. These programs are sometimes sources of hidden spyware and adware.</p>
					<p class="title">sign-in seal</p>
					<p class="desc">A feature of Yahoo! that helps to protect you against phishing scams. You create your personalized sign-in seal and then look for it every time you sign in to Yahoo!. If your sign-in seal isn't displayed, or isn't the one you created, you might be on a fraudulent web site, designed to look like a legitimate Yahoo! site.</p>
					<p class="title">social engineering</p>
					<p class="desc">A common ploy used to gain access to accounts by manipulating unsuspecting victims into revealing confidential information. Perpetrators may befriend potential victims and use information provided by them to guess a password or other secret data, which they use to access the victim's online accounts.</p>
					<p class="title">spam</p>
					<p class="desc">Any message, regardless of its content, that is sent to multiple recipients who haven't specifically requested it. Spam can be an email message or an instant message. Posting the same message multiple times to newsgroups or list servers is also considered spamming &#151; especially if it isn't related to the topic. Spam is also called UCE (unsolicited commercial email) and UBE (unsolicited bulk email).</p>
					<p class="title">Spam folder</p>
					<p class="desc">A folder in Yahoo! Mail used to hold email identified as spam.</p>
					<p class="title">spearfishing or spearphishing</p>
					<p class="desc">A kind of phishing scheme that targets a specific organization or individual in an attempt to gain access to confidential data. Like phishing messages, spearphishing messages appear to come from a trusted source, and may even appear to be from an employee within the recipient's company. Typically, a spearphishing email asks for user names and passwords or instructs the recipient to click on a link. That link could result in the downloading of spyware or other malicious programs. If a single employee falls for the spearphishing scam, the attacker can pretend to be that individual and gain access to sensitive data.</p>
					<p class="title">spoofing</p>
					<p class="desc">Imitating a legitimate web site. Phishing scams use spoofing to create site that looks like a legitimate web site to fool potential victims into signing on with their user ID and password. The spoofing site captures this information and uses it to gather personal and financial information.</p>
					<p class="title">spyware</p>
					<p class="desc">A program or technology that aids in gathering information about a person or organization, often without their knowledge. It includes programs like hijackers and keyloggers. Spyware is often hidden alongside other programs, and you may unknowingly install spyware when you download a program from the Internet or install software from disks. You usually need a specialized anti-spyware program to remove spyware from your computer.</p>
					<p class="title">SSL</p>
					<p class="desc">Abbreviation for Secure Sockets Layer. A set of rules that defines the format and sequence of messages sent over the Internet to provide a level of security when transmitting private information. When you sign in to Yahoo!, your password is always transmitted over a SSL encrypted connection.</p>
				</span>
				<span class="glossary_body" id="glos_t">
					<p class="title">Trojan</p>
					<p class="desc">A shortened name for Trojan horse.</p>
					<p class="title">Trojan horse</p>
					<p class="desc">A program that disguises itself as another program. Similar to a virus, a Trojan horse is hidden and usually causes an unwanted effect, such as installing a "back door" in your computer that can be used by hackers. Unlike a virus, a Trojan horse typically doesn't create copies of itself.</p>
				</span>
				<span class="glossary_body" id="glos_u">
					<p class="title">UBE</p>
					<p class="desc">Abbreviation for unsolicited bulk email. More commonly known as spam.</p>
					<p class="title">UCE</p>
					<p class="desc">Abbreviation for unsolicited commercial email. More commonly known as spam.</p>
				</span>
				<span class="glossary_body" id="glos_v">
					<p class="title">virus</p>
					<p class="desc">A program that hides in other programs or documents and spreads as a side-effect of something you do, like opening an attachment to an email. Viruses come in many forms, and you don't need to install a program for your computer to be infected. For example, some viruses are spread when you open a word-processing document, particularly if you enabled macros in your word processor. An email virus may create copies of itself and automatically mail itself to everyone in your address book or attach itself to outgoing files.</p> 
				</span>
				<span class="glossary_body" id="glos_w">
				        <p class="title">worm</p>
				        <p class="desc">A malicious program that spreads without your taking any action, typically by exploiting vulnerabilities in popular programs like Microsoft Outlook and Microsoft Outlook Express email software. Once activated, a worm generally uses the Internet or your local network to spread to other computers.</p>
				</span>
				<span class="glossary_body" id="glos_z">
				        <p class="title">zombie</p>
				        <p class="desc">A computer that has been attacked by a hacker, virus, or Trojan horse and is then used to perform malicious acts, such as sending spam, under remote direction. The zombie computer gets its name from the zombie &#151; an undead or apathetic person &#151; because the computer and its owner are unaware that the computer is controlled remotely.</p>
				</span>
				</div>

					</div><!-- end: bot chunk -->
					<!-- end: your content here -->
					<!-- end: bottom secondary column -->
				</div>
			</div>
			<div id="ft">
				<!-- start: dynamically included page footer -->
				<!-- SpaceID=0 robot -->
 <!-- end: page footer -->
			</div>
		</div><script type="text/javascript" src="http://us.js2.yimg.com/us.js.yimg.com/lib/common/utils/2/yahoo_2.0.6.js">
</script><script type="text/javascript" src="http://us.js2.yimg.com/us.js.yimg.com/lib/common/utils/2/event_2.0.6.js">
</script><script type="text/javascript" src="http://us.js2.yimg.com/us.js.yimg.com/lib/common/utils/2/dom_2.0.5.js">
</script><script type="text/javascript" src="http://us.js2.yimg.com/us.js.yimg.com/lib/seccntr/glossary_1.js">
</script>
	</body>
</html>
<!--0.024633--><!-- w5.yop.re2.yahoo.com uncompressed/chunked Tue Apr  5 08:12:13 PDT 2011 -->
