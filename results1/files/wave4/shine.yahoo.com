<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta property="og:url" content="http://shine.yahoo.com"/>
    <meta property="og:title" content="Shine: Fashion and Beauty, Healthy Living, Parenting, Sex and Love, Career and Money, Food, and more - Shine on Yahoo!"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://l.yimg.com/a/i/us/shine/facebook/yahoo_shine.jpg"/>
    <meta property="og:description" content="Yahoo! Shine is the online destination for women looking the latest information and advice from experts and the community on parenting, health, fashion, beauty, sex, love, horoscopes, career, money, food, home, celebrity news, and entertainment."/>
    <meta property="og:site_name" content="Yahoo! Shine"/>
    
    <link rel="canonical" href="http://shine.yahoo.com"/>
    
    <meta property="fb:app_id" content="143301585685307" />
    
	
	<meta name="description" lang="en" content="Yahoo! Shine is the online destination for women looking the latest information and advice from experts and the community on parenting, health, fashion, beauty, sex, love, horoscopes, career, money, food, home, celebrity news, and entertainment.">
	<title>Shine: Fashion and Beauty, Healthy Living, Parenting, Sex and Love, Career and Money, Food, and more - Shine on Yahoo!</title>
	<script type="text/javascript" charset="UTF-8" src="http://l.yimg.com/jn/js/20110318042012/ylf_core.js"></script>
	
    
    
<LINK rel="stylesheet" href="http://l.yimg.com/jn/css/20110318042012/base.css" type="text/css"></LINK>
</head>
<body id="ylf-home">
<div id="wrapper">
    <!-- no eyebrow content -->
    <!-- SpaceID=0 robot -->

    	<div id="ylf-nav" class="nav">
		<ul class="cls">
			<li class="first selected"><a href="/">Home</a></li><li><a href="/channel/life">Manage Your Life</a></li><li><a href="/channel/beauty/">Fashion + Beauty</a></li><li><a href="/channel/health/">Healthy Living</a></li><li><a href="/channel/parenting/">Parenting</a></li><li><a href="/channel/sex/">Love + Sex</a></li><li><a href="/channel/food/">Food</a></li><li><a href="http://yourwisdom.yahoo.com">Your Wisdom</a></li><li><a href="/astrology">Astrology</a></li>
		</ul>
	</div>
	<div id="ylf-subnav">
		<!-- -->
		<div class="nav"><span class="title">Featured on Shine:</span> <ul class="topics cls"><li class="first"><a href="/event/the-thread/">the thread: celeb fashion video</a></li><li class=""><a href="/event/financiallyfit/">financially fit</a></li><li class=""><a href="/event/momentsofmotherhood/">moments of motherhood</a></li><li class=""><a href="http://royalwedding.yahoo.com/">royal wedding</a></li></ul></div>
	</div>

    <div id="ylf-searchbar" class="pop-search">
        <div class="search-container">
            
  <div class="searchbar">
  <form action="/search" name="searchform" id="searchform">
      <label><input type="radio" name="type" value="all" checked="checked"><span>All</span></label>
  <label><input type="radio" name="type" value="recipes" ><span>Recipes</span></label>
    <label class="hidden" for="p">Search Yahoo! Shine for:</label>
    <input type="text" class="search-input" value="" size="32" id="p" name="p"/>
    <button type="submit">Shine Search</button>
  </form>
  </div>

            <div class="ylf-popular-search">
<div class="hd"><h3>Trending Now:</h3></div>
<div class="bd">
<div class="pad">
<ol class="col-1" start="1">
<li class="item-1"><a href="http://shine.yahoo.com/search?cs=bz&p=Nut+Butter&fr=shine-tts"><strong>Nut Butter</strong></a></li>
<li class="item-2"><a href="http://shine.yahoo.com/search?cs=bz&p=Dollhouses&fr=shine-tts"><strong>Dollhouses</strong></a></li>
<li class="item-3"><a href="http://shine.yahoo.com/search?cs=bz&p=Autism&fr=shine-tts"><strong>Autism</strong></a></li>
<li class="item-4"><a href="http://shine.yahoo.com/search?cs=bz&p=Spring+2011&fr=shine-tts"><strong>Spring 2011</strong></a></li>
</ol>
</div><!-- /.pad -->
</div><!-- /.bd -->
</div><!-- /.ylf-popular-search-->

        </div>
    </div>
    <!-- SpaceID=0 robot -->

    
    <div class="doc cls">
        <div class="main">
            
<div id="ylf-hdrbar" class="cls">



<p>Tuesday, April 5, 2011</p>



</div>
            		<div class="mod feature cls">
			<ul class="cols col1">
							<li class="last">
				<a href="http://shine.yahoo.com/channel/beauty/six-short-spring-haircuts-2471756/" class="img"><img src="http://l.yimg.com/jn/util/anysize/310*275c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fhp%2F1%2F310.best_new_hair.jpg" width="310" height="275" alt="The best new hairstyles for spring"/></a>
				<div class="attr">
					
					<h1><a href="http://shine.yahoo.com/channel/beauty/six-short-spring-haircuts-2471756/" class="lnk">The best new hairstyles for spring</a></h1>
					<cite>By <strong><a href="http://shine.yahoo.com/blog/DNU3XGSKBWT3MREVMNUUGLKE6A/;_ylt=AikbQ5t4SEbHiQ6BOjro_YdpbqU5" class="by">Joanna Douglas, Shine Staff</a></strong> </cite>
					<p>Practically every long-haired leading lady chopped her hair this spring, and with cuts this fun and flattering, we can understand why. Celebrity stylists fill us in on the prettiest styles, how to get them, and who can pull them off best. Consider this your salon cheat sheet. <a class="more" href="http://shine.yahoo.com/channel/beauty/six-short-spring-haircuts-2471756/">Read More &raquo;</a></p>
				</div>
			</li>
			</ul>
		</div>
            <div class="cls col-bg">
                <div class="col">
                    
<div id="ylf-cheat-sheet" class="mod line">
	<div class="hd cls">
		<div id="ylf-logo"><!-- SpaceID=0 robot -->
</div>
		<h2> Cheat Sheet</h2>
	</div>
	<div class="bd">
		<ol class="nums">
			<li class="num1"><span><!-- --></span><div><a href="http://shine.yahoo.com/channel/life/international-pillow-fight-day-was-this-weekend-and-man-did-it-look-fun-video-2471654/">Video of the day: International Pillow Fight Day!</a></div></li><li class="num2"><span><!-- --></span><div><a href="http://shine.yahoo.com/channel/parenting/family-fun-contest-win-a-ps3-and-little-fockers-blu-ray-combo-2471786/">Family Fun contest: Win a PS3 and "Little Fockers" Blu-Ray combo</a></div></li><li class="num3"><span><!-- --></span><div><a href="http://shine.yahoo.com/channel/food/boys-family-turns-away-barefoot-contessa-oh-snap-2471658/">Boy's family turns down Barefoot Contessa. Oh snap.</a></div></li><li class="num4"><span><!-- --></span><div><a href="http://shine.yahoo.com/event/springfashion/10-easy-ways-to-incorporate-color-into-your-wardrobe-2469947/#photoViewer=1">10 easy ways to incorporate color in your wardrobe</a></div></li><li class="num5"><span><!-- --></span><div><a href="http://royalguestbook.yahoo.com/">Sign our royal wedding guestbook!  Share your best marriage advice</a></div></li>
		</ol>
	</div>
</div>

                    
<div class="mod channel ch-L2 line" id="ylf-ch-L2">
	<div class="hd"><h2><a href="/channel/sex/">Love + Sex</a></h2></div>
	<div class="bd">
				<a href="http://shine.yahoo.com/channel/sex/heavier-women-are-better-in-bed-2471683/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="300" height="172" alt="Are curvy women better in bed?" class="lz"/></a>
		<div class="title">
			<h3><a href="http://shine.yahoo.com/channel/sex/heavier-women-are-better-in-bed-2471683/">Are curvy women better in bed?</a></h3>
			<cite> </cite>
		</div>
		<ul class="blt">
			<li><div class="cls"><a href="http://shine.yahoo.com/channel/sex/older-women-younger-men-is-there-still-a-double-standard-2471646/">Older women, younger men: Is there still a double standard?</a></div></li><li><div class="cls"><a href="http://shine.yahoo.com/channel/sex/3-simple-secrets-happy-couples-know-2471598/">3 simple secrets happy couples know</a></div></li><li><div class="cls"><a href="http://shine.yahoo.com/channel/sex/emotional-infidelity-how-to-safeguard-your-relationship-2467487/">Emotional infidelity: How to safeguard your relationship</a></div></li>
		</ul>
	</div>
	<div class="ft"><a href="/channel/sex/" class="more">More in Love + Sex &raquo;</a></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-ch-L2", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/300*172c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fsex%2F300.curvy_lady.jpg"]});</script>

                    
<div id="ylf-blurb" class="mod channel cls">
	<div class="hd"><h2><a href="http://shine.yahoo.com/channel/none/keep-those-great-user-posts-coming-2460955/">community</a></h2></div>
	<div class="bd sm-img">
		<a class="img" href="http://shine.yahoo.com/channel/none/keep-those-great-user-posts-coming-2460955/"><img width="92" height="110" alt="We love user posts!" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
		<div class="title">
			<em>We love user posts!</em>
			<p>Chatter is back but that's not the only thing we're cheering about. Our 25th user post was just promoted on the front page of Yahoo!. <a href="http://shine.yahoo.com/channel/none/keep-those-great-user-posts-coming-2460955/">Find out how your post could be next!</a></p>
			<cite></cite>
		</div>
	</div>
</div>

<script>YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-blurb", cl:"lz", imgs:["http://l.yimg.com/a/i/us/shine/community/megaphone_92x110.jpg"]});</script>

                </div>
                <div class="col-c">
                    
<div class="mod channel ch-C1 line" id="ylf-ch-C1">
	<div class="hd"><h2><a href="/channel/parenting/">Parenting</a></h2></div>
	<div class="bd">
				<a href="http://shine.yahoo.com/channel/parenting/to-be-or-not-to-be-a-mom-2471607/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="300" height="172" alt="8 questions to ask yourself before kids" class="lz"/></a>
		<div class="title">
			<h3><a href="http://shine.yahoo.com/channel/parenting/to-be-or-not-to-be-a-mom-2471607/">8 questions to ask yourself before kids</a></h3>
			<cite> </cite>
		</div>
		<ul class="blt">
			<li><div class="cls"><a href="http://shine.yahoo.com/channel/parenting/extraordinary-breastfeeding-how-old-is-too-old-to-breastfeed-2470751/">How old is too old to breastfeed?</a></div></li><li><div class="cls"><a href="http://shine.yahoo.com/channel/parenting/5-crazy-facts-about-birth-in-the-united-states-2471645/">5 crazy facts about birth you didn't know</a></div></li><li><div class="cls"><a href="http://shine.yahoo.com/channel/parenting/do-not-give-kids-these-medicines-2470777/">What you need to know about kids' medicine</a></div></li>
		</ul>
	</div>
	<div class="ft"><a href="/channel/parenting/" class="more">More in Parenting &raquo;</a></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-ch-C1", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/300*172c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fparenting%2F300.baby_booties.jpg"]});</script>

                    
<div class="mod channel ch-C2" id="ylf-ch-C2">
	<div class="hd"><h2><a href="/channel/food/">Food</a></h2></div>
	<div class="bd">
		
		<ul class="thumbs">
			<li><a href="http://www.shine.yahoo.com/channel/food/the-best-chocolate-bars-2470684/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="73" height="73" alt="Taste test: The best chocolate bar you can buy" class="lz"/></a><div class="txt"><h3><a href="http://www.shine.yahoo.com/channel/food/the-best-chocolate-bars-2470684/">Taste test: The best chocolate bar you can buy</a></h3> <cite> </cite></div></li><li><a href="http://www.shine.yahoo.com/channel/food/5-fresh-healthy-ways-to-dress-up-plain-ol-pasta-2471600/#photoViewer=1" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="73" height="73" alt="Healthy pasta? 5 recipes we can't wait to make" class="lz"/></a><div class="txt"><h3><a href="http://www.shine.yahoo.com/channel/food/5-fresh-healthy-ways-to-dress-up-plain-ol-pasta-2471600/#photoViewer=1">Healthy pasta? 5 recipes we can't wait to make</a></h3> <cite> </cite></div></li><li><a href="http://shine.yahoo.com/channel/food/insane-food-and-drink-laws-2470009/#photoViewer=1" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="73" height="73" alt="Insane food and drink laws in America" class="lz"/></a><div class="txt"><h3><a href="http://shine.yahoo.com/channel/food/insane-food-and-drink-laws-2470009/#photoViewer=1">Insane food and drink laws in America</a></h3> <cite> </cite></div></li><li><a href="http://shine.yahoo.com/channel/food/the-best-and-worst-pizza-the-chains-we-live-for-and-those-we-dont-2470766/#photoViewer=1" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="73" height="73" alt="The best and worst pizza: Chains we live for and those we don't" class="lz"/></a><div class="txt"><h3><a href="http://shine.yahoo.com/channel/food/the-best-and-worst-pizza-the-chains-we-live-for-and-those-we-dont-2470766/#photoViewer=1">The best and worst pizza: Chains we live for and those we don't</a></h3> <cite> </cite></div></li>
		</ul>
	</div>
	<div class="ft"><a href="/channel/food/" class="more">More in Food &raquo;</a></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-ch-C2", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/73*73c-86400,http%3A%2F%2Fa323.yahoofs.com%2Fphugc%2FmpTwRm3fKzJx%2Fphotos%2F21f311ec0a144666afd2b63405c28cd0%2Fmr_826bedf18cb8c7.jpg%3Fug_____DjBmY0Fnn","http://l.yimg.com/jn/util/anysize/73*73c-86400,http%3A%2F%2Fa323.yahoofs.com%2Fphugc%2F4ypeFc6ltisL%2Fphotos%2Fa907bde9317769fd2db27f37f7da2fd5%2Fori_9661da9585d8a2.jpg%3Fug_____Dcf8E.MfC","http://l.yimg.com/jn/util/anysize/73*73c-86400,http%3A%2F%2Fa323.yahoofs.com%2Fphugc%2FW9D0II5lHwnF%2Fphotos%2Fa6f8888b4b321563b093fd68ed83defd%2Fori_cd217311730fea.jpg%3Fug_____Ds6pa472G","http://l.yimg.com/jn/util/anysize/73*73c-86400,http%3A%2F%2Fa323.yahoofs.com%2Fphugc%2F_bLuDSRXUjdB%2Fphotos%2F6fa33cc8ffc930eb4a080994bc841a89%2Fmr_4773995f6733b4.jpg%3Fug_____DP9hzuYi3"]});</script>

                </div>
            </div>
            
<div class="mod channel ch-L4 line line-top cls" id="ylf-ch-L4">
	<div class="hd"><h2><a href="/channel/health/">Healthy Living</a></h2></div>
	<div class="bd">
		
		<ul class="cols col4">
			<li><div><a href="http://shine.yahoo.com/channel/health/preventing-sexual-violence-on-college-campuses-its-a-title-ix-issue-2471593/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="139" height="139" alt="New guidelines for preventing sexual assault on campuses" class="lz"/></a> <a href="http://shine.yahoo.com/channel/health/preventing-sexual-violence-on-college-campuses-its-a-title-ix-issue-2471593/" class="lnk">New guidelines for preventing sexual assault on campuses</a></h3> <cite> </cite></div></li><li><div><a href="http://shine.yahoo.com/channel/health/state-by-state-breast-cancer-rates-how-does-your-state-rank-2471665/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="139" height="139" alt="Breast cancer rates: the best and worst states" class="lz"/></a> <a href="http://shine.yahoo.com/channel/health/state-by-state-breast-cancer-rates-how-does-your-state-rank-2471665/" class="lnk">Breast cancer rates: the best and worst states</a></h3> <cite> </cite></div></li><li><div><a href="http://shine.yahoo.com/channel/health/user-post-does-the-media-put-too-much-pressure-on-young-girls-2471603/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="139" height="139" alt="'Why are my daughters obsessed with weight?'" class="lz"/></a> <a href="http://shine.yahoo.com/channel/health/user-post-does-the-media-put-too-much-pressure-on-young-girls-2471603/" class="lnk">'Why are my daughters obsessed with weight?'</a></h3> <cite> </cite></div></li><li class="last"><div><a href="http://shine.yahoo.com/channel/health/can-wearing-thongs-be-unhealthy-2471642/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="139" height="139" alt="Yuck: Why thong underwear is so unhealthy" class="lz"/></a> <a href="http://shine.yahoo.com/channel/health/can-wearing-thongs-be-unhealthy-2471642/" class="lnk">Yuck: Why thong underwear is so unhealthy</a></h3> <cite> </cite></div></li>
		</ul>
	</div>
	<div class="ft"><a href="/channel/health/" class="more tr">More in Healthy Living &raquo;</a></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-ch-L4", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/139*139c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fhealth%2F139.college_classroom.jpg","http://l.yimg.com/jn/util/anysize/139*139c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fhealth%2F139.breast_cancer.jpg","http://l.yimg.com/jn/util/anysize/139*139c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fhealth%2F139.feet_scale3.jpg","http://l.yimg.com/jn/util/anysize/139*139c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fhealth%2F139.thong.jpg"]});</script>

            <div class="cls col-bg">
                <div class="col">
                    
<div class="mod channel ch-L5" id="ylf-ch-L5">
	<div class="hd"><h2><a href="/channel/beauty/">Fashion + Beauty</a></h2></div>
	<div class="bd sm-img">
				<a href="http://shine.yahoo.com/channel/beauty/8-great-spring-shoes-you-can-walk-in-2471673/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="92" height="110" alt="Spring shoes you can actually walk in" class="lz"/></a>
		<div class="title">
			<h3><a href="http://shine.yahoo.com/channel/beauty/8-great-spring-shoes-you-can-walk-in-2471673/">Spring shoes you can actually walk in</a></h3>
			<cite> </cite>
		</div>
		<ul class="blt">
			<li><div class="cls"><a href="http://shine.yahoo.com/channel/beauty/is-your-waxer-a-blabbermouth-2471657/">Is your waxer a blabbermouth?</a></div></li><li><div class="cls"><a href="http://shine.yahoo.com/event/springfashion/10-easy-ways-to-incorporate-color-into-your-wardrobe-2469947/#photoViewer=1">10 easy ways to bring color into your wardrobe</a></div></li><li><div class="cls"><a href="/channel/beauty/why-everyone-should-own-dry-shampoo-2470369/">Dry shampoo: why you'll want this $3 miracle</a></div></li>
		</ul>
	</div>
	<div class="ft"><a href="/channel/beauty/" class="more">More in Fashion + Beauty &raquo;</a></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-ch-L5", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/92*110c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Ffashion%2Fnude-platforms.jpg"]});</script>

                </div>
                <div class="col-c">
                    
<div class="mod channel ch-C3" id="ylf-ch-C3">
	<div class="hd"><h2><a href="/channel/life">manage your life</a></h2></div>
	<div class="bd">
				<a href="/channel/life/10-ways-to-end-boring-errands-2471664/;_ylt=AjWgK7gZ8w8_XxUb_u7oO4eifqU5" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="300" height="172" alt="10 boring errands you can avoid" class="lz"/></a>
		<div class="title">
			<h3><a href="/channel/life/10-ways-to-end-boring-errands-2471664/;_ylt=AjWgK7gZ8w8_XxUb_u7oO4eifqU5">10 boring errands you can avoid</a></h3>
			<cite> </cite>
		</div>
		<ul class="blt">
			<li><div class="cls"><a href="/channel/life/katie-couric-reportedly-leaving-cbs-why-was-charlie-sheen-given-more-shots-2471794/">Why did Charlie Sheen get more of a shot than Katie Couric?</a></div></li><li><div class="cls"><a href="/channel/life/international-pillow-fight-day-was-this-weekend-and-man-did-it-look-fun-video-2471654/;_ylt=AtBm98xK2j0geNw47Gv.lYyifqU5">Looks like fun! Videos from International Pillow Fight Day</a></div></li><li><div class="cls"><a href="/channel/life/single-women-life-insurance-2470750/;_ylt=AqSrvNpwYDxKNf65RM.kW4CifqU5">Do single women still need life insurance?</a></div></li>
		</ul>
	</div>
	<div class="ft"><a href="/channel/life" class="more">More in manage your life &raquo;</a></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-ch-C3", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/300*172c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fmyl%2Flongtodolist.jpg"]});</script>

                </div>
            </div>
            
<div class="mod channel ch-L7 line-top cls" id="ylf-ch-L7">
	<div class="hd"><h2>buzz on shine</h2></div>
	<div class="bd">
		
		<ul class="cols col3">
			<li><div><a href="/event/the-thread/stock-up-for-spring-with-allures-quot-best-of-beauty-quot-picks-2466981/" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="203" height="153" alt="Stock Up on These Spring Beauty Must-Haves" class="lz"/></a> <a href="/event/the-thread/stock-up-for-spring-with-allures-quot-best-of-beauty-quot-picks-2466981/" class="lnk">Stock Up on These Spring Beauty Must-Haves</a></h3> <cite> </cite></div></li><li><div><a href="http://www.facebook.com/home.php#!/yahoothethread?sk=app_151965248194515" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="203" height="153" alt="Enter to Win a Thread Makeover!" class="lz"/></a> <a href="http://www.facebook.com/home.php#!/yahoothethread?sk=app_151965248194515" class="lnk">Enter to Win a Thread Makeover!</a></h3> <cite> </cite></div></li><li class="last"><div><a href="/event/momentsofmotherhood/do-your-kids-play-nice-2460923" class="img"><img src="http://l.yimg.com/jn/images/20110318042012/lz.png" width="203" height="153" alt="This Week in MOM: Do your kids play nice?" class="lz"/></a> <a href="/event/momentsofmotherhood/do-your-kids-play-nice-2460923" class="lnk">This Week in MOM: Do your kids play nice?</a></h3> <cite> </cite></div></li>
		</ul>
	</div>
	<div class="ft"></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-ch-L7", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/203*153c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fthethread%2F1%2Ffeat.thread_ep235.jpg","http://l.yimg.com/jn/util/anysize/203*153c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fthethread%2F1%2Ffeat.thread_makeover.jpg","http://l.yimg.com/jn/util/anysize/203*153c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fmom%2Ffeat.mom_ep15.jpg"]});</script>

            
        </div>
        <div class="side">
            	<div id="ylf-you" class="mod">
		<div class="hd cls">
					
		<div class="greets">
			
			<div class="out"><a href="http://login.yahoo.com/config/login?.src=shine&.done=http%3A%2F%2Fshine.yahoo.com%2F" class="first">Sign In</a> for personalized information <p class="new-user">New User? <a href="https://edit.yahoo.com/registration?.intl=us&new=1&.src=shine&.done=http%3A%2F%2Fshine.yahoo.com%2F">Sign Up</a></p></div>
		</div>
		</div>
		<div class="bd">
			<ul class="cls">
				<li class="email"><a href="http://mail.yahoo.com/">Mail</a></li>
				<li class="weath sep" title=""><a href="http://weather.yahoo.com/">Weather</a></li>
				<li class="horo sep"><a href="/astrology/">Horoscopes</a></li>
			</ul>
						<p>Let’s talk:
				
				<a href="/manage/" class="nobg">Manage</a>
				<a href="/my/">My Stuff</a>
				<a href="/write/">Write a Post</a>
			</p>
		</div>
	</div>
            <div><!-- SpaceID=0 robot -->
</div>
			<div id="FBLikeButton" class="mod line" style="height:120px;"></div>

<script>

    YAHOO.util.Event.on(window, "load", function(){
        
        (function(){
            var height = "80",
                width = "300",
                html, imageURL, likeURL;
                
            if (window.location.href.match(/\/the-thread\//)) {
                imageURL = '/images/thethreadonfacebook.jpg';
                likeURL = "http://facebook.com/yahoothethread";
            }
            else {
                imageURL = '/images/shineonfacebook.jpg';
                likeURL = "http://facebook.com/yahooshine";
            }

            html =  '<img src="' + imageURL + '" height="45"> <iframe src="http://www.facebook.com/plugins/like.php?href='  + likeURL +  '&amp;layout=standard&amp;show_faces=true&amp;width=' + width + '&amp;action=like&amp;colorscheme=light&amp;height=' + height + '" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:' + width + 'px; height:' + height + 'px;" allowTransparency="true"></iframe>';

            YAHOO.util.Dom.get('FBLikeButton').innerHTML = html;
        })();
        
    });
    
    
</script>


            
<div id="ylf-popular" class="mod line">
	<div class="hd"><h2>Most Popular <span>On Shine</span></h2></div>
	<div class="bd">
		
		<ul id="lifestyles-tab" class="blt">
			<li><a href="/channel/parenting/twin-baby-boys-have-a-conversation-2469953/">Twin Baby Boys Have a Conversation!</a><span> Wed Mar 30, 2011 1:15pm PDT</span></li><li><a href="/channel/health/the-safest-states-in-the-united-states-2469686/">The "Safest" States in the United States</a><span> Tue Mar 29, 2011 9:52pm PDT</span></li><li><a href="/channel/parenting/when-no-one-can-pronounce-your-kid-s-name-2469569/">When no one can pronounce your kid's name</a><span> Tue Mar 29, 2011 11:54am PDT</span></li><li><a href="/channel/health/menstrual-cups-are-not-the-period-dream-we-ve-been-sold-2470344/">Menstrual cups are not the period dream we've been sold</a><span> Thu Mar 31, 2011 1:28pm PDT</span></li><li><a href="/channel/sex/prince-william-won-t-wear-a-wedding-ring-is-this-okay-2470340/">Prince William Won't Wear A Wedding Ring: Is This Okay?</a><span> Thu Mar 31, 2011 1:13pm PDT</span></li>
		</ul>

	</div>
	<div class="ft"><a class="more" href="/popular/">More Most Popular on Shine &raquo;</a></div></div>
            
            
<div id="ylf-vitality" class="mod line">
	<div class="hd"><h2>Updates <span>Chatter on Shine&hellip;</span></h2></div>
	<div class="bd">
		<ul class="thumbs">
						<li>
				<a class="img" href="/blog/NKFVFWPAO2GQB4PH2JSMTMHKXU/"><img width="32" height="32" alt="BettyConfidential.com’s Avatar" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<a href="http://us.rd.yahoo.com//Updates/wisteria/blog/SIG=1a7gdkojk/*http%3A//shine.yahoo.com/channel/none/get-nicole-kidmans-curls-2472064/;_ylc=X3oDMTViMWMwMm9jBElfYWd1aWQDTktGVkZXUEFPMkdRQjRQSDJKU01UTUhLWFUESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5tZWRpYS5zaGluZS5oZ3Atc2hpbmVfYXBpBElfdWNudHgDZ2xvYmFsBElfdXNyYwN3aXN0ZXJpYQRJX3VzdWlkA3dpc3RlcmlhI2Jsb2cjMjQ3MjA2NARJX3V0eXBlA2Jsb2cEX1MDMjAyMzQzNTI2MQ--" class="title">Get Nicole Kidman&#39;s Curls</a>
					<cite>by <a href="/blog/NKFVFWPAO2GQB4PH2JSMTMHKXU/" class="author">BettyConfidential.com</a> <span class="rel-time" title="Tue Apr 5, 2011 8:03am PDT">8 minutes ago</span></cite>
					
				</div>
			</li>
			<li>
				<a class="img" href="/blog/4MYSDX4AFSU26VSI4N6TXSOUMY/"><img width="32" height="32" alt="Marjorie Orr for Astrology.com’s Avatar" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<a href="http://us.rd.yahoo.com//Updates/wisteria/blog/SIG=1a7joa2bg/*http%3A//shine.yahoo.com/channel/sex/meet-the-parents-properly-2472060/;_ylc=X3oDMTViZTZjaDE4BElfYWd1aWQDNE1ZU0RYNEFGU1UyNlZTSTRONlRYU09VTVkESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5tZWRpYS5zaGluZS5oZ3Atc2hpbmVfYXBpBElfdWNudHgDZ2xvYmFsBElfdXNyYwN3aXN0ZXJpYQRJX3VzdWlkA3dpc3RlcmlhI2Jsb2cjMjQ3MjA2MARJX3V0eXBlA2Jsb2cEX1MDMjAyMzQzNTI2MQ--" class="title">Meet the Parents -- Properly!</a>
					<cite>by <a href="/blog/4MYSDX4AFSU26VSI4N6TXSOUMY/" class="author">Marjorie Orr for Astrology.com</a> <span class="rel-time" title="Tue Apr 5, 2011 7:58am PDT">13 minutes ago</span></cite>
					
				</div>
			</li>
			<li>
				<a class="img" href="/blog/Y757EVWE5N2IN3FKBRDJEPQMXM/"><img width="32" height="32" alt="Julie Strval’s Avatar" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<a href="http://us.rd.yahoo.com//Updates/wisteria/blog/SIG=1b5rfrg85/*http%3A//shine.yahoo.com/channel/none/is-he-physically-attracted-to-me-or-not-7-tips-to-know-2472059/;_ylc=X3oDMTViY2UycDJjBElfYWd1aWQDWTc1N0VWV0U1TjJJTjNGS0JSREpFUFFNWE0ESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5tZWRpYS5zaGluZS5oZ3Atc2hpbmVfYXBpBElfdWNudHgDZ2xvYmFsBElfdXNyYwN3aXN0ZXJpYQRJX3VzdWlkA3dpc3RlcmlhI2Jsb2cjMjQ3MjA1OQRJX3V0eXBlA2Jsb2cEX1MDMjAyMzQzNTI2MQ--" class="title">Is He Physically Attracted to Me or Not? ---7 Tips To Know</a>
					<cite>by <a href="/blog/Y757EVWE5N2IN3FKBRDJEPQMXM/" class="author">Julie Strval</a> <span class="rel-time" title="Tue Apr 5, 2011 7:58am PDT">13 minutes ago</span></cite>
					
				</div>
			</li>
			<li>
				<a class="img" href="/blog/OJXPNQFZDBOWVKXJPZHWUAKI7Q/"><img width="32" height="32" alt="InStyle.com’s Avatar" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<a href="http://us.rd.yahoo.com//Updates/wisteria/blog/SIG=1bdo2u0po/*http%3A//shine.yahoo.com/channel/beauty/kate-middleton-s-tiara-jewelry-designers-sketch-dream-tiaras-2472052/;_ylc=X3oDMTVidmxzdDRtBElfYWd1aWQDT0pYUE5RRlpEQk9XVktYSlBaSFdVQUtJN1EESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5tZWRpYS5zaGluZS5oZ3Atc2hpbmVfYXBpBElfdWNudHgDZ2xvYmFsBElfdXNyYwN3aXN0ZXJpYQRJX3VzdWlkA3dpc3RlcmlhI2Jsb2cjMjQ3MjA1MgRJX3V0eXBlA2Jsb2cEX1MDMjAyMzQzNTI2MQ--" class="title">Kate Middleton’s Tiara: Jewelry Designers Sketch Dream Tiaras</a>
					<cite>by <a href="/blog/OJXPNQFZDBOWVKXJPZHWUAKI7Q/" class="author">InStyle.com</a> <span class="rel-time" title="Tue Apr 5, 2011 7:54am PDT">17 minutes ago</span></cite>
					
				</div>
			</li>
			<li>
				<a class="img" href="/blog/5EPM4C5RXAOAEQVEMEFOARWRVA/"><img width="32" height="32" alt="The Editors of EatingWell Magazine’s Avatar" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<a href="http://us.rd.yahoo.com//Updates/wisteria/blog/SIG=1a8cq2fdt/*http%3A//shine.yahoo.com/channel/health/4-foods-that-fight-pain-2472055/;_ylc=X3oDMTVidGo3MDdtBElfYWd1aWQDNUVQTTRDNVJYQU9BRVFWRU1FRk9BUldSVkEESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5tZWRpYS5zaGluZS5oZ3Atc2hpbmVfYXBpBElfdWNudHgDZ2xvYmFsBElfdXNyYwN3aXN0ZXJpYQRJX3VzdWlkA3dpc3RlcmlhI2Jsb2cjMjQ3MjA1NQRJX3V0eXBlA2Jsb2cEX1MDMjAyMzQzNTI2MQ--" class="title">4 foods that fight pain</a>
					<cite>by <a href="/blog/5EPM4C5RXAOAEQVEMEFOARWRVA/" class="author">The Editors of EatingWell Magazine</a> <span class="rel-time" title="Tue Apr 5, 2011 7:50am PDT">21 minutes ago</span></cite>
					
				</div>
			</li>
			<li>
				<a class="img" href="/blog/CFXB5HFXQ42ZWX7OVESTQXJCRA/"><img width="32" height="32" alt="amy’s Avatar" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<a href="http://us.rd.yahoo.com//Updates/wisteria/blog/SIG=1ajinpk8m/*http%3A//shine.yahoo.com/channel/life/getting-over-the-hurt-please-help-me-2472053/;_ylc=X3oDMTViNjNxbHBmBElfYWd1aWQDQ0ZYQjVIRlhRNDJaV1g3T1ZFU1RRWEpDUkEESV9jZ3VpZAMESV9jcHJvcAN5YWhvby5tZWRpYS5zaGluZS5oZ3Atc2hpbmVfYXBpBElfdWNudHgDZ2xvYmFsBElfdXNyYwN3aXN0ZXJpYQRJX3VzdWlkA3dpc3RlcmlhI2Jsb2cjMjQ3MjA1MwRJX3V0eXBlA2Jsb2cEX1MDMjAyMzQzNTI2MQ--" class="title">Getting Over The Hurt (Please Help Me)</a>
					<cite>by <a href="/blog/CFXB5HFXQ42ZWX7OVESTQXJCRA/" class="author">amy</a> <span class="rel-time" title="Tue Apr 5, 2011 7:50am PDT">21 minutes ago</span></cite>
					
				</div>
			</li>

		</ul>
	</div>
	<div class="ft"><a class="more" href="/chatter/">See More Chatter &raquo;</a></div>
</div>
<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-vitality", cl:"lz", imgs:["http://a323.yahoofs.com/coreid/49bd2111i23cbzul2re3/yw7KJiEzfrKGqrRFt0fapF6IaD9KUdErng--/4/tn48.jpg?ciAw8eOBW1DRnK4_","http://a323.yahoofs.com/coreid/4ace25dai80bzul4sp1/DZlqwx8waLJdgg2Wu7W.gA--/1/tn48.jpg?ciAw8eOBEK3cibzh","http://l.yimg.com/a/i/identity2/profile_48a.png","http://a323.yahoofs.com/coreid/49bb8045id05zul1re3/AdhNlxs4dbX6Ra4LkgSj90F6/1/tn48.jpg?ciAw8eOB0KxKMH5C","http://a323.yahoofs.com/coreid/4d6bb53di18f2zws102ac4/ATt6kfM8cqUjIR6nxErj6GqtCw--/19/tn48.jpeg?ciAw8eOBs8BK4XxG","http://avatars.zenfs.com/users/1pMAntktbAAEC4QSDfu_0DQ==.medium.png"]});</script>
            
<div id="ylf-pick" class="mod line">
	<div class="hd"><h2>Manage Your Life<br>Pick of the Day</h2></div>
	<div class="bd">
		<a class="img" href="http://shine.yahoo.com/channel/life/international-pillow-fight-day-was-this-weekend-and-man-did-it-look-fun-video-2471654/"><img width="300" height="172" alt="International Pillow Fight Day sure looks fun... (video)" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
		<h3><a href="http://shine.yahoo.com/channel/life/international-pillow-fight-day-was-this-weekend-and-man-did-it-look-fun-video-2471654/">International Pillow Fight Day sure looks fun... (video)</a></h3>
		<ul class="thumbs">
			<li>
				<div class="txt">
					<a href="http://shine.yahoo.com/channel/life/international-pillow-fight-day-was-this-weekend-and-man-did-it-look-fun-video-2471654/">Over the weekend, organized mobs of folks gathered in more than 100 cities to participate in a new holiday they're calling International Pillow Fight Day. Once there they went all-out at each other with feather-filled sacks. The results are exuberant, childlike, playful—basically just everything good.</a>
					<cite> Posted by <a href="http://shine.yahoo.com/blog/V24YRUKBAO3ZUHWAZFBBTUW2NA/" class="by">Jennifer Romolini, Shine Staff</a></cite>
				</div>
			</li>
		</ul>
	</div>
	<div class="ft"></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-pick", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/300*172c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fmyl%2Finternational-pillow-fight-day.jpg"]});</script>

            
<div id="ylf-ad" class="mod line">
	<div class="hd"><h2>healthy byte</h2></div>
	<div class="bd">
		<p>How to stay focused (and healthy) at the supermarket</p>
	</div>
	<div class="ft cls">
		<a class="more" href="http://www.shine.yahoo.com/channel/health/stay-focused-at-the-supermarket-2468105/">5 quick tips &raquo;</a>
		<div id="ylf-btn2"><!-- SpaceID=0 robot -->
</div>
	</div>
</div>

            
<div id="ylf-discover" class="mod line">
	<div class="hd"><h2>More on Yahoo!</h2></div>
	<div class="bd">
		<ul class="thumbs">
			
			<li>
				<a class="img" href="http://shine.yahoo.com/event/the-thread/nows-your-chance-get-made-over-in-manhattan-with-the-thread-2461127/"><img width="74" height="74" alt="Get made over in NYC with The Thread" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<h4><a class="title" href="http://shine.yahoo.com/event/the-thread/nows-your-chance-get-made-over-in-manhattan-with-the-thread-2461127/">Get made over in NYC with The Thread</a></h4>
					<a class="more" href="http://shine.yahoo.com/event/the-thread/nows-your-chance-get-made-over-in-manhattan-with-the-thread-2461127/">The ultimate makeover &raquo;</a>
				</div>
			</li>
			<li>
				<a class="img" href="http://omg.yahoo.com/photos/red-carpet-report-card-2011-acm-awards/4712"><img width="74" height="74" alt="2011 ACM Awards" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<h4><a class="title" href="http://omg.yahoo.com/photos/red-carpet-report-card-2011-acm-awards/4712">2011 ACM Awards</a></h4>
					<a class="more" href="http://omg.yahoo.com/photos/red-carpet-report-card-2011-acm-awards/4712">Red Carpet Report Card &raquo;</a>
				</div>
			</li>
			<li>
				<a class="img" href="http://www.mixingbowl.com/group/contest/view.castle?g=2707105"><img width="74" height="74" alt="Easter dessert contest" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<h4><a class="title" href="http://www.mixingbowl.com/group/contest/view.castle?g=2707105">Easter dessert contest</a></h4>
					<a class="more" href="http://www.mixingbowl.com/group/contest/search.castle">More recipe categories &raquo;</a>
				</div>
			</li>
			<li>
				<a class="img" href="http://www.appolicious.com/shine/articles/7208-tasty-iphone-apps-for-vegetarians"><img width="74" height="74" alt="Tasty iPhone apps for vegetarians" src="http://l.yimg.com/jn/images/20110318042012/lz.png" class="lz"/></a>
				<div class="txt">
					<h4><a class="title" href="http://www.appolicious.com/shine/articles/7208-tasty-iphone-apps-for-vegetarians">Tasty iPhone apps for vegetarians</a></h4>
					<a class="more" href="http://www.appolicious.com/shine/articles/7208-tasty-iphone-apps-for-vegetarians">See more apps &raquo;</a>
				</div>
			</li>
		</ul>
	</div>
	<div class="ft"></div>
</div>

<script type="text/javascript" charset="UTF-8">YAHOO.Lifestyles.ImageLoader.delayByView({el:"ylf-discover", cl:"lz", imgs:["http://l.yimg.com/jn/util/anysize/74*74c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fmoreon%2F74.the_thread.jpg","http://l.yimg.com/jn/util/anysize/74*74c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fmoreon%2F74.taylor_swiftaca.jpg","http://l.yimg.com/jn/util/anysize/74*74c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fmoreon%2F74.easter_dessert.jpg","http://l.yimg.com/jn/util/anysize/74*74c-86400,http%3A%2F%2Fl.yimg.com%2Fa%2Fi%2Fus%2Fshine%2Fmoreon%2F74.veggies_bowl.jpg"]});</script>

            
        </div>
    </div>
    
    <div id="ylf-super-footer">
	<div class="mn-hd"><h2>Right Now on Shine</h2></div>
	<div class="mn-bd">
		<div class="first row cls">
			
			<div class="mod">
				<div class="hd">
					<h2><a href="/channel/life">Manage Your Life</a></h2>
				</div>
				<div class="bd">
					<h3><a href="/channel/life/how-to-survive-getting-lost-in-the-woods-2471647/">How to survive getting lost in the woods</a></h3>
					<ul class="blt">
						<li><a href="/channel/life/medical-credit-cards-life-savers-or-money-mistakes-2471649/">Medical credit cards:  Life savers or money mistakes?</a></li><li><a href="/channel/life/5-last-minute-tax-tips-2471675/">5 Last-Minute Tax Tips</a></li>
					</ul>
				</div>
				<div class="ft"><a class="more" href="/channel/life">More in Manage Your Life &raquo;</a></div>
			</div>

			
			<div class="mod col2">
				<div class="hd">
					<h2><a href="/channel/beauty/">Fashion + Beauty</a></h2>
				</div>
				<div class="bd">
					<h3><a href="/channel/beauty/user-post-espadrille-fever-2471907/">User Post: Espadrille Fever</a></h3>
					<ul class="blt">
						<li><a href="/channel/beauty/your-spanx-are-squeezing-you-sick-2471787/">Your Spanx are squeezing you sick</a></li><li><a href="/channel/beauty/spring-2011-shopping-trend-all-white-everything-2470201/">Spring 2011 Shopping Trend: All White Everything</a></li>
					</ul>
				</div>
				<div class="ft"><a class="more" href="/channel/beauty/">More in Fashion + Beauty &raquo;</a></div>
			</div>

			
			<div class="mod">
				<div class="hd">
					<h2><a href="/channel/health/">Healthy Living</a></h2>
				</div>
				<div class="bd">
					<h3><a href="/channel/health/surprise-eat-sugar-to-lose-weight-10-tasty-snacks-included-2471761/">Surprise! Eat Sugar to Lose Weight (10 Tasty Snacks Included)</a></h3>
					<ul class="blt">
						<li><a href="/channel/health/10-depression-fighting-mood-boosters-2470707/">10 Depression-Fighting Mood-Boosters</a></li><li><a href="/channel/health/army-wives-and-widows-run-to-cope-2471631/">Army wives and widows run to cope</a></li>
					</ul>
				</div>
				<div class="ft"><a class="more" href="/channel/health/">More in Healthy Living &raquo;</a></div>
			</div>

		</div>
		<div class="row cls">
			
			<div class="mod">
				<div class="hd">
					<h2><a href="/channel/parenting/">Parenting</a></h2>
				</div>
				<div class="bd">
					<h3><a href="/channel/parenting/parenting-guru-the-5-year-milestone-birthday-2470652/">Parenting Guru: The 5-year milestone birthday</a></h3>
					<ul class="blt">
						<li><a href="/channel/parenting/should-you-burn-old-love-letters-or-leave-them-to-your-kids-2471754/">Should You Burn Old Love Letters or Leave Them to Your Kids?</a></li><li><a href="/channel/parenting/parenting-guru-skipping-spring-break-are-you-guilty-2470043/">Parenting Guru: Skipping Spring Break; Are you Guilty?</a></li>
					</ul>
				</div>
				<div class="ft"><a class="more" href="/channel/parenting/">More in Parenting &raquo;</a></div>
			</div>

			
			<div class="mod col2">
				<div class="hd">
					<h2><a href="/channel/sex/">Love + Sex</a></h2>
				</div>
				<div class="bd">
					<h3><a href="/channel/sex/it-s-complicated-advice-q-a-my-brother-in-law-is-hurting-my-marriage-2471601/">It's Complicated Advice Q&A: My Brother-in-Law is Hurting My Marriage</a></h3>
					<ul class="blt">
						<li><a href="/channel/sex/can-soul-mates-come-in-any-color-2471590/">Can soul mates come in any color?</a></li><li><a href="/channel/sex/user-post-in-response-to-can-she-ever-forget-her-first-love-2471336/">User post: In Response to "Can She Ever Forget Her First Love?"</a></li>
					</ul>
				</div>
				<div class="ft"><a class="more" href="/channel/sex/">More in Love + Sex &raquo;</a></div>
			</div>

			
			<div class="mod">
				<div class="hd">
					<h2><a href="/channel/food/">Food</a></h2>
				</div>
				<div class="bd">
					<h3><a href="/channel/food/5-meat-free-dinners-for-lent-2470673/">5 Meat-Free Dinners for Lent</a></h3>
					<ul class="blt">
						<li><a href="/channel/food/how-long-should-i-keep-spices-2471854/">How long should I keep spices?</a></li><li><a href="/channel/food/insane-food-and-drink-laws-2470009/">Insane Food and Drink Laws</a></li>
					</ul>
				</div>
				<div class="ft"><a class="more" href="/channel/food/">More in Food &raquo;</a></div>
			</div>

		</div>
		<div class="row last cls">
			
			<div class="mod">
				<div class="hd">
					<h2><a href="/event/the-thread/">The Thread</a></h2>
				</div>
				<div class="bd">
					<h3><a href="/event/the-thread/how-to-decode-a-wedding-invitation-so-you-arrive-in-style-2470211/">How to decode a wedding invitation so you arrive in style</a></h3>
					<ul class="blt">
						<li><a href="/event/the-thread/hollywoods-incredible-shape-shifting-celebrities-2470209/">Hollywood&#39;s incredible shape-shifting celebrities</a></li><li><a href="/event/the-thread/jake-gyllenhaal-cracks-the-style-quot-code-quot-2469981/">Jake Gyllenhaal cracks the style &quot;code&quot;</a></li>
					</ul>
				</div>
				<div class="ft"><a class="more" href="/event/the-thread/">More in The Thread &raquo;</a></div>
			</div>

			
			<div class="mod col2">
				<div class="hd">
					<h2><a href="/event/momentsofmotherhood">Moments of Motherhood</a></h2>
				</div>
				<div class="bd">
					<h3><a href="/event/momentsofmotherhood/parenting-guru-more-sleep-less-laundry-spring-break-2471696/">Parenting Guru: More sleep + less laundry= Spring Break</a></h3>
					<ul class="blt">
						<li><a href="/event/momentsofmotherhood/parenting-guru-planes-trains-and-automobiles-with-kids-2470767/">Parenting Guru: Planes, Trains and Automobiles (with kids)</a></li><li><a href="/event/momentsofmotherhood/parenting-guru-my-5-tips-for-traveling-with-kids-2470747/">Parenting Guru:  My 5 tips for traveling with kids</a></li>
					</ul>
				</div>
				<div class="ft"><a class="more" href="/event/momentsofmotherhood">More in Moments of Motherhood &raquo;</a></div>
			</div>

			
			<div class="mod fav-sites">
				<div class="hd">
					<h2>Favorite Sites</h2>
				</div>
				<div class="bd">
					<ul class="blt">
						<li><a href="http:http://www.allure.com/">Allure</a></li><li><a href="http://www.bonappetit.com/">Bon Appétit</a></li><li><a href="http://www.cosmopolitan.com/">Cosmopolitan</a></li><li><a href="http://eatingwell.com/">Eating Well</a></li><li><a href="http://www.forbes.com/">Forbes</a></li><li><a href="http://www.glamour.com/">Glamour</a></li>
					</ul>
					<ul class="blt">
						<li><a href="http://www.health.com/health/">Health.com</a></li><li><a href="http://www.prevention.com/cda/homepage.do">Prevention</a></li><li><a href="http://www.realbeauty.com/">Real Beauty</a></li><li><a href="http://www.self.com/">Self Magazine</a></li><li><a href="http://thestir.cafemom.com/">The Stir</a></li><li><a href="http://www.totalbeauty.com/">Total Beauty</a></li><li><a href="http://www.womansday.com/">Woman's Day</a></li>
					</ul>
				</div>
				<div class="ft"><!-- --></div>
			</div>

		</div>
	</div>
</div>
    <!-- SpaceID=0 robot -->

    <!-- SpaceID=0 robot -->

    <!-- SpaceID=0 robot -->

    
<!-- Advertiser 'Yahoo! - Shine',  Include user in segment 'Shine Remarketing Homepage' - DO NOT MODIFY THIS PIXEL
IN ANY WAY -->
<img src="http://ad.yieldmanager.com/pixel?id=121491&t=2" width="1" height="1"/>
<!-- End of segment tag -->
</div>
</body>
</html>

<!-- Shine:myservice-us:0:Success -->
<!-- fe8.shine.ac4.yahoo.com uncompressed/chunked Tue Apr  5 08:12:13 PDT 2011 -->
