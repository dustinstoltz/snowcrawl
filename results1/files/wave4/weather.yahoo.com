<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-US">
<head>
    	<!--
    ************************** NOTICE *******************************
    The data (conditions, forecasts, news, images, logos) contained in
    this page are copyrighted by Yahoo! Inc. and the Weather Channel
    Enterprises, Inc. You are prohibited from using or repurposing this
    data in any way without express written consent from Yahoo! Inc. and
    the Weather Channel Enterprises, Inc.
    If you are looking for a source of weather data, please see the
    National Weather Service Website at http://www.nws.noaa.gov/. Their
    data is public information, to be used with appropriate
    byline/photo/image credits.
    ************************** NOTICE *******************************
    -->
    <title>Yahoo! Weather - Weather Forecasts | Maps | News</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content="View the latest weather forecasts, maps, news and alerts on Yahoo! Weather. Find local weather forecasts for the USA and cities throughout the world." name="description" />
	<meta content="weather forecasts, weather reports, weather maps, usa weather forecast, worldwide weather forecast, local weather forecast, 5 day forecasts, 10 day forecasts, news, weather alerts, yahoo weather, yahoo" name="keywords"/>
    
<link rel="stylesheet" href="http://l.yimg.com/a/lib/ywc/css/weather.6.6.min.css"></link>
</head>
<body>
<div id="doc" class="yui-t6">
    <div id="hd">
	    <!-- SpaceID=0 robot -->

		<div id="yw-north" align="center"><!-- SpaceID=0 robot -->
</div>
        <ul id="yw-hdmenu"><li class="first on"><div><a href="http://news.yahoo.com/"><b>Home</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/us"><b>U.S.</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/business"><b>Business</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/world"><b>World</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/entertainment"><b>Entertainment</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/sports"><b>Sports</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/technology"><b>Tech</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/politics"><b>Politics</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/science"><b>Science</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/health"><b>Health</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/travel"><b>Travel</b></a></div></li><li class=""><div><a href="http://news.yahoo.com/most-popular"><b>Most Popular</b></a></div></li></ul><ul id="yw-submenu"><li class="first"><a href="http://news.yahoo.com/video">Video</a></li><li class=""><a href="http://news.yahoo.com/photos">Photos</a></li><li class=""><a href="http://news.yahoo.com/blog">Blog</a></li><li class=""><a href="http://news.yahoo.com/opinion">Opinion</a></li><li class=""><a href="http://news.yahoo.com/local">Local</a></li><li class=""><a href="http://news.yahoo.com/odd">Odd News</a></li><li class=""><a href="http://news.yahoo.com/comics">Comics</a></li><li class="on"><a href="http://weather.yahoo.com/">Weather</a></li><li class=""><a href="http://news.yahoo.com/you-witness">You Witness News</a></li><li class=""><a href="http://news.yahoo.com/health/vitality">Vitality</a></li><li class=""><a href="http://news.yahoo.com/page/sitemap">Site Index</a></li></ul>
        
		<div id="yw-hdsearch"><h2>Search</h2><form class="sch" action="http://news.search.yahoo.com/news/search" id="ynw-s1" name="search" method="get" target="_top" ><div><fieldset><legend>Yahoo! News</legend><label for="p">Search:</label>&nbsp;<input class="text" type="text" value="" size="40" name="p" id="p"/>&nbsp;<label for="c" class="hdn">in</label><select name="c" id="c"><option value="">All News</option><option value="yahoo_news" selected>Yahoo! News Only</option><option value="images">News Photos</option><option value="av">Video/Audio</option></select>&nbsp;<input type="submit" title="Search" value="Search" class="submit"/><input type="hidden" value="UTF-8" name="ei"/>&nbsp;<a href="http://news.search.yahoo.com/news/advanced">Advanced</a></fieldset></div></form></div>       
		
        <div id="yw-headtitle">
            <div class="yw-weatherlogo">
                <!-- SpaceID=0 robot -->
 
            </div>
<div class="yw-ulmwrap"><h1>Weather Forecasts</h1></div><div class="yw-wbadge"><a href="/badge/">Add weather on your website</a></div></div>        
    </div>
    <div id="bd" class="clear">
        <div class="yui-t6">
            <div id="yui-main">
                <div class="yui-b">
					<div class="yui-index-search">
	<form name="pform" id="pform" action="/search/weather" method="get" onsubmit="return searchVal('pform');">
	<fieldset><legend>Yahoo! weather Search</legend>
	<div id="yw-search">Enter city or zip code:</div>
		<div class="search-frmbdy">
	<label class="hdn" for="loca-location">Search</label><input type="textbox" value="" autocomplete="off" id="loca-location" name="location" class="size1 location-widget-trigger"/><label class="hdn" for="t">&nbsp;</label><input name="" type="t" id="t" value="" class="hdn"><input id="loca-submit" type="submit" value="&nbsp;Go&nbsp;" class="size1 location-widget-trigger"/><br \=""/>        </div>
	<div id="yw-search-frmbdy"><!-- --></div>	</fieldset>
    </form>

<script type="text/javascript">
     var lwLocale  = "en-US";
	 var lwLang    = "ENG";
	 var lwCountry = "US";
	 document.getElementById('loca-location').focus();
	 var searchVal = function(form) {
		return false;
	 }
</script></div>
                    
					
					<div id="yw-worldweather" class="yw-categories"><h2>Browse for U.S. and International Forecasts</h2><ul class="columns"><li><a href="/united-states/">United States</a></li><li><a href="/regional/africa/">Africa</a></li><li><a href="/regional/asia/">Asia</a></li></ul><ul class="columns"><li><a href="/regional/australia/">Australia</a></li><li><a href="/regional/europe/">Europe</a></li><li><a href="/regional/north-america/">North America</a></li></ul><ul class="columns"><li><a href="/regional/oceania/">Oceania</a></li><li><a href="/regional/south-america/">South America</a></li><li><a href="/regional/united-kingdom/">United Kingdom</a></li></ul></div>
                    <div id="yw-alerts">
	<strong>Get Alerts:</strong>
    <a class="mobile" href="http://us.rd.yahoo.com/weather/resources/mobilealerts/*http://alerts.yahoo.com/alerts/rdr.php?nep=weathermobile">Mobile</a>
    <a class="bulletins" href="http://us.rd.yahoo.com/weather/resources/bulletins/*http://alerts.yahoo.com/alerts/rdr.php?nep=weathernews">Weather Bulletins</a>
</div>

                    <div id="yw-weathermaps" class="yw-categories"><h2>Weather Maps</h2><div class="yui-skin-sam"><div id="ywMap" class="yui-navset"><ul class="yui-nav"><li class="selected" id="tab0Label"><a href="#tab0"><em>U.S. Regional Radar</em></a></li><li id='tab1Label'><a href='#tab1'><em>U.S. Satellite</em></a></li><li id='tab2Label'><a href='#tab2'><em>North America Outlook</em></a></li><li id='tab3Label'><a href='#tab3'><em>Current Heat Index</em></a></li></ul><div class='yui-content'><div id='tab0'><img id='img0' alt='U.S. Regional Radar' title='U.S. Regional Radar' src='/images/us_radar_medium_usen.jpg' /></div><div id='tab1'><img id='img1' alt='U.S. Satellite' title='U.S. Satellite'/><noscript><img src='/images/ussat_440x297.jpg' alt='U.S. Satellite'/></noscript></div><div id='tab2'><img id='img2' alt='North America Outlook' title='North America Outlook'/><noscript><img src='/images/na_outlookf_en_GB_440_mdy_y.jpg' alt='North America Outlook'/></noscript></div><div id='tab3'><img id='img3' alt='Current Heat Index' title='Current Heat Index'/><noscript><img src='/images/actheat_440x297.jpg' alt='Current Heat Index'/></noscript></div></div></div></div><ul class="columns"><li><a href="/imgindex/index.html">U.S. Regional</a></li></ul><ul class="columns"><li><a href="/imgindex/world.html">World</a></li></ul><ul class="columns"><li><a href="/imgindex/uscities.html">U.S. Cities</a></li></ul></div><script type='text/javascript'>var ywMap1='/images/ussat_440x297.jpg';var ywMap2='/images/na_outlookf_en_GB_440_mdy_y.jpg';var ywMap3='/images/actheat_440x297.jpg';</script>
                    <div id="yw-fullcoverage" class="yw-categories"><h2>Full Coverage</h2><ul class="columns"><li><a href="http://news.yahoo.com/topics/climate-change-and-global-warming">Climate Change</a></li></ul><ul class="columns expand"><li><a href="http://news.yahoo.com/topics/hurricanes-and-tropical-storms">Hurricanes and Tropical Storms</a></li></ul></div>
                    <div id="yw-newsfeatures"><h2>Weather News and Features</h2><h3><a href="http://us.rd.yahoo.com/dailynews/rss/weather/*http://news.yahoo.com/s/nm/20110405/us_nm/us_weather_deaths">Severe storms in Southeast states cause deaths, damage </a></h3><em>Reuters- 49 minutes ago</em><p>Reuters - Severe storms blowing across the Southeast killed at least five people in Georgia early on Tuesday, and also were to blame for deaths in two other states, authorities said.</p><div class="yw-newsoptions" id="ywNewsOptions">View:<a id="menulink_" href="#">Headlines Only</a> |<a id="menuoption2_" href="#">Include Summaries</a> |<a class="on" id="menuoption3_" href="#">Include Photos</a></div><div id="yw-news"><ul><li><div class="yw-newsimg"><a href="http://us.rd.yahoo.com/dailynews/rss/weather/*http://news.yahoo.com/s/ap/20110405/ap_on_re_as/as_japan_earthquake"><img alt="Japan sets new radiation safety level for seafood " width="130" height="106" border="0" src="http://d.yimg.com/a/p/ap/20110405/capt.e916cd8e566c47338e81b6ec2188309a-e916cd8e566c47338e81b6ec2188309a-0.jpg?x=130&y=106&q=85&sig=eUDQV3NhGqQZ7j6Uv3BPxg--"></a></div><a href="http://us.rd.yahoo.com/dailynews/rss/weather/*http://news.yahoo.com/s/ap/20110405/ap_on_re_as/as_japan_earthquake">Japan sets new radiation safety level for seafood </a><em>AP - 2 hours,  7 minutes ago</em><p>AP - The government set its first radiation safety standards for fish Tuesday after Japan's tsunami-ravaged nuclear plant reported radioactive contamination in nearby seawater measuring at several million times the legal limit.</p></li><li><a href="http://us.rd.yahoo.com/dailynews/rss/weather/*http://news.yahoo.com/s/nm/20110405/us_nm/us_weather_us_watch">Northeast squalls, parched Plains pinched </a><em>Reuters - 2 hours,  46 minutes ago</em><p>Reuters - Squalls were racing up the East Coast on Tuesday, forecast to bring rain to the Northeast from the mid-Atlantic states to New England.</p></li><li><div class="yw-newsimg"><a href="http://us.rd.yahoo.com/dailynews/rss/weather/*http://news.yahoo.com/s/nm/20110405/wl_nm/us_japan_quake_snapshot"><img alt="Snapshot: Japan's nuclear crisis " width="130" height="105" border="0" src="http://d.yimg.com/a/p/afp/20110405/capt.photo_1302009097204-1-0.jpg?x=130&y=105&q=85&sig=.79bq5E1uSL1WPWuYfv7tA--"></a></div><a href="http://us.rd.yahoo.com/dailynews/rss/weather/*http://news.yahoo.com/s/nm/20110405/wl_nm/us_japan_quake_snapshot">Snapshot: Japan's nuclear crisis </a><em>Reuters - Tue, 05 Apr 2011 02:42:43 PDT</em><p>Reuters - Following are main developments after a massive earthquake and tsunami devastated northeast Japan and crippled a nuclear power station, raising the risk of an uncontrolled radiation leak.</p></li><li><a href="http://us.rd.yahoo.com/dailynews/rss/weather/*http://news.yahoo.com/s/ap/20110405/ap_on_re_us/us_weatherpage_weather">The nation's weather </a><em>AP - Tue, 05 Apr 2011 02:04:22 PDT</em><p>AP - Severe storms continue sweeping across the Eastern half of the nation Tuesday, while wet and snowy conditions persist in the Northwest.</p></li><li><a class="newsMore" href="http://news.yahoo.com/news?tmpl=index2&cid=1112">&raquo;More weather news</a></li></ul></div></div>					
					
                </div>
            </div>
            <div class="yui-b">
				<div id="yw-weathercom">
    <h2>More from Weather.com</h2>
    <ul><li><!-- SpaceID=0 robot -->
</li><li><!-- SpaceID=0 robot -->
</li><li><!-- SpaceID=0 robot -->
</li><li><!-- SpaceID=0 robot -->
</li><li><!-- SpaceID=0 robot -->
</li><li><!-- SpaceID=0 robot -->
</li></ul><div class="weatherLogo"><!-- SpaceID=0 robot -->
</div></div>

                <div class="yui-b"><div class="yw-ad"><!-- SpaceID=0 robot -->
</div></div>
				<div class="yui-b"><div id="yw-myloc" class=" yui-skin-sam"><div class="myloc-hd" id="ywTest"><a name="saved_locations"></a><h2>Save locations here</h2><a style="display:none" id="ywSavedLoc"></a><a mapleUltPriority="1000" id="ywlogin" style="display:none" href="http://login.yahoo.com/config/login?.done=http%3A%2F%2Fweather.yahoo.com%2F&.src=yw&.intl=us">Add</a><script type="text/javascript">document.getElementById('ywlogin').style.display='block'</script></div><div id="myloc-msg"></div><div id="myLocContainer"><ul>
<li id="ywmyloc2379574">
<a href="/united-states/illinois/chicago-2379574/" title="Full forecast for Chicago, Illinois, United States (Cloudy)" >Chicago, IL</a>

							<em>34...49 &deg;F</em>

							<div class="yw-temperature">

								<div class="yw-iconcontainer">

									<div class="yw-iconbg "></div>

				 <div class="yw-wicons"><img id="wiff" style="background-position: -1586px -0px; width: 61px; height: 34px"  alt="Cloudy" src="http://l.yimg.com/a/lib/ywc/img/spacer.gif"/></div>

				</div>

							</div>

						</li>
<li id="ywmyloc44418">
<a href="/england/greater-london/london-44418/" title="Full forecast for London, Greater London, England (Fair)" >London, GB</a>

							<em>48...54 &deg;F</em>

							<div class="yw-temperature">

								<div class="yw-iconcontainer">

									<div class="yw-iconbg yw-nightbg"></div>

				 <div class="yw-wicons"><img id="wiff" style="background-position: -2013px -0px; width: 61px; height: 34px"  alt="Fair" src="http://l.yimg.com/a/lib/ywc/img/spacer.gif"/></div>

				</div>

							</div>

						</li>
<li id="ywmyloc2459115">
<a href="/united-states/new-york/new-york-2459115/" title="Full forecast for New York, New York, United States (Partly Cloudy)" >New York, NY</a>

							<em>58...67 &deg;F</em>

							<div class="yw-temperature">

								<div class="yw-iconcontainer">

									<div class="yw-iconbg "></div>

				 <div class="yw-wicons"><img id="wiff" style="background-position: -1830px -0px; width: 61px; height: 34px"  alt="Partly Cloudy" src="http://l.yimg.com/a/lib/ywc/img/spacer.gif"/></div>

				</div>

							</div>

						</li>
<li id="ywmyloc2487956">
<a href="/united-states/california/san-francisco-2487956/" title="Full forecast for San Francisco, California, United States (Partly Cloudy)" >San Francisco, CA</a>

							<em>53...68 &deg;F</em>

							<div class="yw-temperature">

								<div class="yw-iconcontainer">

									<div class="yw-iconbg "></div>

				 <div class="yw-wicons"><img id="wiff" style="background-position: -1830px -0px; width: 61px; height: 34px"  alt="Partly Cloudy" src="http://l.yimg.com/a/lib/ywc/img/spacer.gif"/></div>

				</div>

							</div>

						</li>
</ul>
</div></div><script type="text/javascript">
			 var addLocSuccess  = "Locations updated successfully";
			 var addLocMaxLimit = "You have reached the maximum limit of locations";
			 var addLocAdded    = "Location already added to the list";
			 var addLocFailed   = "Location update failed";
			 var addLocCrumb    = "";
			 var addLocFCUnit   = "F";
			 var delLocText     = "Delete";
			 var maxLocMsg      = "You have reached the maximum number of locations. Please remove a location before attempting to add another.";
        </script>	<div id="yUpdatedMesg">
		<div class="bdUpdatedMesg">
		  <div id="yDispMesg">
			&nbsp;We have improved the coverage of weather locations. Some of the locations saved earlier may need to be updated by you.&nbsp;
		  </div>
		</div>
	</div></div>
                <div class="yui-b"><div id="yw-weathervideos"><h3>Weather Video</h3><div class="yw-video"><a class="yw-videoimg" href="javascript:void(window.open('http://us.rd.yahoo.com/dailynews/external/wcom/av_wcom_forc/57f9abc5aa1e8b7fd30972a4df78496c/40962221/*http://news.yahoo.com/video/weather-15749664/24794928','playerWindow','width=793,height=608,scrollbars=no'));"><img width="59" height="33" border="1" alt="Breaking Weather Video" src="http://d.yimg.com/a/p/weathercom/20110405/vidsthumb.96dcab151fcce9ab0695dccd42eb8812.jpg"></a><div><a href="javascript:void(window.open('http://us.rd.yahoo.com/dailynews/external/wcom/av_wcom_forc/57f9abc5aa1e8b7fd30972a4df78496c/40962221/*http://news.yahoo.com/video/weather-15749664/24794928','playerWindow','width=793,height=608,scrollbars=no'));">Ridge warmth</a><p>4/5/2011  7:15 AM</p></div></div><div class="yw-video"><a class="yw-videoimg" href="javascript:void(window.open('http://us.rd.yahoo.com/dailynews/external/wcom/av_wcom_forc/44d5943f794090bf0b16ae7ef88c4939/40961582/*http://news.yahoo.com/video/weather-15749664/24794131','playerWindow','width=793,height=608,scrollbars=no'));"><img width="59" height="33" border="1" alt="Breaking Weather Video" src="http://d.yimg.com/a/p/weathercom/20110405/vidsthumb.0830f16e779a8ff27dd418ecb267415c.jpg"></a><div><a href="javascript:void(window.open('http://us.rd.yahoo.com/dailynews/external/wcom/av_wcom_forc/44d5943f794090bf0b16ae7ef88c4939/40961582/*http://news.yahoo.com/video/weather-15749664/24794131','playerWindow','width=793,height=608,scrollbars=no'));">West Forecast</a><p>4/4/2011  9:00 PM</p></div></div><div id="yw-showallvideos"><a class="yw-morevideos" href="/vidindex/index.html">&raquo;Show all weather videos</a></div></div></div>
                <div class="yui-b"><div class="yw-ad"><!-- SpaceID=0 robot -->
</div></div>
				<div class="yui-b"><div class="yw-ad"><!-- SpaceID=0 robot -->
</div></div>
            </div>
        </div>
    </div>
    <div id="ft">
        <div id="yw-ftsearch"><fieldset><legend>Yahoo! News</legend><h2>Search</h2><form class="sch" action="http://news.search.yahoo.com/news/search" id="ynw-s2" name="search" method="get" target="_top" ><div><label for="p">Search:</label>&nbsp;<input class="text" type="text" value="" size="30" name="p" id="p">&nbsp;<select name="c" id="c"><option value="">All News & Blogs</option><option value="yahoo_news">Yahoo! News Only</option><option value="news_photos">News Photos</option><option value="av">Video/Audio</option></select>&nbsp;<input type="submit" title="Search" value="Search" class="submit"><input type="hidden" value="UTF-8" name="ei"/>&nbsp;<a href="http://news.search.yahoo.com/news/advanced">Advanced</a></div></form><div class="yw-network"><a href="http://www.yahoo.com">Yahoo!</a> - <a href="http://my.yahoo.com">My Yahoo!</a> - <a href="http://mail.yahoo.com">Mail</a></div></fieldset></div>  
        <div id="yw-footernav"><h2>Primary Navigation</h2><ul><li class="first"><a href="http://news.yahoo.com/">Home</a></li><li class=""><a href="http://news.yahoo.com/i/718">U.S.</a></li><li class=""><a href="http://news.yahoo.com/i/749">Business</a></li><li class=""><a href="http://news.yahoo.com/i/721">World</a></li><li class=""><a href="http://news.yahoo.com/i/762">Entertainment</a></li><li class=""><a href="http://news.yahoo.com/i/755">Sports</a></li><li class=""><a href="http://news.yahoo.com/i/738">Tech</a></li><li class=""><a href="http://news.yahoo.com/i/703">Politics</a></li><li class=""><a href="http://news.yahoo.com/i/753">Science</a></li><li class=""><a href="http://news.yahoo.com/i/751">Health</a></li><li class=""><a href="http://news.yahoo.com/i/2683">Travel</a></li><li class=""><a href="http://news.yahoo.com/i/964">Most Popular</a></li><li class=""><a href="http://news.yahoo.com/i/757">Odd News</a></li><li class=""><a href="http://news.yahoo.com/i/742">Opinion</a></li></ul></div>  
        <div id="yw-univfooter">
<!-- SpaceID=0 robot -->
<!-- SpaceID=0 robot -->
</div>
		
    </div>
</div>
</body>

<script src="http://l.yimg.com/a/lib/ywc/js/ywutilsintl.0.11.min.js" type="text/javascript"></script>
<script type="text/javascript" src="http://us.js.yimg.com/lib/rapid/rapid_2.0.0.js"></script>
        <script type="text/javascript">var modIds =new String('yw-worldweather,yw-hdmenu,yw-submenu,yw-headtitle,yw-ftsearch,yw-footernav,myLocContainer,yw-alerts,yw-weathermaps,yw-newsfeatures,yw-fullcoverage,yw-showallvideos').split(',');
var keys={A_pn:'index',A_id:'weather_us_index'};
var conf={spaceid:'16335623',tracked_mods:modIds,keys:keys,lt_attr:'text',client_only:0,ywa:{project_id:'10001684311662',cf:{'11':'us'}}};
var ins=new YAHOO.i13n.Track(conf);ins.init()</script><noscript><img width="1" height="1" alt="" src="http://a.analytics.yahoo.com/p.pl?a='10001684311662'&amp;js=no" /></noscript>
</html><!-- fe6.weather.ac4.yahoo.com uncompressed/chunked Tue Apr  5 08:12:10 PDT 2011 -->
