<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
        <title>Yahoo! Publisher Reports</title>
        <meta name="description" content="Yahoo! Publisher Network offers easy ways to make money online. Publishers of all sizes have access to products and services designed to earn money.">
        <meta name="keywords" content="make money online, qualified clicks, earn money online, yahoo publisher network, content match">
        <link rel="stylesheet" type="text/css" href="http://us.js2.yimg.com/us.js.yimg.com/lib/common/css/reset_2.0.5.css">
        <link rel="stylesheet" type="text/css" href="http://us.js2.yimg.com/us.js.yimg.com/lib/common/css/fonts_2.0.5.css">
        <link rel="stylesheet" type="text/css" href="http://us.js2.yimg.com/us.js.yimg.com/lib/common/css/grids_2.0.5.css">
		<link rel="stylesheet" type="text/css" href="stylesheet/login.css">
		<link rel="stylesheet" type="text/css" href="stylesheet/main.css">
   

	<script type="text/javascript" src="http://us.js2.yimg.com/us.js.yimg.com/lib/common/utils/2/yahoo_2.0.6.js"></script>
	<script type="text/javascript" src="http://us.js2.yimg.com/us.js.yimg.com/lib/common/utils/2/connection_2.0.5.js"></script>
</head>
<body>
<div id="doc" class="yui-t7">
	<div id="hd">
	<div id="yui-main">
	<div class="yui-g" id="header-bar">
	<div class="yui-u first" id="header-bar-links">
		<p><a href="http://www.yahoo.com">Yahoo!</a> <a href="http://my.yahoo.com">My Yahoo!</a> <a href="http://mail.yahoo.com">Mail</a></p>
	</div>
	<div class="yui-u" id="header-bar-search">
		<form name="srch" action="http://search.yahoo.com/search"><input type="hidden" name="fr" value="">
		<img src="http://us.i1.yimg.com/us.yimg.com/i/us/search/test/stw2.gif" width="38" height="16" alt="Search the web" /><input id="search-input" type="text" name="p" size="12" title="Enter search terms here" maxlength="100"><input id="search-submit" type="submit" value="Search" title="Search">
		</form>
	</div>	
	</div>
	
	<div class="yui-g" id="header-links">
	<div class="yui-u first" id="header-link-logo">
		<a href="index.html"><img src="img/logo_pr.gif" width="342" height="33" id="ygmalogo" border="0" alt="Yahoo! Publisher Reports" /></a>
	</div>
	<div class="yui-u" id="header-link-text">
	<div id="header-link-home">
		<a href="index.html">Publisher Reports Home</a>	
	</div>
	</div>
	</div>	
	</div>
   <!-- Main body -->
   <table width=750 border=0 cellspacing=0 cellpadding=0>
    <tr>
     <td>
	<p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="ypnLeftBox">
         <h4>Welcome to the Yahoo! Publisher Network Reports Portal</h4>
         <ul class="ypnList">
           <li>Access your reports from a secured online portal.</li>
           <li>Download your current and previous reports.</li>
         </ul>
         <p>&nbsp;</p>
        </div>
        <div class="ypnRightBox">

        <div class="section_border" style="width:270px;background-color:#EEEEEE;">
        <div class="section_corner" style="background-color:#EEEEEE;">
         <div class="top_left"></div>
         <div class="top_right"></div>
        </div>
        <div class="section_content">
         <p><a href="reports/reports.php">Sign In</a> to view your Yahoo! Publisher Network Reports.</p>
         <p>&nbsp;</p>
         <p>If you don't have a login, please contact your Yahoo! Account Manager.</p> 
        </div>  
       
        <div class="section_corner">
<div class="bottom_left"></div><div class="bottom_right"></div></div></div>
</div>
       </div>
      </div>
     <div style="clear: both;"></div>
</div>

   <!-- End of main body -->
   <p>&nbsp;</p>
   <p>&nbsp;</p>

<hr color="#999999" noshade size="1">	
        <div id="ft">
<p><a href="https://publisher.yahoo.com/legal/priv_policy.php" target="_blank">Privacy Policy</a> | <a href="http://info.yahoo.com/legal/us/yahoo/publishernetwork/publishernetwork-265.html" target="_blank">Terms of Service</a><br />Copyright &copy; 2008 Yahoo! Inc. All rights reserved. | <a href="http://docs.yahoo.com/info/copyright/copyright.html">Copyright Notice</a></p>
        </div>

</div>

</html>
