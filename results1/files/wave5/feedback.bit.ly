<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Customer Feedback for bit.ly</title>



  <link rel="canonical" href="http://feedback.bit.ly/forums/5239-suggestions" />






  <link rel="alternate" type="application/atom+xml" title="All activity on feedback.bit.ly" href="http://feedback.bit.ly/activity.atom" />


  <link rel="alternate" type="application/atom+xml" title="Suggestions forum activity on feedback.bit.ly" href="http://feedback.bit.ly/forums/5239.atom" />


<script src="http://cdn.uservoice.com/javascripts/application_packaged.js?1301964340" type="text/javascript"></script>

<script type="text/javascript">
//<![CDATA[

  ZeroClipboard.setMoviePath('http://feedback.bit.ly/swf/ZeroClipboard.swf' );

//]]>
</script>

<link href="http://cdn.uservoice.com/stylesheets/application_packaged.css?1301964370" media="all" rel="stylesheet" type="text/css" />
<!--[if IE]><link href="http://cdn.uservoice.com/stylesheets/ie/application.css?1301964331" media="all" rel="stylesheet" type="text/css" /><![endif]-->

  
  <style type="text/css">
    
    
    /* DO NOT OVERRIDE
       Use the configuration options provided to set 
       these colors *not* CSS which cannot be 
       guaranteed to be forward compatible.
    ----------------------------------------------- */

    
    body { 
      background-color: #FFFFFF; 
    }
    

    #search-results .header h3 {
      color: #F4940C;
    }

    #status.has-votes h4,
    #status.has-votes .help,
    .suggestion .moderation .votes,
    .supporter .votes,
    #user .activity .comment .votes,
    .suggestion .moderation li.selected a {
      background-color: #F4940C;
    }

    .sidebar .rank,
    .sidebar li.score {
      background-color: #00BCBA;
    }
    
    .uservoice-component a {
      color: #0066CC;
      text-decoration: none;
    }

    .uservoice-component a:hover,
    .uservoice-component a:focus {
      background-color: #0066CC;
      color: #fff;
    }
    
    a {
      color: #0066CC;
      text-decoration: none;
    }

    a:hover,
    a:focus {
      background-color: #0066CC;
      color: #fff;
    }
    
    #ribbon a:hover,
    #ribbon a:focus,
    .dark-background #ribbon a {
      color: #fff;
    }
    
    .tabs li a:hover,
    .tabs li a:focus {
      color: #fff;
      background-color: #0066CC;
    }
    
    .sidebar .nav li a:hover,
    .sidebar .nav li a:focus {
      background-color: #0066CC;
    }
    
    .sidebar a.leads:hover,
    .sidebar a.leads:focus {
      background-color: #0066CC;
    }
    
    .suggestion .moderation a:hover,
    .suggestion .moderation a:focus {
      background-color: #0066CC;
      opacity: 1.0;
      color: #fff;
    }
    
    .suggestion .moderation li a:hover,
    .suggestion .moderation li a:focus {
      background-color: #0066CC;
    }
    
  </style>
 
  


<meta name="title" content="Customer Feedback for bit.ly" />
  <!-- ChartBeat -->
  <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>
</head>
<body class="application-layout locale-en has-sidebar light-background page-pages-show forum_5239 topic_5239">
  
  <div class="uservoice-component">
      <div id="flashes" class="has-layout">
    <div class="always-on-top">
<div class="always-on-top">
</div></div>
<script type="text/javascript">
//<![CDATA[
setTimeout("$$('.message').each(function(element){ new Effect.Appear(element); });", 0);
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
setTimeout("$$('.temporary-message').each(function(element){ new Effect.Fade(element); });", 7000);
//]]>
</script>
    <noscript>
      <p class="error">Please enable Javascript <small>(it's required)</small></p>
    </noscript>
  </div>
  </div>
  
  <div class="uservoice-component" style="position: relative;">
  <a href="http://uservoice.com/?utm_campaign=Referral&amp;utm_content=top&amp;utm_medium=Powered+By+-+Site+Top&amp;utm_source=bitly.uservoice.com" class="powered-by top-right">powered by UserVoice</a>
  <div id="page-header" class="clear-fix page">
    
    <h1 class="has-layout">
      
      
      <a href="/forums/5239-suggestions" rel="nofollow" title="Customer Feedback for bit.ly"><img alt="Customer Feedback for bit.ly" src="https://s3.amazonaws.com/uploads.uservoice.com/logo/subdomain/4519/original/bitly_logo.png?1265663278" title="Customer Feedback for bit.ly" /></a>
    </h1>
    

    <ul class="top-nav">
      
      <li><a href="http://bit.ly/" class="has-layout" rel="nofollow">Go to <strong>bit.ly</strong> <img alt="External" src="http://cdn.uservoice.com/images/icons/external.png?1301964331" width="12" /></a></li>
      
      
    </ul>
  </div>
</div>

<style type="text/css">

.uservoice-component .top-right {
float: none;
position: absolute;
right: 0;
top: 0;
}

.uservoice-component #page-header {
padding: 3em 10px 0 10px;
}

#page-header {
position: relative;
}

.dark-background #page-header h1 a {
color: #fff;
}

#page-header h1 {
padding: 10px 5px;
font-weight: bold;
font-size: 280%;
letter-spacing: -2px;
}

#page-header h1 a:hover,
#page-header h1 a:focus {
text-decoration: underline;
background: transparent;
color: #06c;
}

#page-header .top-nav {
position: absolute;
left: 1em;
top: 0;
}

#page-header .top-nav li {
float: left;
margin-right: 0.5em;
}

#page-header .top-nav li a {
padding: 0.3em 0.5em 0.2em 0.5em;
font-size: 120%;
display: inline-block;
border: solid 1px #;
background-color: #fff;
-moz-border-radius-bottomleft: 0.5em;
-moz-border-radius-bottomright: 0.5em;
-webkit-border-bottom-left-radius: 0.5em;
-webkit-border-bottom-right-radius: 0.5em;
}

#page-header .top-nav li.admin a {
background-color: #ff8;
}

#page-header .top-nav li strong {
font-size: 110%;
}

.light-background #page-header .top-nav li a {
border: solid 1px #ccc;
border-top: none;
}

#page-header .top-nav li a:hover,
#page-header .top-nav li a:focus {
color: #06c;
text-decoration: underline;
}
</style>
  
  <div class="uservoice-component">
    <div id="ribbon" class="page has-layout">
  
  <h2><a href="/forums/5239-suggestions">Suggestions <small>Forum</small></a></h2>
  <div id="user-info">
    <ul class="user-breadcrumbs">
      
        <li class="signin">
          
            <a href="/signin" class="uv-button-sm" data-popover-src="#mainlogin" data-popover="{&quot;label&quot;:&quot;Sign in&quot;, &quot;position&quot;:&quot;bottom&quot;, &quot;action&quot;:&quot;ribbon&quot;, &quot;src&quot;:&quot;#mainlogin&quot;, &quot;onLoginKeepPopup&quot;:false}">Sign in</a>
          
          <div id="mainlogin" style="display:none;">
            



<form class="uv-signin" action="/session" method="POST">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous uv-field-has-button">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
              <button class="uv-button" type="submit" tabindex="0">Sign in</button>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password uv-field-has-button">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
              <button class="uv-button" type="submit" tabindex="0">Sign in</button>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</form>


          </div>
        </li>
      
    </ul>
  </div>
</div>
    <div class="page">
      <div id="forum" class="has-layout">
        

<div class="main-column">
  
  <div id="welcome">
    <p class="textilish">Welcome to our official suggestion forum. Do you have an idea? Do you recognize a good idea when you see one? We want to hear from you!</p>
  </div>

  <div id="search">
    
<form action="/forums/5239-suggestions/topics/5239-i-suggest-you-/suggestions/search" id="suggestion-search" method="get" onsubmit="new Ajax.Request('/forums/5239-suggestions/topics/5239-i-suggest-you-/suggestions/search', {asynchronous:true, evalScripts:true, method:'get', onComplete:function(request){$('indicator').hide(); $('suggestion-search').down('.submit').show()}, onLoading:function(request){$('indicator').show(); $('suggestion-search').down('.submit').hide()}, parameters:Form.serialize(this)}); return false;">
  <input class="hidden" id="or_new" name="or_new" type="hidden" value="1" />
  <fieldset>
    <ol>
      <li class="title">
        <span class="label">I suggest you ...</span>
        <div class="field">
          <label class="overlabel" for="query" style='display:none;'>- enter your idea (new feature, fix bug, etc) -</label>
          <input autocomplete="off" class="text-input text" id="query" maxlength="100" name="query" size="50" type="text" />
          <input class="submit submit" name="commit" type="submit" value="Search" />
          <div id="indicator" style='display:none;'></div>
          <p class="counter" style='display:none;'><em class='remaining'>100</em> characters left</p>
        </div>
      </li>
    </ol>
  </fieldset>
</form>

<script type="text/javascript">
//<![CDATA[

// submit the form *without* params[:or_new]

function setupQueryWatcher() {
  Form.Element.AfterActivity('query', function(val) {
    new Ajax.Request($('suggestion-search').action, {
      method: 'get',
      evalScripts: true,
      parameters: 'query=' + encodeURIComponent(val),
      onLoading: function(request) { $('indicator').show(); $('suggestion-search').down('.submit').hide() },
      onComplete: function(request) { $('indicator').hide(); $('suggestion-search').down('.submit').show() }
    });
  });
  
  new Form.Element.LengthCounter('query', $('suggestion-search').down('.counter'), true);
};
setupQueryWatcher();

//]]>
</script>
  </div>

  <div id="views">
    <ol class="tabs">
      <li id="top_ideas_link" class="current">
        <a href="/forums/5239-suggestions/topics/5239-i-suggest-you-/filter/top">Top<small> Ideas</small></a>
      </li>
      <li id="hot_ideas_link" >
        <a href="/forums/5239-suggestions/topics/5239-i-suggest-you-/filter/hot">Hot<small> Ideas</small></a>
      </li>
      <li id="recent_ideas_link" >
        <a href="/forums/5239-suggestions/topics/5239-i-suggest-you-/filter/recent">New<small> Ideas</small></a>
      </li>
      <li id="accepted_ideas_link" >
        <a href="/forums/5239-suggestions/topics/5239-i-suggest-you-/filter/accepted">Accepted<small> Ideas</small> <em class="count">611</em></a>
      </li>
      <li id="completed_ideas_link" >
        <a href="/forums/5239-suggestions/topics/5239-i-suggest-you-/filter/completed">Completed<small> Ideas</small> <em class="count">460</em></a>
      </li>
    </ol>
  </div>

  <div id="suggestions">
    

<ol class="suggestions">
  
  
    <li>
        
        
        

        
          <div id="suggestion-136321" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-4">
    <em>2,912</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_136321","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_136321" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/136321-allow-me-to-specify-my-timezone-/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/136321-allow-me-to-specify-my-timezone-?ref=title" class="title">Allow me to specify my timezone.</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">All my hits are being displayed as coming in at weird times because I'm on GMT and this service isn't. Please let me specify my timezone so the result times make sense to me :D</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/305634-keiron" class="user">keiron</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/136321-allow-me-to-specify-my-timezone-?ref=comments" class="has-comments">54 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #6FBC00">
          started
        </span>
      </div>
    
    
      <div class="text">
        <p class="textilish">The date displayed for items in your history is now displayed in the correct timezone. We are working towards improving the info pages to account for timezones as well.</p>

<p class="textilish">(this was incorrectly marked as completed for a short period of time)</p>
        <div class="vcard">
          <img alt="" class="avatar" src="http://www.gravatar.com/avatar/49b31b7264aa9367f83872a4112312d6?size=25&amp;default=http://cdn.uservoice.com/images/icons/default-avatar.jpg" width="25" />
          <a href="/users/229071-jehiah" class="fn">jehiah</a>
          <span class="title">Admin</span>
        </div>
      </div>
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-183662" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-4">
    <em>1,844</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_183662","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_183662" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/183662-google-analytics-integration/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/183662-google-analytics-integration?ref=title" class="title">Google Analytics integration</a>
      
      
    </h2>
    
    <div class="description">
      <div class="truncated-text"><p class="textilish">Allow the ability to add a suffix on all urls for a domain so webmasters can track twitter traffic in Google Analytics.</p>

<p class="textilish">E.g. if someone posts a link to <a href="http://www.example.com/index2.html" rel="nofollow">www.example.com/index2.html</a> automatically change to <a href="http://www.example.com/index2.html" rel="nofollow">http://www.example.com/index2.html</a>
<br>?utm_source=twitter&amp;utm_medium=micro-blog&amp;utm_campaign=Twitt <a class="more" href="#" onclick="$(this).up('.truncated-text').hide().next().show(); return false;">more</a></p></div><div class="full-text" style="display: none;"><p class="textilish">Allow the ability to add a suffix on all urls for a domain so webmasters can track twitter traffic in Google Analytics.</p>

<p class="textilish">E.g. if someone posts a link to <a href="http://www.example.com/index2.html" rel="nofollow">www.example.com/index2.html</a> automatically change to <a href="http://www.example.com/index2.html" rel="nofollow">http://www.example.com/index2.html</a>
<br />?utm_source=twitter&amp;utm_medium=micro-blog&amp;utm_campaign=Twitter</p>

<p class="textilish">Or something similar. Could also do this across this board if bit.ly sees twitter.com as the referrer (as referrer cannot be passed through an HTTP redirect).</p></div>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by anonymous
      
      

      | <a href="/forums/5239-suggestions/suggestions/183662-google-analytics-integration?ref=comments" class="has-comments">29 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-100940" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-4">
    <em>1,473</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_100940","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_100940" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/100940-allow-users-to-export-click-data-for-all-shortened/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/100940-allow-users-to-export-click-data-for-all-shortened?ref=title" class="title">allow users to export click data for all shortened urls</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">If you post a lot of links using bit.ly on various subjects, it's useful to export them all at once to see at a glance which links got the most click-thru traffic.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/206562-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/100940-allow-users-to-export-click-data-for-all-shortened?ref=comments" class="has-comments">27 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-85284" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>998</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_85284","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_85284" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/85284-openid-login-support/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/85284-openid-login-support?ref=title" class="title">OpenID Login Support</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">I would prefer not to use my Twitter login, but I also don't want to have to create a new username and password. If tr.im supported OpenID login, people could use one of many existing accounts that provide OpenID support.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/174902-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/85284-openid-login-support?ref=comments" class="has-comments">20 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-150546" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>972</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_150546","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_150546" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/150546-the-ability-to-sort-my-links-by-most-clicked/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/150546-the-ability-to-sort-my-links-by-most-clicked?ref=title" class="title">The ability to sort my links by most clicked</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">I love seeing my list of recent links, and I love how I can find out how many clicks each has had. I would love to have the ability to sort my recent/total links by the number of times they've been clicked, that way I see how well my links are performing overall.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/327788-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/150546-the-ability-to-sort-my-links-by-most-clicked?ref=comments" class="has-comments">40 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #6FBC00">
          started
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-126915" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>496</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_126915","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_126915" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/126915-allow-posting-in-hellotxt/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/126915-allow-posting-in-hellotxt?ref=title" class="title">Allow posting in hellotxt</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">As with Ping.fm I would like to post through hellotxt.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/40511-koltregaskes" class="user">koltregaskes</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/126915-allow-posting-in-hellotxt?ref=comments" class="has-comments">14 comments</a>

      
      
      
          
    </div>    
    
    
  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-112842" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>452</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_112842","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_112842" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/112842-allow-scheduled-tweets-i-e-send-later-/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/112842-allow-scheduled-tweets-i-e-send-later-?ref=title" class="title">allow scheduled tweets (i.e. send later)</a>
      
      
    </h2>
    
    <div class="description">
      
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/232313-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/112842-allow-scheduled-tweets-i-e-send-later-?ref=comments" class="has-comments">12 comments</a>

      
      
      
          
    </div>    
    
    
  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-132476" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>441</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_132476","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_132476" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/132476-see-the-user-names-of-those-who-click-on-your-link/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/132476-see-the-user-names-of-those-who-click-on-your-link?ref=title" class="title">See the user names of those who click on your link</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">It would be great to see the user names of the people who click on your link through Twitter, Facebook, etc. to track whether the people you want to reach are clicking on your link or not.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/296732-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/132476-see-the-user-names-of-those-who-click-on-your-link?ref=comments" class="has-comments">38 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
      <div class="text">
        <p class="textilish">What username? Facebook? Bit.ly? Twitter?</p>
        <div class="vcard">
          <img alt="" class="avatar" src="http://www.gravatar.com/avatar/5220cd8b95ce5865f6291a521123c919?size=25&amp;default=http://cdn.uservoice.com/images/icons/default-avatar.jpg" width="25" />
          <a href="/users/321064-rex-dixon" class="fn">Rex Dixon</a>
          <span class="title">Admin</span>
        </div>
      </div>
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-234205" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>371</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_234205","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_234205" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/234205-add-an-api-to-do-reverse-lookup-/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/234205-add-an-api-to-do-reverse-lookup-?ref=title" class="title">Add an API to do &quot;reverse lookup&quot;</a>
      
      
    </h2>
    
    <div class="description">
      <div class="truncated-text"><p class="textilish">The existing APIs focus on mapping from a long URL to a short URL.  I would like an API that I could give a long URL, and it would return to me the list of short URLs that map to that long URL.</p>

<p class="textilish">Ideally the API would support wildcard/globbing so that I could find all short URLs that point to a speci <a class="more" href="#" onclick="$(this).up('.truncated-text').hide().next().show(); return false;">more</a></p></div><div class="full-text" style="display: none;"><p class="textilish">The existing APIs focus on mapping from a long URL to a short URL.  I would like an API that I could give a long URL, and it would return to me the list of short URLs that map to that long URL.</p>

<p class="textilish">Ideally the API would support wildcard/globbing so that I could find all short URLs that point to a specific domain or subpage.</p></div>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/622512-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/234205-add-an-api-to-do-reverse-lookup-?ref=comments" class="has-comments">23 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-229637" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>295</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_229637","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_229637" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/229637-geographic-mapping/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/229637-geographic-mapping?ref=title" class="title">Geographic Mapping</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">I'd like to see a map of where my clicks are coming from -- more closely than displaying &quot;country&quot; I'd like to see a dot in Michigan where my click comes from... it will help me map results to where my clients are</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/606475-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/229637-geographic-mapping?ref=comments" class="has-comments">11 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
      <div class="text">
        <p class="textilish">I like this suggestion. :)</p>
        <div class="vcard">
          <img alt="" class="avatar" src="http://www.gravatar.com/avatar/5220cd8b95ce5865f6291a521123c919?size=25&amp;default=http://cdn.uservoice.com/images/icons/default-avatar.jpg" width="25" />
          <a href="/users/321064-rex-dixon" class="fn">Rex Dixon</a>
          <span class="title">Admin</span>
        </div>
      </div>
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-94621" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>260</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_94621","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_94621" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/94621-automatically-import-links-from-twitter/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/94621-automatically-import-links-from-twitter?ref=title" class="title">automatically import links from twitter</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">monitor my twitter username feed. Any bit.ly links should automatically appear in my bit.ly account when I log in so I can monitor my click through stats. Look at tweetburner.com for inspiration. Bit.ly - great stats. Poor twitter integration unless you tweet through the bit.ly service.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/192043-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/94621-automatically-import-links-from-twitter?ref=comments" class="has-comments">29 comments</a>

      
      
      
          
    </div>    
    
    
  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-183659" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>244</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_183659","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_183659" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/183659-list-all-links-and-stats-for-a-domain-for-webmaste/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/183659-list-all-links-and-stats-for-a-domain-for-webmaste?ref=title" class="title">List all links and stats for a domain for webmasters</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">Allow webmasters to register a domain (through verification) and see all bit.ly links created to their domain and any traffic generated through them.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by anonymous
      
      

      | <a href="/forums/5239-suggestions/suggestions/183659-list-all-links-and-stats-for-a-domain-for-webmaste?ref=comments" class="has-comments">10 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-183131" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>233</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_183131","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_183131" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/183131-user-name-and-account-deletion/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/183131-user-name-and-account-deletion?ref=title" class="title">user name and account deletion</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">Please provide the ability to change user account and delete account if required.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/449639-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/183131-user-name-and-account-deletion?ref=comments" class="has-comments">12 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-67813" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>232</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_67813","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_67813" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/67813-post-to-identi-ca/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/67813-post-to-identi-ca?ref=title" class="title">Post to identi.ca</a>
      
      
    </h2>
    
    <div class="description">
      <div class="truncated-text"><p class="textilish">Identi.ca is an Open Source microblogging service. It's API is a clone of Twitter's, so it's dead easy to implement when you already have support for Twitter. Laconica is the software that identi.ca runs on and that anyone can install on their own server, like Leo Laporte has done on army.twit.com.  <a class="more" href="#" onclick="$(this).up('.truncated-text').hide().next().show(); return false;">more</a></p></div><div class="full-text" style="display: none;"><p class="textilish">Identi.ca is an Open Source microblogging service. It's API is a clone of Twitter's, so it's dead easy to implement when you already have support for Twitter. Laconica is the software that identi.ca runs on and that anyone can install on their own server, like Leo Laporte has done on army.twit.com. It's easy to support any Laconica installation. You could take a look at how twitterfeed.com does it.</p>

<p class="textilish">You should also set up an account for bit.ly on identi.ca and use it as actively as the one on Twitter!</p></div>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/25925-forteller" class="user">forteller</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/67813-post-to-identi-ca?ref=comments" class="has-comments">13 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-82925" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>218</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_82925","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_82925" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/82925-add-a-preview-option-cookie-showing-the-url-w-o/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/82925-add-a-preview-option-cookie-showing-the-url-w-o?ref=title" class="title">Add a preview option (cookie), showing the URL w/o redirect.</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">Like tinyurl, an option to show a page with the URL instead of simply auto-redirecting would be greatly appreciated.  This should be available to all users, including unregistered users.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/170941-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/82925-add-a-preview-option-cookie-showing-the-url-w-o?ref=comments" class="has-comments">1077 comments</a>

      
      
      
          
    </div>    
    
    
  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-286565" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>193</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_286565","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_286565" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/286565-allow-me-to-sort-and-arrange-my-links-in-different/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/286565-allow-me-to-sort-and-arrange-my-links-in-different?ref=title" class="title">Allow me to sort and arrange my links in different folders</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">Allow the option of arranging links into folders, so that if using a link more than once it can be easily found, and different sorts of links click rates can be easily compared - e.g. sales messages V informative</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/832278-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/286565-allow-me-to-sort-and-arrange-my-links-in-different?ref=comments" class="has-comments">10 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-78983" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>154</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_78983","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_78983" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/78983-post-to-facebook-linkedin-myspace/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/78983-post-to-facebook-linkedin-myspace?ref=title" class="title">Post to Facebook, LinkedIn, MySpace</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">A great addition would be to allow users to post to their Facebook, LinkedIn, or MySpace accounts.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/163290-blazinbsdagility" class="user">blazinBSDAgility</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/78983-post-to-facebook-linkedin-myspace?ref=comments" class="has-comments">13 comments</a>

      
      
      
          
    </div>    
    
    
  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-238814" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>141</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_238814","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_238814" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/238814-youtube/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/238814-youtube?ref=title" class="title">youtube</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">Wouldn't it be a cool idea to start all youtube links with YT?
<br />Example: <a href="http://bit.ly/YTxyz" rel="nofollow">http://bit.ly/YTxyz</a>
<br />Could be the same for other services ..</p>

<p class="textilish">This way viewers know somehow what to expect.</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/634701-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/238814-youtube?ref=comments" class="has-comments">11 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-93747" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>139</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_93747","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_93747" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/93747-allow-posting-through-ping-fm/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/93747-allow-posting-through-ping-fm?ref=title" class="title">Allow posting through ping.fm</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">similar to the twitter feature, I would love the option to post the url using ping.fm</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/190394-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/93747-allow-posting-through-ping-fm?ref=comments" class="has-comments">57 comments</a>

      
      
      
          
    </div>    
    
    
  <div class="response">
    
      <div class="status">
        <strong>Status:</strong>
        <span class="tag" style="background-color: #999999">
          under review
        </span>
      </div>
    
    
  </div>

  </div>
</div>
        
      
    </li>
  
    <li>
        
        
        

        
          <div id="suggestion-151658" class="suggestion ">
  <div class="left-column">
    <div class="badge">
  
  <div class="points digits-3">
    <em>126</em>  votes    
  </div>
  
  <div class="moderation">
    

  
    
      <button class="uv-button-sm vote-button" data-popover='{"label":"Vote","position":"right","type":"ajax","action":"vote","src":"#vote_for_right_151658","email_name_optional":true,"enter_does_nothing":true,"submitDataType":"script","onLogin":"inline_refresh","onLoginKeepPopup":true,"onLoginSubmitClosePopup":true}'>vote</button>
    
  
  <div id="vote_for_right_151658" style="display:none;">
    <div class="uv-signin-wrapper">
  <form class="uv-suggestion-vote" method="POST" action="/forums/5239-suggestions/suggestions/151658-to-bring-up-a-bit-ly-preview-plugin-for-safari/votes">
    



<div class="uv-signin">

  <div class="uv-signin-content">
    
    
      
        <div class="uv-signin-header">
          <div class="uv-signin-header-left">
            <span class="uv-signin-prompt">
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous">
                Enter your email address
              </span>
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password uv-signin-mode-nopassword">
                Sign in to your profile.
              </span>
              <!--
              <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-password">
                                            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
              </span>
              -->
            </span>
          </div>
          <div class="uv-signin-header-right">
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-nopassword">
              <a class="link forgot_password" href="/users/forgot_password">Email me a new password.</a>
            </span>
            <span class="uv-signin-header-text uv-signin-mode uv-signin-mode-anonymous uv-signin-mode-password">…or, sign in with</span>
            <ul class="uv-signin-providers uv-signin-mode uv-signin-mode-anonymous">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
            <ul class="uv-signin-providers uv-signin-providers-custom uv-signin-mode uv-signin-mode-password">
              
<li class="uv-signin-provider" data-provider="facebook" title="Sign in via Facebook"><a href="https://graph.facebook.com/oauth/authorize?client_id=46468144668&redirect_uri=http://auth.uservoice.com/facebook/4519&scope=email&display=popup" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/facebook.png" alt="facebook" /></a></li>
<li class="uv-signin-provider" data-provider="google" title="Sign in via Google"><a href="http://auth.uservoice.com/google/4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/google.png" alt="google" /></a></li>
<li class="uv-signin-rpx">
  <div class="uv-signin-rpx-trigger" title="Other providers for existing users" tabindex="0"></div>
  <div class="uv-signin-rpx-container uv-field-group">
    <div class="uv-field uv-field-first" data-rpx-callback="http://auth.uservoice.com/rpx/4519">
      <span class="label">
        <span class="uv-field-aside"><a href="http://www.janrain.com/?utm_source=login.uservoice.com&amp;utm_medium=partner&amp;utm_campaign=attribution" target="_blank">Powered by Janrain</a></span>
        <span class="uv-field-instructions">
          Previously signed in with one of these?
        </span>
      </span>
    </div>
    <div class="uv-field uv-field-last">
      <span class="label">
        <span class="uv-field-label">Sign in</span>
        <span class="uv-field-wrapper">
          <ul class="uv-signin-providers">
            <li class="uv-signin-provider" title="Sign in via AOL" data-provider="aol"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://openid.aol.com/{Enter your AOL Screen Name:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/aol.png" alt="aol" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Blogger" data-provider="blogger"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your Blogger domain:}" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/blogger.png" alt="blogger" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Hyves" data-provider="hyves"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://hyves.nl/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/hyves.png" alt="hyves" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MS Live" data-provider="live"><a href="https://login.uservoice.com/liveid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/live.png" alt="live" /></a></li>
            <li class="uv-signin-provider" title="Sign in via MySpace" data-provider="myspace"><a href="https://login.uservoice.com/myspace/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/myspace.png" alt="myspace" /></a></li>
            <li class="uv-signin-provider" title="Sign in via OpenID" data-provider="openid"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="{Enter your OpenID URL:}" data-openid_identifier-hint="http://" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/openid.png" alt="openid" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Twitter" data-provider="twitter"><a href="https://login.uservoice.com/twitter/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/twitter.png" alt="twitter" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Verisign" data-provider="verisign"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://pip.verisignlabs.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/verisign.png" alt="verisign" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Wordpress" data-provider="wordpress"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://{Enter your WordPress user name:}.wordpress.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/wordpress.png" alt="wordpress" /></a></li>
            <li class="uv-signin-provider" title="Sign in via Yahoo!" data-provider="yahoo"><a href="https://login.uservoice.com/openid/start?token_url=http%3A%2F%2Fauth.uservoice.com%2Frpx%2F4519" data-openid_identifier="http://me.yahoo.com/" tabindex="0"><img class="favicon" src="http://cdn.uservoice.com/images/shared/favicons/yahoo.png" alt="yahoo" /></a></li>
          </ul>
        </span>
      </span>
    </div>
  </div>
</li>
            </ul>
          </div>
        </div>
        <div class="uv-field-group">
          <div class="uv-field uv-field-first">
            <label>
              <span class="uv-field-label">Email</span>
              <span class="uv-field-wrapper"><input type="text" class="required email" name="email" placeholder="Required, but not displayed" maxlength="255" tabindex="0"></span>
              <span class="uv-field-status">
                <img class="uv-field-thinking" alt="thinking" src="/images/clients/widget/search-small.gif" />
                <img class="uv-field-reset" alt="reset" src="/images/shared/uv-field-reset.gif" />
              </span>
            </label>
          </div>
          <div class="uv-field uv-field-last uv-signin-mode uv-signin-mode-anonymous">
            <label>
              <span class="uv-field-label">Name</span>
              <span class="uv-field-wrapper"><input type="text" name="display_name" placeholder="Display name" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-password">
            <label>
              <span class="uv-field-label important">Password</span>
              <span class="uv-field-wrapper"><input type="password" name="password" maxlength="100" tabindex="0"></span>
            </label>
            
          </div>
          <div class="uv-field uv-field-last uv-signin-auth uv-signin-mode uv-signin-mode-nopassword">
            <span class="label">
              <span class="uv-field-label important">Sign in</span>
              <span class="uv-field-wrapper">
                <ul class="uv-signin-providers uv-signin-providers-custom"></ul>
              </span>
            </span>
          </div>
        </div>
        <div class="uv-field-sm uv-field-checkbox">
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-anonymous" title="By signing in you agree to UserVoice's Terms of Service">I agree to the <a href="http://uservoice.com/tos" tabindex="0" target="_blank">Terms of Service</a></span>
          <span class="uv-field-aside uv-signin-mode uv-signin-mode-password">
            <a class="link forgot_password" href="/users/forgot_password">Forgot password?</a>
          </span>
          <label title="If checked, we'll remember your session for 2 weeks.">
            <input class="checkbox" id="remember_me" name="remember_me" tabindex="0" type="checkbox" value="1" />
            Remember me
          </label>
        </div>
      
    
    <div class="uv-signin-flash uv-signin-error">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
    <div class="uv-signin-flash uv-signin-info">
      <div class="uv-signin-flash-message"></div>
      <div class="uv-signin-flash-close">Close</div>
    </div>
  </div>

</div>


    <div class="uv-vote-wrapper">
      
      
      
      <p>You have <span class="votes-remaining">10</span> votes remaining</p>
      <div class="uv-field uv-field-radio submitting">
        <ul class="options x3 set-votes">
          
          <li class="first">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="1" data-initial-checked="false"   /><span>1 vote</span>
            </label>
          </li>
          <li>
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="2" data-initial-checked="false"   /><span>2 votes</span>
            </label>
          </li>
          <li class="last">
            <label class="uv-button" tabindex="0">
              <input type="radio" name="to" value="3" data-initial-checked="false"   /><span>3 votes</span>
            </label>
          </li>
        </ul>
      </div>
    </div>
  </form>
</div>
  </div>

  </div>
</div>
  </div>
  
  <div class="content">
    <h2>
      <a href="/forums/5239-suggestions/suggestions/151658-to-bring-up-a-bit-ly-preview-plugin-for-safari?ref=title" class="title">to bring up a bit.ly Preview Plugin for Safari</a>
      
      
    </h2>
    
    <div class="description">
      <p class="textilish">The bit.ly Preview Plugin for Firefox is awesome. But Firefox itself is somewhat slow, bulky, mac-unlike and not well integrated in MacOSX. So, I like Safari much better. Only the bit.ly Preview Plugin is missing...</p>
      
      
    </div>

    <div class="footer">
      
        
      
	    
      
      by <a href="/users/340637-anonymous" class="user">anonymous</a>
      
      

      | <a href="/forums/5239-suggestions/suggestions/151658-to-bring-up-a-bit-ly-preview-plugin-for-safari?ref=comments" class="has-comments">5 comments</a>

      
      
      
          
    </div>    
    
    
  </div>
</div>
        
      
    </li>
  
</ol>
    
    <div class="pagination"><span class="disabled prev_page">&#171; Newer</span> <span class="current">1</span> <a href="/pages/5239-suggestions?filter=top&amp;page=2" rel="next">2</a> <a href="/pages/5239-suggestions?filter=top&amp;page=3">3</a> <a href="/pages/5239-suggestions?filter=top&amp;page=4">4</a> <a href="/pages/5239-suggestions?filter=top&amp;page=5">5</a> <a href="/pages/5239-suggestions?filter=top&amp;page=6">6</a> <a href="/pages/5239-suggestions?filter=top&amp;page=7">7</a> <span class="gap">&hellip;</span> <a href="/pages/5239-suggestions?filter=top&amp;page=38">38</a> <a href="/pages/5239-suggestions?filter=top&amp;page=39">39</a> <a href="/pages/5239-suggestions?filter=top&amp;page=2" class="next_page" rel="next">Older &#187;</a></div>
  </div>
  
  <div id="search_results">
  </div>
</div>


<div class="sidebar">
      
  <div id="status" class="has-votes">
    <h4 id="points-remaining">
      <strong>10</strong> votes left!
    </h4>
    <ul class="help">
      <li><a href="#" onclick="new Ajax.Request('/forums/5239-suggestions/out_of_votes', {asynchronous:true, evalScripts:true, method:'get', parameters:'authenticity_token=' + encodeURIComponent('FNNDqW1jArjozcTnjc7bd7vG7roZ6FCAOTTs/ZVAMtw=')}); return false;">What happens if I run out?</a></li>
    </ul>
  </div>

  <h3 style="display:none;">
    Your Ideas
  </h3>

  <ol class="suggestions my-suggestions" id="my-suggestions">
    
  </ol>

  <ul class="stripes">
    
    
      <li><a href="http://uservoice.com/signup?utm_campaign=Referral&amp;utm_content=sidebar&amp;utm_medium=Ad+-+Site+Sidebar&amp;utm_source=bitly.uservoice.com" class="leads"><em>Want </em>your own<em> forum like this?</em></a></li>
    
		
	    <li><a href="/forums/5239-suggestions.atom" class="feed"><em>Suggestions</em> activity feed</a></li>
		
    
      
        <li><a class="support" href="#" onclick="UserVoice.showPopupWidget(); return false;; return false;">Contact bit.ly</a></li>    
      
    
  </ul>
  
  
  
    
  
  
  
  
    <div id="forums" class="nav">
      <h3>Forums</h3>
      <ol>
        
        <li class="has-layout ">
          <a href="/forums/16149-ask-and-answer">Ask And Answer <small>(13) </small></a>
        </li>
        
        <li class="has-layout current">
          <a href="/forums/5239-suggestions">Suggestions <small>(765) </small></a>
        </li>
        
      </ol>
    </div>
  

</div>

<div class="clear-fix">

</div>
        <div class="clear-fix"></div>
                
        <a href="http://uservoice.com/?utm_campaign=Referral&amp;utm_content=bottom&amp;utm_medium=Powered+By+-+Site+Bottom&amp;utm_source=bitly.uservoice.com" class="powered-by">powered by UserVoice</a>
        
        <img alt="" class="pixel" height="1" src="http://feedback.bit.ly/track.gif?eyJmb3J1bV9pZCI6NTIzOSwic291cmNlIjoic2l0ZSIsInN1YmRvbWFpbl9pZCI6NDUxOSwiaG9zdCI6ImZlZWRiYWNrLmJpdC5seSJ9" style="float: right" width="1" />        
      </div>
    </div>
  </div>
    
  
  




<script type="text/javascript">
  var contactOptions = {
    key: 'bitly',
    host: 'feedback.bit.ly', 
    forum: 5239,
    lang: 'en',
    showTab: false,
    params: {view: 'contact'}
  };
  
  function _loadUserVoice() {
    var s = document.createElement('script');
    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', ("https:" == document.location.protocol ? "https://" : "http://") + "cdn.uservoice.com/javascripts/widgets/tab.js");
    document.getElementsByTagName('head')[0].appendChild(s);
  }
  _loadAppSuper = window.onload;
  window.onload = (typeof window.onload != 'function') ? _loadUserVoice : function() { _loadAppSuper(); _loadUserVoice(); };
</script>


<div id="footer" class="uservoice-component">
  
  <p>
    <strong>
      Need help with bit.ly?
      <a href="#" onclick="UserVoice.showPopupWidget(); return false;; return false;">Contact bit.ly</a>
    </strong>
  </p>
  
  <p>
    <a href="http://uservoice.com">Feedback Site</a> powered by UserVoice (&#169; 2011) for <a href="http://bit.ly">bit.ly</a> and subject to UserVoice's <a href="http://uservoice.com/tos">Terms of Service</a>.
  </p>
</div>

<style type="text/css">
#footer {
margin: 0 auto;
width: 850px;
clear: both;
padding: 10px;
}

#footer p {
padding: 2px 5px;
font-size: 11px;
margin-bottom: 10px;
color: #666;
text-align: center;
}

#footer select {
  font-size: 11px;
  font-weight: normal;
  letter-spacing: 0;
}

.dark-background #footer p {
color: #fff;
}

.dark-background #footer a {
color: #2395FF;
}

.dark-background #footer a:hover {
color: #fff;
}
</style>
  
  
    <script type="text/javascript">  
      (function() {
        var uv = document.createElement('script'); uv.type = 'text/javascript'; uv.async = true;
        uv.src = ('https:' == document.location.protocol ? 'https://' : 'http://') +  'widget.uservoice.com/PZhzHZeOR0nSEr80OO4Aw.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(uv, s);
      })();
    </script>
  
  
<!-- Google Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3735534-8']);
  _gaq.push(['_setDomainName', '.uservoice.com']);
  _gaq.push(['_trackPageview']);
  
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>  
<!-- Google Analytics for AJAX -->
<script type="text/javascript">
  if (typeof(Ajax) !== 'undefined') {
    Ajax.Responders.register({
      onComplete: function(request) {
        _gaq.push(['_trackPageview', request.url]);
      }
    })
  }
</script>


<!-- Start Mixpanel tag -->

<script type="text/javascript">
  var mpmetrics = null;
  function _loadMixpanel() {
    var s = document.createElement('script');
    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', ("https:" == document.location.protocol ? "https://" : "http://") + "api.mixpanel.com/site_media/js/api/mixpanel.js");
    document.getElementsByTagName('head')[0].appendChild(s);
    _sendEventsToMixpanel();
    
  }
  function _sendEventsToMixpanel() {
    if (typeof(MixpanelLib) != "function") {
      setTimeout('_sendEventsToMixpanel()', 750);
    } else {
      mpmetrics = MixpanelLib('7a0a5074ecca7025afedfe04d9eb214f');
      try { mpmetrics.register({"utm_source":"not specified","using_logo":true,"locale":"en","utm_content":"not specified","authentication":"uservoice","is_private":false,"referral":false,"user_type":"logged_out","using_customized_prompt":false,"browser":"unknown","utm_campaign":"not specified","using_domain_alias":true,"authentication_full":"uservoice_with_anonymous","utm_medium":"not specified","using_custom_design":false}); } catch(e) {}  try { mpmetrics.track("Forum view", {"subdomain_key":"bitly","page_id":5239}); } catch(e) {}  try { mpmetrics.track_funnel("signin_engagement_conversion", 1, "Forum Viewed", {"subdomain_key":"bitly","page_id":5239}); } catch(e) {}  try { mpmetrics.track_funnel("search_conversion", 1, "Forum Viewed", {"subdomain_key":"bitly","page_id":5239}); } catch(e) {} 
      
    }
  }
  _loadMixpanelSuper = window.onload;
  window.onload = (typeof window.onload != 'function') ? _loadMixpanel : function() { _loadMixpanelSuper(); _loadMixpanel(); };
</script>  
<!-- End Mixpanel tag -->


  


</body>
</html>
