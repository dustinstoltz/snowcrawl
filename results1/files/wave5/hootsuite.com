<!DOCTYPE html> 
<html class="static" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

			<meta http-equiv="X-UA-Compatible" content="ie=edge,chrome=1">
		

	<meta name="keywords" content="HootSuite, Social Media Dashboard, Multiple Twitter Accounts, Corporate Twitter, Scheduled Tweets, Twitter Multiple User, Customized Twitter Layout, Twitter Stats" />
	<meta name="description" content="HootSuite - Social Media Dashboard. With HootSuite, you can monitor keywords, manage multiple Twitter, Facebook, LinkedIn, Foursquare, Ping.fm and WordPress profiles, schedule messages, and measure your success." />
	    <meta property="fb:page_id" content="177463958820" />
    
	<!-- IE9 application pinning tags -->
	<meta name="application-name" content="HootSuite"/>
	<meta name="msapplication-tooltip" content="Social Media Dashboard" />
	<meta name="msapplication-starturl" content="http://hootsuite.com/" />
	<meta name="msapplication-task" content="name=Streams;action-uri=http://hootsuite.com/dashboard;icon-uri=https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/streams.ico"/>
	<meta name="msapplication-task" content="name=Analytics;action-uri=http://hootsuite.com/dashboard#/analytics/custom;icon-uri=https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/stats.ico"/>
	<meta name="msapplication-task" content="name=Contacts;action-uri=http://hootsuite.com/dashboard#/contacts;icon-uri=https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/contacts.ico"/>
	<meta name="msapplication-task" content="name=Assignments;action-uri=http://hootsuite.com/dashboard#/assignments;icon-uri=https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/assignments.ico"/>
	<!-- end IE9 app pinning tags-->
	
	<link rel="apple-touch-icon" href="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/apple-touch-icon.png"/>

<meta name="application-name" content="HootSuite"/>
<meta name="application-url" content="http://hootsuite.com"/>

<link rel="icon" href="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/hs_128x128.png" sizes="128x128"/>
<link rel="icon" href="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/hs_48x48.png" sizes="48x48"/>
<link rel="icon" href="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/hs_32x32.png" sizes="32x32"/>
<link rel="shortcut icon" href="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/favicon.ico" />
<link rel="icon" href="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/hs_16x16.png" sizes="16x16"/>

					
	<link href="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/css/base.css" type="text/css" rel="stylesheet" />
    <!--[if IE 7]>
	<link href="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/css/ie7.gz.css" type="text/css" rel="stylesheet" />
    <![endif]-->		   
			<title>HootSuite - Social Media Dashboard for Teams using Twitter, Facebook, Linkedin</title>
</head>
	<body>
        <!--[if IE]>
        <a name="#" href="#"></a>
        <![endif]-->
		<script type="text/javascript">
		var hs = hs || {};
	hs.timers = new Object();
	hs.prefs = new Object();
	hs.c = new Object();
	hs.c.rootUrl 	= "http://hootsuite.com";
	hs.c.rootUrlSSL = "https://hootsuite.com";
	hs.c.imageUrl 	= "https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images";
	hs.c.swfUrl 	= "https://d2l6uygi1pgnys.cloudfront.net/2-2-09/swf";
	hs.c.jsUrl 		= "https://d2l6uygi1pgnys.cloudfront.net/2-2-09/js";
	
	hs.c.reportHeaderImageUrl = "https://d2l6uygi1pgnys.cloudfront.net/report_header_image/production";
	
									hs.c.jsTemplateRootUrl = ('https:' == document.location.protocol.toLowerCase() ? hs.c.rootUrlSSL : hs.c.rootUrl) + '/js/internal/templates/';	
		hs.c.tweetPageSize = 30;
	hs.prefs.language = '';
	hs.timezoneOffset=null;
	hs.memberId=null;
	hs.socialNetworks=[];
	hs.socialNetworksKeyedByType=[];
	hs.memberAutoInitial="";
	hs.prefs.isNotifyNewTweet=0;
	hs.prefs.isNewRetweet = 0;
	hs.prefs.theme = '';
	hs.prefs.allowSlimStreams = false; // TODO: replace with value from DB
	hs.languagePack = {};
	hs.gp = 0;
	hs.fbAppKey="e0bebeb3a55265b11821edce13e316fe";
	hs.fbChannelPath="http://hootsuite.com/xd_receiver.htm";
	hs.fbFanpageId="177463958820";
</script>
					
	<script type="text/javascript" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/js/hs.js"></script>		   
<div id="statusContainer" style="display: none;position:absolute;">
	<div class="statusMessage rb-a-4"><span class="_statusMsgContent"></span></div>
</div>
		<div id="container" class="home">
						<div id="content">
					<div class="header">
		<h1><a href="http://hootsuite.com"><img src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home_logo.png" /></a></h1>
        <a id="loginButton" class="btn-glass" href="#" tabindex="1">Login&nbsp;<span class="icon-19 expand"></span></a>
        <div id="loginBox" class="rb-a-5 offScreen">
            <div id="openId">
                
	<script type="text/javascript">
		$().ready(function() {
			openid.init('openid_identifier');
		});
	</script>
	
	<form id="openid_form" action="https://hootsuite.com/openid-start" method="post" >       
	                <div id="openid_choice" style="display: block; ">
		<h3>Login or Signup with OpenID</h3>
			            <p>Select one of these third-party accounts:</p>
	            <div id="openid_btns"></div>
            </div>
		<div id="openid_input_area"></div>
    </form>            </div>
            <div id="secureId" class="rb-a-4">
                <form name="memberLoginForm" id="homePageMemberLoginForm" method="post" onKeyPress="checkForEnterKey(event, '_submitLogin');" action="https://hootsuite.com/login">
                	<h3>Login</h3>
                    <span class="formError"></span>
                    <span class="formError"></span>
                    <span class="formError"></span>
                    <label class="emailInput defaultTextInput title" for="loginEmail">
                    	Email Address:
                    </label>
                    <input id="loginEmail" type="text" name="loginInfo[email]" maxlength="100" tabindex="2" />
                    <label class="passInput defaultTextInput title" for="loginPassword">
                    	Password:
                    </label>
                    <input id="loginPassword" name="loginInfo[password]" type="password" tabindex="3"/>
                    <p class="forgotPassword"><a href="retrieve-password" tabindex="4"><strong>Forgot Password?</strong></a></p>
                    <p class="remember">
                        <label class="title"><input class="checkbox" type="checkbox" name="loginInfo[rememberMe]" checked="checked" tabindex="5" />&nbsp;Remember Me</label>
                    </p>
                    <div class="btns-last">
                    <a class="btn-cmt _submitLogin" href="#" onclick="hs.throbberMgrObj.add('._submitLogin'); $('#homePageMemberLoginForm').submit(); return false;" tabindex="6">Secure Login</a>
                    </div>
        
                    <input type="hidden" name="redirect" value="" />
                    <input type="hidden" name="form_submit" value="Login" />
                </form>
            </div>
            <script type="text/javascript">
				/*
				$('#loginButton')
				*/
				$('#loginButton')
					.bind('click', function() { return false; })
					.bind('mousedown', toggleLoginBox)
					.bind('focus', function(e) {
						setTimeout(function() {
							if ($('#loginBox').is('.visHide')) {
								// not already visible
								toggleLoginBox(e);	
							} else {
								// already visible, do nothing
							}
						}, 1);
					});
				
				function toggleLoginBox(e) {
					e.preventDefault();
					$('#loginButton').toggleClass('btn-glass-dropdown');
					$('#loginButton').find('.icon-19').swapClass('collapse','expand');
					$('#loginBox').toggleClass('offScreen');
					setTimeout(function() {
						$('#loginEmail').focus();
					},1);
					return false;
				}
			</script>
        </div>
	</div>
	<div class="title">		
	   	<h1>Social Media Dashboard</h1>
		<a class="signup" href="https://hootsuite.com/signup">Sign Up Now</a>
	</div>
	<div class="slider">
		<a class="slideLeft" href="#">Left</a>
		<div class="itemHolder">
		<div class="items unique">
			
			<div class="item">
				<a href="#" title="Enlarge" onclick="showImagePopup('https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/large_networks.png', 640, 480); return false;" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Spread Messages</h2>
					<p>Update multiple networks in one step, including Twitter, Facebook, LinkedIn, Wordpress.com, and Ping.fm</p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_networks.png" alt="Networks" />					
				</a>
			</div>
			<div class="item">
				<a href="http://hootsuite.com/mobile" title="View apps page" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Social on the Go</h2>
					<p>Compose and converse on the go using mobile apps for iPhone, Android, Blackberry, iPad and more</p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_mobile.png" alt="Retweet" />					
				</a>
			</div>
			<div class="item">
				<a href="#" title="Enlarge" onclick="showImagePopup('https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/large_analytics.jpg', 640, 480); return false;" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Track Results</h2>
					<p>Review success in real-time with click-through statistics and easy report exporting</p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_analytics.png" alt="Analytics" />					
				</a>
			</div>
			 <div class="item">
				<a href="#" title="Enlarge" onclick="showImagePopup('https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/large_team.png', 640, 480); return false;" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Team Collaboration</h2>
					<p>Manage multiple contributors and share data and access without sharing passwords</p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_team.png" alt="Team" />					
				</a>
			</div>
			<div class="item">
				<a href="#" title="Enlarge" onclick="showImagePopup('https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/large_themes.png', 640, 480); return false;" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Custom Interface</h2>
					<p>Work efficiently with social streams, tabs, and columns -- plus a choice of design themes</p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_themes.png" alt="Themes" />					
				</a>
			</div>
			<div class="item">
				<a href="#" title="Enlarge" onclick="showImagePopup('https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/large_assignment.jpg', 640, 480); return false;" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Assign Tasks</h2>
					<p>Fine-tune your team by delegating messages and monitoring responses and progress</p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_assignments.png" alt="Assignments" />					
				</a>
			</div>
			<div class="item">
				<a href="#" title="Enlarge" onclick="showImagePopup('https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/large_schedule.png', 640, 480); return false;" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Scheduled Updates</h2>
					<p>Optimize delivery by choosing the best time and date to reach your audience </p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_schedule.png" alt="Schedule" />					
				</a>
			</div>
			<div class="item">
				<a href="#" title="Enlarge" onclick="showImagePopup('https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/large_internationalize.jpg', 640, 480); return false;" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Internationalize</h2>
					<p>Feel comfortable with language localization in Japanese, French and Italian (more languages to come)</p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_international.png" alt="International" />					
				</a>
			</div>
			<div class="item">
				<a href="#" title="Enlarge" onclick="showImagePopup('https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/large_mentions.png', 640, 480); return false;" tabindex="-1">
					<img class="cta" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/item_btn.png" alt="&raquo;" />
					<h2>Monitor Mentions</h2>
					<p>Gather intelligence by tracking mentions of your brand, industry, or search terms</p>
					<img class="thumb" src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/images/static/home/thumb_mentions.png" alt="Mentions" />					
				</a>
			</div>			
		</div>
		</div>
		<a class="slideRight" href="#">Right</a>
	</div>
	<div class="buckets">
		<div class="users">
			<h2><a href="http://hootsuite.com/enterprise" target="_blank">Learn About HootSuite Enterprise &raquo;</a>Hoo's Using HootSuite?</h2>
		</div>
		<div class="blog">
			<h2>HootSuite Blog</h2>
			
			<ul class="messageList">
				<li><a href="http://blog.hootsuite.com/happy-owls/">#HootSuite Launches New #HappyOwls Game For Web And Mobile</a></li>
<li><a href="http://blog.hootsuite.com/secure-profiles/">New #HootSuite Secure Profiles Protect Brand Messaging for Enterprises</a></li>
<li><a href="http://blog.hootsuite.com/hootsuite-owls-at-sxsw2011/">#HootSuite Brings The Owl to #SXSW 2011 ~ News Roundup</a></li>
<li><a href="http://blog.hootsuite.com/social-analytics-news-roundup/">#HootSuite Social Analytics Close the Gap Between Actions and Results ~ News Roundup</a></li>
<li><a href="http://blog.hootsuite.com/hootsuite-info-sheet-listening-engagement/">Effective Listening and Audience Engagement ~ New #HootSuite Info Sheet</a></li>
<li><a href="http://blog.hootsuite.com/marca-case-study/">@marca Builds Community Using Lists and Streams ~ New #HootSuite Case Study</a></li>

			</ul>
		</div>
	</div>
	<div id="aboutImagePopup" style="display:none;"></div> 
<script type="text/javascript">
$(document).ready(function () {
	
	var itemTotal = $('div.slider .unique .item').size();
	var itemWidth = $('div.slider .unique .item:first').outerWidth();
	var itemsWidth = (itemTotal*itemWidth);
	var slideActive = 1;
	
	$('div.slider .unique').width(itemsWidth);
	$('div.slider').width(952);//ie6 fix tried container.width but still breaks
	
	var holderWidth = $('div.slider .itemHolder').outerWidth();
	currentPos = 0;
	
	$('div.slider .unique').clone().prependTo($('div.slider .itemHolder'));
	$('div.slider .items:first-child').addClass('clone').removeClass('unique');
	$('div.slider .clone').css('margin-left', (itemTotal*itemWidth)*-1);

	
	$('#homePageMemberLoginForm input#loginEmail').focus().select();
	
	
	function slideTimer() 
	{
		if (hs.timers.homeSlideTimer != undefined)
		{
			clearTimeout(hs.timers.homeSlideTimer);
			delete hs.timers.homeSlideTimer;
		}
		hs.timers.homeSlideTimer = setTimeout(function(){
			if(!slideActive){return;} else {
				$('a.slideRight').click();
			}
		}, 6000);
	}
	
	slideTimer();
	
	$('a.slideLeft').click(function (e) {
		
		if($('div.slider .unique:animated').length <= 0) {
			targetPos = currentPos + itemWidth;
			currentPos = targetPos;
			
			$('div.slider .unique').animate( {
				marginLeft:targetPos
			}, 700 );
			$('div.slider .clone').animate( {
				marginLeft:(targetPos-itemsWidth)
			}, 700, sliderContinuity );
		}
		slideTimer();
		e.preventDefault();
	});
	$('a.slideRight').click(function (e) {
		if($('div.slider .unique:animated').length <= 0) {
			targetPos = currentPos - itemWidth;
			currentPos = targetPos;
			
			$('div.slider .unique').animate( {
				marginLeft:targetPos
			}, 700 );
			$('div.slider .clone').animate( {
				marginLeft:(targetPos-itemsWidth)
			}, 700, sliderContinuity );
		}
		slideTimer();
		e.preventDefault();
	});
	
	function sliderContinuity() {/* MOVES THE ITEM CONTAINERS AROUND SO THE SCROLL IS CONTINUOUS */
		var newPosLeft = (currentPos - itemsWidth + itemWidth);
		var newPosRight = currentPos + (itemWidth*((Math.round($('div.slider').outerWidth()/itemWidth))-1));
		
		if(newPosLeft == 0) {
		$('div.slider .unique').css('margin-left', (currentPos-itemsWidth));
		$('div.slider .clone').css('margin-left', ((currentPos-itemsWidth)-itemsWidth));
		currentPos = (currentPos-itemsWidth);
		}
		if(newPosRight == 0) {
		$('div.slider .unique').css('margin-left', (currentPos+itemsWidth));
		$('div.slider .clone').css('margin-left', ((currentPos+itemsWidth)-itemsWidth));
		currentPos = (currentPos+itemsWidth);
		}
		
	}
	
	$('.home .slider .items .item').mouseenter(function () {
		$(this).addClass('active');
		slideActive = 0;
	}).mouseleave(function(){
		$(this).removeClass('active');
		slideActive = 1;
		slideTimer();
	});
	
																					/*$('#homePageMemberLoginForm').mouseenter(function () {
																						$('#homePageMemberLoginForm .remember').show();
																					}).mouseleave(function(){
																						$('#homePageMemberLoginForm .remember').hide();
																					});
																					$('#homePageMemberLoginForm input').focus(function () {
																						$('#homePageMemberLoginForm .remember').show();
																					})*/

	
});
</script>






		    </div>
			<div id="footer">

				<div class="footerLinks">
					<span class="copy" title="0.332 | 0 | 0% | 0">&copy;2008-2011 <a href="http://blog.hootsuite.com/company/" title="HootSuite Media" target="_blank">HootSuite Media</a></span>
					<div class="language">
					<form name="languageSelectionForm" id="languageSelectionForm" method="get" formtype="input" action="">
	<label for="language" class="title">Language: </label>
	<select name="language" onChange="changeSiteLanguage();return false;">
		<option label="English" value="en" selected="selected">English</option>
<option label="French (Français)" value="fr">French (Français)</option>
<option label="Italian (Italiano)" value="it">Italian (Italiano)</option>
<option label="Japanese (日本語)" value="ja">Japanese (日本語)</option>

	</select>
</form> 
<script type="text/javascript">
$(document).ready(function() {
	$("#languageSelectionForm select[name='language'] option[value='ja']").after('<option disabled="disabled">--------------------</option>');
});
function changeSiteLanguage(){	
	hs.statusObj.update(translation._("Switching language..."), 'info', true, 8000);
	var lang = $("#languageSelectionForm select option:selected").val();
	ajaxCall({
   		type: 'POST',
   		data: "language="+lang,
   		url: "/ajax/index/change-language",
   		success: function(data) {
			hs.statusObj.reset();
   			if (data.success)
   			{
   				//window.location=window.location.href;
   				if ($.isFunction(window.location.reload)) {
					window.location.reload(true);	// force reload
   				} else {
   					window.location = hs.c.rootUrl;
   				}
   			}
   			else if (data.inProgress) {
				var params = {
						width: 347,
						maxHeight: 700,
					    resizable: false,
					    draggable: false,
					    position: ['center', 60],
					    modal: true,
					    title: translation._("HootSuite Translation Project"),
					    content: hsEjs.getEjs('index/language_translation').render(data)
					},
				$popup = $.dialogFactory.create('inProgressLanguagePopup', params);
   	   			return false;
   			}
   			else
   			{
   	   			if (data.paymentProcessorUnavailable && hs.statusObj != undefined)
   	   			{
   					hs.statusObj.update(translation._("Sorry, we are unable to complete this operation right now. Please try again later."), "error", true);
   	   			}
   			}
   		},
   		error: function(){hs.statusObj.reset();}		
   	}, 'abortOld');	
}
</script>					</div>
					                    <a href="http://feedback.hootsuite.com" target="_blank" class="_feedback">Feedback</a> |
                    <a href="http://help.hootsuite.com" target="_blank">Help Desk</a> |
                    <a href="http://blog.hootsuite.com/company/" target="_blank">Company</a> |
                    <a href="http://hootsuite.com/about">About</a> |
                    <a href="http://help.hootsuite.com/forums/81675-faqs" target="_blank">FAQ</a> |
                    <a href="http://blog.hootsuite.com/" target="_blank">Blog</a> |
                    <a href="http://hootsuite.com/terms">Terms</a> |
                    <a href="http://hootsuite.com/privacy">Privacy Policy</a> |
                    <a href="http://hootsuite.com/affiliate">Affiliate</a>

		        </div>
		    </div>
		</div>
        <script type="text/javascript">
        	$(document).ready(function() {
        		hs.statusObj = new statusObject();
        	});
		</script>
        <script type="text/javascript">
			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript"><!--
			var _kmq = _kmq || [];
			var KM_NO_SWF = true;
			function _kms(u){
				setTimeout(function(){
					var s = document.createElement('script'); var f = document.getElementsByTagName('script')[0]; s.type = 'text/javascript'; s.async = true;
					s.src = u; f.parentNode.insertBefore(s, f);
				}, 1);
			}
			_kms('//i.kissmetrics.com/i.js');_kms('//doug1izaerwt3.cloudfront.net/ec496c29d1e36d71c98a9352e630d6a1eff5db68.1.js');
			
			$(document).ready(function() {
				hs.trackFunelEvent('pageView');
			});
		--></script>
		<script type="text/javascript">
			try {
				var pageTracker = _gat._getTracker("UA-17737250-1");
				pageTracker._trackPageview();
			}
			catch(err) {}
        </script>
        			<script type="text/javascript">
			    __compete_code = '71a19afdb2cc5969a9b8957043af665b';
			    (function () {
			        var s = document.createElement('script'),
			            e = document.getElementsByTagName('script')[0],
			            t = document.location.protocol.toLowerCase() === 'https:' ?
			                'https://c.compete.com/bootstrap/' :
			                'http://c.compete.com/bootstrap/';
			            s.src = t + __compete_code + '/bootstrap.js';
			            s.type = 'text/javascript';
			            s.async = true;
			            if (e) { e.parentNode.insertBefore(s, e); }
			        }());
			</script>
		        <!--[if lt IE 7]>
        <script src="https://d2l6uygi1pgnys.cloudfront.net/2-2-09/js/third_party/ie6warning.js"></script><script>window.onload=function(){e("js/ie6/")}</script>
        <![endif]-->

                			<!-- Segment Pixel – Hootsuite - DO NOT MODIFY -->
			<img src="https://secure.adnxs.com/seg?add=67726&t=2" width="1" height="1" />
			<!-- End of Segment Pixel -->
        	</body>
</html>