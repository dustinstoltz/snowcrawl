<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<!-- why is this here? mike 10/27 -->
	
	
	<title>Beauty - Tips and Product Reviews - Real Beauty</title>
	
	<meta name="description" content="Welcome to RealBeauty.com, where women can get real expert advice, receive personalized beauty information based on their specific beauty needs, and connect with other women who have similar beauty interests. Find tips and tricks for creating a new hairstyle, real solutions to skin care issues, losing weight, improving mental well-being, and learning how to make yourself beautiful from the inside out." />
	
	<link rel="shortcut icon" href="/cm/realbeauty/site_images/favicon/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="shortcut icon" href="/cm/realbeauty/site_images/favicon/favicon.ico" type="image/x-icon" />
	
	<meta name="keywords" content=" beauty, advice, beauty tips, product reviews, beuty, beaty, beuaty" />
	
	
    
	<!-- START OF CANONICAL META TAG -->


 
   <link rel="canonical" href="http://www.realbeauty.com"/>
 


<!-- //END OF CANONICAL META TAG -->

    <!-- TMPL xs_meta_head_sprop49 -->
<script language="javascript">var s_prop49 = '';</script>
<!-- /TMPL xs_meta_head_sprop49 --> 
	
    <script language="javascript"type="text/javascript">var docCookies=document.cookie.split(';');var origRefer=null;for(var i=0;i<docCookies.length;i++){var c=docCookies[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf("original_referrer=")==0){origRefer=c.substring("original_referrer=".length,c.length)}}if(origRefer==null){document.cookie="original_referrer="+document.referrer+"; path=/"}</script>
	
    
	
	<!-- audience science segment reader script -->
	<script src="/cm/shared/audience_science/audience_science_segment_reader.js" language="javascript" type="text/javascript"></script><!-- END audience science segment reader script -->
    
	<!-- AD Refresh Script Include -->
	
<script>var _ad_refresh_interval = 3;</script>
<script src="/cm/shared/scripts/refreshads-v2.1.0.js" language="javascript" type="text/javascript"></script>
	<!-- END AD Refresh Script Include -->
	
	<!-- styles -->
	<link rel="stylesheet" type="text/css" href="/cm/realbeauty/styles/global.css" />
	<!--<link rel="stylesheet" type="text/css" href="/cm/realbeauty/styles/navigation.css" />-->
	<link rel="stylesheet" type="text/css" href="/cm/realbeauty/styles/blogs.css" />
	
	
	
	
	<!-- /styles -->
	
	<!-- scripts -->
	<script src="/cm/shared/scripts/jquery-1.4.2.min.js" language="javascript" type="text/javascript"></script>
	<script language="javascript" type="text/javascript" src="/cm/shared/scripts/jquery.json.js"></script>
    <script src="/cm/shared/scripts/jquery-ui-1.8.5.custom.min.js" type="text/javascript"></script>
	<script src="/cm/shared/scripts/jquery.multiselect.js"></script>
	<link rel="stylesheet" href="/cm/shared/scripts/jquery.multiselect.css" />
	<link rel="stylesheet" href="/cm/shared/styles/jquery-ui-themes/realbeauty/jquery-ui-1.8.5.custom.css" />
	<script src="/cm/shared/scripts/jqgetcomments.js" type="text/javascript"></script>
    <script src="/cm/realbeauty/scripts/jquery.simplemodal.js" type="text/javascript"></script>
	<script src="/cm/shared/scripts/jquery.bgiframe.min.js" type="text/javascript"></script>
	<script src="/cm/shared/assets/build/3/scripts/hearst.js"></script>
	<script src="/cm/shared/assets/build/3/scripts/hearst.tempUtils.js"></script>
	<!-- /scripts -->
	
	<!-- video -->
    
	<!-- /video -->
	
	
    <link rel="stylesheet" href="/cm/realbeauty/styles/registration.css" type="text/css" /> 
	
	
    <!--[if lt IE 7.]>
		<script defer type="text/javascript" src="/cm/realbeauty/scripts/pngfix.js"></script>
	<![endif]-->

	<!-- TMPL xs_js_site_object -->
<script>
var HEARST = HEARST || {};
HEARST.site = {
	name: 'Real Beauty',
	prefix: 'realbeauty', 
	mode: 'live',
	root: document.location.protocol + '//' + document.location.hostname,
	urlPath: '/',
	template: {
		fileName: 'homepage'
	},
	section: false,
	topLevelSection: false,
	article: false,
	ha: false,
	trackImage: '/circulation/shared/images/clear.gif'
};
</script>
<!-- /TMPL xs_js_site_object -->

	<!-- START Login/Comments -->
	<script language="javascript">var articleID = null;</script>
	<script language="javascript">var theType = "article";</script>
	<script language="javascript" type="text/javascript" >
//Article Source Code
var source_id = "";
var source_link_type = "3";
</script>
	<script type='text/javascript' src='/cm/realbeauty/scripts/omniture-tags.js'></script>

	<!-- CROSS SITE: xs_test_script_includes.tmpl -->
	<!-- TEST script includes  xs_script_include_user_session.tmpl-->
<script>
var _ghearst_vars = {};
_ghearst_vars["ams_ads_script_src"] = "/ams/page-ads.js?ad_category_prefix=&ad_sub_category_prefix=&ams_promo=&article_type_prefix=&browser_path=%2F&cat_prefixes=&keywords=&position_list=ams_bea_nl01%2Cams_beauty_circ_footer_300x250%2Cams_beauty_wild%2Cams_bty_book_sponlogo%2Cams_bty_fixed_panel%2Cams_bty_footer%2Cams_bty_half_page%2Cams_bty_hp_tout%2Cams_bty_sponsored_links%2Cams_mbox_bty_global&section_prefix=&site_prefix=realbeauty&sub_cat_prefixes=&subdomain=www&url_name=";
</script>
<!--<script src="/cm/shared/scripts/get_mag_user_local_v01.js" type="text/javascript" ></script>-->
<script src="/cm/shared/scripts/get_mag_user_local_v02.js" type="text/javascript" ></script>
<!-- /TEST script includes  xs_script_include_user_session.tmpl live-->

	<!-- TEST script includes  xs_script_include_user_session.tmpl-->
	<!-- END Login/Comments  -->
	<script language="javascript" type="text/javascript" src="/cm/shared/scripts/fixed-panel-ad.js"></script>
<style type="text/css">
#fixedPanelAd {width:336px;height:700px;display:none;}
</style>
<script language="javascript" type="text/javascript">
	<!-- Site specific ad objects -->
	
	$h.fixedPanel.positionList = ['ams_bty_half_page','ams_bty_gallery_hub','ams_bty_gallery','ams_bty_gallery_bot','ams_bty_skyscraper'];
	
	
	
	
	
	
</script>


	<script type='text/javascript' src='/qsp/js/qspUtils.js'></script>
	
	<!-- do not include swf object on tips tool pages as the application includes its own copy (including it twice causes an error in ie8) -->
	<script src="/cm/cosmopolitan/scripts/swfobject2.js" language="javascript" type="text/javascript"></script>
	
	<script src="/cm/realbeauty/scripts/global.js" type="text/javascript"></script>

	<!-- user Contribution css and js -->
	

	

	

	<!-- PUT IN EMAIL THIS SCRIPT and/or FUNCTION -->

	

	

	

	

	


	<!-- start mc-ff-meta-includes -->
<!-- FF styles -->














<!-- FF Scripts -->

<script>var HEARST = HEARST || {};</script>










<!-- end mc-ff-meta-includes -->

	
		<script type="text/javascript" src="http://w.sharethis.com/button/sharethis.js#publisher=d63a4976-501a-446d-81e6-434d03d8388c&amp;type=website&amp;buttonText=&amp;embeds=true&amp;post_services=facebook%2Ctwitter%2Cstumbleupon%2Cmyspace%2Cybuzz%2Cgoogle_bmarks%2Cfriendfeed%2Cblogger%2Cdelicious%2Cwordpress%2Cyahoo_bmarks%2Ctypepad%2Cdigg%2Creddit%2Ctechnorati%2Cmixx%2Cwindows_live%2Cfark%2Cbus_exchange%2Cpropeller%2Cnewsvine%2Clinkedin&amp;headerbg=%2301a0a0&amp;linkfg=%2301a0a0&amp;offsetLeft=-296;onmouseover=false"></script>
	
	
	<!-- quiz includes -->
			
	
	
	
	<!-- Google JS API to search results pages -->
	
	
</head>

<body id="homepage" class="">
<span id="ams_mbox_bty_global"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_mbox_bty_global);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_mbox_bty_global);int=window.clearInterval(int);}},50);}</script></span>



<!-- /END TOP ELEMENTS -->



<div id="site">
<div id="MASTER_Scontainer">
	<div id="MAIN_layout_Scontainer">
		<!-- globalHeader.tmpl -->
<!-- xs_acxiom_tags.tmpl -->

<!-- xs_acxiom_tags.tmpl -->

<div id="HDR_layout_Scontainer">
	

	<div id="HDR_Ccontainer">
		<div id="headerInner">
			<div id="headerLeft">
				<div id="headerTop">
					<div id="headerLogo">
						<img src="/cm/realbeauty/site_images/redesign/header/rb-logo.png" class="sitelogo" alt="Real Beauty - Powered By You" title="Real Beauty - Powered By You" />
						<div class="clear"></div>
					</div>
					<div id="headerSearch">
						<link href="/cm/realbeauty/styles/websearch.css" rel="stylesheet" type="text/css" />
						<script src="/cm/shared/web_search/web_search.js" language="javascript" type="text/javascript"></script>
						<div id="searchbox_top" class="tabbed_search">
							<div id="bg">
								<div id="sitesearch" class="search">
									<form name="result_search" class="search_form" action="/search/fast_search" method="get">
										<input id="gen_search" name="search_term" class="search_input" value="" type="text" autocomplete="off" />
										<a href="javascript:document.result_search.submit();"><img src="/cm/realbeauty/site_images/global/search/button_off.gif" onmouseover="this.src='/cm/realbeauty/site_images/global/search/button_on.gif'" onmouseout="this.src='/cm/realbeauty/site_images/global/search/button_off.gif'" width="21" height="20" class="search_btn" alt="Search" /></a>
									</form>
								</div>
							</div>
							<div class="try"><span>Try: </span><a href="/skin/face/skin-care-secrets?click=try&link=skin-care-secrets">Younger Skin</a><a href="/health/wellness/how-to-have-better-sex?click=try&link=how-to-have-better-sex">Sex Tips</a><a href="/health/diet/drop-five-pounds?click=try&link=drop-five-pounds">Lose 5 lbs</a>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
				<!-- redesignGlobalNavigation.tmpl -->
<div id="globalNavigation">
	<div id="navHair" class="navSection">
		<a class="navLink" title="Hair" href="/hair/"><span>Hair</span></a>
		<div class="dropdown">
			<div class="dropdownOuter">
				<div class="dropdownInner">
					<div class="navColumn left">
						<ul>
							<li><a href="/hair/virtual/hairstyles/" title="Instant Hair Makeover"><span>Instant Hair Makeover</span></a></li>
							<li><a href="/hair/celebrity/" title="Celebrity Hairstyle Gallery"><span>Celebrity Hairstyle Gallery</span></a></li>
							<li><a href="/hair/advice/" title="Hairstylist Q & A"><span>Hairstylist Q&amp;A</span></a></li>
							<li>
								<a href="/hair/styles/" title="Hairstyles & How-Tos"><span>Hairstyles &amp; How-Tos</span></a>
								<span class="catLinks"><a title="curly" href="/hair/styles/curly-hair">curly</a>, <a title="long" href="/hair/styles/long-hair">long</a>, <a title="short" href="/hair/styles/short-hair">short</a>, <a title="shoulder-length" href="/hair/styles/shoulder-length-hair">shoulder-length</a>, <a title="straight" href="/hair/styles/straight-hair">straight</a>, <a title="updo" href="/hair/styles/updo">updo</a>, <a title="wavy" href="/hair/styles/wavy-hair">wavy</a></span>
							</li>
							<li>
								<a href="/hair/care/" title="Hair Care"><span>Hair Care</span></a>
								<span class="catLinks"><a title="dry/damaged" href="/hair/care/dry-damaged-hair">dry/damaged</a>, <a title="fine" href="/hair/care/fine-hair">fine</a>, <a title="frizzy" href="/hair/care/frizzy-hair">frizzy</a>, <a title="normal" href="/hair/care/normal-hair">normal</a>, <a title="oily" href="/hair/care/oily-hair">oily</a>, <a title="thick" href="/hair/care/thick-hair">thick</a></span>
							</li>
							<li class="last"><a href="/products/free-for-you/" title="Win Free Stuff"><span>Win Free Stuff</span></a></li>
						</ul>
					</div>
					<div class="navColumn right">
						<ul>
							<li><a href="/hair/black/" title="African American Hair"><span>African American Hair</span></a></li>
							<li><a href="/hair/asian/" title="Asian Hair"><span>Asian Hair</span></a></li>
							<li><a href="/hair/latina/" title="Latina Hair"><span>Latina Hair</span></a></li>
							<li><a href="/hair/color/" title="Hair Color"><span>Hair Color</span></a></li>
							<li><a href="/hairhowtos" title="Hairstyle How-To Videos"><span>Hairstyle How-To Videos</span></a></li>
							<li><a href="http://answerology.realbeauty.com/" target="_blank" title="Get Hair Advice"><span>Get Hair Advice</span></a></li>
							<li class="last"><a href="/products/hairstyles-makeup/" title="Celebrity Hair & Makeup Blog"><span>Celebrity Hair &amp; Makeup Blog</span></a></li>
						</ul>
					</div>
					<div class="clear"></div>
					<div class="newestLatest">
						
							<a class="imageLink" href="fall-2011-hairstyle-trends"><img class="thumb" src="/cm/realbeauty/images/JN/alexander-wang-centered-part-3-smn.jpg" alt="centered part" /></a>
							<a class="textLink" href="fall-2011-hairstyle-trends">Take a peek at the best hairdos for this coming season.</a>
						
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="navMakeup" class="navSection">
		<a class="navLink" title="Makeup" href="/makeup/"><span>Makeup</span></a>
		<div class="dropdown">
			<div class="dropdownOuter">
				<div class="dropdownInner">
					<div class="navColumn left">
						<ul>
							<li><a href="/makeup/virtual/makeover/" title="Instant Makeup Makeover"><span>Instant Makeup Makeover</span></a></li>
							<li><a href="/makeup/celebrity/" title="Celebrity Makeup Looks"><span>Celebrity Makeup Looks</span></a></li>
							<li><a href="/makeup/advice/makeup-artist/" title="Makeup Artist Q&A"><span>Makeup Artist Q&amp;A</span></a></li>
							<li class="last">
								<a href="/makeup/how-to/" title="Makeup Look How-Tos"><span>Makeup Look How-Tos</span></a>
								<span class="catLinks"><a href="/makeup/how-to/eye-makeup" title="eyes">eyes</a>, <a href="/makeup/how-to/face-makeup" title="face">face</a>, <a href="/makeup/how-to/lip-makeup" title="lips">lips</a>, <a href="/makeup/how-to/makeup-makeover" title="makeovers">makeovers</a></span>
							</li>
						</ul>
					</div>
					<div class="navColumn right">
						<ul>
							<li><a href="/makeuphowtos" title="Makeup How-To Videos"><span>Makeup How-To Videos</span></a></li>
							<li><a href="/makeup/nails/" title="Nail Care & Products"><span>Nail Care &amp; Products</span></a></li>
							<li><a href="http://answerology.realbeauty.com/" title="Get Makeup Advice"><span>Get Makeup Advice</span></a></li>
							<li class="last"><a href="/products/free-for-you" title="Win Free Stuff"><span>Win Free Stuff</span></a></li>
						</ul>
					</div>
					<div class="clear"></div>
					<div class="newestLatest">
						
							<a class="imageLink" href="natural-glow-tips"><img class="thumb" src="/cm/realbeauty/images/u8/rby-on-trend-natural-makeup-philip-lim-smn.jpg" alt="phillip lim spring 2011" /></a>
							<a class="textLink" href="natural-glow-tips">Tips and products to give you glowing skin that looks oh-so-natural.</a>
						
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="navBody" class="navSection">
		<a class="navLink" title="Body & Skin Care" href="/skin/"><span>Body &amp; Skin Care</span></a>
		<div class="dropdown">
			<div class="dropdownOuter">
				<div class="dropdownInner">
					<div class="navColumn left">
						<ul>
							<li>
								<a href="/skin/face/" title="Face"><span>Face</span></a>
								<span class="catLinks"><a href="/skin/face/acne-treatments" title="acne">acne</a>, <a href="/skin/face/anti-age-treatments" title="anti-aging">anti-aging</a>, <a href="/skin/face/dark-circles-puffy-eyes" title="dark circles & puffy eyes">dark circles &amp; puffy eyes</a>, <a href="/skin/face/dry-skin-face" title="dry skin">dry skin</a>, <a href="/skin/face/fine-lines-wrinkles" title="fine lines & wrinkles">fine lines &amp; wrinkles</a>, <a href="/skin/face/redness-rosacea" title="redness">redness</a>, <a href="/skin/face/sensitive-skin" title="sensitive skin">sensitive skin</a>, <a href="/skin/face/whiten-teeth" title="teeth whitening">teeth whitening</a>, <a href="/skin/face/uneven-tone" title="uneven tone">uneven tone</a></span>
							</li>
							<li>
								<a href="/skin/body/" title="Body"><span>Body</span></a>
								<span class="catLinks"><a href="/skin/body/cellulite-creams" title="cellulite">cellulite</a>, <a href="/skin/body/body-acne-treatments" title="body acne">body acne</a>, <a href="/skin/body/dry-skin-treatments" title="dry skin">dry skin</a>, <a href="/skin/body/stretch-mark-treatments" title="stretch marks">stretch marks</a>, <a href="/skin/body/vein-treatments" title="veins & spots">veins &amp; spots</a></span>
							</li>
							<li class="last"><a href="/products/free-for-you" title="Win Free Stuff"><span>Win Free Stuff</span></a></li>
						</ul>
					</div>
					<div class="navColumn right">
						<ul>
							<li><a href="/makeup/advice/dermatologist/" title="Dermatologist Q&A"><span>Dermatologist Q&amp;A</span></a></li>
							<li><a href="/skin/advice/questions/" title="Skin & Body Expert Q&A"><span>Skin &amp; Body Q&amp;A</span></a></li>
							<li><a href="/skin/sun/" title="Suncare & Self-Tanning"><span>Suncare &amp; Self-Tanning</span></a></li>
							<li><a href="/skin/hair-removal/" title="Hair Removal"><span>Hair Removal</span></a></li>
							<li><a href="/fragrance-finder/" title="Fragrance Finder"><span>Fragrance Finder</span></a></li>
							<li class="last"><a href="http://answerology.realbeauty.com" target="_blank" title="Get Body & Skin Advice"><span>Get Body &amp; Skin Advice</span></a></li>
						</ul>
					</div>
					<div class="clear"></div>
					<div class="newestLatest">
						
							<a class="imageLink" href="your-best-skin-ever"><img class="thumb" src="/cm/realbeauty/images/E6/rby-woman-in-winter-smn.jpg" alt="woman in a hat and gloves standing outside in the snow" /></a>
							<a class="textLink" href="your-best-skin-ever">Discover easy ways to get the best skin of your life.</a>
						
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="navDiet" class="navSection">
		<a class="navLink" title="Diet & Health" href="/health/"><span>Diet &amp; Health</span></a>
		<div class="dropdown">
			<div class="dropdownOuter">
				<div class="dropdownInner">
					<div class="navColumn left">
						<ul>
							<li><a href="/health/weight-loss-simulator" title="Virtual Weight Loss Simulator"><span>Virtual Weight Loss Simulator</span></a></li>
							<li><a href="/health/advice/" title="Health Expert Q&A"><span>Health Expert Q&amp;A</span></a></li>
							<li><a href="/exercisehowtos" title="Exercise How-To Videos"><span>Exercise How-To Videos</span></a></li>
							<li><a href="http://answerology.realbeauty.com" target="_blank" title="Get Diet & Fitness Advice"><span>Get Diet &amp; Fitness Advice</span></a></li>
							<li class="last"><a href="/products/free-for-you" title="Win Free Stuff"><span>Win Free Stuff</span></a></li>
						</ul>
					</div>
					<div class="navColumn right">
						<ul>
							<li>
								<a href="/health/fitness/" title="Fitness"><span>Fitness</span></a>
								<span class="catLinks"><a href="/health/fitness/exercise-workouts" title="exercise">exercise</a>, <a href="/health/fitness/yoga-pilates" title="yoga & pilates">yoga &amp; pilates</a></span>
							</li>
							<li>
								<a href="/health/diet/" title="Nutrition"><span>Nutrition</span></a>
								<span class="catLinks"><a href="/health/nutrition/eating-healthy" title="nutrition">nutrition</a>, <a href="/health/nutrition/vitamins-supplements" title="vitamins & supplements">vitamins &amp; supplements</a>, <a href="/health/nutrition/weight-diets" title="weight loss">weight loss</a></span>
							</li>
							<li class="last">
								<a href="/health/wellness/" title="Wellness"><span>Wellness</span></a>
								<span class="catLinks"><a href="/health/wellness/body-image" title="body image">body image</a>, <a href="/health/wellness/sexual-health" title="sexual health">sexual health</a>, <a href="/health/wellness/sleep-remedies" title="sleep">sleep</a>, <a href="/health/wellness/stress" title="stress relief">stress relief</a></span>
							</li>
						</ul>
					</div>
					<div class="clear"></div>
					<div class="newestLatest">
						
							<a class="imageLink" href="look-better-naked"><img class="thumb" src="/cm/realbeauty/images/SN/rby-decolletage-reese-witherspoon-smn.jpg" alt="reese witherspoon standing with her hands on her hips" /></a>
							<a class="textLink" href="look-better-naked">10 total-body moves that will tone all your problem areas so you can feel more confident and finally have sex with the lights on.</a>
						
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="navProduct" class="navSection">
		<a class="navLink" title="Product Finder" href="/products/finder/"><span>Product Finder</span></a>
	</div>
	<div class="clear"></div>
</div>
<!-- /redesignGlobalNavigation.tmpl -->
			</div>
			<div id="headerRight">
				
					<!-- globalBeautyBook.tmpl -->
<link rel="stylesheet" href="/cm/realbeauty/styles/beautybook2.css" />
<script>
//Hacking this until Peter can apply the correct question changes
var anskin = 'oily';
</script>
<script src="/cm/shared/beautybook/v2/beautybook.js"></script>
<a name="beautybook"></a>
<div id="bbContainer">
	<div id="bbShadow"></div>
	<div id="bbModule">
		<div id="bbModuleHeader">
			<span id="bbModuleHeaderSignInLink"><a href="#">sign in</a></span>
			<img id="bbProfileImage" src="/cm/realbeauty/site_images/redesign/beautybook/bb-pink-flower-40x40.png" height="40" width="40" />
			<span id="bbModuleHeaderMessage">Sign up for a free</span>
			<img src="/cm/realbeauty/site_images/redesign/beautybook/bb-tab-header-beautybook.png" alt="BeautyBook" title="BeautyBook" />
			<div class="clear"></div>
		</div>
		<p id="bbModuleMessage">The best way to find new articles and products!</p>
		<div id="bbModuleOpen">
			<div id="bbModuleOpenInner">
				<p id="bbModuleOpenMessage">Get Started!</p>
			</div>
			<span id="ams_bty_book_sponlogo"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_book_sponlogo);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_book_sponlogo);int=window.clearInterval(int);}},50);}</script></span>
		</div>
	</div>
	<div id="bbWindow">		
		<div id="bbWindowOuter">
			<div id="bbWindowInner">
				<div id="bbWindowContent">
					<div id="bbWindowReg" class="bbState active">
						<div class="bbNotification">
							<div class="bbNotificationInner">
								<span>Sign up and get recommended tips, products, and expert advice delivered to you daily. 
									<span class="bbTip">How does this work?
										<span class="bbTooltip">
											<span class="bbTooltipInner">
												<p>Sign up for a <span class="pink">FREE</span> <strong>Beauty Book</strong> by taking our short beauty profile quiz to get personalized tips, tricks, advice, and related articles and best products.</p>
												<p>To learn more about the Beauty Book or find answers to other questions you may have, please visit our <a href="/beauty-book-help">Beauty Book FAQ page.</a></p>
											</span>
										</span>
									</span>
								</span>
							</div>
						</div>
						<div id="bbWindowRegBody">
							<div class="bbAdvertisement">
								<span class="adLabel">Advertisement</span>
								<div class="adBody"></div>
							</div>
							<div id="bbWindowRegContent">
								<div id="loggedOut" class="regState active">
									<div id="bbLogin">
										<p class="heading">Sign In</p>
										<form id="bbLoginForm" name="bbLoginForm">
											<p class="error"></p>
											<label for="user_name">Username or email:</label>
											<input type="text" name="user_name" id="bbLoginUsername" />
											<label for="password">Password:</label>
											<input type="password" name="password" id="bbLoginPassword" />
											<div class="right"><button type="submit" id="bbLoginSubmit"><span>Sign In</span></button></div>
											<div class="clear"></div>
										</form>
										<a class="forgotPassword" href="/registration/forgotPassword.html">Forgot your username or password?</a>
									</div>
									<div id="bbSignup">
										<p class="heading">Create an Account</p>
										<form id="bbSignupForm" name="bbSignupForm">
											<p class="error"></p>
											<label for="email">Enter email:</label>
											<input type="text" name="email" id="bbSignupEmail" />
											<div class="right"><button type="submit" id="bbSignupSubmit"><span>Create an Account</span></button></div>
											<div class="clear"></div>
										</form>
									</div>
								</div>
								<div id="autoRegComplete" class="regState">
									<p class="heading">Thanks for Joining!</p>
									<p>Your information has been saved and an account has been created for you giving your full access to everything RealBeauty.com and Hearst Digital Media Network have to offer. Your username and password have been assigned below.</p>
									<p>To personalize your username and/or password or complete your profile, <a href="/registration/editProfile.html">click here</a>.</p>
									<div id="userData">
										<p>Username: <span class="username"></span></p>
										<p>Password: <span class="password"></span></p>
									</div>
									<a class="continue"><span>Continue</span></a>
								</div>
								<div id="firstBBQuestion" class="regState">
									<p class="heading">To create your Beauty Book profile, begin by answering a few questions about your hair, makeup, skin, and body.</p>
									<div class="questionContainer"></div>
									<p>You can also <a href="/registration/editProfile.html">edit your complete profile.</a></p>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div id="bbWindowLoggedIn" class="bbState">
						<div id="bbWindowTabs">
							<div class="bbTab" id="tab_articlesForYou">
								<a href="#bbArticles" title="Articles For You">Articles For You</a>
							</div>
							<div class="bbTab" id="tab_bestProducts">
								<a href="#bbProducts" title="Best Products">Best Products</a>
							</div>
							<div class="bbTab" id="tab_savedProducts">
								<a href="#bbSavedProducts" title="Saved Products">Saved Products</a>
							</div>
							<div class="bbTab prev" id="tab_savedArticles">
								<a href="#bbSavedArticles" title="Saved Articles">Saved Articles</a>
							</div>
							<div class="bbTab selected" id="tab_aboutMe">
								<a href="#bbAboutMe" title="About Me">About Me</a>
							</div>
							<div class="clear"></div>
						</div>
						<div id="bbWindowTabBody">
							<div class="bbAdvertisement">
								<span class="adLabel">Advertisement</span>
								<div class="adBody"></div>
							</div>
							<div id="bbWindowTabContent">
							
								<div class="bbTabContent" id="articlesForYou">
									<div class="bbNotification">
										<div class="bbNotificationInner">
											<span>Click on the <img src="/cm/realbeauty/site_images/redesign/beautybook/bb-pink-heart-14x14.png" width="14" height="14" alt="Heart" title="Heart" /> icon to save an article into your beauty book.</span>
										</div>
									</div>						
									<div id="recommendedArticles" class="recommendedContent">
										<p class="heading">Just For You:</p>
										<div id="articleRecommendations" class="recommendations"></div>
										<div id="articleRecommendationsEmpty" class="empty">
											<p><strong>You don't have any article recommendations. Please check back later.</strong></p>
										</div>
									</div>
									<div class="editorialContent">
										<p class="heading">You Might Also Like:</p>
										
											<div class="item">
												<img class="itemImage" src="/cm/realbeauty/images/qs/rby-kim-kardashian-smoky-eye-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/how-to/ultimate-smoky-eye-guide?click=bb_ymal">The Ultimate Smoky E...</a>
													
														<div class="itemDescription">
															
																See how to recreate your favorite celebs makeup look.
															
														</div>
													
												</div>
											</div>
										
											<div class="item">
												<img class="itemImage" src="/cm/realbeauty/images/Ic/rby-lip-moisturizers-kissmyface-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/how-to/best-lip-moisturizers?click=bb_ymal">33 Best Lip Moisturi...</a>
													
														<div class="itemDescription">
															
																The best balms, glosses, and sticks to perfect your pucker this winter&hellip;
															
														</div>
													
												</div>
											</div>
										
											<div class="item last">
												<img class="itemImage" src="/cm/realbeauty/images/3l/rby-at-every-age-jennifer-garner-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/celebrity/celebrity-beauty-secrets?click=bb_ymal">Best Celebrity Beaut...</a>
													
														<div class="itemDescription">
															
																Shh! The must-know beauty tips the stars swear by.
															
														</div>
													
												</div>
											</div>
										
									</div>
								</div>
								<div class="bbTabContent" id="bestProducts">
									<div class="bbNotification">
										<div class="bbNotificationInner">
											<span>Click on the <img src="/cm/realbeauty/site_images/redesign/beautybook/bb-pink-heart-14x14.png" width="14" height="14" alt="Heart" title="Heart" /> icon to save a best products article. <a href="/products/finder/">Find Recommended Products &raquo;</a></span>
										</div>
									</div>
									<div id="recommendedProducts" class="recommendedContent">
										<p class="heading">Just For You:</p>
										<div id="productRecommendations" class="recommendations empty"></div>
										<div id="productRecommendationsEmpty" class="empty">
											<p><strong>You don't have any product recommendations. Please check back later.</strong></p>
										</div>
									</div>
									<div class="editorialContent">
										<p class="heading">You Might Also Like:</p>
										
											<div class="item">
												<img class="itemImage" src="/cm/realbeauty/images/qs/rby-kim-kardashian-smoky-eye-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/how-to/ultimate-smoky-eye-guide?click=bb_ymal">The Ultimate Smoky E...</a>
													
														<div class="itemDescription">
															
																See how to recreate your favorite celebs makeup look.
															
														</div>
													
												</div>
											</div>
										
											<div class="item">
												<img class="itemImage" src="/cm/realbeauty/images/Ic/rby-lip-moisturizers-kissmyface-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/how-to/best-lip-moisturizers?click=bb_ymal">33 Best Lip Moisturi...</a>
													
														<div class="itemDescription">
															
																The best balms, glosses, and sticks to perfect your pucker this winter&hellip;
															
														</div>
													
												</div>
											</div>
										
											<div class="item last">
												<img class="itemImage" src="/cm/realbeauty/images/3l/rby-at-every-age-jennifer-garner-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/celebrity/celebrity-beauty-secrets?click=bb_ymal">Best Celebrity Beaut...</a>
													
														<div class="itemDescription">
															
																Shh! The must-know beauty tips the stars swear by.
															
														</div>
													
												</div>
											</div>
										
									</div>
									<div class="clear"></div>
								</div>
								<div class="bbTabContent" id="savedProducts">
									<div class="bbNotification">
										<div class="bbNotificationInner">
											<span>Click on the <img src="/cm/realbeauty/site_images/redesign/beautybook/pink-x-14x14.png" width="14" height="14" alt="X" title="X" /> icon to delete a product. <a href="/products/finder/">Find Recommended Products &raquo;</a></span>
										</div>
									</div>
									<div id="savedProductContent" class="savedContent">
										<p class="heading">Saved Products:</p>
										<div id="savedProductList">
											<div id="bbProductsEmpty" class="empty">
												<p><strong>You don't have any saved products.</strong></p>
												<p>Click on the <img src="/cm/realbeauty/site_images/redesign/beautybook/bb-pink-heart-14x14.png" width="14" height="14" alt="Heart" title="Heart" /> to save a product into your Beauty Book from the <a href="/products/finder/">Product Finder</a>.</p>
											</div>
											<div id="bbProductsLoading" class="loading">
												<p>Please wait while we look for your products...</p>
											</div>
											<div id="bbProducts"></div>
										</div>
									</div>
									
									<div class="editorialContent">
										<p class="heading">You Might Also Like:</p>
										
											<div class="item">
												<img class="itemImage" src="/cm/realbeauty/images/qs/rby-kim-kardashian-smoky-eye-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/how-to/ultimate-smoky-eye-guide?click=bb_ymal">The Ultimate Smoky E...</a>
													
														<div class="itemDescription">
															
																See how to recreate your favorite celebs makeup look.
															
														</div>
													
												</div>
											</div>
										
											<div class="item">
												<img class="itemImage" src="/cm/realbeauty/images/Ic/rby-lip-moisturizers-kissmyface-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/how-to/best-lip-moisturizers?click=bb_ymal">33 Best Lip Moisturi...</a>
													
														<div class="itemDescription">
															
																The best balms, glosses, and sticks to perfect your pucker this winter&hellip;
															
														</div>
													
												</div>
											</div>
										
											<div class="item last">
												<img class="itemImage" src="/cm/realbeauty/images/3l/rby-at-every-age-jennifer-garner-smn.jpg" width="60" height="60" />
												<div class="itemContent">
													<img class="bbSaveHeart" src="/cm/realbeauty/site_images/redesign/beautybook/bb-blue-heart-14x14.png" alt="Click to Save" title="Click to Save" />
													<div class="actionTextOuter"><div class="actionTextInner"><div class="actionText">Save</div></div></div>
													<a class="itemTitle" href="/skin-makeup/celebrity/celebrity-beauty-secrets?click=bb_ymal">Best Celebrity Beaut...</a>
													
														<div class="itemDescription">
															
																Shh! The must-know beauty tips the stars swear by.
															
														</div>
													
												</div>
											</div>
										
									</div>
									<div class="clear"></div>
								</div>
								<div class="bbTabContent" id="savedArticles">
									<div class="bbNotification">
										<div class="bbNotificationInner">
											<span>Click on the <img src="/cm/realbeauty/site_images/redesign/beautybook/pink-x-14x14.png" width="14" height="14" alt="X" title="X" /> icon to delete an article or photo.</span>
										</div>
									</div>
									<div id="savedArticleContent" class="savedContent">
										<p class="heading">Saved Articles:</p>
										<div id="savedArticleList">
											<div id="bbArticlesEmpty" class="empty">
												<p><strong>You haven't saved any articles.</strong></p>
												<p>Start saving <a class="tabOpen_articlesForYou" href="#">tips and expert advice</a> articles now.</p>
											</div>
											<div id="bbArticlesLoading" class="loading">
												<p>Please wait while we look for your articles...</p>
											</div>
											<div id="bbArticles"></div>
										</div>
									</div>
									<div id="savedPhotos" class="editorialContent">
										<p class="heading">Saved Photos:</p>
										<div id="savedPhotosList">
											<div id="bbPhotosEmpty" class="empty">
												<p><strong>You haven't saved any photos.</strong></p>
												<p>Find <a class="tabOpen_articlesForYou" href="#">makeup looks and hairstyles</a> to try now.</p>	
											</div>
											<div id="bbPhotosLoading" class="loading">
												<p>Please wait while we look for your photos...</p>
											</div>
											<div id="bbPhotos"></div>
										</div>
									</div>
								</div>
								<div class="bbTabContent active" id="aboutMe">
									<div class="bbNotification">
										<div class="bbNotificationInner">
											<span><span class="notificationText">Take our beauty profile quiz to get articles &amp; products just for you.</span>
												<span class="bbTip">How does this work?
													<span class="bbTooltip">
														<span class="bbTooltipInner">
															<p>Sign up for a <span class="pink">FREE</span> <strong>Beauty Book</strong> by taking our short beauty profile quiz to get personalized tips, tricks, advice, and related articles and best products.</p>
															<p>To learn more about the Beauty Book or find answers to other questions you may have, please visit our <a href="/beauty-book-help">Beauty Book FAQ page.</a></p>
														</span>
													</span>
												</span>
											</span>
										</div>
									</div>
									<div class="leftContent">
										<div id="bbAboutMeQuestions">
											<div class="questionContainer"></div>
											<a class="tabOpen_articlesForYou" href="#">Or see what's being recommended for you</a>
										</div>
										<div id="bbAboutMeLinks"></div>
									</div>
									<div class="rightContent">
										<div id="bbAboutMeProfile">
											<img class="profileImage" src="#" width="90" height="90" border="0" />
											<div id="profileNav">
												<div id="profileProgress">
													<p class="heading progresstext"></p>
													<div class="progressbar"></div>
												</div>
												<ul id="profileLinks">
													<li><a class="changePhoto" href="/registration/editProfile.html">Change Photo</a></li>
													<li><a class="editProfile" href="/registration/editProfile.html">Edit Profile</a></li>
												</ul>
												<div class="clear"></div>
											</div>
											<div class="clear"></div>
										</div>
										<div id="bbAboutMeAnswers">
											<p class="heading">Previous Questions and Answers</p>
											<div class="listContainer"></div>
											<a href="/registration/editProfile.html">View all answers</a>
										</div>
									</div>
								</div>
								
							</div>
							<div class="clear"></div>
						</div>
					</div>
					
				</div>
				<div id="bbWindowClose">
					<div id="bbWindowCloseInner">
						<a title="Close the Beauty Book">Close</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

$h.beautyBook.init();

</script>
<!-- /globalBeautyBook.tmpl -->
				
			</div>
			<div class="clear"></div>
		</div>
		<div id="headerShadow"></div>
	</div>
</div>
<!-- /globalHeader.tmpl -->
		<div id="CORE_Scontainer">
			<div id="MAIN_2COL_layout_Scontainer">
               <div id="profile_header">
               



<div class="profile_module">

    
    <div class="feature">
	    <div class="banner"><span>{</span>New Today:<span>}</span></div>
        
        <div class="body">
            <p><a href="/hair/celebrity/celebrity-hairstyles-and-makeup-at-baseball-games">Sporty Celeb Style</a></p><div class="clear"></div>
            <p>From ponytails to smoky eyes, these stars cheer on their favorite teams.</p>
            <div class="buttonArrow_HP"><a href="/hair/celebrity/celebrity-hairstyles-and-makeup-at-baseball-games">View Photos</a></div>
        </div>
        
    </div>
    <div class="list">
        <img src="/cm/realbeauty/site_images/global/homepage/listLabel.gif" class="label"/>
        <ul>
            <li><img class="small_th" src="/cm/realbeauty/images/AQ/rb-browsing-laptop-diet-websites-0809-smn.jpg" /><a href="http://www.realbeauty.com/products/beauty-book-tour">Create a Free Beauty Book Now For a Chance to Win a $25 Gift Card to Sephora!</a></li><li><img class="small_th" src="/cm/realbeauty/images/xD/beauty-basic-reverse-lunge-smn.jpg" /><a href="http://www.realbeauty.com/health/fitness/endurance-training">Your Free Personal Trainer: Easy daily exercises to lose weight and get toned.</a></li><li class="last"><img class="small_th" src="/cm/realbeauty/images/Lh/rby-mascara-covergirl-sm-new.jpg" /><a href="http://www.realbeauty.com/makeup/how-to/best-beauty-products-under-25">Best Beauty Under $25: 17 must-have drugstore buys that won't break the bank.</a></li>
        </ul>
    </div>
    
<style>
#homepage #profile_header .profile_module {background:url(/cm/realbeauty/images/2F/celeb_baseball.jpg) no-repeat;}
</style>
 

<div class="clear"></div>
</div>




               </div>
               <div id="spacer">&nbsp;</div>
               <div id="MAIN_CENTER_Ccontainer">
               <div id="content">
                    <div id="expert_promos">
						<div class="page_head"><img src="/cm/realbeauty/site_images/homepage/header-answers-news-tips.gif" width="620" height="18" alt="News, Tips, and Trends" /></div>
                        
                        
                            <div class="promo_wrapper counter__1">
                              <a href="http://www.realbeauty.com/products/hairstyles-makeup/gwyneth-paltrow-food-magazine"><img src="/cm/realbeauty/images/hJ/rby-celeb-hair-paltrow-morroco-2009-smn.jpg" width="120" height="120" class="th" alt="Gwyneth Paltrow: Magazine Editor?" /></a>
                              <div class="hed"><a href="http://www.realbeauty.com/products/hairstyles-makeup/gwyneth-paltrow-food-magazine">Gwyneth Paltrow: Magazine Editor?</a></div>
                              <div class="teaser">The possible next step for the A-list actress.&#8230; <span><a href="http://www.realbeauty.com/products/hairstyles-makeup/gwyneth-paltrow-food-magazine">More &raquo;</a></span></div>
                            </div>
                           
                        
                            <div class="promo_wrapper counter__2">
                              <a href="http://www.realbeauty.com/hair/longwear-hair-and-makeup-products"><img src="/cm/realbeauty/images/5w/urban-decay-pencils-smn.jpg" width="120" height="120" class="th" alt="Beautiful Idea: 24-hour Products" /></a>
                              <div class="hed"><a href="http://www.realbeauty.com/hair/longwear-hair-and-makeup-products">Beautiful Idea: 24-hour Products</a></div>
                              <div class="teaser">The new buys that really go the distance.&#8230; <span><a href="http://www.realbeauty.com/hair/longwear-hair-and-makeup-products">More &raquo;</a></span></div>
                            </div>
                           
                        
                            <div class="promo_wrapper counter__3">
                              <a href="http://www.realbeauty.com/products/hairstyles-makeup/beauty-tips-for-rebecca-black"><img src="/cm/realbeauty/images/d4/rebecca_black-sm.jpg" width="120" height="120" class="th" alt="YouTube's Hair and Makeup Problems" /></a>
                              <div class="hed"><a href="http://www.realbeauty.com/products/hairstyles-makeup/beauty-tips-for-rebecca-black">YouTube's Hair and Makeup Problems</a></div>
                              <div class="teaser">Beauty tips for Rebecca Black and others.&#8230; <span><a href="http://www.realbeauty.com/products/hairstyles-makeup/beauty-tips-for-rebecca-black">More &raquo;</a></span></div>
                            </div>
                           
                        
                            <div class="promo_wrapper promo_last counter__4">
                              <a href="http://www.realbeauty.com/hair/celebrity/princess-hair"><img src="/cm/realbeauty/images/sc/rby-celeb-inspiration-princess-eugenie-smn.jpg" width="120" height="120" class="th" alt="Real Life Princess Hair" /></a>
                              <div class="hed"><a href="http://www.realbeauty.com/hair/celebrity/princess-hair">Real Life Princess Hair</a></div>
                              <div class="teaser">Royal styles from around the globe.&#8230; <span><a href="http://www.realbeauty.com/hair/celebrity/princess-hair">More &raquo;</a></span></div>
                            </div>
                           <div class="promo_wrapper promo_last adTout"><span id="ams_bty_hp_tout"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_hp_tout);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_hp_tout);int=window.clearInterval(int);}},50);}</script></span></div>
                         
                      
                    </div><!-- //end of expert featured promos -->
   				
   				
                    <!-- START OF NEW ON THE SHELVES TOOL -->
                    <div id="new_tool" class="tool">
                              <a href="/hair/celebrity/kate-middleton-wedding"> <img src="/cm/realbeauty/site_images/homepage/hp_tout_kate_middleton.jpg" /></a>
                <style>
body#homepage #new_tool.tool .title a{color:#ff3366;font-style:normal;font-weight:bold; font-size:34px;line-height:36px;}
body#homepage #new_tool.tool .title a:hover{color:#ff3366!important;}
body#homepage #new_tool.tool .dec {font-style:italic;}
</style>    
		</div><!-- //end of promo tool NEW ON THE SHELVES -->
                    
                    
                    <div id="newest_wrapper">
                    	<div class="page_head"><img src="/cm/realbeauty/site_images/homepage/header-newest-latest.gif" width="620" height="18" alt="Newest & Latest" /></div>
                         <div id="articles_wrapper">                   
                         
  
 <div class="article">
      <div class="overlay hidden" onclick="javascript:location='http://www.realbeauty.com/skin/face/how-smart-is-your-skin-care-regime'">
           <div class="overlay_txt">We'll help you brush up on facts if you get a "pore" score.</div>
           <div class="overlay_bg"></div>
      </div>
      <a href="http://www.realbeauty.com/skin/face/how-smart-is-your-skin-care-regime"><img src="/cm/realbeauty/images/al/rby-33-beauty-myths-zit-de.jpg" width="280" height="280" class="newest_img" alt="QUIZ: Are You Skin Smart?" /></a>
      <div class="title"><a href="http://www.realbeauty.com/skin/face/how-smart-is-your-skin-care-regime">QUIZ: Are You Skin Smart?</a></div>
 </div>
 
 <div class="article right">
      <div class="overlay hidden" onclick="javascript:location='http://www.realbeauty.com/skin/body/weird-beauty-treatments'">
           <div class="overlay_txt">We've got you covered from to toe&#151;with bull semen facials and fish pedicures.</div>
           <div class="overlay_bg"></div>
      </div>
      <a href="http://www.realbeauty.com/skin/body/weird-beauty-treatments"><img src="/cm/realbeauty/images/mt/rby-weird-beauty-treatments-fish-pedicure-lgn.jpg" width="280" height="280" class="newest_img" alt="Weird Beauty Treatments from Around the World" /></a>
      <div class="title"><a href="http://www.realbeauty.com/skin/body/weird-beauty-treatments">Weird Beauty Treatments from Around the World</a></div>
 </div>
 
 <div class="article">
      <div class="overlay hidden" onclick="javascript:location='http://www.realbeauty.com/makeup/how-to/online-dating-profile-tips'">
           <div class="overlay_txt">The biggest dating site profile mistakes, and how to fix them.</div>
           <div class="overlay_bg"></div>
      </div>
      <a href="http://www.realbeauty.com/makeup/how-to/online-dating-profile-tips"><img src="/cm/realbeauty/images/7A/rby-online-dating-sign-up-de.jpg" width="280" height="280" class="newest_img" alt="Online Dating: Is Your Profile a Turn-Off?" /></a>
      <div class="title"><a href="http://www.realbeauty.com/makeup/how-to/online-dating-profile-tips">Online Dating: Is Your Profile a Turn-Off?</a></div>
 </div>
 
 <div class="article right">
      <div class="overlay hidden" onclick="javascript:location='/products/free-for-you/'">
           <div class="overlay_txt">All month long we're giving away these must-see prizes.</div>
           <div class="overlay_bg"></div>
      </div>
      <a href="/products/free-for-you/"><img src="/circulation/shared/images/strivectin.jpg" width="280" height="280" class="newest_img" alt="Win Free Stuff!" /></a>
      <div class="title"><a href="/products/free-for-you/">Win Free Stuff!</a></div>
 </div>
  <!-- this is new test -->                       
                       </div>
                    </div><!-- //end of newest wrapper -->
                    <div id="answerologyCenterContent">
    					        <div style="width:600px;height:360px;background:#fff;font-family:georgia,sans-serif;font-size:12px;line-height:16px;color:#333;border-bottom:1px dotted #D0D0D0;">

  <div style="height:95px;position:relative;">
    <img src="http://answerology.realbeauty.com//cobrands/realbeauty/widgets/realbeauty_homepage/assets/head-beauty_circles.png" alt="Bueauty Circles" style="border:0;display:block;" />
    <div style="border-top: 1px dotted #D0D0D0;font-size:16px;line-height:30px;color:#656565;">Get advice. Give advice. It's beauty powered by you.</div>
  </div>
    
  <div style="overflow:hidden;zoom:1;">

    <div style="float:left;width:260px;">
      <div style="line-height:22px;font-size:22px;color:#000;padding:0 0 15px;">Today's Question</div>
      <script type="text/javaScript" src="http://answerology.realbeauty.com//index.aspx?template=cobrand_question_of_day_widget.ascx&widgetName=realbeauty_homepage&au=realbeautymodules&__as_javascript=true"></script>
    </div>    
    
    <div style="float:right;width:300px;">
      <div style="line-height:22px;font-size:22px;color:#000;padding:0 0 15px;">Ask Your Question</div>
      <form method="post" action="http://answerology.realbeauty.com/index.aspx?click=ans_uni" style="overflow:hidden;zoom:1;margin:0;padding:0;background:url(http://answerology.realbeauty.com//cobrands/realbeauty/widgets/realbeauty_homepage/assets/ask_bg.png) 0 0 no-repeat;">
       <input name="template" value="ask_question.ascx" type="hidden">
       <input name="setReferrer" value="loveandsex121" type="hidden">
       <div style="padding: 5px 10px 5px 15px; margin: 4px 0pt 10px;">
         <textarea name="question" cols="40" rows="5" style="outline: medium none; font-family: verdana,sans-serif; font-size: 14px; line-height: 19px; border: 0pt none; background: none repeat scroll 0% 0% transparent; height: 110px; width: 272px;" oninput='ans_limitChars(this);' onfocus='ans_limitChars(this);' onKeyDown='ans_limitChars(this);' onpropertychange='ans_limitChars(this);'>In a nutshell, what would you like to ask?</textarea>
       </div>
       <input type="submit" style="margin-right:5px;float:right;height:22px;width:72px;overflow:hidden;border:0;cursor:pointer;text-indent:-9999px;background:url(http://answerology.realbeauty.com//cobrands/realbeauty/widgets/realbeauty_homepage/assets/btn-submit.png) no-repeat;" value="" onmouseover="this.style.backgroundPosition='0 -22px'"  onmouseout="this.style.backgroundPosition='0 0'">
       <div id="ans_char_count" style="float:left;width:220px;font-size:11px;line-height:18px;color:#878787;padding:0 0 0 2px;font-style:italic;">You have 100 characters remaining</div>
      </form>
    </div>
      
  </div>
    
</div>

<script type="text/javascript">
function ans_limitChars(element) {
    var defaultText = "In a nutshell, what would you like to ask?";	
    var currentText = element.value;
    var truncatedString = currentText.substr(0,100);

    if (truncatedString == defaultText) {
        document.getElementById("ans_char_count").innerHTML = "You have 100 characters remaining";
        element.value = '';
        return true;
    }

    document.getElementById("ans_char_count").innerHTML = "You have " + (100 - truncatedString.length) + " characters remaining";

    if (truncatedString != currentText) {
        element.value = truncatedString;
    }
}
</script>

<script type="text/javascript">function ansunifiedTracking(){s.events=s.appendList(s.events,'event53',',','1');}</script>

      
    				</div>
               </div><!-- //end of content -->
                    <!-- globalFooterAdModule.tmpl -->

	<div id="footer_ad_modules">

    <!-- ad_SponsoredLinks -->
<div id="AD_sponsoredLinks">
<p class="adLabels">Advertisement</p>
<span id="ams_bty_sponsored_links"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_sponsored_links);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_sponsored_links);int=window.clearInterval(int);}},50);}</script></span>
<div class="clear"></div>

</div>


    <div id="adCirc300X250"><p class="adLabels">Special Offer</p>
		
			<span id="ams_beauty_circ_footer_300x250"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_beauty_circ_footer_300x250);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_beauty_circ_footer_300x250);int=window.clearInterval(int);}},50);}</script></span>
		
	<div class="clear"></div>
</div>
	    
</div>
<br clear="all" />


                   
                </div><!-- //end of main center con -->
                
                
                
				<div id="MAIN_RIGHT_Ccontainer"><!-- globalRightRail.tmpl -->
<div id="fixedPanelRROuter">

	<div id="fixedPanelAd"><span id="ams_bty_fixed_panel"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_fixed_panel);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_fixed_panel);int=window.clearInterval(int);}},50);}</script></span></div>
	
	<div id="fixedPanelRRInner">
		
			
			
			
				<div id="AD_wildcard">

<span id="ams_beauty_wild"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_beauty_wild);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_beauty_wild);int=window.clearInterval(int);}},50);}</script></span>
<div class="clear"></div>
</div>


				
				<div id="AD_half">
<p class="adLabels">Advertisement</p>
<span id="ams_bty_half_page"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_half_page);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_half_page);int=window.clearInterval(int);}},50);}</script></span>
</div>


				
				
					 <div id="rb_articles">
						  
						  <div class="page_head"><img src="/cm/realbeauty/site_images/homepage/header-expert_advice.gif" width="342" height="21" alt="Expert Advice" /></div>
					 
							   
							   <div class="promo_wrapper">	
									<div class="txt_wrapper">
										 <div class="hed"><a href="/makeup/advice/makeup-artist/blush-bronzer-brushes">Picking the Right Brush for Your Blush and Bronzer</a></div>
										 <div class="teaser"><span class="quote">&ldquo;</span><span class="txt">Find out how to determine what size brush you should use for your blush or bronzer.</span><span class="quote end">&rdquo;</span></div>
										 <span><a href="/makeup/advice/makeup-artist/blush-bronzer-brushes">Read More &raquo;</a></span>
									</div>
									<a href="/makeup/advice/makeup-artist/blush-bronzer-brushes"><img src="/cm/realbeauty/images/WU/33-ways-brushes-sm.jpg" width="120" height="120" class="rb_thumb" alt="makeup brushes" /></a>
							   </div>
							   
							   <div class="promo_wrapper">	
									<div class="txt_wrapper">
										 <div class="hed"><a href="/makeup/advice/makeup-artist/lower-lashes-mascara-tips">How to Apply Mascara on Your Lower Lashes</a></div>
										 <div class="teaser"><span class="quote">&ldquo;</span><span class="txt">Perk up your eyes by giving your lower lashes some love.</span><span class="quote end">&rdquo;</span></div>
										 <span><a href="/makeup/advice/makeup-artist/lower-lashes-mascara-tips">Read More &raquo;</a></span>
									</div>
									<a href="/makeup/advice/makeup-artist/lower-lashes-mascara-tips"><img src="/cm/realbeauty/images/Gc/Claire-Danes-Latisse-lgn-small_new.jpg" width="120" height="120" class="rb_thumb" alt="claire danes" /></a>
							   </div>
							   
							   <div class="promo_wrapper">	
									<div class="txt_wrapper">
										 <div class="hed"><a href="/hair/advice/hairstylist/ombre-hair">How Can I Pull Off the Ombr&#233; Hair Trend?</a></div>
										 <div class="teaser"><span class="quote">&ldquo;</span><span class="txt">How to get ombr&#233; hair and not make it look like you forgot about your roots.</span><span class="quote end">&rdquo;</span></div>
										 <span><a href="/hair/advice/hairstylist/ombre-hair">Read More &raquo;</a></span>
									</div>
									<a href="/hair/advice/hairstylist/ombre-hair"><img src="/cm/realbeauty/images/SI/conrad-ombre-sm.jpg" width="120" height="120" class="rb_thumb" alt="lauren conrad at a book signing in 2010" /></a>
							   </div>
							   
							   <div class="promo_wrapper">	
									<div class="txt_wrapper">
										 <div class="hed"><a href="/health/advice/lose-weight-fast-and-safe">What's an Effective Way to Lose Weight Fast?</a></div>
										 <div class="teaser"><span class="quote">&ldquo;</span><span class="txt">Have a big event coming up that you need to slim down for? Here's how to lose weight quick but safel</span><span class="quote end">&rdquo;</span></div>
										 <span><a href="/health/advice/lose-weight-fast-and-safe">Read More &raquo;</a></span>
									</div>
									<a href="/health/advice/lose-weight-fast-and-safe"><img src="/cm/realbeauty/images/ug/rb-shy-feet-on-scale-diet-0809-lgn-small_new.jpg" width="120" height="120" class="rb_thumb" alt="older weight gain" /></a>
							   </div>
							   
							   <div class="promo_wrapper">	
									<div class="txt_wrapper">
										 <div class="hed"><a href="/makeup/advice/makeup-artist/breaking-eyeliner-tips">How Can I Keep My Eyeliner from Breaking?</a></div>
										 <div class="teaser"><span class="quote">&ldquo;</span><span class="txt">Does your eyeliner crumble at the slightest amount of pressure? Here's the quick fix.</span><span class="quote end">&rdquo;</span></div>
										 <span><a href="/makeup/advice/makeup-artist/breaking-eyeliner-tips">Read More &raquo;</a></span>
									</div>
									<a href="/makeup/advice/makeup-artist/breaking-eyeliner-tips"><img src="/cm/realbeauty/images/Lm/rby-mary-kay-eyeliner-tahitian-gold-smn.jpg" width="120" height="120" class="rb_thumb" alt="scribble from an eyeliner pencil" /></a>
							   </div>
							   
							   <div class="promo_wrapper">	
									<div class="txt_wrapper">
										 <div class="hed"><a href="/skin/advice/questions/how-to-keep-smooth-between-waxes">How to Keep Smooth Between Waxing Appointments</a></div>
										 <div class="teaser"><span class="quote">&ldquo;</span><span class="txt">Maintain your bikini line here.</span><span class="quote end">&rdquo;</span></div>
										 <span><a href="/skin/advice/questions/how-to-keep-smooth-between-waxes">Read More &raquo;</a></span>
									</div>
									<a href="/skin/advice/questions/how-to-keep-smooth-between-waxes"><img src="/cm/realbeauty/images/Wz/rb-bikini-line-waxing-advice-1-0809-smn.jpg" width="120" height="120" class="rb_thumb" alt="red and black bikini bottoms" /></a>
							   </div>
							   
						  
					 </div><!-- //end of Real Beauties: WHAT I DO articles -->
				
				
				<!-- ad_Newsletters -->
<div id="AD_NL">
<span id="ams_bea_nl01"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_bea_nl01);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_bea_nl01);int=window.clearInterval(int);}},50);}</script></span>
<div class="clear"></div>
</div>
<!-- /ad_Newsletters -->

				
		
			
		
		
	</div>
</div>
</div>
                 <div class="clear"></div>
                    
			</div><!-- //end of main 2col -->
		</div> <!-- //end of core -->
        <div id="MAIN_2COL_SPAN_BOTTOM_Ccontainer">
	   <!-- do I need stuff here -->
        </div><!-- //end of main 2col span bottom con -->
        <!-- TMPL globalFooter -->

<div id="FTR_Ccontainer">
	<div id="fixedPanelFooter">
		<div class="search_try_links">
			<div id="searchform_bottom" class="tabbed_search">
				<div id="bg">
					<!-- start site search -->
					<div id="sitesearch_b" class="search">
						<form name="result_search_b" class="search_form" action="/search/fast_search" method="get">
							<input id="gen_search" name="search_term" class="search_input" value="" type="text" autocomplete="off" />
							<a href="javascript:document.result_search_b.submit();"><img src="/cm/realbeauty/site_images/global/search/button_off.gif" onmouseover="this.src='/cm/realbeauty/site_images/global/search/button_on.gif'" onmouseout="this.src='/cm/realbeauty/site_images/global/search/button_off.gif'" width="21" height="20" class="search_btn" alt="Search" /></a>
						</form>
					</div><!-- //end of site search -->
				</div><!-- //end of bg -->
				
				<div class="try"><span>Try: </span><a href="/skin/face/skin-care-secrets?click=try&link=skin-care-secrets">Younger Skin</a><a href="/health/wellness/how-to-have-better-sex?click=try&link=how-to-have-better-sex">Sex Tips</a><a href="/health/diet/drop-five-pounds?click=try&link=drop-five-pounds">Lose 5 lbs</a></div>
			</div><!-- //end of searchform bottom -->
		</div>
		
		<div id="ftrSiteLinks">
			<div class="about_realbeauty link_list">
				<h6>About Real Beauty</h6>
				<ul>
					<li><a href="/sitemap">Site Map</a></li>
					<li><a href="http://cosmetics.realbeauty.com/sitemap.html" rel="nofollow" target=_blank>Product Finder Site Map</a></li>
					<li><a href="/about/privacy-policy" rel="nofollow">Privacy and Terms of Use</a></li>
					<li><a href="/about/contact-us" rel="nofollow">Contact Us</a></li>
					<li><a href="/about/faq" rel="nofollow">About Us</a></li>
					<li><a href="/about/community-guidelines" rel="nofollow">Community Guidelines</a></li>
					<li><a href="/about/advertise-online" rel="nofollow">Advertise Online</a></li>
					<li><a href="/about/about-our-ads" rel="nofollow">About Our Ads</a></li>					
				</ul>
			</div>
			
			<div class="realbeauty_magazine link_list">
				<h6>Helpful Links</h6>
				<ul>
					<li><a href="http://subscribe.hearstmags.com/circulation/shared/email/newsletters/signup/bea-su01.html" rel="nofollow">Free Newsletter</a></li>
					<li><a href="http://www.hearstmags.com" rel="nofollow">Magazine Subscriptions</a></li>
					<li><a href="http://www.hearstmags.com" rel="nofollow">Gift Subscriptions</a></li>
				</ul>
			</div>
			
			<!-- START OF XS SEO FOOTER LINKS -->

	<link href="/cm/realbeauty/styles/seo-footer.css" rel="stylesheet" type="text/css" />

      
<div id="seo_links">
	
	
	
	<!-- WOMEN'S NETWORK SEO LINKS - COS, HBZ, MC, RDB, BTY, SEV-->
		
			           
			  
				<div class="links_wrapper first">
						<div class="footerHeds">Hair Ideas</div>
							
							<div class="link"><a href="http://www.realbeauty.com/shopping/new-products/ted-gibson-keratin-review?src=global_footer">Keratin Treatments</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/hair/celebrity/kim-kardashian-hairstyles?src=global_footer">Kim Kardashian's Hair</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/hair/celebrity/victoria-beckham-hairstyles?src=global_footer">Victoria Beckham Hair</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/hair/celebrity/sexy-updos?src=global_footer">Sexy Updos</a></div>
							
							<div class="link"><a href="http://www.marieclaire.com/hair-beauty/celebrity-hairstyles?src=global_footer" target="_blank">Celebrity Hairstyles</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/beauty/hair-ideas/french-braids-0608?src=global_footer" target="_blank">French Braid</a></div>
							
							<div class="link"><a href="http://www.cosmopolitan.com/hairstyles-beauty/hair-care/hair-color-ideas?src=global_footer" target="_blank">Best Hair Colors</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/hair/asian/best-hair-colors-for-asian-skin?src=global_footer">Asian Hair Color</a></div>
							
							<div class="link"><a href="http://www.marieclaire.com/hair-beauty/trends/articles/best-hair-color?src=global_footer" target="_blank">Celebrity Hair Color</a></div>
							
							<div class="link"><a href="http://www.redbookmag.com/beauty-fashion/tips-advice/best-at-home-hair-color?src=global_footer" target="_blank">At-Home Hair Color</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/shopping/new-products/brazilian-blowout-review?src=global_footer">Brazilian Blowout</a></div>
							
				</div><!-- //end of link group #1 name: Hair Ideas -->
			                  
			 
				 
					<div class="links_wrapper">
						<div class="footerHeds">Makeup Tips</div>
							
							<div class="link"><a href="http://www.realbeauty.com/products/new-products/drugstore-makeup-primer?src=global_footer">Best Makeup Primer </a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/skin-makeup/how-to/lips/best-red-lipstick-shade?src=global_footer">Best Red Lipstick</a></div>
							
							<div class="link"><a href="http://cosmetics.realbeauty.com/lips/lipstick/?src=global_footer" target="_blank">Find the Perfect Lipstick</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/skin-makeup/solutions/under-eye/best-eye-creams?src=global_footer">Best Eye Cream</a></div>
							
							<div class="link"><a href="http://cosmetics.realbeauty.com/eyes/?src=global_footer" target="_blank">Best Eye Cosmetics</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/skin-makeup/reviews/lipsticks-glosses/best-pink-lipstick-shade?src=global_footer">Pink Lipstick Shades</a></div>
							
							<div class="link"><a href="http://cosmetics.realbeauty.com/lips/lip-gloss/?src=global_footer" target="_blank">Best Lip Gloss</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/beauty/nail-tips/diy-nail-designs?src=global_footer" target="_blank">Nail Polish Designs</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/skin-makeup/virtual/makeover?src=global_footer">Virtual Makeover</a></div>
							
							<div class="link"><a href="http://cosmetics.realbeauty.com/nails/nail-polish/?src=global_footer" target="_blank">Nail Polish Ideas</a></div>
							
							<div class="link"><a href="http://cosmetics.realbeauty.com/face/mineral-makeup/?src=global_footer" target="_blank">Best Mineral Makeup</a></div>
							
					</div><!-- //end of link group #2 name: Makeup Tips -->
				 
			                   
			 
				 
					<div class="links_wrapper">
						<div class="footerHeds">Fashion</div>
							
							<div class="link"><a href="http://www.harpersbazaar.com/fashion/fashion-articles/best-gifts-for-women?src=global_footer" target="_blank">Best Gifts for Women</a></div>
							
							<div class="link"><a href="http://www.harpersbazaar.com/fashion/fashion-articles/michelle-obama-fashion?src=global_footer" target="_blank">Michelle Obama Fashion</a></div>
							
							<div class="link"><a href="http://www.harpersbazaar.com/fashion/party-snaps/oscars-2010-red-carpet-0310?src=global_footer" target="_blank">Red Carpet Dresses</a></div>
							
							<div class="link"><a href="http://www.kaboodle.com/products/handbags?src=global_footer" target="_blank">Best Handbags</a></div>
							
							<div class="link"><a href="http://www.kaboodle.com/products/shoes?src=global_footer" target="_blank">Sexy Shoes</a></div>
							
							<div class="link"><a href="http://www.kaboodle.com/products/sunglasses?src=global_footer" target="_blank">Hot Sunglasses</a></div>
							
							<div class="link"><a href="http://www.cosmopolitan.com/celebrity/tiptool/look-hot-without-going-broke?src=global_footer" target="_blank">How to Look Hot</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/health/wellness/how-to-pose-for-pictures?src=global_footer">Pose for Pictures</a></div>
							
							<div class="link"><a href="http://www.realbeauty.com/health/fitness/tips-to-get-a-flat-stomach?src=global_footer">Get a Flat Stomach</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/fashion/tips/look-your-best-fbt-0207?src=global_footer" target="_blank">First-Date Fashion</a></div>
							
							<div class="link"><a href="http://www.kaboodle.com/poppicks?src=global_footer" target="_blank">Exclusive Fashion Deals</a></div>
							
					</div><!-- //end of link group #3 name: Fashion -->
				 
			                   
			 
				 
					<div class="links_wrapper last">
						<div class="footerHeds">Relationship Advice</div>
							
							<div class="link"><a href="http://www.marieclaire.com/horoscopes/love/?src=global_footer" target="_blank">Love Horoscopes</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/fun/quizzes/relationship-quizzes?src=global_footer" target="_blank">Relationship Quizzes</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/love/boyfriend-gifts/?src=global_footer" target="_blank">Gifts for Boyfriend</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/love/advice/best-flirting-tips?src=global_footer" target="_blank">Flirting Tips</a></div>
							
							<div class="link"><a href="http://www.cosmopolitan.com/sex-love/relationship-advice/What-Makes-Men-Fall-in-Love?src=global_footer" target="_blank">How to Fall in Love</a></div>
							
							<div class="link"><a href="http://www.cosmopolitan.com/sex-love/dating-advice/How-to-Be-a-Total-Man-Magnet?src=global_footer" target="_blank">How to Attract Men</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/love/date-ideas-for-teens?src=global_footer" target="_blank">Great Date Ideas</a></div>
							
							<div class="link"><a href="http://www.marieclaire.com/health-fitness/ovulation-calendar?src=global_footer" target="_blank">Fertility Calendar</a></div>
							
							<div class="link"><a href="http://www.redbookmag.com/love-sex/advice/love-signs?src=global_footer" target="_blank">Signs He Loves You</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/love/advice/how-to-hook-up?src=global_footer" target="_blank">How to Hook Up</a></div>
							
							<div class="link"><a href="http://www.seventeen.com/love/advice/master-the-make-out?src=global_footer" target="_blank">How to Make Out</a></div>
							
					</div><!-- //end of link group #4 name: Relationship Advice -->
				 
			     
			
		
	
</div><!-- //END OF XS SEO FOOTER LINKS  -->  
			
		</div>
		
		<div id="networkInfo">
			<p>&#169;2011 Hearst Communications, Inc. All Rights Reserved.  <a href="http://www.hearst.com/beinggreen/" target="_blank" >Being Green</a><a class="oba" href="/about/oba-policy" >Why did I get this ad ? </a></p><p><img src="/cm/realbeauty/site_images/global/footer/network.gif" width="264" height="40" alt="Hearst Beauty &amp; Fashion Network" /></p>
		</div>
	</div>
</div>

<div class="clear"></div>


<script>$("#ftrSiteLinks>div").height($("#ftrSiteLinks").height()+"px");</script>
<!-- DO NOT REMOVE -->

<style>#hearstmag_tag_overlay{position:absolute;top:0px;left:0px;}</style>
<!-- SiteCatalyst code version: H.8.
Copyright 1997-2006 Omniture, Inc. More info available at
http://www.omniture.com -->
<script language="JavaScript">var s_account="hmagglobal,hmagrealbeauty"</script>

<script language="JavaScript" src="/cm/shared/scripts/s_code.js"></script>
<script language="JavaScript"><!--
/* You may give each page an identifying name, server, and channel on
the next lines. */
s.pageName="Real Beauty: Home Page"
s.server = "RBEA"
s.channel=""
s.pageType=""

s.prop1=""
s.prop3=""
s.prop4=""
s.prop5=""
s.prop9=""
s.prop10=""
s.prop11=" "
s.prop12=""
s.prop26=""
s.prop29=""
s.hier1=""
/* E-commerce Variables */
s.linkInternalFilters="javascript:,realbeauty.com"

s.visitorNamespace="hearstmagazines"



var rootSection = "";
var userName = "";
s.eVar33 = "";

/* Deal with user data */
if( typeof(mag_user) != "undefined"){
    userName = mag_user.user_name;
    if( mag_user.logged_in == 1 && mag_user.initial_login == 1){
	s.eVar33 = "Logged In";
    } else {
	s.eVar33 = "";
    }
}

if( typeof(hearst_user) != "undefined" && !userName){
    userName = hearst_user.user_name 
    if( hearst_user.logged_in == 1 && hearst_user.initial_login == 1){
        s.eVar33 = "Logged In";
    } else {
        s.eVar33 = "";
    }
}

if( userName && rootSection ){
    s.eVar23 = userName + "|" + rootSection;
} else {
    s.eVar23 = "";
}

s.prop44 = userName;
s.eVar21 = userName;

/* Deal with cookie'd data */
try{
    if(readCookie("redirected_from") != null){
    	s.prop17 = "http://www.realbeauty.com" + unescape(readCookie("redirected_from"))
    	eraseCookie("redirected_from");
    } else {
    	s.prop17 = "";
    }
} catch (err){
    s.prop17 = "";
}

try {
    if(readCookie("original_referrer") != null){
        s.original_referrer=unescape(readCookie("original_referrer"))
    } else {
        createCookie("original_referrer", document.referrer, 0)
        s.original_referrer=document.referrer
    }
} catch(err){
    s.original_referrer = "";
}

s.campaign=""
s.state=""
s.zip=""
s.events=""
s.products=""
s.purchaseID=""
s.eVar1=""
s.eVar2=s.pageName
s.eVar3=s.channel
s.eVar4=s.hier1
s.eVar5=""
s.referrer=document.referrer
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code=s.t();if(s_code)document.write(s_code)//--></script>
<script language="JavaScript" type="text/javascript"><!--
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
//--></script>
<noscript><img src="http://hearstmagazines.112.2o7.net/b/ss/hmagglobal/1/H.22.1--NS/0"
height="1" width="1" border="0" alt="" /></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.22.1. -->


<!-- Audience Science script -->
<script type="text/javascript" src="http://js.revsci.net/gateway/gw.js?csid=I09839" CHARSET="ISO-8859-1"></script>
<SCRIPT LANGUAGE="JavaScript">
<!--	
	var has_bb = mag_user.has_beauty_book || "False";		
	if (has_bb == 0) {
		has_bb = "False";				
	}
	else {
		if (has_bb > 0) {
		has_bb = "True";
		}				
	}	
	I09839.DM_cat("Real Beauty");
	I09839.DM_addEncToLoc("Beauty Book Status",has_bb);
	I09839.DM_tag();
	
//-->
</SCRIPT>
<!-- /Audience Science script -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-10217217-1");
pageTracker._trackPageview();
} catch(err) {}</script>


<script type='text/javascript'>
var dc_UnitID = 14;
var dc_PublisherID = 140073;
var dc_AdLinkColor = '#01a0a0';
var dc_adprod = 'ADL';
</script>
<script type="text/javascript">
if(document.location.protocol=='http:'){
 var Tynt=Tynt||[];Tynt.push('dChsy2rNCr4iIladbi-bpO');Tynt.i={"st":true,"su":false,"ap":"Read more:"};
 (function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
}
</script>
<!-- /TMPL globalFooter -->



<span id="ams_bty_footer"><script>if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_footer);}else{var int=window.setInterval(function(){if(typeof pageAds!="undefined"){document.write(pageAds.ams_bty_footer);int=window.clearInterval(int);}},50);}</script></span>



	</div><!-- //end of main layout scon -->
</div>
</div>
<!-- homepage.tmpl -->
</body>
</html>


