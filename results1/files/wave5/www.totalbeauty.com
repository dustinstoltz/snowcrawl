<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<title>Beauty Tips, Product Reviews, and News from Total Beauty</title>
<meta http-equiv="keywords" content="beauty, beauty products, beauty tips, beauty news, product reviews, free makeup samples, free samples, makeup samples, total beauty" />
<meta http-equiv="description" content="Expert beauty advice, product reviews, beauty tips, makeup samples, cosmetics, and hairstyles all in one place at Total Beauty" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <meta name="robots" content="noodp,noydir" />
<meta property="fb:admins" content="681980083" />
<meta property="fb:page_id" content="20176471206" />
<link rel="alternate" type="application/rss+xml" title="Total Beauty (RSS 2.0)" href="http://www.totalbeauty.com/feeds/totalbeauty.xml" />
<link rel="icon" href="http://images.totalbeauty.com/favicon.ico" type="image/x-icon" />
<link rel="canonical" href="http://www.totalbeauty.com/" />
<link rel="stylesheet" type="text/css" href="http://static1.totalbeauty.com/css/homepage_v7.css?v=20110321181737" />
<link rel="stylesheet" type="text/css" href="http://static1.totalbeauty.com/css/popup_v7.css?v=20110321181737" />
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://static1.totalbeauty.com/css/homepage_v7_ie7.css?v=20110321181737" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="http://static1.totalbeauty.com/css/homepage_v7_ie6.css?v=20110321181737" />
<![endif]-->
  <script type="text/javascript" src="http://static1.totalbeauty.com/js/loadSpeedControl.js?v=20110321181737" id="jsLoadSpeedControl"></script>
</head>
<body>


    <script type="text/javascript">
        trackingURL = document.location.href;
      	if (trackingURL.indexOf('lc=rr1001') !== -1) {
          createCookie('rr','1',.5); //set cookie for 12 hours  
        }
        if(readCookie('rr') == true) {
          document.write('<iframe src="http://www.rr.com/commerce/totalbeautyheader" class="rr_iframe" frameborder="0" scrolling="no"></iframe>');
          //$('body').prepend($('<iframe>').addClass('rr_iframe').attr('scrolling', 'no').attr('frameborder', '0').attr('src', 'http://www.rr.com/commerce/totalbeautyheader'));
          $('#bodyWrap').addClass('body_rr');
        }
            
    </script>


  <div id="bodyWrap">
  <!-- BEGIN WRAPPER -->
    <div id="wrapper" class="home">
      <!-- mastHead -->
      <div id="mastHead">
  <div class="header_top">
    <div class="header_top_inside">
      <div class="logo">
        <div class="logo1">
          <a href="/"><img src="http://images.totalbeauty.com/img/v7/logo.png" width="251" height="41" alt="" /></a>
        </div> 
        <div class="logo_left">
          <a href="/how-tos">Expert <span>advice</span></a>. <a href="/reviews">Unbiased <span>reviews</span></a>. 
        </div>
      </div>
      <div id="dashboard">
        <div id="controlPanel">

      <script type="text/javascript">
    		userHeaderMessage='<div id="centerWrap"><div id="joinNow"><a href="/community/about-badges">Earn rewards. Learn how.</a></div><div id="links"><a href="/accounts/register">Sign in</a><div class="sep"><img src="http://images.totalbeauty.com/img/spacer.gif" /></div><a href="/accounts/register">Join now</a></div></div>';               
    		footerIframe = '';
        if(_checkCookie('login[id]')) {
            //alert('logged in');
            var headerLinks = $.ajax({
          		 type: "POST",
          		 url: '/accounts/setheader',
          		 dataType: 'json',
          		 data: {template:'header_home'},
          		 success: function(data){
              	 if(data.login) {
                  userHeaderMessage = data.data;
                  setTimeout("flashPoints()",1500);
                  footerIframe = data.footer;
            	   }               },
               async: false
              }).responseText;
    			}
        	document.write(userHeaderMessage);
              trackingURL = document.location.href;

      </script>

        </div>
        <div id="social">
          <a href="http://www.facebook.com/totalbeauty?ref=ts" class="newWindow" rel="nofollow">
            <div id="fb_icon"><img src="http://images.totalbeauty.com/img/spacer.gif" /></div>
          </a>
          <a href="http://www.twitter.com/totalbeauty" class="newWindow" rel="nofollow">
            <div id="tw_icon"><img src="http://images.totalbeauty.com/img/spacer.gif" /></div>
          </a>
          <a href="/feeds/totalbeauty.xml" target="_blank">
            <div id="rss_icon"><img src="http://images.totalbeauty.com/img/spacer.gif" /></div>
          </a>
        </div>
      </div>
      <div class="header_advertisement">
        <div class="header_advertisement_text">ADVERTISEMENT</div>
        <div class="header_advertisement_img">
          
		   <!-- begin ad tag -->
	      <div id="adzone" class="tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null"></div>
          <script language="JavaScript" type="text/javascript">
      	   dcopt_print="";
      	   //If it's the first tile set dcopt=ist for js
                 if(0){
      	   	dcopt_print=";dcopt=ist";
      	   }
            //Override for killing dcopt=ist, and thus any interstitial ad
             var full_url=window.location.href;
             if (full_url.indexOf("no_ist=1")!=-1 || full_url.indexOf('lc=rr1001')!==-1 || readCookie('rr') == true){
                  dcopt_print="";
           }
           document.write('<script language="JavaScript" src="http://ad.doubleclick.net/adj/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=234x60' +
dcopt_print + ';tile=6;ord=' + ord + '?" type="text/javascript"></scr' + 'ipt>');
          </script>
          <noscript>
            <a href="http://ad.doubleclick.net/jump/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=234x60;tile=6;ord=123456789?" target="_blank">
            <img src="http://ad.doubleclick.net/ad/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=234x60;tile=6;ord=123456789?"
width="234" height="60" border="0" alt="" /></a>
          </noscript>
        <!-- End ad tag -->
                  
        </div>
      </div>        
    </div>
  </div>
  <div class="menu">
    <div class="header_line"></div>
    <div class="menu_item">
      <ul class="menu_ul">
        <li class="nav_home"><a href="/">Home</a></li>
	<li class="nav_reviews"><a href="/reviews">Reviews</a> </li>
	<li class="nav_howtos"><a href="/beauty-tips">Advice</a></li>
	<li class="nav_videos"><a href="/video">Video</a></li>
	<li class="nav_sc"><a href="/samples_contests">Samples</a></li>
  <li class="nav_community"><a href="/community">Community</a></li>
      </ul>
    </div>
    <div class="nav_sponsor">
      
		   <!-- begin ad tag -->
	      <div id="adzone" class="tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null"></div>
          <script language="JavaScript" type="text/javascript">
      	   dcopt_print="";
      	   //If it's the first tile set dcopt=ist for js
                 if(0){
      	   	dcopt_print=";dcopt=ist";
      	   }
            //Override for killing dcopt=ist, and thus any interstitial ad
             var full_url=window.location.href;
             if (full_url.indexOf("no_ist=1")!=-1 || full_url.indexOf('lc=rr1001')!==-1 || readCookie('rr') == true){
                  dcopt_print="";
           }
           document.write('<script language="JavaScript" src="http://ad.doubleclick.net/adj/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=130x55' +
dcopt_print + ';tile=7;ord=' + ord + '?" type="text/javascript"></scr' + 'ipt>');
          </script>
          <noscript>
            <a href="http://ad.doubleclick.net/jump/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=130x55;tile=7;ord=123456789?" target="_blank">
            <img src="http://ad.doubleclick.net/ad/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=130x55;tile=7;ord=123456789?"
width="130" height="55" border="0" alt="" /></a>
          </noscript>
        <!-- End ad tag -->
        
    </div>

    <form name="searchBox" id="headerSearchForm" action="/search" method="get">
      <div class="menu_search">
        <div class="menu_text_input">
          <input type="text" name="search" value="Search Beauty" id="searchInput" onclick="javascript:if(this.value == 'Search Beauty'){this.value = '';}"/>
        </div>
        <div class="menu_search_button">
          <input class="searchSubmit" type="image" alt="Search" src="http://images.totalbeauty.com/img/spacer.gif" />
        </div>
      </div>
    </form>
  </div>
</div>
        <!-- End mastHead-->
<div id="tbFeaturedNav">
  <b>Featured on TotalBeauty.com:</b>
  <a href="/my-beauty-life">My Beauty Life</a><span>|</span>
  <a href="/wedding">Bridal Beauty</a><span>|</span>
  <a href="/deals-samples-newsletter">Little Luxury Samples Store</a><span>|</span>
  <a href="/awards2011">Beauty Awards</a><span>|</span>
  <a href="/blogs_we_love">Blogs</a>
</div>      <!-- End mastHead-->

			<div id="neapolitan">
           
  <div class="column_row_3">
                          <div class="ad954x60">
	<!-- begin ad tag -->
	<div id="adzone" class="tbm.po.tb/home;zone=home"></div>
	<script language="JavaScript" type="text/javascript">
	dcopt_print="";
	//If it's the first tile set dcopt=ist for js
	if(0){
	  dcopt_print=";dcopt=ist";
	}
	//Override for killing dcopt=ist, and thus any interstitial ad
	var full_url=window.location.href;
	if (full_url.indexOf("no_ist=1")!=-1){
	  dcopt_print="";
	}
	document.write('<script language="JavaScript" src="http://ad.doubleclick.net/adj/tbm.po.tb/home;zone=home;sz=954x60;tile=8' + dcopt_print + ';ord=' + ord + '?" type="text/javascript"></scr' + 'ipt>');
	</script>

	<noscript>
					<a href="http://ad.doubleclick.net/jump/tbm.po.tb/home;zone=home;sz=954x60;tile=8;ord=123456789?" target="_blank">
					<img src="http://ad.doubleclick.net/ad/tbm.po.tb/home;zone=home;sz=954x60;tile=8;ord=123456789?" width="954" height="60" border="0" alt="" /></a>
	</noscript>
	<!-- End ad tag -->
</div><!--end ad954x60-->

            </div>
 <div class="column_row_3">
     <div class="picture top_block">
                                             <div class="picture1">
            <div class="picture_img">
                <a href="/content/gallery/a-list-african-american"><img src="http://images.totalbeauty.com/uploads/editorial/xlrg/a-list-african-american-XL.jpg" width="300" alt="Celebs" /></a>
            </div>
            <div class="picture_title2"><a href="/content/gallery/a-list-african-american">Celebs</a></div>
            <div class="picture_content">
                <div class="picture_content_font1"><a href="/content/gallery/a-list-african-american">African American Beauty</a></div>
                <div class="picture_content_font2"><a href="/content/gallery/a-list-african-american">See how to copy these 5 hot celebrity hair and makeup looks</a></div>
            </div>
        </div>
                                         <div class="picture1">
            <div class="picture_img">
                <a href="http://www.totalbeauty.com/my-beauty-life/total-look-look-pretty-exercising"><img src="http://images.totalbeauty.com/uploads/editorial/xlrg/mbl_ep113-XL.jpg" width="300" alt="Video" /></a>
            </div>
            <div class="picture_title2"><a href="http://www.totalbeauty.com/my-beauty-life/total-look-look-pretty-exercising">Video</a></div>
            <div class="picture_content">
                <div class="picture_content_font1"><a href="http://www.totalbeauty.com/my-beauty-life/total-look-look-pretty-exercising">Look Pretty While Exercising</a></div>
                <div class="picture_content_font2"><a href="http://www.totalbeauty.com/my-beauty-life/total-look-look-pretty-exercising">Learn to achieve the perfect balance between bare-faced and made up</a></div>
            </div>
        </div>
                                         <div class="picture1">
            <div class="picture_img">
                <a href="/content/gallery/date-night-beauty-tips"><img src="http://images.totalbeauty.com/uploads/editorial/xlrg/date-night-beauty-tips-XL.jpg" width="300" alt="Tips" /></a>
            </div>
            <div class="picture_title2"><a href="/content/gallery/date-night-beauty-tips">Tips</a></div>
            <div class="picture_content">
                <div class="picture_content_font1"><a href="/content/gallery/date-night-beauty-tips">Date Night Dos and Don'ts</a></div>
                <div class="picture_content_font2"><a href="/content/gallery/date-night-beauty-tips">What to steer clear of if you want your evening to be as romantic as possible</a></div>
            </div>
        </div>
                               </div>
      </div>
 <div class="column_row_3">
                   <div class="product_reviews_promo">
            <div class="image2_left">
              <p><a href="/reviews">UNBIASED PRODUCT REVIEWS<br />trusted by MILLIONS of real women.</a></p>
              <a rel="nofollow" href="/reviews" class="sublink">Go read reviews</a>
            </div>
            <div class="image2_mid">
                <h2 class="latest_review"><a href="/reviews/product/6157901/suave-professionals-sleek-shampoo?sort=1">Latest Review</a></h2>
                <div class="image2_mid1"><a href="/reviews/product/6157901/suave-professionals-sleek-shampoo">Suave Professionals Sleek Shampoo</a></div>
                <div class="image2_mid2">i really like how soft this shampoo leaves my hair, and the smell.. but it... <a rel="nofollow" href="/reviews/product/6157901/suave-professionals-sleek-shampoo?sort=1">more</a></div>
            </div>
            <div class="image2_right">
              <div class="search_btn_heading"><a href="/search/best+and+worst" rel="nofollow">Search the <strong>largest</strong><br />beauty product database</a></div>
              <div class="image2_right1">
                  <a href="javascript:void(0);" class="popupBrandBtn"><img src="http://images.totalbeauty.com/img/spacer.gif" width="88" height="25" alt="" /></a>
              </div>
              <div class="image2_right2">OR</div>
              <div class="image2_right3">
                  <a href="javascript:void(0);" class="popupCatBtn"><img src="http://images.totalbeauty.com/img/spacer.gif" width="88" height="25" alt="" /></a>
              </div>
            </div>
          </div>
                   </div>






   <div class="column_row_3 bottom_block">
       <div class="left_2column bottom_block_left">
                                  <div class="whats_new_beauty">
            <h1><a href="/editors_blogs">What's New in Beauty</a></h1>

            <div class="callout_blue_line"></div>
            <div class="whats_new_inner">
                          <div class="news_item_block">
                  <div class="news_main_img">
                      <a href="/editors_blogs/behind-the-scenes-of-the-pulp-fiction-makeover-on-hit-tv-show-community">
                        <img src="http://images.totalbeauty.com/uploads/editorial/eblog/main 85.jpg" width="71" alt="Behind the Scenes of the &quot;Pulp Fiction&quot; Makeover on Hit TV Show &quot;Community&quot;" /></a>
                  </div>
                  <div class="news_main_title">
                      <div class="news_heading_medium"><a href="/editors_blogs/behind-the-scenes-of-the-pulp-fiction-makeover-on-hit-tv-show-community">Behind the Scenes of the "Pulp Fiction" Makeover on Hit TV Show "Community"</a></div>
                      <div class="news_main_author">Posted by <span class="author"><a href="http://community.totalbeauty.com/service/displayKickPlace.kickAction?u=26613670">annatotalbeauty</a></span> on 03/25/2011 at 01:52 PM</div>
                  </div>
              </div>
              <div class="news_line"></div>
                           <div class="news_item_block">
                  <div class="news_main_img">
                      <a href="/editors_blogs/amanda-peets-favorite-beauty-products">
                        <img src="http://images.totalbeauty.com/uploads/editorial/eblog/Untitled85.jpg" width="71" alt="Amanda Peet&#039;s Favorite Beauty Products " /></a>
                  </div>
                  <div class="news_main_title">
                      <div class="news_heading_medium"><a href="/editors_blogs/amanda-peets-favorite-beauty-products">Amanda Peet's Favorite Beauty Products </a></div>
                      <div class="news_main_author">Posted by <span class="author"><a href="http://community.totalbeauty.com/service/displayKickPlace.kickAction?u=26613670">annatotalbeauty</a></span> on 03/21/2011 at 05:35 PM</div>
                  </div>
              </div>
              <div class="news_line"></div>
                           <div class="news_item_block">
                  <div class="news_main_img">
                      <a href="/editors_blogs/gorgeous-makeup-looks-from-the-essence-black-women-in-hollywood-event-and-how-to-get-them">
                        <img src="http://images.totalbeauty.com/uploads/editorial/eblog/gabrielle-union-s.jpg" width="71" alt="Gorgeous Makeup Looks from the &quot;Essence&quot; Black Women in Hollywood Event -- and How to Get Them  " /></a>
                  </div>
                  <div class="news_main_title">
                      <div class="news_heading_medium"><a href="/editors_blogs/gorgeous-makeup-looks-from-the-essence-black-women-in-hollywood-event-and-how-to-get-them">Gorgeous Makeup Looks from the "Essence" Black Women in Hollywood Event -- and How to Get Them  </a></div>
                      <div class="news_main_author">Posted by <span class="author"><a href="http://community.totalbeauty.com/service/displayKickPlace.kickAction?u=25618472">Kristen_TBeditor</a></span> on 03/15/2011 at 12:22 PM</div>
                  </div>
              </div>
              <div class="news_line"></div>
                                       <div class="news_item_block">
                <div class="news_title1"><a href="/blogs_we_love/blog-profile/531/canadian-beauty">Canadian Beauty</a></div>
                <div class="news_heading_medium"><a href="http://canadianbeauty.com/the-body-shop-illuminating-face-base-review/" target="_blank">The Body Shop Illuminating Face Base Review</a></div>
                <div class="news_main_author">04/ 5/2011 at 08:10 AM</div>
              </div>
              <div class="news_line2"></div>
                           <div class="news_item_block">
                <div class="news_title1"><a href="/blogs_we_love/blog-profile/931/frappelattes">Frappelattes</a></div>
                <div class="news_heading_medium"><a href="http://frappelattes.com/arrojo-styling-whip/" target="_blank">Whip Your Hair Into Shape Using Arrojo Styling Whip</a></div>
                <div class="news_main_author">03/25/2011 at 12:00 AM</div>
              </div>
              <div class="news_line2"></div>
                         <div class="callout_blue_line"></div>
          </div>
          <a href="/editors_blogs"><div class="see_all_posts">see ALL POSTS</div></a>
        </div>
        <div class="top_5_list">
          <div class="image_heading">
            <a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23092/page21"><img src="http://images.totalbeauty.com/uploads/editorial/top5/5_best_drugstore_buys.jpg"/></a>
          </div>
          <ul>
                        <li class="selected">
              <div onClick="location.href='http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23092/page21'" class="top_5_wrap">

                <div class="left">
                  <img src="http://images.totalbeauty.com/img/v7/top1.jpg" alt="" />
                </div>
                <div class="right">
                  <p class="top5_head"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23092/page21">Arm & Hammer </a></p>
                  <p class="top5_sub">Essentials Natural Deodorant</p>                  <p class="top5_blurb"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23092/page21">"This lasts me 6-8 hours ..."</a></p>
                </div>

              </div>
            </li>
                        <li >
              <div onClick="location.href='http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23082/page20'" class="top_5_wrap">

                <div class="left">
                  <img src="http://images.totalbeauty.com/img/v7/top2.jpg" alt="" />
                </div>
                <div class="right">
                  <p class="top5_head"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23082/page20">Dove</a></p>
                  <p class="top5_sub">Sensitive Skin Unscented Bar</p>                  <p class="top5_blurb"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23082/page20">"This cleaned off all my makeup ..."</a></p>
                </div>

              </div>
            </li>
                        <li >
              <div onClick="location.href='http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23072/page19'" class="top_5_wrap">

                <div class="left">
                  <img src="http://images.totalbeauty.com/img/v7/top3.jpg" alt="" />
                </div>
                <div class="right">
                  <p class="top5_head"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23072/page19">St. Ives </a></p>
                  <p class="top5_sub">Whipped Silk Intense Body Moisturizer</p>                  <p class="top5_blurb"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23072/page19">"It moisturizes skin wonderfully ..."</a></p>
                </div>

              </div>
            </li>
                        <li >
              <div onClick="location.href='http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23062/page18'" class="top_5_wrap">

                <div class="left">
                  <img src="http://images.totalbeauty.com/img/v7/top4.jpg" alt="" />
                </div>
                <div class="right">
                  <p class="top5_head"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23062/page18">N.Y.C. New York Color </a></p>
                  <p class="top5_sub">Waterproof Eyeliner Pencil</p>                  <p class="top5_blurb"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23062/page18">"I love a product that's cheap ..."</a></p>
                </div>

              </div>
            </li>
                        <li >
              <div onClick="location.href='http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23052/page17'" class="top_5_wrap">

                <div class="left">
                  <img src="http://images.totalbeauty.com/img/v7/top5.jpg" alt="" />
                </div>
                <div class="right">
                  <p class="top5_head"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23052/page17">Burt's Bees </a></p>
                  <p class="top5_sub">Repair Serum</p>                  <p class="top5_blurb"><a href="http://www.totalbeauty.com/content/gallery/p_drugstore_buys/p23052/page17">"I have been using this product on my face ..."</a></p>
                </div>

              </div>
            </li>
                      </ul>
        </div>
    
      
      
            <div class="column_row_2">
         <div class="presented_by">
                  <div class="present_block">
            <div class="present_img">
                <a href="http://www.totalbeauty.com/content/gallery/celeb-curly-cuts"><img src="http://images.totalbeauty.com/uploads/editorial/sponsors/Pantene/pantene-sponsored-hp-block.gif" width="285" height="80" alt="Pantene" /></a>
                
            </div>
                        <div class="present_content">Copy-Worthy Curly Cuts <span class="author"><a href="http://www.totalbeauty.com/content/gallery/celeb-curly-cuts">more</a></span></div>
          </div>
                   <div class="present_block">
            <div class="present_img">
                <a href="http://www.totalbeauty.com/content/gallery/african-american-beauty-tips"><img src="http://images.totalbeauty.com/uploads/editorial/sponsors/motions/motions_hp_sponsor-v2.gif" width="285" height="80" alt="Motions" /></a>
                
            </div>
                        <div class="present_content">Spotlight on African American Beauty <span class="author"><a href="http://www.totalbeauty.com/content/gallery/african-american-beauty-tips">more</a></span></div>
          </div>
                   </div>
        </div>
      
      <div class="column_row_2">
                                                                        <div class="column_1">
                <div class="beauty_news_block">
                  <h2><a href="/news">Beauty News</a></h2>
                  <div class="callout_blue_line"></div>
                  <div class="beauty_news_block_inner">
                                          <div class="beauty_news_item">
                        <div class="news_heading_medium"><a href="/news/health-and-beauty/baby-and-pregnancy/elizabeth-banks-welcomes-baby-boy-surrogate" title="Elizabeth Banks Welcomes a Baby Boy Via Surrogate">Elizabeth Banks Welcomes a Baby Boy Via Surrogate</a></div>
                        <div class="news_main_author">March 30, 2011 01:42 PM</div>
                      </div>
                                           <div class="beauty_news_item">
                        <div class="news_heading_medium"><a href="/news/health-and-beauty/baby-and-pregnancy/rachel-zoe-welcomes-new-baby-boy" title="Rachel Zoe Welcomes New Baby Boy">Rachel Zoe Welcomes New Baby Boy</a></div>
                        <div class="news_main_author">March 24, 2011 12:08 PM</div>
                      </div>
                                           <div class="beauty_news_item">
                        <div class="news_heading_medium"><a href="/news/health-and-beauty/natural-beauty/elizabeth-taylor-died-wednesday" title="Elizabeth Taylor Died Wednesday">Elizabeth Taylor Died Wednesday</a></div>
                        <div class="news_main_author">March 23, 2011 11:05 AM</div>
                      </div>
                                           <div class="beauty_news_item">
                        <div class="news_heading_medium"><a href="/news/hairstyles/celebrity-hairstyles/reese-witherspoon-marry-weekend" title="Reese Witherspoon to Marry this Weekend">Reese Witherspoon to Marry this Weekend</a></div>
                        <div class="news_main_author">March 16, 2011 06:40 AM</div>
                      </div>
                                           <div class="beauty_news_item">
                        <div class="news_heading_medium"><a href="/news/hairstyles/celebrity-hairstyles/katherine-heigls-new-short-platinum-hairstyle" title="Katherine Heigl's New Short, Platinum Hairstyle">Katherine Heigl's New Short, Platinum Hairstyle</a></div>
                        <div class="news_main_author">March 11, 2011 12:08 PM</div>
                      </div>
                                       </div>
                  <div class="callout_blue_line"></div>
                  <a href="/news"><div class="see_all_news">see ALL NEWS</div></a>
                </div>
              </div>
                                                    <div class="column_1">
                <div class="get_free_samples">
                   <form class="newsletterSignUp">
                  <div class="get_free_input">
                    <input type="text" name="free" value="Enter your email address" class="newsletterEmail"/>
                  </div>
                  <div class="get_free_button">
                    <input type="image" src="http://images.totalbeauty.com/img/spacer.gif" width="56" height="33" alt="" />
                  </div>
                  </form>
                  <div class="get_free_text">
                      *by submitting your email, you agree to our <a href="/privacy" rel="nofollow">privacy policy</a> &amp; <a href="/terms" rel="nofollow">terms &amp; conditions</a>
                  </div>
                </div>
              </div>
              
    </div>
  




        
           </div>
           <div class="right_column">
              <div class="right_advertisement">
                <p>ADVERTISEMENT</p>
                 
		   <!-- begin ad tag -->
	      <div id="adzone" class="tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null"></div>
          <script language="JavaScript" type="text/javascript">
      	   dcopt_print="";
      	   //If it's the first tile set dcopt=ist for js
                 if(1){
      	   	dcopt_print=";dcopt=ist";
      	   }
            //Override for killing dcopt=ist, and thus any interstitial ad
             var full_url=window.location.href;
             if (full_url.indexOf("no_ist=1")!=-1 || full_url.indexOf('lc=rr1001')!==-1 || readCookie('rr') == true){
                  dcopt_print="";
           }
           document.write('<script language="JavaScript" src="http://ad.doubleclick.net/adj/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=300x250,300x600' +
dcopt_print + ';tile=1;ord=' + ord + '?" type="text/javascript"></scr' + 'ipt>');
          </script>
          <noscript>
            <a href="http://ad.doubleclick.net/jump/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=300x250,300x600;tile=1;ord=123456789?" target="_blank">
            <img src="http://ad.doubleclick.net/ad/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=300x250,300x600;tile=1;ord=123456789?"
width="300" height="250" border="0" alt="" /></a>
          </noscript>
        <!-- End ad tag -->
        
              </div>
              <div class="problem_center">
                <h2><a href="/how-tos">Problem Center: How-Tos</a></h2>
                <div class="problem_line"> </div>
                <div class="problem_center_block">
                  <div class="hair_style">
                    <ul>
                      <li><a class="hair_title1" href="/how-tos/hairstyles">Hairstyles</a></li>
                      <li><a href="/how-tos/hairstyles/celebrity-hairstyles">Celebrity Hairstyles</a></li>
                      <li><a href="/how-tos/hairstyles/hairstyles-by-face-shape">Hairstyles by face shape</a></li>
                      <li><a href="/how-tos/hairstyles/haircut">Haircut</a></li>
                      <li><a class="more" href="/how-tos/hairstyles">more</a></li>
                    </ul>
                  </div>
                  <div class="hair_care">
                    <ul>
                      <li><a class="hair_title1" href="/how-tos/hair-care">Hair care</a></li>
                      <li><a href="/how-tos/hair-care/bad-hair">Bad hair</a></li>
                      <li><a href="/how-tos/hair-care/frizz">Frizz</a></li>
                      <li><a href="/how-tos/hair-care/color-fading">Color fading</a></li>
                      <li><a class="more" href="/how-tos/hair-care">more</a></li>
                    </ul>
                  </div>
                  <div class="skin_care">
                    <ul>
                      <li><a class="hair_title1" href="/how-tos/skin-care">Skin Care</a></li>
                      <li><a href="/how-tos/skin-care/acne">Acne</a></li>
                      <li><a href="/how-tos/skin-care/wrinkles">Wrinkles</a></li>
                      <li><a href="/how-tos/skin-care/dark-circles">Dark circles</a></li>
                      <li><a class="more" href="/how-tos/skin-care">more</a></li>
                    </ul>
                  </div>

                  <div class="category_row">
                    <div class="category_row_item first_item"><a href="/how-tos/make-up">Makeup</a></div>
                    <div class="category_row_item"><a href="/how-tos/body">Body</a></div>
                    <div class="category_row_item"><a href="/how-tos/perfume">Perfume</a></div>
                    <div class="category_row_item"><a href="/how-tos/nails">Nails</a></div>
                    <div class="category_row_item"><a href="/how-tos/health-and-beauty">Health &amp; Beauty</a></div>

                  </div>
                </div>
              </div>
              <div class="tip_of_the_day">
                <h2><a href="/content/flash/c_clean_nail_salons" title="Beauty Tip of the Day">Beauty Tip of the Day</a></h2>
                <div class="callout_blue_line"> </div>
                <div class="tip_middle">
                  <div class="bracket"><img src="http://images.totalbeauty.com/img/spacer.gif" height="1" width="1" /></div>
                  <div class="tiptext">
                  <br />
                    If anything seems off at a nail salon, speak up or walk out the door
                    <a href="/content/flash/c_clean_nail_salons" class="more">&raquo; more</a>
                  </div>
                  <div class="sponsor">
                   
		   <!-- begin ad tag -->
	      <div id="adzone" class="tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null"></div>
          <script language="JavaScript" type="text/javascript">
      	   dcopt_print="";
      	   //If it's the first tile set dcopt=ist for js
                 if(0){
      	   	dcopt_print=";dcopt=ist";
      	   }
            //Override for killing dcopt=ist, and thus any interstitial ad
             var full_url=window.location.href;
             if (full_url.indexOf("no_ist=1")!=-1 || full_url.indexOf('lc=rr1001')!==-1 || readCookie('rr') == true){
                  dcopt_print="";
           }
           document.write('<script language="JavaScript" src="http://ad.doubleclick.net/adj/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=3x3' +
dcopt_print + ';tile=5;ord=' + ord + '?" type="text/javascript"></scr' + 'ipt>');
          </script>
          <noscript>
            <a href="http://ad.doubleclick.net/jump/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=3x3;tile=5;ord=123456789?" target="_blank">
            <img src="http://ad.doubleclick.net/ad/tbm.po.tb/home;kw=null;zone=home;cat=general;pcat=null;scat=null;scat=null;scat=null;gen=null;eth=null;age=null;ugc=null;brand=null;sz=3x3;tile=5;ord=123456789?"
width="3" height="3" border="0" alt="" /></a>
          </noscript>
        <!-- End ad tag -->
        
                  </div>
                </div>
                <div class="callout_blue_line"> </div>
<!--                <div class="see_all_tips">
                   <a href="/how-tos"><img width="111" alt="" src="http://images.totalbeauty.com/img/v7/btn_see_all_tips.gif" /></a>
                </div>-->
                <a href="/how-tos" class="see_all_tips">see ALL BEAUTY TIPS</a>
              </div>

           </div>

        </div>
			</div><!--neapolitan-->

      <!-- footer -->
      <!-- BEGIN FOOTER -->
             <div id="footer">
               <div class="footer-inside">
                <div class="footer1">
                    <div class="footer_logo">
                        <img src="http://images.totalbeauty.com/img/v7/logo_footer.gif" width="187" height="23" alt="" />
                        <p><a href="/beauty-tips">Expert <span>advice</span></a>. <a href="/reviews">Unbiased <span>reviews</span></a>.</p>
                    </div>
                    <div class="footer_newsletter">
                        <label for="newsletter">Get our Newsletter!</label>
                        <p class="status"></p>
                        <form class="newsletterFooterSignUp">
                          
                          <div class="newsletter_input">
                            <input class="newsletterFooterEmail" type="text" name="newsletter" value="Enter your email address" />
                          </div>
                          <div class="newsletter_signup">
                              <input type="image" src="http://images.totalbeauty.com/img/spacer.gif" />
                          </div>
                        </form>
                    </div>
                </div>
                <div class="footer2">
                    <div class="footer_title"><a href="/about_us" rel="nofollow">Company Info</a></div>
                    <div class="footer_content"><a href="/about_us" rel="nofollow">About Total Beauty</a></div>
                    <div class="footer_content"><a href="/advertise" rel="nofollow">Advertise with Us</a></div>
                    <div class="footer_content"><a href="/jobs" rel="nofollow">Jobs</a></div>
                    <div class="footer_content"><a href="/contact" rel="nofollow">Contact Us</a></div>
                    <div class="footer_content"><a href="/faq" rel="nofollow">FAQ</a></div>
                    <div class="footer_content"><a href="/sitemap">Sitemap</a></div>
                    <br/>
                    <div class="footer_title"><a href="http://www.beautyriot.com" target="_blank">Visit BeautyRiot.com</a></div>
                    <div class="footer_content"><a href="http://www.beautyriot.com/free-virtual-makeover-intro" target="_blank" >Free Virtual Makeover</a></div>
                    <div class="footer_content"><a href="http://www.beautyriot.com/celebrities" target="_blank" >Celebrity Hairstyles</a></div>
                    <div class="footer_content"><a href="http://www.beautyriot.com/fashion" target="_blank" >Celebrity Fashion</a></div>
                    <div class="footer_content"><a href="http://www.beautyriot.com/celebrities/jessica-simpson" target="_blank" >Jessica Simpson</a></div>
                </div>
                <div class="footer3">
                    <div class="footer_title"><a href="/">Main Site</a></div>
                    <div class="footer_content"><a href="/">Home</a></div>
                    <div class="footer_content"><a href="/reviews">Reviews</a></div>
                    <div class="footer_content"><a href="/beauty-tips">Advice</a></div>
                    <div class="footer_content"><a href="/video">Video</a></div>
                    <div class="footer_content"><a href="/samples_contests">Samples &amp; Deals</a></div>
                    <div class="footer_content"><a href="/blogs_we_love">Blogs</a></div>
                    <div class="footer_content"><a href="/community">Community</a></div>
                </div>
                <div class="footer4">
                    <div class="footer_title"><a href="/how-tos">How-Tos &amp; Quick Tips</a></div>
                    <div class="footer_content"><a href="/how-tos/hairstyles">Hairstyles</a></div>
                    <div class="footer_content"><a href="/how-tos/hair-care">Hair Care</a></div>
                    <div class="footer_content"><a href="/how-tos/skin-care">Skin Care</a></div>
                    <div class="footer_content"><a href="/how-tos/make-up">Makeup</a></div>
                    <div class="footer_content"><a href="/how-tos/body">Body</a></div>
                    <div class="footer_content"><a href="/how-tos/perfume">Perfume</a></div>
                    <div class="footer_content"><a href="/how-tos/nails">Nails</a></div>
                    <div class="footer_content"><a href="/how-tos/health-and-beauty">Health &amp; Beauty</a></div>
                </div>
                <div class="footer5">
                    <div class="footer_title1"><a href="/search">Search TotalBeauty.com</a></div>
                     <div class="footer_search">
                      <form method="get" action="/search" id="footerSearchForm" name="searchBox2">
        		<div class="footer_search_input">
                          <input id="searchInput2" class="siteSearch" type="text" value="Search Beauty" onclick="javascript:if(this.value == 'Search Beauty'){this.value = '';}" />
        		</div>
        		<div class="footer_search_button">
                          <input class="searchSubmit2" type="image" src="http://images.totalbeauty.com/img/spacer.gif" />
        		</div>
                      </form>
                    </div>
                    <div class="footer_title2">Find products:</div>
                        <div class="footer_brand">
                            <div class="image2_right1">
                            <a href="javascript:void(0);" class="popupBrandFooterBtn"><img src="http://images.totalbeauty.com/img/spacer.gif" width="1" height="1" alt="" /></a>
                        </div>
                        <div class="image2_right2">OR</div>
                        <div class="image2_right3">
                            <a href="javascript:void(0);" class="popupCatFooterBtn"><img src="http://images.totalbeauty.com/img/spacer.gif" width="1" height="1" alt="" /></a>
                        </div>

                    </div>
                </div>

                <div class="footer_below">
                    <div class="below_left">
                        Our Friends: 
                        <br/>
                        <span class="left_blue"><a class="newWindow" href="http://www.limelife.com" target="_blank">Celebrity News</a></span> |
                        <span class="left_blue"><a class="newWindow" href="http://www.modernman.com" target="_blank">Modern Man</a></span>
                    </div>
                    <div class="below_right">
                        &copy; 2007-2010 Total Beauty Media, Inc. All rights reserved. 
                        <br/>
                        <span class="left_blue"><a rel="nofollow" href="/terms">Terms &amp; Conditions</a></span>  |  
                        <span class="left_blue"><a href="/privacy" rel="nofollow">Privacy Policy</a></span>
                    </div>

                </div>
             </div>
        </div>
  <script type="text/javascript">
    	document.write(footerIframe);      
  </script>


<script type="text/javascript">
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "6035713" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=6035713&cv=2.0&cj=1" />
</noscript>

<!-- END FOOTER -->      <!-- End footer-->
		</div><!--wrapper-->
	</div><!-- end body wrap -->


  
	<!-- Start Quantcast tag -->
	<noscript>
	<a href="http://www.quantcast.com/p-f07swHXQlH6kA" title="" target="_blank"><img src="http://pixel.quantserve.com/pixel/p-f07swHXQlH6kA.gif" alt="" style="display: none;" border="0" height="1" width="1" title="Quantcast" /></a>
	</noscript>
	<!-- End Quantcast tag -->
  
</body>
</html>