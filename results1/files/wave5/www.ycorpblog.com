<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Yahoo!'s Yodel Anecdotal</title>

<meta name="generator" content="WordPress 3.0.4" /> <!-- leave this for stats -->
<script type="text/javascript" src="/wp-includes/js/slide.js" ></script>
<script type="text/javascript" src="http://ycorpblog.com/wp-content/themes/yodel/search.js" ></script>
<script type="text/javascript" charset="utf-8" src="http://bit.ly/javascript-api.js?version=latest&login=mtyahooayc&apiKey=R_644832330f376c2d26012eeea4e2d5f6"></script>
<link rel="stylesheet" href="http://ycorpblog.com/wp-content/themes/yodel/style.css" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="Yodel Anecdotal RSS Feed" href="http://ycorpblog.com/feed/" />
<link rel="pingback" href="http://ycorpblog.com/xmlrpc.php" />

<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://ycorpblog.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://ycorpblog.com/wp-includes/wlwmanifest.xml" /> 
<link rel='index' title='Yodel Anecdotal' href='http://ycorpblog.com/' />
<meta name="generator" content="WordPress 3.0.4" />

<!-- All in One SEO Pack 1.6.4 by Michael Torbert of Semper Fi Web Design[-1,-1] -->
<meta name="description" content="A look inside the big purple house of Yahoo!, where we'll provide insights into our company, our people, our culture, and the things we think about in the shower." />
<meta name="keywords" content="yahoo, yahoo company blog, yahoo corporate blog, yahoo blog" />
<link rel="canonical" href="http://ycorpblog.com/" />
<!-- /all in one seo pack -->
<script language="javascript1.4" type="text/javascript" src="http://ycorpblog.com/wp-content/plugins/audio-player/audio-player.js"></script>

	<!-- Added By Democracy Plugin. Version 2.0.1 -->
	<script type='text/javascript' src='http://ycorpblog.com/wp-content/plugins/democracy/democracy.js'></script>
	<link rel='stylesheet' href='http://ycorpblog.com/wp-content/plugins/democracy/basic.css' type='text/css' />
	<link rel='stylesheet' href='http://ycorpblog.com/wp-content/plugins/democracy/style.css' type='text/css' />
<style type="text/css" media="screen">

/* Begin Contact Form CSS */
.contactform {
	position: static;
	overflow: hidden;
	width: 95%;
}

.contactleft {
	width: 25%;
	white-space: pre;
	text-align: right;
	clear: both;
	float: left;
	display: inline;
	padding: 4px;
	margin: 5px 0;
}

.contactright {
	width: 70%;
	text-align: left;
	float: right;
	display: inline;
	padding: 4px;
	margin: 5px 0;
}

.contacterror {
	border: 1px solid #ff0000;
}

.contactsubmit {
}
/* End Contact Form CSS */

	</style>

<!--[if IE]>
<style type="text/css">
.right img.moveup {margin-top: -3px;}
</style>
<![endif]-->
</head>
<body>
<div class="gradient_top">

</div>
<div class="page">
	<div class="top">
		<a href="/"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/logo.gif" alt="Yahoo! Yodel Anecdotal" class="logo"/></a>
		<img src="/wp-content/uploads/rotation32.jpg" alt="Courtyard" />		<div class="clr"></div>
		<div class="nav">
			<ul>
				<li><a href="/" class='selected'>Blog Home</a></li>
				<li><a href="/about/" >About</a></li>
				<li><a href="/archives/" >Archives</a></li>
				<li><a href="/stuff/" >Y! Stuff</a></li>
			</ul>
		</div>
	</div>
	<div class="orange">
		<img src="http://ycorpblog.com/wp-content/themes/yodel/images/orange_fade.gif" alt="" class="orange_fade"/>
		<div class="left">


			
			<div class="post" id="post-5752">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/04/04/royal-wedding-invite/" rel="bookmark" title="Permanent Link to You’re invited to the Royal Wedding! (Online, of course).">You’re invited to the Royal Wedding! (Online, of course).</a></h1>
				<p class="postedby">Posted April 4th, 2011 at 6:59 am by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/04/04/royal-wedding-invite/#respond" title="Comment on You’re invited to the Royal Wedding! (Online, of course).">No Comments &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/cool-stuff/" title="View all posts in Cool Stuff" rel="category tag">Cool Stuff</a>, <a href="http://ycorpblog.com/category/shine-2/" title="View all posts in Shine" rel="category tag">Shine</a></p>

				<p>Since the news of the royal engagement, searches worldwide on Yahoo! for the royal wedding have increased 8 million percent. And just in the last month, searches have grown more than 1,500 percent! Clearly people are hungry for royal wedding content, and as the premier digital media company, only Yahoo! can create and curate the best content to satiate you and to celebrate the royal couple.</p>
<p style="text-align: center;"><a href=" http://royalguestbook.yahoo.com/videos"><img class="alignnone" src="http://farm6.static.flickr.com/5029/5574596099_13838e6960.jpg" alt="" width="500" height="315" /></a></p>
<p>Yahoo!’s <a href="http://royalwedding.yahoo.com/">royal wedding site</a> will be an extension of <a href="http://shine.yahoo.com/">Shine From Yahoo!</a>, the leading Website for women’s lifestyle content, and demonstrates Yahoo!’s position as the premier Web destination for landmark global events. And speaking of global – the royal wedding site is localized and live in nine markets outside the US.  We invite you to experience the royal wedding through all the features our site offers:</p>
<ul>
<li><strong>Guestbook:</strong> The <a href="http://royalguestbook.yahoo.com/">Royal Wedding Yahoo! Guestbook</a> gives people a place to offer their marital advice and best wishes to Prince William and Kate Middleton, then share those wishes with users’ social networks.  Even <a href="http://royalguestbook.yahoo.com/videos">celebrities have left messages</a> for the couple on our guestbook!<strong> </strong></li>
<li><strong>Photo galleries: </strong>View photos of the couple’s courtship, Kate Middleton as a child, and much more.<strong> </strong></li>
<li><strong>Articles and blogs</strong>: The latest news on the royal wedding, plus articles on the wedding trends, fashion, the royal wedding&#8217;s participants and guests, and more.</li>
<li><strong>Wedding live stream</strong>: Watch the royal wedding live by visiting <a href="http://royalwedding.yahoo.com">http://royalwedding.yahoo.com</a> on April 29.</li>
<li><strong>Video</strong>:  Yahoo! will feature video from Associated Press, Reuters, and ABC News, along with original video content including a joint ABC News/Yahoo! News Web show called “Wedding Royale.” ABC News will also provide weekly flashback videos of the royal family. During the week of the wedding, Yahoo! will have a crew on the ground in London producing stories on topics people are most interested in.</li>
<li><strong>Mobile site: </strong>Yahoo! allows you to<strong> </strong>get your Royal Wedding fix anytime, anywhere, by visiting <a href="http://m.royalwedding.yahoo.com">http://m.royalwedding.yahoo.com</a> from your mobile phone. <strong></strong></li>
<li><strong>A chance to enter the “Live It Up Like Royalty” sweepstakes</strong>: By signing the Royal Wedding Yahoo! Guestbook or by “liking” Shine from Yahoo! on <a href="https://www.facebook.com/yahooshine">Facebook</a>, users will automatically be entered to win a trip to Las Vegas to be treated like a king or queen.</li>
</ul>
<p>Additionally, Flickr by Yahoo!, one of the world’s best places to post and share photos, features The Official British Monarchy Flickr stream: <a href="http://www.flickr.com/photos/britishmonarchy/tags/rw2011/">http://www.flickr.com/photos/britishmonarchy/tags/rw2011/</a> Join the celebration, the Yahoo! way. Cheerio!</p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/04/04/royal-wedding-invite/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/abc-news/" rel="tag">ABC News</a>, <a href="http://ycorpblog.com/tag/facebook/" rel="tag">facebook</a>, <a href="http://ycorpblog.com/tag/flickr/" rel="tag">Flickr</a>, <a href="http://ycorpblog.com/tag/kate/" rel="tag">Kate</a>, <a href="http://ycorpblog.com/tag/live-it-up-like-royalty/" rel="tag">Live It Up Like Royalty</a>, <a href="http://ycorpblog.com/tag/livestream/" rel="tag">Livestream</a>, <a href="http://ycorpblog.com/tag/mobile/" rel="tag">mobile</a>, <a href="http://ycorpblog.com/tag/royal-wedding/" rel="tag">Royal Wedding</a>, <a href="http://ycorpblog.com/tag/search/" rel="tag">search</a>, <a href="http://ycorpblog.com/tag/shine-from-yahoo/" rel="tag">Shine from Yahoo!</a>, <a href="http://ycorpblog.com/tag/sweepstakes/" rel="tag">sweepstakes</a>, <a href="http://ycorpblog.com/tag/video/" rel="tag">Video</a>, <a href="http://ycorpblog.com/tag/wedding-royale/" rel="tag">Wedding Royale</a>, <a href="http://ycorpblog.com/tag/william/" rel="tag">William</a>, <a href="http://ycorpblog.com/tag/yahoo-news/" rel="tag">yahoo! news</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5752 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/04/04/royal-wedding-invite/'; 
			document.getElementById("link_twitter5752").innerHTML = '<a href="http://twitter.com/home/?status=You%E2%80%99re+invited+to+the+Royal+Wedding%21+%28Online%2C+of+course%29.++'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/04/04/royal-wedding-invite/', 'BitlyCB.myShortenCallback5752');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5752" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F04%2Froyal-wedding-invite%2F&t=You%E2%80%99re+invited+to+the+Royal+Wedding%21+%28Online%2C+of+course%29.+" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F04%2Froyal-wedding-invite%2F&title=You’re invited to the Royal Wedding! (Online, of course).&summary=<p>Since the news of the royal engagement, searches worldwide on Yahoo! for the royal wedding have increased 8 million percent. And just in the last month, searches have grown more than 1,500 percent! Clearly people are hungry for royal wedding content, and as the premier digital media company, only Yahoo! can create and curate the [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F04%2Froyal-wedding-invite%2F&title=You%E2%80%99re+invited+to+the+Royal+Wedding%21+%28Online%2C+of+course%29.+" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F04%2Froyal-wedding-invite%2F&title=You%E2%80%99re+invited+to+the+Royal+Wedding%21+%28Online%2C+of+course%29.+" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=You%E2%80%99re+invited+to+the+Royal+Wedding%21+%28Online%2C+of+course%29.++http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F04%2Froyal-wedding-invite%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F04%2Froyal-wedding-invite%2F&title=You%E2%80%99re+invited+to+the+Royal+Wedding%21+%28Online%2C+of+course%29.+" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F04%2Froyal-wedding-invite%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F04%2Froyal-wedding-invite%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/04/04/royal-wedding-invite/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5760">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/04/01/searchtrends04012011/" rel="bookmark" title="Permanent Link to Weekly Yahoo! Search Trends – April 1, 2011">Weekly Yahoo! Search Trends – April 1, 2011</a></h1>
				<p class="postedby">Posted April 1st, 2011 at 1:18 pm by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/04/01/searchtrends04012011/#respond" title="Comment on Weekly Yahoo! Search Trends – April 1, 2011">No Comments &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/cool-stuff/" title="View all posts in Cool Stuff" rel="category tag">Cool Stuff</a></p>

				<p>Yahoo! aggregates the billions of searches performed across Yahoo! properties to give the pulse on what people are thinking and talking about. Searches represent the people, an instant poll every moment of the day, looking into the fleeting moods and entrenched attitudes of Internet users across the world. Each week<a href="http://video.yahoo.com/mypage/video?s=2913230"> Yahoo! Web Life Editor Heather Cabot</a> explores these trends on local and national broadcast. Tax Day, the Royal Wedding,  and the Final Four are buzzing this week.<span style="color: #1f497d;"> </span></p>
<p><object id="yahoovideoplayer" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="576" height="324" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="flashVars" value="vid=24751032&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide" /><param name="wmode" value="transparent" /><param name="allowscriptaccess" value="always" /><param name="allowFullScreen" value="true" /><param name="src" value="http://d.yimg.com/nl/yahoo-digital-media-bureau/ydmb/player.swf" /><param name="flashvars" value="vid=24751032&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide" /><param name="allowfullscreen" value="true" /><embed id="yahoovideoplayer" type="application/x-shockwave-flash" width="576" height="324" src="http://d.yimg.com/nl/yahoo-digital-media-bureau/ydmb/player.swf" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" flashvars="vid=24751032&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide"></embed></object><br />
<strong><span style="text-decoration: underline;">Tax Day<br />
</span></strong>Now that it’s April first, people aren’t joking about getting their taxes done on time. Searches for tax filing are up 1500% this week.  More than two thirds of those queries come from men and the top states looking for tax info are Alabama, New York and Ohio. We’ve noticed that folks are a little confused this year about when tax day falls.  Yahoo! searches for “when is tax day” are ramping up.  Here’s the scoop &#8212; the IRS moved the deadline to Monday April 18th because the District of Columbia will be observing emancipation day on the 15th. For more information on taxes check out <a href="http://finance.yahoo.com/taxes">Yahoo! Finance</a>:</p>
<p><strong><span style="text-decoration: underline;">Royal Cake<br />
</span></strong>With the royal wedding around the corner, curiosity about the big day continues on Yahoo! including lots of questions about the cake.  Lots of people want to know more about Fiona Cairns who will be designing the wedding cake and also prince William’s favorite – chocolate biscuit cake which will be a second dessert served at the celebration.</p>
<p>Since Prince William and Kate announced their engagement in November, searches for details on the happy couple and their nuptials have grown 8-million percent! To meet the insatiable demand for details – we’ve set up a site – <a href="http://royalwedding.yahoo.com">royalwedding.yahoo.com</a> – where people can get the latest news about the event and even sign a digital guestbook – with well-wishes for Kate and William.</p>
<p><strong><span style="text-decoration: underline;">Basketball Buzz: Final Four<br />
</span></strong>In sports news, the run up to the Final Four and the unlikely teams who will be playing in Houston this weekend are keeping internet users buzzing.  Searches for “unthinkable final four” are off the chart this week. VCU is getting the most buzz – queries are up 143% and fans are especially interested in Virginia’s coach – Shaka Smart. Searches for him are up nearly 200%.</p>
<p><strong>According to Yahoo!, this Week’s Top Searched Final Four Teams are:</strong></p>
<p><strong></strong></p>
<ol>
<li>VCU Basketball (searches up 143% this week)</li>
<li>Kentucky Basketball</li>
<li>Butler Basketball</li>
<li>UConn Basketball</li>
</ol>
<p><strong>According to Yahoo!, this Week’s Top Searched Final Four Coaches are:</strong></p>
<p><strong></strong></p>
<p><strong></p>
<ol>
<li><span style="font-weight: normal;"> Shaka Smart, VCU (searches up 2,199% this week)</span></li>
<li><span style="font-weight: normal;">Brad Stevens, Butler (searches up 416% this week)</span></li>
<li><span style="font-weight: normal;">John Calipari, Kentucky (searches up 252% this week)</span></li>
<li><span style="font-weight: normal;">Jim Calhoun, UConn</span></li>
</ol>
<p></strong></p>
<p><strong><span style="text-decoration: underline;">Email Habits: New Yahoo! Mail Survey<br />
</span></strong>And last, a new Yahoo! Mail survey shows digital etiquette may be reaching a new low.  Close to half of people polled say they check their blackberries at the dinner table.   On the other hand, more than 70% think it’s fine to send email thank you notes. So at least we&#8217;re minding our manners even if it&#8217;s electronically.</p>
<p><strong>Check out the video for more trends spiking this week.</strong></p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/04/01/searchtrends04012011/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/basketball/" rel="tag">basketball</a>, <a href="http://ycorpblog.com/tag/email-habits/" rel="tag">Email Habits</a>, <a href="http://ycorpblog.com/tag/heather-cabot/" rel="tag">Heather Cabot</a>, <a href="http://ycorpblog.com/tag/ncaa/" rel="tag">NCAA</a>, <a href="http://ycorpblog.com/tag/royal-cake/" rel="tag">Royal Cake</a>, <a href="http://ycorpblog.com/tag/vcu/" rel="tag">VCU</a>, <a href="http://ycorpblog.com/tag/yahoo-search-trends/" rel="tag">Yahoo! Search Trends</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5760 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/04/01/searchtrends04012011/'; 
			document.getElementById("link_twitter5760").innerHTML = '<a href="http://twitter.com/home/?status=Weekly+Yahoo%21+Search+Trends+%E2%80%93+April+1%2C+2011+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/04/01/searchtrends04012011/', 'BitlyCB.myShortenCallback5760');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5760" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fsearchtrends04012011%2F&t=Weekly+Yahoo%21+Search+Trends+%E2%80%93+April+1%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fsearchtrends04012011%2F&title=Weekly Yahoo! Search Trends – April 1, 2011&summary=<p>Yahoo! aggregates the billions of searches performed across Yahoo! properties to give the pulse on what people are thinking and talking about. Searches represent the people, an instant poll every moment of the day, looking into the fleeting moods and entrenched attitudes of Internet users across the world. Each week Yahoo! Web Life Editor Heather [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fsearchtrends04012011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+April+1%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fsearchtrends04012011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+April+1%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Weekly+Yahoo%21+Search+Trends+%E2%80%93+April+1%2C+2011+http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fsearchtrends04012011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fsearchtrends04012011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+April+1%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fsearchtrends04012011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fsearchtrends04012011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/04/01/searchtrends04012011/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5770">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/04/01/www2011/" rel="bookmark" title="Permanent Link to @Yahoo! Labs all over #WWW2011">@Yahoo! Labs all over #WWW2011</a></h1>
				<p class="postedby">Posted April 1st, 2011 at 9:25 am by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/04/01/www2011/#respond" title="Comment on @Yahoo! Labs all over #WWW2011">No Comments &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/yahoo-labs/" title="View all posts in yahoo! labs" rel="category tag">yahoo! labs</a></p>

				<p>This week a host of Yahoo! scientists and researchers, along with their colleagues from academic institutions and technology companies from across the world, gathered in Hyderabad, India for the 20<sup>th</sup> <a href="http://wwwconference.org/www2011/index.html">International World Wide Web Conference</a> (WWW2011). WWW2011 is an annual event held in the spring that focuses on the evolution of the Web, the standardization of Web technologies, and its impact on society and culture.</p>
<p>Yahoo!’s scientific leadership was clearly evident at WWW2011 – <a href="http://labs.yahoo.com/">Yahoo! Labs</a> presented 26 papers and posters and participated in several signature discussions around <a href="http://wwwconference.org/www2011/panel.html">crowdsourcing, social media and the digital divide</a>. Yahoo! scientists also provided demos on their latest projects, drawing a great deal of interest and attention from attendees.</p>
<p>You may have seen some of Yahoo! Labs’ thought provoking research in the news this week, whether it was focused on <a href="http://gigaom.com/2011/03/28/twitter-as-media-yes-celebrities-and-brands-still-matter/">“Who Talks to Whom on Twitter”</a> or if it’s possible to detect the <a href="http://arstechnica.com/science/news/2011/03/accurate-and-credible-news-tweets-automated-system-finds-them.ars">credibility of Tweets</a>.</p>
<p>For those who couldn’t make it to WWW2011, we checked in with <a href="http://www.flickr.com/photos/80846835@N00/5578176738/">Prabhakar Raghavan, Head of Yahoo! Labs, from the event</a>:<br />
[<object type="application/x-shockwave-flash" width="576" height="324" data="http://www.flickr.com/apps/video/stewart.swf?v=71377" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><param name="flashvars" value="intl_lang=en-us&#038;photo_secret=12d439b1f5&#038;photo_id=5578176738"></param><param name="movie" value="http://www.flickr.com/apps/video/stewart.swf?v=71377"></param><param name="bgcolor" value="#000000"></param><param name="allowFullScreen" value="true"></param><embed type="application/x-shockwave-flash" src="http://www.flickr.com/apps/video/stewart.swf?v=71377" bgcolor="#000000" allowfullscreen="true" flashvars="intl_lang=en-us&#038;photo_secret=12d439b1f5&#038;photo_id=5578176738" height="324" width="576"></embed></object>]<br />
For a full recap of all the activities from this week’s WWW2011, check out the Yahoo! Labs <a href="http://labs.yahoo.com/">Web site</a> and follow us on Twitter @YahooLabs. You can also see Tweets related to WWW2011 at #WWW2011.</p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/04/01/www2011/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/international-world-wide-web-conference/" rel="tag">International World Wide Web Conference</a>, <a href="http://ycorpblog.com/tag/media/" rel="tag">Media</a>, <a href="http://ycorpblog.com/tag/prabhakar-raghavan/" rel="tag">prabhakar raghavan</a>, <a href="http://ycorpblog.com/tag/www2011/" rel="tag">WWW2011</a>, <a href="http://ycorpblog.com/tag/yahoo-labs/" rel="tag">yahoo! labs</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5770 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/04/01/www2011/'; 
			document.getElementById("link_twitter5770").innerHTML = '<a href="http://twitter.com/home/?status=%40Yahoo%21+Labs+all+over+%23WWW2011+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/04/01/www2011/', 'BitlyCB.myShortenCallback5770');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5770" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fwww2011%2F&t=%40Yahoo%21+Labs+all+over+%23WWW2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fwww2011%2F&title=@Yahoo! Labs all over #WWW2011&summary=<p>This week a host of Yahoo! scientists and researchers, along with their colleagues from academic institutions and technology companies from across the world, gathered in Hyderabad, India for the 20th International World Wide Web Conference (WWW2011). WWW2011 is an annual event held in the spring that focuses on the evolution of the Web, the standardization [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fwww2011%2F&title=%40Yahoo%21+Labs+all+over+%23WWW2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fwww2011%2F&title=%40Yahoo%21+Labs+all+over+%23WWW2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=%40Yahoo%21+Labs+all+over+%23WWW2011+http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fwww2011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fwww2011%2F&title=%40Yahoo%21+Labs+all+over+%23WWW2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fwww2011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F04%2F01%2Fwww2011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/04/01/www2011/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5745">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/03/29/wretchfunparty/" rel="bookmark" title="Permanent Link to Yahoo! Taiwan Wretch Fun! Party: The Grand Party for Bloggers">Yahoo! Taiwan Wretch Fun! Party: The Grand Party for Bloggers</a></h1>
				<p class="postedby">Posted March 29th, 2011 at 2:15 pm by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/03/29/wretchfunparty/#respond" title="Comment on Yahoo! Taiwan Wretch Fun! Party: The Grand Party for Bloggers">No Comments &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/apac/" title="View all posts in APAC" rel="category tag">APAC</a>, <a href="http://ycorpblog.com/category/cool-stuff/" title="View all posts in Cool Stuff" rel="category tag">Cool Stuff</a></p>

				<p style="text-align: center;"><img class="alignnone" src="http://farm6.static.flickr.com/5065/5572285156_fd718e3db0.jpg" alt="" width="500" height="333" /></p>
<p>Yahoo! Taiwan really knows how to party&#8211;and the recent <a href="http://www.wretch.cc/">Wretch</a> Fun! Party was top of the blog-parade!  More than 200 of Taiwan’s leading bloggers all turned up to join the fun. Invited to share some tips and ideas about owning and developing a successful blog, party-goers heard from popular Internet voices including青青(yoke0918),小熊(ikai123),貓博士夫人(palin88) and Tramy (bobo888). The party included live performances from several bands and groups – all of whom recognized the power of the Internet, and more specifically, the role played by <a href="http://www.wretch.cc/">Wretch</a> and social networking to build their careers and following.</p>
<p>With the most qualified and comprehensive UGC, <a href="http://www.wretch.cc/">Wretch</a> continuously optimizes the browsing and exchange experience of its users by professional editing and back-office technologies. The concept of the Fun! Party this year was to emphasize our on-going commitments to optimize the sharing and exchange of high quality content which is key to building a strong interaction between the virtual and physical worlds.</p>
<p style="text-align: center;"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="576" height="324" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="allowFullScreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="src" value="http://www.youtube.com/v/5SR15V_SiYE?fs=1&amp;hl=en_US&amp;rel=0" /><param name="allowfullscreen" value="true" /><embed type="application/x-shockwave-flash" width="576" height="324" src="http://www.youtube.com/v/5SR15V_SiYE?fs=1&amp;hl=en_US&amp;rel=0" allowscriptaccess="always" allowfullscreen="true"></embed></object></p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/03/29/wretchfunparty/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/apac/" rel="tag">APAC</a>, <a href="http://ycorpblog.com/tag/wretch/" rel="tag">Wretch</a>, <a href="http://ycorpblog.com/tag/yahoo/" rel="tag">yahoo</a>, <a href="http://ycorpblog.com/tag/yahootaiwan/" rel="tag">Yahoo!Taiwan</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5745 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/03/29/wretchfunparty/'; 
			document.getElementById("link_twitter5745").innerHTML = '<a href="http://twitter.com/home/?status=Yahoo%21+Taiwan+Wretch+Fun%21+Party%3A+The+Grand+Party+for+Bloggers+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/03/29/wretchfunparty/', 'BitlyCB.myShortenCallback5745');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5745" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F29%2Fwretchfunparty%2F&t=Yahoo%21+Taiwan+Wretch+Fun%21+Party%3A+The+Grand+Party+for+Bloggers" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F29%2Fwretchfunparty%2F&title=Yahoo! Taiwan Wretch Fun! Party: The Grand Party for Bloggers&summary=<p>Yahoo! Taiwan really knows how to party&#8211;and the recent Wretch Fun! Party was top of the blog-parade!  More than 200 of Taiwan’s leading bloggers all turned up to join the fun. Invited to share some tips and ideas about owning and developing a successful blog, party-goers heard from popular Internet voices including青青(yoke0918),小熊(ikai123),貓博士夫人(palin88) and Tramy (bobo888). [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F29%2Fwretchfunparty%2F&title=Yahoo%21+Taiwan+Wretch+Fun%21+Party%3A+The+Grand+Party+for+Bloggers" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F29%2Fwretchfunparty%2F&title=Yahoo%21+Taiwan+Wretch+Fun%21+Party%3A+The+Grand+Party+for+Bloggers" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Yahoo%21+Taiwan+Wretch+Fun%21+Party%3A+The+Grand+Party+for+Bloggers+http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F29%2Fwretchfunparty%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F29%2Fwretchfunparty%2F&title=Yahoo%21+Taiwan+Wretch+Fun%21+Party%3A+The+Grand+Party+for+Bloggers" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F29%2Fwretchfunparty%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F29%2Fwretchfunparty%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/03/29/wretchfunparty/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5740">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/03/25/searchtrends03252011/" rel="bookmark" title="Permanent Link to Weekly Yahoo! Search Trends – March 25, 2011">Weekly Yahoo! Search Trends – March 25, 2011</a></h1>
				<p class="postedby">Posted March 25th, 2011 at 11:19 am by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/03/25/searchtrends03252011/#respond" title="Comment on Weekly Yahoo! Search Trends – March 25, 2011">No Comments &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/cool-stuff/" title="View all posts in Cool Stuff" rel="category tag">Cool Stuff</a></p>

				<p style="text-align: left;">Yahoo! aggregates the billions of searches performed across Yahoo! properties to give the pulse on what people are thinking and talking about. Searches represent the people, an instant poll every moment of the day, looking into the fleeting moods and entrenched attitudes of Internet users across the world. Each week <a href="http://video.yahoo.com/mypage/video?s=2913230">Yahoo! Web Life Editor Heather Cabot</a> explores these trends on local and national broadcast. The US’s involvement in Libya, the loss of a film legend and the finale of The Jersey Shore all spiked searches on Yahoo! this week.<br />
<object id="yahoovideoplayer" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="576" height="324" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="flashVars" value="vid=24660347&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide" /><param name="wmode" value="transparent" /><param name="allowscriptaccess" value="always" /><param name="allowFullScreen" value="true" /><param name="src" value="http://d.yimg.com/nl/yahoo-digital-media-bureau/ydmb/player.swf" /><param name="flashvars" value="vid=24660347&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide" /><param name="allowfullscreen" value="true" /><embed id="yahoovideoplayer" type="application/x-shockwave-flash" width="576" height="324" src="http://d.yimg.com/nl/yahoo-digital-media-bureau/ydmb/player.swf" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" flashvars="vid=24660347&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide"></embed></object><br />
<strong></strong></p>
<p style="text-align: left;"><strong>War In Libya</strong><br />
Lots of folks are turning to Yahoo1 for perspective on what’s going on in Libya.    Searches for maps of the African nation are up more than 400%t and we’re seeing lots of questions about the players in the conflict. People want to understand who the allies are and who the rebels are and why the US is involved in the conflict.  Men seem to be doing a lot of the lookups this week according to<a href="http://clues.yahoo.com/#q1=libya&amp;q2=&amp;ts=1"> Yahoo! Clues</a>, 67% of searches for “Libya” come from men.</p>
<p><strong>Elizabeth Taylor</strong><br />
Word of Elizabeth Taylor’s death sent people running to their laptops and smartphones. As soon as the news broke, searches for the beloved movie star’s films and details about her family and friendship with the late Michael Jackson soared off the charts.  Yet most unique were those eyes, and people wanted another look into them as they sought out &#8220;Elizabeth Taylor eyes,&#8221; &#8220;elisabeth Taylor violet eyes pictures,&#8221; and &#8220;elisabeth Taylor’s eye close up.&#8221;</p>
<p style="text-align: center;"><a href="http://clues.yahoo.com/#q1=elizabeth%20taylor&amp;q2=&amp;ts=2"><img class="alignnone" src="http://farm6.static.flickr.com/5025/5558497743_28ddeb3f2f_o.jpg" alt="" width="357" height="462" /></a></p>
<p><strong>The Jersey Shore &amp; Hairstyle Trends</strong><br />
The MTV reality hit Jersey Shore is spurring lots of buzz, too.  Snooki has become quite the internet darling – searches for her  ramped up773% this week. And she may have lots of potential dates after the show ends – two-thirds of those queries are coming from men – mostly in California, New Jersey and Illinois.  It’s no secret Snooki has been a trendsetter when it comes to her hair.  The Snooki bump is the top searched hairstyle on Yahoo! right now.  Other spring styles gaining popularity include the wedge (kind of like an updated Dorothy Hamill cut), emo hair (essentially spiky jet black hair) and Katie Holmes’ haircut &#8212;  all are making “waves” as women “comb” the web for new ways to freshen up their  looks for spring.</p>
<p><strong>Check out the video for more trends spiking this week.</strong></p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/03/25/searchtrends03252011/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/elizabeth-taylor/" rel="tag">Elizabeth Taylor</a>, <a href="http://ycorpblog.com/tag/hairstyles/" rel="tag">Hairstyles</a>, <a href="http://ycorpblog.com/tag/heather-cabot/" rel="tag">Heather Cabot</a>, <a href="http://ycorpblog.com/tag/jersey-shore/" rel="tag">Jersey Shore</a>, <a href="http://ycorpblog.com/tag/mtv/" rel="tag">MTV</a>, <a href="http://ycorpblog.com/tag/war-in-libya/" rel="tag">War in Libya</a>, <a href="http://ycorpblog.com/tag/yahoo-search/" rel="tag">yahoo! search</a>, <a href="http://ycorpblog.com/tag/yahoo-search-trends/" rel="tag">Yahoo! Search Trends</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5740 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/03/25/searchtrends03252011/'; 
			document.getElementById("link_twitter5740").innerHTML = '<a href="http://twitter.com/home/?status=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+25%2C+2011+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/03/25/searchtrends03252011/', 'BitlyCB.myShortenCallback5740');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5740" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F25%2Fsearchtrends03252011%2F&t=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+25%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F25%2Fsearchtrends03252011%2F&title=Weekly Yahoo! Search Trends – March 25, 2011&summary=<p>Yahoo! aggregates the billions of searches performed across Yahoo! properties to give the pulse on what people are thinking and talking about. Searches represent the people, an instant poll every moment of the day, looking into the fleeting moods and entrenched attitudes of Internet users across the world. Each week Yahoo! Web Life Editor Heather [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F25%2Fsearchtrends03252011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+25%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F25%2Fsearchtrends03252011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+25%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+25%2C+2011+http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F25%2Fsearchtrends03252011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F25%2Fsearchtrends03252011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+25%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F25%2Fsearchtrends03252011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F25%2Fsearchtrends03252011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/03/25/searchtrends03252011/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5730">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/03/24/closing-bell/" rel="bookmark" title="Permanent Link to Yahoo! Finance Rings NASDAQ Closing Bell">Yahoo! Finance Rings NASDAQ Closing Bell</a></h1>
				<p class="postedby">Posted March 24th, 2011 at 2:03 pm by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/03/24/closing-bell/#comments" title="Comment on Yahoo! Finance Rings NASDAQ Closing Bell">1 Comment &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/cool-stuff/" title="View all posts in Cool Stuff" rel="category tag">Cool Stuff</a>, <a href="http://ycorpblog.com/category/yahoo-finance-2/" title="View all posts in Yahoo! Finance" rel="category tag">Yahoo! Finance</a></p>

				<p>On Thursday March 24th, Yahoo! Finance was honored with the opportunity to ring the NASDAQ Closing Bell to announce the new name of their popular web show Tech Ticker to <a href="http://finance.yahoo.com/blogs/daily-ticker/">The Daily Ticker</a> and celebrate the launch of a new original web show <a href="http://finance.yahoo.com/blogs/daily-ticker">Breakout</a>.</p>
<p>The Daily Ticker will continue to be hosted by Aaron Task, Daniel Gross, and Henry Blodget and be broadcast at the NASDAQ MarketSite studio.  The name change is better aligned with the content of the show which focuses on all the news of the day that effects Wall Street including the financial impact of political happenings.</p>
<p><img class="alignnone" src="http://farm6.static.flickr.com/5053/5557009306_de1381af5c.jpg" alt="Yahoo! Finance Team at NASDAQ preparing to ring the closing bell  " width="500" height="282" /></p>
<p style="text-align: left;">Launched on Monday, March 21st Breakout is the second video program from Yahoo! Finance and is hosted by Jeff Macke, Formerly of CNBC’s Fast Money, and Business Day reporter and former Bloomberg TV correspondent Matt Nesto.</p>
<p><img class=" alignnone" src="http://farm6.static.flickr.com/5143/5556424117_fc4658d836.jpg" alt="Yahoo! Finance The Daily Ticker &amp; Breakout Hosts along with managing editor Diane Galligan at NASDAQ.  " width="500" height="282" /></p>
<p style="text-align: left;">“Tech Ticker” averages more than 350k streams per day and according to Comscore draws a monthly audience of 4.3 million viewers.  This makes it the most popular finance show on the Internet.  And it&#8217;s not surprising considering Yahoo! Finance is the most popular destination in financial media.  The site now attracts 40 million people each month, who come for the financial news and information they need to make informed investment decisions.</p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/03/24/closing-bell/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/nasdaq/" rel="tag">nasdaq</a>, <a href="http://ycorpblog.com/tag/tech-ticker/" rel="tag">tech ticker</a>, <a href="http://ycorpblog.com/tag/yahoo-finance/" rel="tag">yahoo! finance</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5730 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/03/24/closing-bell/'; 
			document.getElementById("link_twitter5730").innerHTML = '<a href="http://twitter.com/home/?status=Yahoo%21+Finance+Rings+NASDAQ+Closing+Bell++'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/03/24/closing-bell/', 'BitlyCB.myShortenCallback5730');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5730" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fclosing-bell%2F&t=Yahoo%21+Finance+Rings+NASDAQ+Closing+Bell+" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fclosing-bell%2F&title=Yahoo! Finance Rings NASDAQ Closing Bell&summary=<p>On Thursday March 24th, Yahoo! Finance was honored with the opportunity to ring the NASDAQ Closing Bell to announce the new name of their popular web show Tech Ticker to The Daily Ticker and celebrate the launch of a new original web show Breakout. The Daily Ticker will continue to be hosted by Aaron Task, Daniel [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fclosing-bell%2F&title=Yahoo%21+Finance+Rings+NASDAQ+Closing+Bell+" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fclosing-bell%2F&title=Yahoo%21+Finance+Rings+NASDAQ+Closing+Bell+" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Yahoo%21+Finance+Rings+NASDAQ+Closing+Bell++http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fclosing-bell%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fclosing-bell%2F&title=Yahoo%21+Finance+Rings+NASDAQ+Closing+Bell+" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fclosing-bell%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fclosing-bell%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/03/24/closing-bell/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5722">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/03/24/ie9yahoo/" rel="bookmark" title="Permanent Link to IE9: Internet Exploration…with a Yahoo! Twist">IE9: Internet Exploration…with a Yahoo! Twist</a></h1>
				<p class="postedby">Posted March 24th, 2011 at 8:10 am by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/03/24/ie9yahoo/#respond" title="Comment on IE9: Internet Exploration…with a Yahoo! Twist">No Comments &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/cool-stuff/" title="View all posts in Cool Stuff" rel="category tag">Cool Stuff</a></p>

				<p>You may have seen last week Microsoft launched the new Internet Explorer 9 browser. Well we want to make sure you’re getting the most out of it, so we’re bringing you a Yahoo!-enhanced experience on IE9 for Windows 7 and Vista users. So what does this mean? Things just got much simpler.</p>
<p>Leveraging IE9’s powerful engine and updated standards (like HTML 5), the new Yahoo!-enhanced browser gives you direct access to some of your favorite sites and products including Yahoo! Search, Yahoo! Toolbar, and of course, the Yahoo! Homepage. For example, you can get instant results for the day’s important news, weather forecast, or the latest stock quotes by simply typing in the search box from your browser. You can also customize your Yahoo! Toolbar and take advantage of  IE9’s built-in security and privacy technologies and cleaner design to more securely access results and view Yahoo! properties and other sites from across the Web with far less clutter.</p>
<p>Some other cool features and functionality:</p>
<ul>
<li><strong>Pinning </strong>– Catch up on celebrity news more quickly or check out your friend’s photostream anytime by creating a shortcut to sites like OMG and Flickr and attaching them directly to the taskbar. Launch your favorite sites with a single click by simply dragging the icon or tab from the browser down to the Windows Taskbar.</li>
<li><strong>Jump Lists</strong> – Once you’ve pinned a site, right click on the icon from the Windows Taskbar to see the most recent files, news, and updates. Get information on the latest global topics or keep up with your favorite teams with our first Jump Lists for Homepage (globally available), Yahoo! News and Yahoo! Sports (both U.S.-only). Look out for more to come as we roll out support for additional sites.</li>
</ul>
<p style="text-align: center;"><img class="alignnone" src="http://farm6.static.flickr.com/5066/5555149944_a1441db9c3_o.png " alt="" width="251" height="322" /></p>
<p>We’re looking forward to adding even more integrations across Yahoo! properties in the coming months to make your Web experience even easier to get the information you want most. In the meantime, you can upgrade to the new Internet Explorer 9 enhanced by Yahoo! by downloading it here: <a href="http://downloads.yahoo.com/internetexplorer/">http://downloads.yahoo.com/internetexplorer/</a></p>
<p><a href="http://downloads.yahoo.com/internetexplorer/"></a><br />
Happy browsing!</p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/03/24/ie9yahoo/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/html5/" rel="tag">HTML5</a>, <a href="http://ycorpblog.com/tag/ie9/" rel="tag">IE9</a>, <a href="http://ycorpblog.com/tag/internet-explorer-9/" rel="tag">Internet Explorer 9</a>, <a href="http://ycorpblog.com/tag/jump-lists/" rel="tag">Jump Lists</a>, <a href="http://ycorpblog.com/tag/microsoft/" rel="tag">microsoft</a>, <a href="http://ycorpblog.com/tag/pinning/" rel="tag">Pinning</a>, <a href="http://ycorpblog.com/tag/search/" rel="tag">search</a>, <a href="http://ycorpblog.com/tag/toolbar/" rel="tag">toolbar</a>, <a href="http://ycorpblog.com/tag/windows-7/" rel="tag">Windows 7</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5722 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/03/24/ie9yahoo/'; 
			document.getElementById("link_twitter5722").innerHTML = '<a href="http://twitter.com/home/?status=IE9%3A+Internet+Exploration%E2%80%A6with+a+Yahoo%21+Twist+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/03/24/ie9yahoo/', 'BitlyCB.myShortenCallback5722');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5722" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fie9yahoo%2F&t=IE9%3A+Internet+Exploration%E2%80%A6with+a+Yahoo%21+Twist" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fie9yahoo%2F&title=IE9: Internet Exploration…with a Yahoo! Twist&summary=<p>You may have seen last week Microsoft launched the new Internet Explorer 9 browser. Well we want to make sure you’re getting the most out of it, so we’re bringing you a Yahoo!-enhanced experience on IE9 for Windows 7 and Vista users. So what does this mean? Things just got much simpler. Leveraging IE9’s powerful [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fie9yahoo%2F&title=IE9%3A+Internet+Exploration%E2%80%A6with+a+Yahoo%21+Twist" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fie9yahoo%2F&title=IE9%3A+Internet+Exploration%E2%80%A6with+a+Yahoo%21+Twist" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=IE9%3A+Internet+Exploration%E2%80%A6with+a+Yahoo%21+Twist+http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fie9yahoo%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fie9yahoo%2F&title=IE9%3A+Internet+Exploration%E2%80%A6with+a+Yahoo%21+Twist" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fie9yahoo%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F24%2Fie9yahoo%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/03/24/ie9yahoo/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5710">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/03/23/searchdirect/" rel="bookmark" title="Permanent Link to Introducing Search Direct – A Simpler Way to Find Answers Fast">Introducing Search Direct – A Simpler Way to Find Answers Fast</a></h1>
				<p class="postedby">Posted March 23rd, 2011 at 9:33 am by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/03/23/searchdirect/#comments" title="Comment on Introducing Search Direct – A Simpler Way to Find Answers Fast">2 Comments &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/search/" title="View all posts in search" rel="category tag">search</a></p>

				<p style="text-align: center;"><object id="yahoovideoplayer" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="576" height="324" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="flashVars" value="vid=24629954&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide" /><param name="wmode" value="transparent" /><param name="allowscriptaccess" value="always" /><param name="allowFullScreen" value="true" /><param name="src" value="http://d.yimg.com/nl/yahoo-digital-media-bureau/ydmb/player.swf" /><param name="flashvars" value="vid=24629954&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide" /><param name="allowfullscreen" value="true" /><embed id="yahoovideoplayer" type="application/x-shockwave-flash" width="576" height="324" src="http://d.yimg.com/nl/yahoo-digital-media-bureau/ydmb/player.swf" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" flashvars="vid=24629954&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide"></embed></object></p>
<p style="text-align: left;">Ever wish you could have answers and direct access to websites before you complete a query, hit the search button, or go to a search results page? Welcome to <a href="http://search.yahoo.com/">Search Direct</a>.</p>
<p>This new feature (currently in beta) taps into Yahoo!’s unique opportunity to combine content and structured data and to provide a rich search experience. Search Direct predicts search results as fast as a person types, character by character, and presents those results dynamically, generating a fast, simple search experience that goes beyond a list of blue links.  Search Direct rolls out in a public beta to Yahoo! users across the U.S. today, and will be available in other Yahoo! products and markets later this year.</p>
<p>With Search Direct, Yahoo! content is combined with information from the Web to provide rich answers, not just links, and to give people the option to immediately engage or continue to a traditional search results page. In this beta release, coverage includes top trending searches, movies, TV, sports teams and players, weather, local, travel, stocks, and shopping categories now available at search.yahoo.com.</p>
<p style="text-align: center;"><img class="alignnone" src="http://farm6.static.flickr.com/5261/5553529060_085502836b.jpg" alt="" width="500" height="237" /></p>
<ul>
<li><strong>Trending Searches</strong> – The moment the cursor hits the search box, top search trends appear and are updated every 10 minutes to display the latest and greatest search trends.</li>
<li><strong>Search Previews</strong> – Search Direct predicts the search term as you type, providing the 10 most likely searches. You can then easily scan each option to see the related top results and find the best match for your needs.</li>
<li><strong>Direct Answers</strong> – For many common searches, Search Direct provides instant answers before you click the Search button. Find an address or phone number, a three-day weather forecast, financial stock performance, the top trending stories at Yahoo! News, or when and where a movie is playing – all without going to a results page.</li>
<li><strong>Direct Results</strong> – When you scan the search options and find the site you need, Search Direct provides exactly that – direct access to the site. No more overwhelming pages of links.</li>
<li><strong>Rich Content</strong> – For all top searches about sports, top news stories, and finance, Search Direct displays rich content that only the world’s largest digital media company can provide. For example, type “n” to get the Yahoo! News display, which always shows the top two trending stories.</li>
</ul>
<p>We will continue to enhance and update Search Direct with new content, such as popular music and local listings.  For more information and a demo video of Search Direct from Yahoo!, visit <a href="http://search.yahoo.com/">search.yahoo.com</a></p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/03/23/searchdirect/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/answers/" rel="tag">answers</a>, <a href="http://ycorpblog.com/tag/beta/" rel="tag">Beta</a>, <a href="http://ycorpblog.com/tag/content/" rel="tag">content</a>, <a href="http://ycorpblog.com/tag/data/" rel="tag">Data</a>, <a href="http://ycorpblog.com/tag/direct-answers/" rel="tag">direct answers</a>, <a href="http://ycorpblog.com/tag/direct-results/" rel="tag">direct results</a>, <a href="http://ycorpblog.com/tag/movie/" rel="tag">movie</a>, <a href="http://ycorpblog.com/tag/news/" rel="tag">news</a>, <a href="http://ycorpblog.com/tag/results/" rel="tag">results</a>, <a href="http://ycorpblog.com/tag/rich-content/" rel="tag">rich content</a>, <a href="http://ycorpblog.com/tag/search-direct/" rel="tag">Search Direct</a>, <a href="http://ycorpblog.com/tag/search-previews/" rel="tag">search previews</a>, <a href="http://ycorpblog.com/tag/sports/" rel="tag">Sports</a>, <a href="http://ycorpblog.com/tag/stock/" rel="tag">stock</a>, <a href="http://ycorpblog.com/tag/trending-searches/" rel="tag">trending searches</a>, <a href="http://ycorpblog.com/tag/trends/" rel="tag">trends</a>, <a href="http://ycorpblog.com/tag/weather/" rel="tag">weather</a>, <a href="http://ycorpblog.com/tag/yahoo/" rel="tag">yahoo</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5710 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/03/23/searchdirect/'; 
			document.getElementById("link_twitter5710").innerHTML = '<a href="http://twitter.com/home/?status=Introducing+Search+Direct+%E2%80%93+A+Simpler+Way+to+Find+Answers+Fast+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/03/23/searchdirect/', 'BitlyCB.myShortenCallback5710');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5710" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F23%2Fsearchdirect%2F&t=Introducing+Search+Direct+%E2%80%93+A+Simpler+Way+to+Find+Answers+Fast" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F23%2Fsearchdirect%2F&title=Introducing Search Direct – A Simpler Way to Find Answers Fast&summary=<p>Ever wish you could have answers and direct access to websites before you complete a query, hit the search button, or go to a search results page? Welcome to Search Direct. This new feature (currently in beta) taps into Yahoo!’s unique opportunity to combine content and structured data and to provide a rich search experience. [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F23%2Fsearchdirect%2F&title=Introducing+Search+Direct+%E2%80%93+A+Simpler+Way+to+Find+Answers+Fast" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F23%2Fsearchdirect%2F&title=Introducing+Search+Direct+%E2%80%93+A+Simpler+Way+to+Find+Answers+Fast" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Introducing+Search+Direct+%E2%80%93+A+Simpler+Way+to+Find+Answers+Fast+http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F23%2Fsearchdirect%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F23%2Fsearchdirect%2F&title=Introducing+Search+Direct+%E2%80%93+A+Simpler+Way+to+Find+Answers+Fast" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F23%2Fsearchdirect%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F23%2Fsearchdirect%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/03/23/searchdirect/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5704">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/03/21/girlgeekdinner11/" rel="bookmark" title="Permanent Link to Geek Out Over Dinner">Geek Out Over Dinner</a></h1>
				<p class="postedby">Posted March 21st, 2011 at 6:31 am by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/03/21/girlgeekdinner11/#comments" title="Comment on Geek Out Over Dinner">1 Comment &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/developer/" title="View all posts in Developer" rel="category tag">Developer</a>, <a href="http://ycorpblog.com/category/women-in-tech/" title="View all posts in Women in Tech" rel="category tag">Women in Tech</a>, <a href="http://ycorpblog.com/category/working-at-yahoo/" title="View all posts in Working at Yahoo!" rel="category tag">Working at Yahoo!</a></p>

				<p style="text-align: center;"><img class="alignnone" src="http://farm6.static.flickr.com/5300/5546755431_789c6f2ae4.jpg" alt="" width="500" height="375" /></p>
<p>In conjunction with Women’s History Month, Yahoo! is proud to be hosting its second <a href="http://www.bayareagirlgeekdinners.com/">Bay Area Girl Geek Dinner on March 22nd</a>, to celebrate and honor all of the tremendous women who work in the technology industry – past, present and future.</p>
<p>More than 400 women from across the leading tech companies will meet, network and share their wealth of tech experience, including diverse perspectives from women at different stages of their careers. Guests can participate in an informal Yahoo! panel discussion on ensuring the success of women in technology in the future, as well as a host of other fun events that will run throughout the night. There will be two photo booths, a “Women in History” Jeopardy game and an anagram puzzle station. Attendees will also be able to experience Yahoo! products, such as <a href="http://www.livestand.com/">Livestand</a>, <a href="http://connectedtv.yahoo.com/">Connected TV</a>, <a href="http://developer.yahoo.com/blogs/hadoop/">Hadoop</a> and <a href="http://developer.yahoo.com/yslow/">YSlow</a> &#8211; all of which have had major contributions from Yahoo!’s female technologists.</p>
<p>So you may be asking, what is a girl geek?  We define girl geeks as women who are passionate about technology and are making a difference in their field each and every day.  The organization behind the dinner, <a href="http://www.flickr.com/groups/yahoo_women_in_tech/">Yahoo! Women in Tech</a>, touts more than 600 members across the globe and focuses on empowering female tech professionals to succeed, as well as encouraging K-12 girls to pursue technical careers.  The group is thrilled to host this event again and showcase the incredible contributions and achievements of both women at Yahoo! and the wider industry.</p>
<p>We hope you can join us for an informative and enjoyable evening that celebrates your efforts and achievements in tech!</p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/03/21/girlgeekdinner11/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/bay-area/" rel="tag">Bay Area</a>, <a href="http://ycorpblog.com/tag/connected-tv/" rel="tag">connected tv</a>, <a href="http://ycorpblog.com/tag/games/" rel="tag">games</a>, <a href="http://ycorpblog.com/tag/girl-geek/" rel="tag">girl geek</a>, <a href="http://ycorpblog.com/tag/hadoop/" rel="tag">hadoop</a>, <a href="http://ycorpblog.com/tag/livestand/" rel="tag">Livestand</a>, <a href="http://ycorpblog.com/tag/network/" rel="tag">network</a>, <a href="http://ycorpblog.com/tag/puzzles/" rel="tag">puzzles</a>, <a href="http://ycorpblog.com/tag/technology/" rel="tag">technology</a>, <a href="http://ycorpblog.com/tag/womens-history-month/" rel="tag">Women's History Month</a>, <a href="http://ycorpblog.com/tag/yahoo/" rel="tag">yahoo</a>, <a href="http://ycorpblog.com/tag/yahoo-women-in-tech/" rel="tag">Yahoo! Women in Tech</a>, <a href="http://ycorpblog.com/tag/yslow/" rel="tag">YSlow</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5704 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/03/21/girlgeekdinner11/'; 
			document.getElementById("link_twitter5704").innerHTML = '<a href="http://twitter.com/home/?status=+Geek+Out+Over+Dinner+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/03/21/girlgeekdinner11/', 'BitlyCB.myShortenCallback5704');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5704" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F21%2Fgirlgeekdinner11%2F&t=+Geek+Out+Over+Dinner" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F21%2Fgirlgeekdinner11%2F&title=Geek Out Over Dinner&summary=<p>In conjunction with Women’s History Month, Yahoo! is proud to be hosting its second Bay Area Girl Geek Dinner on March 22nd, to celebrate and honor all of the tremendous women who work in the technology industry – past, present and future. More than 400 women from across the leading tech companies will meet, network [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F21%2Fgirlgeekdinner11%2F&title=+Geek+Out+Over+Dinner" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F21%2Fgirlgeekdinner11%2F&title=+Geek+Out+Over+Dinner" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=+Geek+Out+Over+Dinner+http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F21%2Fgirlgeekdinner11%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F21%2Fgirlgeekdinner11%2F&title=+Geek+Out+Over+Dinner" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F21%2Fgirlgeekdinner11%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F21%2Fgirlgeekdinner11%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/03/21/girlgeekdinner11/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
			<div class="post" id="post-5701">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
				<h1><a href="http://ycorpblog.com/2011/03/18/searchtrends03182011/" rel="bookmark" title="Permanent Link to Weekly Yahoo! Search Trends – March 18, 2011">Weekly Yahoo! Search Trends – March 18, 2011</a></h1>
				<p class="postedby">Posted March 18th, 2011 at 12:30 pm by <a href="http://ycorpblog.com/author/lucas-mast/" title="Posts by Yahoo!">Yahoo!</a>, Blog Editors</p>
				<p class="filed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_num_comments.gif" alt="Number of Comments"/>&nbsp;<a href="http://ycorpblog.com/2011/03/18/searchtrends03182011/#comments" title="Comment on Weekly Yahoo! Search Trends – March 18, 2011">1 Comment &#187;</a> / Filed in: <a href="http://ycorpblog.com/category/cool-stuff/" title="View all posts in Cool Stuff" rel="category tag">Cool Stuff</a></p>

				<p style="text-align: left;">Yahoo! aggregates the billions of searches performed across Yahoo! properties to give the pulse on what people are thinking and talking about. Searches represent the people, an instant poll every moment of the day, looking into the fleeting moods and entrenched attitudes of Internet users across the world. Each week <a href="http://video.yahoo.com/mypage/video?s=2913230">Yahoo! Web Life Editor Heather Cabot</a> explores these trends on local and national broadcast. The Japan earthquake and tsunami dominated searches this week on Yahoo!.</p>
<p style="text-align: center;"><object id="yahoovideoplayer" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="576" height="324" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="flashVars" value="vid=24574955&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide" /><param name="wmode" value="transparent" /><param name="allowscriptaccess" value="always" /><param name="allowFullScreen" value="true" /><param name="src" value="http://d.yimg.com/nl/yahoo-digital-media-bureau/ydmb/player.swf" /><param name="flashvars" value="vid=24574955&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide" /><param name="allowfullscreen" value="true" /><embed id="yahoovideoplayer" type="application/x-shockwave-flash" width="576" height="324" src="http://d.yimg.com/nl/yahoo-digital-media-bureau/ydmb/player.swf" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" flashvars="vid=24574955&amp;playlistId=22824372&amp;shareUrl=http://ycorpblog.com/2011/02/16/barcelona-livestand/&amp;browseCarouselUI=hide"></embed></object></p>
<p style="text-align: left;"><strong>Japan Crisis</strong><br />
The crisis in Japan continues to dominate searches on Yahoo!.  It’s been more than a week since the 8.9 earthquake and tsunami wreaked havoc on the island nation. The rescue and recovery efforts coupled with the threat of nuclear disaster sent online to find real time photos, videos and the latest news.</p>
<p>Yahoo! users wanted to know more about the threat of radiation exposure and how it’s treated. After multiple explosions rocked the Fukushima power plant, searches for preventive measures, including, potassium iodide tablets spiked 150%.  People wanted to understand how far radiation can travel, which other countries could be affected and they also looked back at the history of other nuclear disasters such as Chernobyl and Three Mile Island.  In addition to finding out how to help, concerned citizens also sought information on how to prepare for emergencies in their own homes.  Searches for emergency kits ramped up across the web this week.</p>
<p>To learn more about how you can make a difference, visit <a href=" http://news.yahoo.com/s/yblog_newsroom/20110311/wl_yblog_newsroom/japan-earthquake-and-tsunami-how-to-help)">Yahoo!’s How to Help  page</a>.</p>
<p><strong>March Madness</strong><br />
March Madness provided a lighter topic to talk about around the water cooler, this week. The teams folks seem to be rooting for most are Ohio, Duke and BYU.  Fans are busy focusing on their brackets – searches for NCAA are up 109,000%.  The states most interested in the tournament according to search are Ohio, Illinois and California, Michigan and Pennsylvania.</p>
<p>Fans can fill out brackets at Yahoo! Tourney Pick’em  at Yahoo! Sports for a chance to win $1 million for the perfect bracket, and there is a guaranteed $10K prize for the best bracket.</p>
<p style="text-align: center;"><a href="http://tournament.fantasysports.yahoo.com/"><img class="alignnone" src="http://farm6.static.flickr.com/5140/5538259244_71b0274ef6.jpg" alt="" width="500" height="262" /></a></p>
<p><strong>Check out the video for more trends spiking this week.</strong></p>

				<div class="clr"></div>
				<p class="buzz">		<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yodel_anecdot236:http://ycorpblog.com/2011/03/18/searchtrends03182011/</script>
		</p>
				<p class="tagged">Tagged: <a href="http://ycorpblog.com/tag/heather-cabot/" rel="tag">Heather Cabot</a>, <a href="http://ycorpblog.com/tag/japan/" rel="tag">Japan</a>, <a href="http://ycorpblog.com/tag/march-madness/" rel="tag">March Madness</a>, <a href="http://ycorpblog.com/tag/yahoo-search-trends/" rel="tag">Yahoo! Search Trends</a></p>
				<div class="divider"></div>
				<div class="bookmarks">
<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback5701 = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://ycorpblog.com/2011/03/18/searchtrends03182011/'; 
			document.getElementById("link_twitter5701").innerHTML = '<a href="http://twitter.com/home/?status=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+18%2C+2011+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://ycorpblog.com/2011/03/18/searchtrends03182011/', 'BitlyCB.myShortenCallback5701');
	});
</script>
<br />

<ul class="postlinks">
	<li><div id="link_twitter5701" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F18%2Fsearchtrends03182011%2F&t=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+18%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F18%2Fsearchtrends03182011%2F&title=Weekly Yahoo! Search Trends – March 18, 2011&summary=<p>Yahoo! aggregates the billions of searches performed across Yahoo! properties to give the pulse on what people are thinking and talking about. Searches represent the people, an instant poll every moment of the day, looking into the fleeting moods and entrenched attitudes of Internet users across the world. Each week Yahoo! Web Life Editor Heather [...]</p>
&source=http%3A%2F%2Fycorpblog.com" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F18%2Fsearchtrends03182011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+18%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F18%2Fsearchtrends03182011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+18%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+18%2C+2011+http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F18%2Fsearchtrends03182011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F18%2Fsearchtrends03182011%2F&title=Weekly+Yahoo%21+Search+Trends+%E2%80%93+March+18%2C+2011" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F18%2Fsearchtrends03182011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fycorpblog.com%2F2011%2F03%2F18%2Fsearchtrends03182011%2F" target="_blank"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
</div>				<p class="icons"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_post.gif" alt="Post a comment"/><a href="http://ycorpblog.com/2011/03/18/searchtrends03182011/#comment">Post a Comment</a></p>
<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			</div>

		
		<div class="navigation">
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/post_bottom.gif" alt="" class="post_bottom"/>
			<p><a href="http://ycorpblog.com/page/2/" >&laquo; Previous Entries</a>&nbsp;&nbsp;&nbsp;</p>
		</div>

	

		</div>
		<div class="right">
			<div class="purple">		
				<form method=get action="http://search.yahoo.com/search" id="searchform" onsubmit="OnSearchFormSubmit();">
<input type="hidden" name="fr" value="yscpb" />
<p class="search"><span>Search:</span><input type="radio" name="mobvs" id="blog" value="1" checked /><label for="blog">Blog</label> <input type="radio" name="mobvs" value="0" id="web" /><label for="web">Web</label></p>
<input type="text" class="text" name="p" id="s" /><input type="image" src="http://ycorpblog.com/wp-content/themes/yodel/images/bg_go.gif" class="go" id="searchsubmit"/>
<input type=hidden name=mobid value="U0ROzK69GDJdQAADgp6BU" /></form>


				<div class="subscribe">
					<p style="font-size: 11px;">Subscribe: <a href="http://yodel.yahoo.com/feed"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_rss.gif" alt=""/>RSS</a> <a href="javascript:slidedown_showHide();"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_email.gif" alt=""/>E-mail</a><a href="http://twitter.com/yahoo"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_twitter.gif" alt=""/>Twitter</a></p>
				</div>

				<div id="emailsubscribebox" class="emailbox">
					<a href="javascript:slidedown_showHide();"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/close.gif" alt="Close This Box" class="close"/></a>
					<form action="http://www.feedburner.com/fb/a/emailverify" method="post" target="popupwindow" onsubmit="window.open('http://www.feedburner.com', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
						<p>Enter your email address:</p>
						<p><input type="text" class="input" name="email"/></p>
						<input type="hidden" value="Yodel Anecdotal" name="title"/>
						<input type="hidden" value="http://feeds.feedburner.com/~e?ffid=436518" name="url"/>
      					<input type="hidden" name="loc" value="en_US"/>
						<input type="submit" id="subscribesubmit" value="SUBSCRIBE" class="subscribebutton" />
						</p>
					</form>
				</div>

				<h1>Recent Posts:</h1>
				<p class='recent'><a href='/2011/04/04/royal-wedding-invite'>You’re invited to the Royal Wedding! (Online, of course). </a><br/>April 4, 2011</p><img src='/wp-content/themes/yodel/images/divider_purple.gif' class='purple_line'/><p class='recent'><a href='/2011/04/01/searchtrends04012011'>Weekly Yahoo! Search Trends – April 1, 2011</a><br/>April 1, 2011</p><img src='/wp-content/themes/yodel/images/divider_purple.gif' class='purple_line'/><p class='recent'><a href='/2011/04/01/www2011'>@Yahoo! Labs all over #WWW2011</a><br/>April 1, 2011</p><img src='/wp-content/themes/yodel/images/divider_purple.gif' class='purple_line'/><p class='recent'><a href='/2011/03/29/wretchfunparty'>Yahoo! Taiwan Wretch Fun! Party: The Grand Party for Bloggers</a><br/>March 29, 2011</p><img src='/wp-content/themes/yodel/images/divider_purple.gif' class='purple_line'/>
			</div>
			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/purple_bottom.gif" alt=""/>

                        <img class="follow_top" src="http://ycorpblog.com/wp-content/themes/yodel/images/follow-us-top.gif" alt=""/>
			<div class="follow-us">
				<h1>Follow Us</h1>
				<div style="padding: 0px 0px 6px 27px">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr align="center">
							<td align="center">
								<a href="http://www.facebook.com/yahoo" target="_blank">
			    					<img height="24" width="24" alt="Facebook" src="http://ycorpblog.com/wp-content/themes/yodel/images/facebook.gif" style="padding-left: 10px;"/>
								</a>
							</td>
							<td align="center">
								<a href="http://www.twitter.com/yahoo" target="_blank">
    								<img height="24" width="24" alt="Twitter" src="http://ycorpblog.com/wp-content/themes/yodel/images/twitter.gif"style="padding-left: 3px;"/>
								</a>
							</td>
							<td align="center">
								<a href="http://www.youtube.com/yahoo" target="_blank">
    								<img height="24" width="24" alt="YouTube" src="http://ycorpblog.com/wp-content/themes/yodel/images/youtube.gif"style="padding-left: 7px;"/>
								</a>
							</td>
							<td align="center">
								<a href="http://add.my.yahoo.com/rss?url=http://www.ycorpblog.com/feed/" target="_blank">
  			    					<img height="24" width="24" border="0" src="http://ycorpblog.com/wp-content/themes/yodel/images/myyahoo.gif" alt="My Yahoo!" style="padding-left: 10px;" />
								</a>
							</td>
						</tr>
						<tr align="center">
							<td align="center"><p>Facebook</p></td>
							<td align="center"><p>Twitter</p></td>
							<td align="center"><p>YouTube</p></td>
							<td align="center"><p>My Yahoo!</p></td>
						</tr>
					</table>
				</div>
			</div>
			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/follow-us-bottom.gif" alt=""/>

			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/greatest-hits-top.gif" alt=""/>
			<div class="greatest-hits">
				<h1>Greatest Hits</h1>
				<p class="recent tagline">The stuff you dug the most</p>
																								<p class="recent"><a href="http://ycorpblog.com/2009/02/26/getting-our-house-in-order/" rel="bookmark" title="Permanent Link to Getting our house in order">Getting our house in order</a><br/>
								February 26, 2009</p>
								<img src="http://ycorpblog.com/wp-content/themes/yodel/images/blue-line.gif" alt="" class="purple_line"/>
														<p class="recent"><a href="http://ycorpblog.com/2008/11/25/backstage-at-our-homepage/" rel="bookmark" title="Permanent Link to Backstage at our homepage">Backstage at our homepage</a><br/>
								November 25, 2008</p>
								<img src="http://ycorpblog.com/wp-content/themes/yodel/images/blue-line.gif" alt="" class="purple_line"/>
														<p class="recent"><a href="http://ycorpblog.com/2008/08/04/and-now-we-dance/" rel="bookmark" title="Permanent Link to And now we dance">And now we dance</a><br/>
								August 4, 2008</p>
								<img src="http://ycorpblog.com/wp-content/themes/yodel/images/blue-line.gif" alt="" class="purple_line"/>
														<p class="recent"><a href="http://ycorpblog.com/2007/07/08/theres-no-winning-the-yahoo-lottery/" rel="bookmark" title="Permanent Link to There&#8217;s no winning the Yahoo! lottery">There&#8217;s no winning the Yahoo! lottery</a><br/>
								July 8, 2007</p>
								<img src="http://ycorpblog.com/wp-content/themes/yodel/images/blue-line.gif" alt="" class="purple_line"/>
														</div>
			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/greatest-hits-bottom.gif" alt=""/>
			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/photos_top.gif" alt="" class="photos_top"/>
			<div class="photos">
				<div>
					<a href="http://www.flickr.com/photos/yodelanecdotal/5553916442/" title="Search Direct-Sports View Lakers"><img src="http://farm6.static.flickr.com/5309/5553916442_002115f011_s.jpg" alt="Search Direct-Sports View Lakers" /></a><a href="http://www.flickr.com/photos/yodelanecdotal/5553903564/" title="Search Direct-Trending News"><img src="http://farm6.static.flickr.com/5253/5553903564_28f2d19645_s.jpg" alt="Search Direct-Trending News" /></a><a href="http://www.flickr.com/photos/yodelanecdotal/5553317799/" title="Search Direct Trending Local News"><img src="http://farm6.static.flickr.com/5301/5553317799_8d3c02704c_s.jpg" alt="Search Direct Trending Local News" /></a><a href="http://www.flickr.com/photos/yodelanecdotal/5553903074/" title="Search Direct-Sports Teams"><img src="http://farm6.static.flickr.com/5148/5553903074_e1c934a4b3_s.jpg" alt="Search Direct-Sports Teams" /></a><a href="http://www.flickr.com/photos/yodelanecdotal/5553882610/" title="Search Direct-Movie showtimes"><img src="http://farm6.static.flickr.com/5137/5553882610_93a807e6a5_s.jpg" alt="Search Direct-Movie showtimes" /></a><a href="http://www.flickr.com/photos/yodelanecdotal/5553529060/" title="Search Direct-Weather"><img src="http://farm6.static.flickr.com/5261/5553529060_085502836b_s.jpg" alt="Search Direct-Weather" /></a>				</div>	
				<p>View Yahoo! on <a href="http://www.flickr.com/photos/yodelanecdotal/"><img src="http://ycorpblog.com/wp-content/themes/yodel/images/icon_flickr.gif" alt="Flickr"/></a> </p>	
			</div>
			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/photos_bottom.gif" alt=""/>

			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/links_top.gif" class="photos_top" alt=""/>
			<div class="links">
				<div class="archives">
					<p>Categories:</p>
					<ul>
							<li class="cat-item cat-item-522"><a href="http://ycorpblog.com/category/15th-birthday/" title="View all posts filed under 15th Birthday">15th Birthday</a>
</li>
	<li class="cat-item cat-item-1372"><a href="http://ycorpblog.com/category/apac/" title="View all posts filed under APAC">APAC</a>
</li>
	<li class="cat-item cat-item-9"><a href="http://ycorpblog.com/category/behind-the-scenes/" title="View all posts filed under Behind the Scenes">Behind the Scenes</a>
</li>
	<li class="cat-item cat-item-686"><a href="http://ycorpblog.com/category/brand-campaign/" title="View all posts filed under Brand Campaign">Brand Campaign</a>
</li>
	<li class="cat-item cat-item-16"><a href="http://ycorpblog.com/category/conferencesevents/" title="View all posts filed under Conferences/Events">Conferences/Events</a>
</li>
	<li class="cat-item cat-item-168"><a href="http://ycorpblog.com/category/connected-tv/" title="View all posts filed under connected tv">connected tv</a>
</li>
	<li class="cat-item cat-item-10"><a href="http://ycorpblog.com/category/cool-stuff/" title="View all posts filed under Cool Stuff">Cool Stuff</a>
</li>
	<li class="cat-item cat-item-312"><a href="http://ycorpblog.com/category/developer/" title="View all posts filed under Developer">Developer</a>
</li>
	<li class="cat-item cat-item-31"><a href="http://ycorpblog.com/category/flickr/" title="View all posts filed under Flickr">Flickr</a>
</li>
	<li class="cat-item cat-item-8"><a href="http://ycorpblog.com/category/general/" title="View all posts filed under General">General</a>
</li>
	<li class="cat-item cat-item-209 current-cat"><a href="http://ycorpblog.com/category/greatest-hits/" title="View all posts filed under Greatest Hits">Greatest Hits</a>
</li>
	<li class="cat-item cat-item-3"><a href="http://ycorpblog.com/category/guest-opinions/" title="View all posts filed under Guest Opinions">Guest Opinions</a>
</li>
	<li class="cat-item cat-item-1188"><a href="http://ycorpblog.com/category/how-good-grows/" title="View all posts filed under How Good Grows">How Good Grows</a>
</li>
	<li class="cat-item cat-item-119"><a href="http://ycorpblog.com/category/human-rights/" title="View all posts filed under human rights">human rights</a>
</li>
	<li class="cat-item cat-item-584"><a href="http://ycorpblog.com/category/ipad/" title="View all posts filed under iPad">iPad</a>
</li>
	<li class="cat-item cat-item-122"><a href="http://ycorpblog.com/category/local/" title="View all posts filed under local">local</a>
</li>
	<li class="cat-item cat-item-140"><a href="http://ycorpblog.com/category/mail/" title="View all posts filed under mail">mail</a>
</li>
	<li class="cat-item cat-item-185"><a href="http://ycorpblog.com/category/marketing/" title="View all posts filed under marketing">marketing</a>
</li>
	<li class="cat-item cat-item-42"><a href="http://ycorpblog.com/category/messenger/" title="View all posts filed under messenger">messenger</a>
</li>
	<li class="cat-item cat-item-82"><a href="http://ycorpblog.com/category/mobile/" title="View all posts filed under mobile">mobile</a>
</li>
	<li class="cat-item cat-item-53"><a href="http://ycorpblog.com/category/news/" title="View all posts filed under news">news</a>
</li>
	<li class="cat-item cat-item-156"><a href="http://ycorpblog.com/category/olympics/" title="View all posts filed under olympics">olympics</a>
</li>
	<li class="cat-item cat-item-154"><a href="http://ycorpblog.com/category/online-safety/" title="View all posts filed under online safety">online safety</a>
</li>
	<li class="cat-item cat-item-5"><a href="http://ycorpblog.com/category/our-users/" title="View all posts filed under Our Users">Our Users</a>
</li>
	<li class="cat-item cat-item-366"><a href="http://ycorpblog.com/category/partnership/" title="View all posts filed under Partnership">Partnership</a>
</li>
	<li class="cat-item cat-item-11"><a href="http://ycorpblog.com/category/photo-essay/" title="View all posts filed under Photo Essay">Photo Essay</a>
</li>
	<li class="cat-item cat-item-138"><a href="http://ycorpblog.com/category/poll/" title="View all posts filed under Poll">Poll</a>
</li>
	<li class="cat-item cat-item-165"><a href="http://ycorpblog.com/category/privacy/" title="View all posts filed under privacy">privacy</a>
</li>
	<li class="cat-item cat-item-13"><a href="http://ycorpblog.com/category/product-pulse/" title="View all posts filed under Product Pulse">Product Pulse</a>
</li>
	<li class="cat-item cat-item-141"><a href="http://ycorpblog.com/category/public-policy/" title="View all posts filed under public policy">public policy</a>
</li>
	<li class="cat-item cat-item-345"><a href="http://ycorpblog.com/category/right-media/" title="View all posts filed under right media">right media</a>
</li>
	<li class="cat-item cat-item-133"><a href="http://ycorpblog.com/category/search/" title="View all posts filed under search">search</a>
</li>
	<li class="cat-item cat-item-1319"><a href="http://ycorpblog.com/category/shine-2/" title="View all posts filed under Shine">Shine</a>
</li>
	<li class="cat-item cat-item-196"><a href="http://ycorpblog.com/category/social/" title="View all posts filed under social">social</a>
</li>
	<li class="cat-item cat-item-470"><a href="http://ycorpblog.com/category/sports/" title="View all posts filed under Sports">Sports</a>
</li>
	<li class="cat-item cat-item-14"><a href="http://ycorpblog.com/category/those-crazy-yahoos/" title="View all posts filed under Those Crazy Yahoos">Those Crazy Yahoos</a>
</li>
	<li class="cat-item cat-item-146"><a href="http://ycorpblog.com/category/tips/" title="View all posts filed under tips">tips</a>
</li>
	<li class="cat-item cat-item-4"><a href="http://ycorpblog.com/category/trends-news/" title="View all posts filed under Trends &amp; News">Trends &amp; News</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://ycorpblog.com/category/uncategorized/" title="View all posts filed under Uncategorized">Uncategorized</a>
</li>
	<li class="cat-item cat-item-20"><a href="http://ycorpblog.com/category/video/" title="View all posts filed under Video">Video</a>
</li>
	<li class="cat-item cat-item-305"><a href="http://ycorpblog.com/category/women-in-tech/" title="View all posts filed under Women in Tech">Women in Tech</a>
</li>
	<li class="cat-item cat-item-6"><a href="http://ycorpblog.com/category/working-at-yahoo/" title="View all posts filed under Working at Yahoo!">Working at Yahoo!</a>
</li>
	<li class="cat-item cat-item-705"><a href="http://ycorpblog.com/category/world-cup/" title="View all posts filed under world cup">world cup</a>
</li>
	<li class="cat-item cat-item-1104"><a href="http://ycorpblog.com/category/yahoo-contributor-network/" title="View all posts filed under Yahoo! Contributor Network">Yahoo! Contributor Network</a>
</li>
	<li class="cat-item cat-item-449"><a href="http://ycorpblog.com/category/yahoo-cycling-team/" title="View all posts filed under Yahoo! Cycling Team">Yahoo! Cycling Team</a>
</li>
	<li class="cat-item cat-item-267"><a href="http://ycorpblog.com/category/yahoo-employee-foundation/" title="View all posts filed under yahoo! employee foundation">yahoo! employee foundation</a>
</li>
	<li class="cat-item cat-item-1304"><a href="http://ycorpblog.com/category/yahoo-finance-2/" title="View all posts filed under Yahoo! Finance">Yahoo! Finance</a>
</li>
	<li class="cat-item cat-item-12"><a href="http://ycorpblog.com/category/yahoo-for-good/" title="View all posts filed under Yahoo! For Good">Yahoo! For Good</a>
</li>
	<li class="cat-item cat-item-40"><a href="http://ycorpblog.com/category/yahoo-green/" title="View all posts filed under yahoo! green">yahoo! green</a>
</li>
	<li class="cat-item cat-item-484"><a href="http://ycorpblog.com/category/yahoo-in-asia/" title="View all posts filed under Yahoo! in Asia">Yahoo! in Asia</a>
</li>
	<li class="cat-item cat-item-1228"><a href="http://ycorpblog.com/category/yahoo-kimo/" title="View all posts filed under Yahoo! Kimo">Yahoo! Kimo</a>
</li>
	<li class="cat-item cat-item-253"><a href="http://ycorpblog.com/category/yahoo-labs/" title="View all posts filed under yahoo! labs">yahoo! labs</a>
</li>
	<li class="cat-item cat-item-1110"><a href="http://ycorpblog.com/category/yahoo-leadership/" title="View all posts filed under Yahoo! Leadership">Yahoo! Leadership</a>
</li>
	<li class="cat-item cat-item-50"><a href="http://ycorpblog.com/category/yahoo-movies/" title="View all posts filed under yahoo! movies">yahoo! movies</a>
</li>
	<li class="cat-item cat-item-54"><a href="http://ycorpblog.com/category/yahoo-music/" title="View all posts filed under yahoo! music">yahoo! music</a>
</li>
	<li class="cat-item cat-item-24"><a href="http://ycorpblog.com/category/yahoo-news/" title="View all posts filed under yahoo! news">yahoo! news</a>
</li>
	<li class="cat-item cat-item-822"><a href="http://ycorpblog.com/category/yahoo-news-bytes/" title="View all posts filed under Yahoo! News Bytes">Yahoo! News Bytes</a>
</li>
	<li class="cat-item cat-item-2"><a href="http://ycorpblog.com/category/yahoo-opinions/" title="View all posts filed under Yahoo! Opinions">Yahoo! Opinions</a>
</li>
	<li class="cat-item cat-item-58"><a href="http://ycorpblog.com/category/yahoo-small-business/" title="View all posts filed under yahoo! small business">yahoo! small business</a>
</li>
	<li class="cat-item cat-item-103"><a href="http://ycorpblog.com/category/yahoo-sports/" title="View all posts filed under yahoo! sports">yahoo! sports</a>
</li>
	<li class="cat-item cat-item-203"><a href="http://ycorpblog.com/category/yahoo-tv/" title="View all posts filed under yahoo! tv">yahoo! tv</a>
</li>
	<li class="cat-item cat-item-205"><a href="http://ycorpblog.com/category/year-in-review/" title="View all posts filed under year in review">year in review</a>
</li>
	<li class="cat-item cat-item-383"><a href="http://ycorpblog.com/category/you-in/" title="View all posts filed under You in?">You in?</a>
</li>
					</ul>
					<p>Archives:</p>
					<ul>
							<li><a href='http://ycorpblog.com/2011/04/' title='April 2011'>April 2011</a></li>
	<li><a href='http://ycorpblog.com/2011/03/' title='March 2011'>March 2011</a></li>
	<li><a href='http://ycorpblog.com/2011/02/' title='February 2011'>February 2011</a></li>
	<li><a href='http://ycorpblog.com/2011/01/' title='January 2011'>January 2011</a></li>
	<li><a href='http://ycorpblog.com/2010/12/' title='December 2010'>December 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/11/' title='November 2010'>November 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/10/' title='October 2010'>October 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/09/' title='September 2010'>September 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/08/' title='August 2010'>August 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/07/' title='July 2010'>July 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/06/' title='June 2010'>June 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/05/' title='May 2010'>May 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/04/' title='April 2010'>April 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/03/' title='March 2010'>March 2010</a></li>
	<li><a href='http://ycorpblog.com/2010/02/' title='February 2010'>February 2010</a></li>
						<li><a href="/archives/">More Archives</a></li>				
					</ul>
				</div>
				<div class="blogs">			
					<li id="linkcat-18" class="linkcat"><p>Other Yahoo! Blogs:</p>
	<ul>
<li><nobr><nobr><a href="http://blog.del.icio.us/">del.icio.us Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://blog.flickr.com/">FlickrBlog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://myyblog.com/">My Yahoo! Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://mybloglogb.typepad.com/my_weblog/" title="MyBlogLog Blog">MyBlogLog Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://blog.rightmedia.com/">Right Media Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://dir.yahoo.com/thespark">The Spark Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://upcoming.org/news/">Upcoming.org News</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.yadvertisingblog.com/">Yahoo! Advertising Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.yanswersblog.com/">Yahoo! Answers Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://buzz.yahoo.com/buzz_log/">Yahoo! Buzz Log</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.yctvblog.com/">Yahoo! Connected TV Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://developer.yahoo.net/blog/">Yahoo! Developer Network Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://yfinanceblog.com">Yahoo! Finance Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.ygroupsblog.com/blog">Yahoo! Groups Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.yhumanrightsblog.com/blog/rights-blog/">Yahoo! Human Rights Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://id.yseablog.com/">Yahoo! Indonesia Yodel</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://ymailupdates.com/blog/">Yahoo! Mail Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://blog.messenger.yahoo.com/">Yahoo! Messenger Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://ymobileblog.com/">Yahoo! Mobile Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://ymusicblog.com/">Yahoo! Music Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.ypolicyblog.com/">Yahoo! Policy Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.yprofileblog.com">Yahoo! Profiles News</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.ypnblog.com/">Yahoo! Publisher Network Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://ysearchblog.com/">Yahoo! Search Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.ysmblog.com/">Yahoo! Search Marketing Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://shopping.yahoo.com/blog">Yahoo! Shopping Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://ystoreblog.com/">Yahoo! Store Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://yuiblog.com">Yahoo! User Interface Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.yvideoblog.com/">Yahoo! Video Blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.wretch.cc/blog/ycorpblog" title="Yahoo!Kimo in Taiwan blogs about the exciting activities in the market">Yahoo!Kimo (Taiwan) blog</nobr></a></nobr></li>
<li><nobr><nobr><a href="http://www.zimbrablog.com">Zimbra Blog</nobr></a></nobr></li>

	</ul>
</li>
				</div>
				<div class="clr"></div>
			</div>
			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/links_bottom.gif" alt=""/>

			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/readers_top.gif" alt="" class="readers_top"/>
			<div class="readers">
				<p><span>Recent Readers:</span> Provided by <a href="http://www.mybloglog.com">MyBlogLog</a></p>
				<script type="text/javascript" src="http://pub.mybloglog.com/comm2.php?mblID=2006111715525625&amp;c_sn_opt=n&amp;c_rows=3&amp;c_img_size=f&amp;c_heading_text=&amp;c_color_heading_bg=FFFFFF&amp;c_color_heading=FFFFFF&amp;c_color_link_bg=FFFFFF&amp;c_color_link=FFFFFF&amp;c_color_bottom_bg=FFFFFF"></script>			
			</div>
			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/readers_bottom.gif" alt="" class="moveup"/>

			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/bluebox_top.gif" alt="" class="bluebox_top"/>
			<div class="bluebox">
				<h1>About Yodel Anecdotal</h1>
				<p>A look inside the big purple house of Yahoo!, where we'll provide insights into our company, our people, our culture, and the things we think about in the shower. <a href="/about/">Learn more</a>.</p>
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/divider_bluebox.gif" class="divide" alt=""/>
				
				<h1>Write to Us</h1>
				<p>Have a great story to tell about how you've used Yahoo!? Or have a story you'd like us to tell? <a href="/about/write-to-us">Drop us a line</a>.</p>
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/divider_bluebox.gif" class="divide" alt=""/>
				
				<h1>Comment Policy</h1>
				<p>Give us your $.02. We encourage your comments, quibbles, questions, and suggestions. But please mind your manners. You know the drill... stay on topic, be respectful, and avoid spam, profanity, or anything that violates our <a href="http://docs.yahoo.com/info/terms/">Terms of Service</a>.<br/>
				<a href="/about/comment-policy/">Learn more about our comment policy</a>.</p>
				<img src="http://ycorpblog.com/wp-content/themes/yodel/images/divider_bluebox.gif" class="divide" alt=""/>

				<h1>Shameless Self-Promotion</h1>
				<p><a href="http://yhoo.client.shareholder.com/press/releases.cfm">The Latest News From Yahoo!</a><br/>
				<a href="http://docs.yahoo.com/info/">Company Info</a><br/>
				<a href="http://careers.yahoo.com/">Become a Yahoo</a><br/>
				<a href="http://brand.yahoo.com/forgood">Yahoo! For Good</a><br/>
				<a href="http://docs.yahoo.com/docs/family/more/">All Yahoo! Services</a></p>
			</div>
			<img src="http://ycorpblog.com/wp-content/themes/yodel/images/bluebox_bottom.gif" alt=""/>
		</div>
		<div class="clr"></div>
		<img src="http://ycorpblog.com/wp-content/themes/yodel/images/orange_bottom.gif" alt="" class="orange_bottom"/>
	</div>
</div>
<!--<script type="text/javascript" src="/wp-includes/js/readers.js"></script>-->

<!-- Yahoo! Web Analytics - All rights reserved --> 
<script type="text/javascript" src="http://d.yimg.com/mi/ywa.js">
</script>
<script type="text/javascript">
var YWATracker = YWA.getTracker("1000197113212"); 
YWATracker.submit();
</script>
<noscript> <div><img src="http://a.analytics.yahoo.com/p.pl?a=1000197113212&amp;js=no"
width="1" height="1" alt="" /></div> </noscript>
<!-- End of Yahoo! Web Analytics Code -->

</body>
</html>

<!-- Dynamic Page Served (once) in 0.958 seconds -->
<!-- Cached page served by WP-Cache -->
