<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">


<head>

	<title>Yahoo! Search Marketing Blog</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <script type="text/javascript" charset="utf-8" src="http://bit.ly/javascript-api.js?version=latest&login=mtyahooayc&apiKey=R_644832330f376c2d26012eeea4e2d5f6"></script>

	<link rel="stylesheet" href="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/style.css" type="text/css" media="screen, projection" />

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.ysmblog.com/blog/feed/" />

	<link rel="alternate" type="text/xml" title="RSS .92" href="http://www.ysmblog.com/blog/feed/rss/" />

	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="http://www.ysmblog.com/blog/feed/atom/" />

	<link rel="pingback" href="http://www.ysmblog.com/blog/xmlrpc.php" />

	<link rel="shortcut icon" href="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/favicon.ico" />


	
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.ysmblog.com/blog/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.ysmblog.com/blog/wp-includes/wlwmanifest.xml" /> 
<link rel='index' title='Yahoo! Search Marketing Blog' href='http://www.ysmblog.com/blog' />
<meta name="generator" content="WordPress 2.8.4" />

	<script language="javascript" src="http://sm.feeds.yahoo.com/Buttons/V2.0/yactions.js"></script>

<script type="text/javascript" src="http://ysmblog.com/scripts/swfobject.js"></script>

<script type="text/javascript" src="http://ysmblog.com/scripts/ywa_yep.js"></script>





</head>



<body>

<DIV class="container">

  <DIV align="right" style="padding: 10px;" class="sm"><A href="http://searchmarketing.yahoo.com/">Yahoo! Search Marketing Home</A></DIV>



<div style="padding: 0; margin: 0; height: 200px;">

<div style="float: left; padding: 0; margin: 0;"><a href="/"><img src="http://l.yimg.com/us.yimg.com/i/us/ysm/cn/blg/ysmblog.jpg" alt="Yahoo! Search Marketing Blog" width="411" height="200" border="0"></a></div>

<div style="float: right; padding: 0; margin: 0;"><script language="JavaScript">

<!--

function random_imglink(){

var myimages=new Array()

myimages[1]="http://l.yimg.com/us.yimg.com/i/us/ysm/cn/blg/ysmblog-photo-1.jpg"

myimages[2]="http://l.yimg.com/us.yimg.com/i/us/ysm/cn/blg/ysmblog-photo-2.jpg"

myimages[3]="http://l.yimg.com/us.yimg.com/i/us/ysm/cn/blg/ysmblog-photo-3.jpg"

myimages[4]="http://l.yimg.com/us.yimg.com/i/us/ysm/cn/blg/ysmblog-photo-4.jpg"

myimages[5]="http://l.yimg.com/us.yimg.com/i/us/ysm/cn/blg/ysmblog-photo-5.jpg"

myimages[6]="http://l.yimg.com/us.yimg.com/i/us/ysm/cn/blg/ysmblog-photo-6.jpg"

myimages[7]="http://l.yimg.com/us.yimg.com/i/us/ysm/cn/blg/ysmblog-photo-7.jpg"



var ry=Math.floor(Math.random()*myimages.length)

if (ry==0)

ry=1

document.write('<img src="'+myimages[ry]+'" width=338 height=200 border=0>')

}

random_imglink()

//-->

</script></div>

</div>



  <UL class="nav">

    <LI  class="on"><A href="/">Blog Home</A></LI>

    <LI ><A href="/blog/archives/">Archives</A></LI>

    <LI><A href="/blog/about/">About the Blog</A></LI>





  </UL>

  <TABLE cellpadding="0" cellspacing="0">

    

    <TR>

    <TD class="content">


	
		
		<DIV class="sm">January 3rd, 2011</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2011/01/03/adcenter-transition-tool-closing-january-5/" title="Permalink">adCenter Transition Tool Closing January 5</a></h1>

		<h3>If you haven’t already, transition your Yahoo! Search Marketing account before it’s too late</h3>
<p>While most Yahoo! Search Marketing advertisers moved their accounts to Microsoft adCenter before <a title="SA Transition completed" href="http://www.yadvertisingblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed/" target="_blank">the ad serving transition was completed in October</a>, there are still a small number who have not. If you are part of this group, please take note: The transition tool inside your YSM account will <em><strong>permanently close on January 5, 2011</strong></em>. Accounts that are not transitioned by January 5 can only be manually transitioned by exporting and importing campaigns into the adCenter UI.</p>
<p>So, how can you check to make sure you transitioned? If you have an “adCenter” tab with a yellow “Get Started” button in your account after you <a title="YSM log-in" href="https://marketingsolutions.login.yahoo.com/adui/signin/displaySignin.do" target="_blank">log in</a>, you did not transition that account.</p>
<p> <a href="http://www.ysmblog.com/blog/2011/01/03/adcenter-transition-tool-closing-january-5/#more-2049" class="more-link">(more&#8230;)</a></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2011/01/03/adcenter-transition-tool-closing-january-5/#comments" class="commentlink"  title="Comment on adCenter Transition Tool Closing January 5">16 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/products/" title="View all posts in Products" rel="category tag">Products</a>,  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2011/01/03/adcenter-transition-tool-closing-january-5/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=adCenter+Transition+Tool+Closing+January+5+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2011/01/03/adcenter-transition-tool-closing-january-5/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2011%2F01%2F03%2Fadcenter-transition-tool-closing-january-5%2F&t=adCenter+Transition+Tool+Closing+January+5" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2011%2F01%2F03%2Fadcenter-transition-tool-closing-january-5%2F&title=adCenter Transition Tool Closing January 5&summary=<p>If you haven’t already, transition your Yahoo! Search Marketing account before it’s too late<br />
While most Yahoo! Search Marketing advertisers moved their accounts to Microsoft adCenter before the ad serving transition was completed in October, there are still a small number who have not. If you are part of this group, please take note: The transition [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2011%2F01%2F03%2Fadcenter-transition-tool-closing-january-5%2F&title=adCenter+Transition+Tool+Closing+January+5" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2011%2F01%2F03%2Fadcenter-transition-tool-closing-january-5%2F&title=adCenter+Transition+Tool+Closing+January+5" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=adCenter+Transition+Tool+Closing+January+5+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2011%2F01%2F03%2Fadcenter-transition-tool-closing-january-5%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2011%2F01%2F03%2Fadcenter-transition-tool-closing-january-5%2F&title=adCenter+Transition+Tool+Closing+January+5" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2011%2F01%2F03%2Fadcenter-transition-tool-closing-january-5%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2011%2F01%2F03%2Fadcenter-transition-tool-closing-january-5%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2011/01/03/adcenter-transition-tool-closing-january-5/"
    dc:identifier="http://www.ysmblog.com/blog/2011/01/03/adcenter-transition-tool-closing-january-5/"
    dc:title="adCenter Transition Tool Closing January 5"
    trackback:ping="http://www.ysmblog.com/blog/2011/01/03/adcenter-transition-tool-closing-january-5/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 27th, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/27/mission-accomplished-2/" title="Permalink">Mission Accomplished</a></h1>

		<h3>While the transition is done, search and the future of the search alliance is a critical part of Yahoo!’s strategy</h3>
<p>Yahoo! is pleased to announce that we’ve completed the process of moving search ad serving for Yahoo! to the Microsoft Advertising adCenter platform in the U.S. and Canada. With this milestone achieved, the transition of advertisers’ Yahoo! Search Marketing accounts to adCenter is also complete.</p>
<p>The transition comes after months of testing and collaboration with Microsoft, with strict attention being paid to protecting the quality of the experience for you prior to the holiday season. The employees of both Yahoo! and Microsoft have been, and continue to be, 100% committed to supporting you through this period.</p>
<p><img title="More..." src="http://www.yadvertisingblog.com/blog/wp-includes/js/tinymce/plugins/wordpress/img/trans.gif" alt="" />To be sure, search and the future of the search alliance is a critical part of Yahoo!’s strategy. Yahoo! also remains focused on delivering unique consumer experiences&#8212;such as <a title="New search features" href="http://www.yadvertisingblog.com/blog/2010/10/07/discover-more-with-new-yahoo-search-experiences/" target="_blank">the ones we launched a few weeks ago</a>&#8212;and on increasing user engagement in our owned-and-operated search products to help drive high-quality traffic into the combined marketplace.</p>
<p> <a href="http://www.ysmblog.com/blog/2010/10/27/mission-accomplished-2/#more-2046" class="more-link">(more&#8230;)</a></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/27/mission-accomplished-2/#comments" class="commentlink"  title="Comment on Mission Accomplished">6 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/guestposts/" title="View all posts in Guest Posts" rel="category tag">Guest Posts</a>,  <a href="http://www.ysmblog.com/blog/category/search/" title="View all posts in Search" rel="category tag">Search</a>,  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a>,  <a href="http://www.ysmblog.com/blog/category/yahoonews/" title="View all posts in Yahoo News" rel="category tag">Yahoo News</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/27/mission-accomplished-2/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=Mission+Accomplished+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/27/mission-accomplished-2/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fmission-accomplished-2%2F&t=Mission+Accomplished" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fmission-accomplished-2%2F&title=Mission Accomplished&summary=<p>While the transition is done, search and the future of the search alliance is a critical part of Yahoo!’s strategy<br />
Yahoo! is pleased to announce that we’ve completed the process of moving search ad serving for Yahoo! to the Microsoft Advertising adCenter platform in the U.S. and Canada. With this milestone achieved, the transition of advertisers’ [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fmission-accomplished-2%2F&title=Mission+Accomplished" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fmission-accomplished-2%2F&title=Mission+Accomplished" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Mission+Accomplished+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fmission-accomplished-2%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fmission-accomplished-2%2F&title=Mission+Accomplished" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fmission-accomplished-2%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fmission-accomplished-2%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/27/mission-accomplished-2/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/27/mission-accomplished-2/"
    dc:title="Mission Accomplished"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/27/mission-accomplished-2/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 27th, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed-2/" title="Permalink">Search Alliance Transitions have been Completed</a></h1>

		<h3>Your adCenter account is the only one that you will now need to manage your search advertising on Yahoo! Search and Bing</h3>
<p>Today is a momentous day for us, as Yahoo! has completed the process of moving search ad serving for Yahoo! to the Microsoft Advertising adCenter platform in the U.S. and Canada. Additionally, this marks the completion of the transition of advertisers’ Yahoo! Search Marketing accounts to adCenter.</p>
<p>adCenter advertisers now have access to 163 million unique searchers and 33.8% of U.S. search queries (source: comScore Core Search (custom) August 2010) on Yahoo! Search, Bing and partner sites, with the convenience of one account.</p>
<p>Here are some important points for our advertisers to remember:</p>
<h3><strong> <a href="http://www.ysmblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed-2/#more-2044" class="more-link">(more&#8230;)</a></strong></h3>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed-2/#comments" class="commentlink"  title="Comment on Search Alliance Transitions have been Completed">4 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/search/" title="View all posts in Search" rel="category tag">Search</a>,  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a>,  <a href="http://www.ysmblog.com/blog/category/yahoonews/" title="View all posts in Yahoo News" rel="category tag">Yahoo News</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed-2/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=Search+Alliance+Transitions+have+been+Completed+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed-2/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fsearch-alliance-transitions-have-been-completed-2%2F&t=Search+Alliance+Transitions+have+been+Completed" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fsearch-alliance-transitions-have-been-completed-2%2F&title=Search Alliance Transitions have been Completed&summary=<p>Your adCenter account is the only one that you will now need to manage your search advertising on Yahoo! Search and Bing<br />
Today is a momentous day for us, as Yahoo! has completed the process of moving search ad serving for Yahoo! to the Microsoft Advertising adCenter platform in the U.S. and Canada. Additionally, this marks [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fsearch-alliance-transitions-have-been-completed-2%2F&title=Search+Alliance+Transitions+have+been+Completed" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fsearch-alliance-transitions-have-been-completed-2%2F&title=Search+Alliance+Transitions+have+been+Completed" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Search+Alliance+Transitions+have+been+Completed+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fsearch-alliance-transitions-have-been-completed-2%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fsearch-alliance-transitions-have-been-completed-2%2F&title=Search+Alliance+Transitions+have+been+Completed" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fsearch-alliance-transitions-have-been-completed-2%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F27%2Fsearch-alliance-transitions-have-been-completed-2%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed-2/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed-2/"
    dc:title="Search Alliance Transitions have been Completed"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/27/search-alliance-transitions-have-been-completed-2/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 25th, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/25/ysmadcenter-feature-comparison-6-match-types-and-negative-keywords/" title="Permalink">YSM/adCenter Feature Comparison #6: Match Types and Negative Keywords</a></h1>

		<h3>The match types and negative keywords you choose can help improve performance of your campaigns in adCenter</h3>
<p><em>This is the latest in a series of articles that explains how Microsoft Advertising adCenter differs from Yahoo! Search Marketing. They’re intended to help Yahoo! advertisers prepare for the transition and become familiar with adCenter.</em></p>
<p>A “match type” refers to how your keyword is matched to a user’s search query. By adjusting the match types you use when bidding on keywords, you can potentially increase your traffic and click-through-rates (CTR) and decrease your cost-per-click (CPC).</p>
<p>While both adCenter and Yahoo! allow you to bid on different match types, there are several differences in the available match types and the levels at which you can bid on them. In Yahoo! Search Marketing, the two available match types have been Standard and Advanced. adCenter offers three <a title="Match type options" href="http://community.microsoftadvertising.com/blogs/advertiser/archive/2010/01/26/sem-beginner-series-the-relationship-between-keyword-match-types-and-search-queries.aspx" target="_blank">match type options</a>: broad, phrase and exact.</p>
<p><img title="More..." src="http://www.yadvertisingblog.com/blog/wp-includes/js/tinymce/plugins/wordpress/img/trans.gif" alt="" /> <a href="http://www.ysmblog.com/blog/2010/10/25/ysmadcenter-feature-comparison-6-match-types-and-negative-keywords/#more-2028" class="more-link">(more&#8230;)</a></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/25/ysmadcenter-feature-comparison-6-match-types-and-negative-keywords/#comments" class="commentlink"  title="Comment on YSM/adCenter Feature Comparison #6: Match Types and Negative Keywords">2 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/25/ysmadcenter-feature-comparison-6-match-types-and-negative-keywords/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=YSM%2FadCenter+Feature+Comparison+%236%3A+Match+Types+and+Negative+Keywords+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/25/ysmadcenter-feature-comparison-6-match-types-and-negative-keywords/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F25%2Fysmadcenter-feature-comparison-6-match-types-and-negative-keywords%2F&t=YSM%2FadCenter+Feature+Comparison+%236%3A+Match+Types+and+Negative+Keywords" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F25%2Fysmadcenter-feature-comparison-6-match-types-and-negative-keywords%2F&title=YSM/adCenter Feature Comparison #6: Match Types and Negative Keywords&summary=<p>The match types and negative keywords you choose can help improve performance of your campaigns in adCenter<br />
This is the latest in a series of articles that explains how Microsoft Advertising adCenter differs from Yahoo! Search Marketing. They’re intended to help Yahoo! advertisers prepare for the transition and become familiar with adCenter.<br />
A “match type” refers to [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F25%2Fysmadcenter-feature-comparison-6-match-types-and-negative-keywords%2F&title=YSM%2FadCenter+Feature+Comparison+%236%3A+Match+Types+and+Negative+Keywords" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F25%2Fysmadcenter-feature-comparison-6-match-types-and-negative-keywords%2F&title=YSM%2FadCenter+Feature+Comparison+%236%3A+Match+Types+and+Negative+Keywords" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=YSM%2FadCenter+Feature+Comparison+%236%3A+Match+Types+and+Negative+Keywords+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F25%2Fysmadcenter-feature-comparison-6-match-types-and-negative-keywords%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F25%2Fysmadcenter-feature-comparison-6-match-types-and-negative-keywords%2F&title=YSM%2FadCenter+Feature+Comparison+%236%3A+Match+Types+and+Negative+Keywords" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F25%2Fysmadcenter-feature-comparison-6-match-types-and-negative-keywords%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F25%2Fysmadcenter-feature-comparison-6-match-types-and-negative-keywords%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/25/ysmadcenter-feature-comparison-6-match-types-and-negative-keywords/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/25/ysmadcenter-feature-comparison-6-match-types-and-negative-keywords/"
    dc:title="YSM/adCenter Feature Comparison #6: Match Types and Negative Keywords"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/25/ysmadcenter-feature-comparison-6-match-types-and-negative-keywords/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 21st, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/21/ysmadcenter-feature-comparison-5-budgeting-bidding-and-billing/" title="Permalink">YSM/adCenter Feature Comparison #5: Budgeting, Bidding and Billing</a></h1>

		<h3>Everything you need to know about how your ad dollars are allocated and spent on adCenter</h3>
<p><em>This is the latest in a series of articles that explains how Microsoft Advertising adCenter differs from Yahoo! Search Marketing. They’re intended to help Yahoo! advertisers prepare for the upcoming transition and become familiar with adCenter.</em></p>
<p>By the end of this month, you will be able to advertise on Yahoo! Search, Bing and our partner sites with one account in Microsoft Advertising adCenter. So now is the perfect time to learn how budgeting, bidding and billing work in adCenter, and how they differ from what you’re used to with Yahoo! Search Marketing. This will help you to maximize performance for both engines and take advantage of the increased traffic during and after the transition period.</p>
<p><strong>Budgeting in Microsoft adCenter</strong></p>
<p>Microsoft adCenter requires that you set a monthly budget at the campaign level, whereas Yahoo! Search Marketing allows you to set your daily spending limit at the account and/or campaign levels. When a Yahoo! campaign is transferred into adCenter, the Yahoo! daily spend limit will be multiplied by 30 to create a monthly budget in adCenter, then divided evenly across your campaigns.</p>
<p>As you know, during this period when ad serving is transitioning, your traffic volume on adCenter is likely increasing while your Yahoo! Search Marketing traffic is decreasing. This means that you’ll need to closely monitor your budgets in both platforms during the next week, and make adjustments as needed.</p>
<p><img title="More..." src="http://www.yadvertisingblog.com/blog/wp-includes/js/tinymce/plugins/wordpress/img/trans.gif" alt="" /> <a href="http://www.ysmblog.com/blog/2010/10/21/ysmadcenter-feature-comparison-5-budgeting-bidding-and-billing/#more-2024" class="more-link">(more&#8230;)</a></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/21/ysmadcenter-feature-comparison-5-budgeting-bidding-and-billing/#comments" class="commentlink"  title="Comment on YSM/adCenter Feature Comparison #5: Budgeting, Bidding and Billing">2 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/21/ysmadcenter-feature-comparison-5-budgeting-bidding-and-billing/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=YSM%2FadCenter+Feature+Comparison+%235%3A+Budgeting%2C+Bidding+and+Billing+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/21/ysmadcenter-feature-comparison-5-budgeting-bidding-and-billing/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F21%2Fysmadcenter-feature-comparison-5-budgeting-bidding-and-billing%2F&t=YSM%2FadCenter+Feature+Comparison+%235%3A+Budgeting%2C+Bidding+and+Billing" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F21%2Fysmadcenter-feature-comparison-5-budgeting-bidding-and-billing%2F&title=YSM/adCenter Feature Comparison #5: Budgeting, Bidding and Billing&summary=<p>Everything you need to know about how your ad dollars are allocated and spent on adCenter<br />
This is the latest in a series of articles that explains how Microsoft Advertising adCenter differs from Yahoo! Search Marketing. They’re intended to help Yahoo! advertisers prepare for the upcoming transition and become familiar with adCenter.<br />
By the end of this [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F21%2Fysmadcenter-feature-comparison-5-budgeting-bidding-and-billing%2F&title=YSM%2FadCenter+Feature+Comparison+%235%3A+Budgeting%2C+Bidding+and+Billing" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F21%2Fysmadcenter-feature-comparison-5-budgeting-bidding-and-billing%2F&title=YSM%2FadCenter+Feature+Comparison+%235%3A+Budgeting%2C+Bidding+and+Billing" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=YSM%2FadCenter+Feature+Comparison+%235%3A+Budgeting%2C+Bidding+and+Billing+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F21%2Fysmadcenter-feature-comparison-5-budgeting-bidding-and-billing%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F21%2Fysmadcenter-feature-comparison-5-budgeting-bidding-and-billing%2F&title=YSM%2FadCenter+Feature+Comparison+%235%3A+Budgeting%2C+Bidding+and+Billing" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F21%2Fysmadcenter-feature-comparison-5-budgeting-bidding-and-billing%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F21%2Fysmadcenter-feature-comparison-5-budgeting-bidding-and-billing%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/21/ysmadcenter-feature-comparison-5-budgeting-bidding-and-billing/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/21/ysmadcenter-feature-comparison-5-budgeting-bidding-and-billing/"
    dc:title="YSM/adCenter Feature Comparison #5: Budgeting, Bidding and Billing"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/21/ysmadcenter-feature-comparison-5-budgeting-bidding-and-billing/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 20th, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/20/is-your-adcenter-account-ready/" title="Permalink">Is Your adCenter Account Ready?</a></h1>

		<h3>Ad serving is shifting to adCenter; transition your Yahoo! account immediately to avoid losing traffic</h3>
<p>The switch for Yahoo! Search marketing advertisers to the Microsoft Advertising adCenter platform is almost here. To help ensure that you’re prepared to make a smooth transition, please take a moment to review the information below and complete any steps that you’ve yet to take:</p>
<p><strong>Make sure your adCenter account is ready to go</strong></p>
<ul>
<li>We’ve now moved more than 30% of Yahoo! Search traffic to adCenter and we anticipate increasing this to 50-60% by the end of this week. Please be sure you have an active adCenter account in place now, so you don’t miss out on this valuable traffic.</li>
<li>By October 25, make sure that your adCenter account is funded with the full budget amount you’ll need to cover all of your potential traffic from both Yahoo! Search and Bing.</li>
<li>If you’re uncertain about how much to budget, review your overall search engine marketing spend across all search engines and consider allocating one-third of the total to adCenter, as Yahoo! and Microsoft sites combined receive 33.8%* of all search queries in the U.S. (Source: comScore Core Search (custom) August 2010)</li>
<li>Monitor your bids on adCenter frequently, because the continuing transition of Yahoo! advertisers could cause some initial pricing variability. While keeping your ROI in mind, be prepared to <a title="Adjust your bids" href="http://advertising.microsoft.com/learning-center/microsoft-transition-center/bidding-and-budgeting" target="_blank">adjust your bids</a> or <a title="Set incremental bids" href="http://adcenterhelp.microsoft.com/help.aspx?project=adcenter_live_std&amp;mkt=en-us&amp;querytype=keyword&amp;query=yekdi07" target="_blank">set incremental bids</a> if increased competition impacts your ads’ positions.</li>
<li>Be sure to take advantage of adCenter’s <a title="Enhanced features" href="http://www.yadvertisingblog.com/blog/2010/09/29/ysmadcenter-feature-comparison-2-distribution-controls" target="_blank">enhanced search network distribution and website exclusion features</a>, to help you manage your ROI on partner network traffic.</li>
</ul>
<p> <a href="http://www.ysmblog.com/blog/2010/10/20/is-your-adcenter-account-ready/#more-2020" class="more-link">(more&#8230;)</a></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/20/is-your-adcenter-account-ready/#comments" class="commentlink"  title="Comment on Is Your adCenter Account Ready?">7 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/20/is-your-adcenter-account-ready/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=Is+Your+adCenter+Account+Ready%3F+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/20/is-your-adcenter-account-ready/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F20%2Fis-your-adcenter-account-ready%2F&t=Is+Your+adCenter+Account+Ready%3F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F20%2Fis-your-adcenter-account-ready%2F&title=Is Your adCenter Account Ready?&summary=<p>Ad serving is shifting to adCenter; transition your Yahoo! account immediately to avoid losing traffic<br />
The switch for Yahoo! Search marketing advertisers to the Microsoft Advertising adCenter platform is almost here. To help ensure that you’re prepared to make a smooth transition, please take a moment to review the information below and complete any steps that [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F20%2Fis-your-adcenter-account-ready%2F&title=Is+Your+adCenter+Account+Ready%3F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F20%2Fis-your-adcenter-account-ready%2F&title=Is+Your+adCenter+Account+Ready%3F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Is+Your+adCenter+Account+Ready%3F+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F20%2Fis-your-adcenter-account-ready%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F20%2Fis-your-adcenter-account-ready%2F&title=Is+Your+adCenter+Account+Ready%3F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F20%2Fis-your-adcenter-account-ready%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F20%2Fis-your-adcenter-account-ready%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/20/is-your-adcenter-account-ready/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/20/is-your-adcenter-account-ready/"
    dc:title="Is Your adCenter Account Ready?"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/20/is-your-adcenter-account-ready/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 14th, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/14/ysmadcenter-feature-comparison-4-analytics/" title="Permalink">YSM/adCenter Feature Comparison #4: Analytics</a></h1>

		<p><strong>Recent enhancements to adCenter’s conversion-tracking capabilities will help you discover which ads work best</strong></p>
<p><em>This is the latest in a series of articles that explains how Microsoft Advertising adCenter differs from Yahoo! Search Marketing. They’re intended to help Yahoo! advertisers prepare for the upcoming transition and become familiar with adCenter.</em></p>
<p>One important component of paid search marketing is tracking conversions. This performance metric provides insight into the effectiveness of your paid search campaigns, and allows you to analyze and optimize for improved performance.</p>
<p>To help you prepare for the completion of the Yahoo! ad serving transition to adCenter, please take a moment to review some recent enhancements made to the conversion-tracking product in adCenter.</p>
<p><strong>Microsoft adCenter conversion tracking</strong></p>
<p>Microsoft adCenter previously offered a basic version of <a title="Conversion tracking" href="http://community.microsoftadvertising.com/blogs/advertiser/archive/2010/07/12/conversion-tracking-what-it-is-and-how-to-enable-and-disable-it-in-microsoft-adcenter.aspx" target="_blank">conversion tracking</a> with three different options for conversion counting: count one conversion per click, count one conversion per unique URL, or count all conversion events.</p>
<p><img title="More..." src="http://www.yadvertisingblog.com/blog/wp-includes/js/tinymce/plugins/wordpress/img/trans.gif" alt="" /> <a href="http://www.ysmblog.com/blog/2010/10/14/ysmadcenter-feature-comparison-4-analytics/#more-2016" class="more-link">(more&#8230;)</a></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/14/ysmadcenter-feature-comparison-4-analytics/#comments" class="commentlink"  title="Comment on YSM/adCenter Feature Comparison #4: Analytics">3 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/14/ysmadcenter-feature-comparison-4-analytics/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=YSM%2FadCenter+Feature+Comparison+%234%3A+Analytics+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/14/ysmadcenter-feature-comparison-4-analytics/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F14%2Fysmadcenter-feature-comparison-4-analytics%2F&t=YSM%2FadCenter+Feature+Comparison+%234%3A+Analytics" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F14%2Fysmadcenter-feature-comparison-4-analytics%2F&title=YSM/adCenter Feature Comparison #4: Analytics&summary=<p>Recent enhancements to adCenter’s conversion-tracking capabilities will help you discover which ads work best<br />
This is the latest in a series of articles that explains how Microsoft Advertising adCenter differs from Yahoo! Search Marketing. They’re intended to help Yahoo! advertisers prepare for the upcoming transition and become familiar with adCenter.<br />
One important component of paid search marketing [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F14%2Fysmadcenter-feature-comparison-4-analytics%2F&title=YSM%2FadCenter+Feature+Comparison+%234%3A+Analytics" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F14%2Fysmadcenter-feature-comparison-4-analytics%2F&title=YSM%2FadCenter+Feature+Comparison+%234%3A+Analytics" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=YSM%2FadCenter+Feature+Comparison+%234%3A+Analytics+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F14%2Fysmadcenter-feature-comparison-4-analytics%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F14%2Fysmadcenter-feature-comparison-4-analytics%2F&title=YSM%2FadCenter+Feature+Comparison+%234%3A+Analytics" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F14%2Fysmadcenter-feature-comparison-4-analytics%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F14%2Fysmadcenter-feature-comparison-4-analytics%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/14/ysmadcenter-feature-comparison-4-analytics/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/14/ysmadcenter-feature-comparison-4-analytics/"
    dc:title="YSM/adCenter Feature Comparison #4: Analytics"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/14/ysmadcenter-feature-comparison-4-analytics/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 13th, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/13/ad-serving-now-shifting-to-adcenter/" title="Permalink">Ad Serving Now Shifting to adCenter</a></h1>

		<p><strong>How your Yahoo! account balance will transfer, and final tips to help ensure a smooth transition</strong></p>
<p>This week marks an important milestone, as we have begun to shift ad serving for Yahoo! Search Marketing over to the Microsoft Advertising adCenter platform. This means that some of your traffic from Yahoo! is now coming through your adCenter account, rather than your Yahoo! Search Marketing account. The ad serving transition is expected to be completed by the end of October.</p>
<p>With the search transition in the home stretch, we want to make sure that you are well prepared to reach searchers on Yahoo! Search, Bing and our partner sites. We’ll also try to clarify some points about the transition process.</p>
<p> <a href="http://www.ysmblog.com/blog/2010/10/13/ad-serving-now-shifting-to-adcenter/#more-2005" class="more-link">(more&#8230;)</a></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/13/ad-serving-now-shifting-to-adcenter/#respond" class="commentlink"  title="Comment on Ad Serving Now Shifting to adCenter">Add comment</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/13/ad-serving-now-shifting-to-adcenter/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=Ad+Serving+Now+Shifting+to+adCenter+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/13/ad-serving-now-shifting-to-adcenter/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F13%2Fad-serving-now-shifting-to-adcenter%2F&t=Ad+Serving+Now+Shifting+to+adCenter" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F13%2Fad-serving-now-shifting-to-adcenter%2F&title=Ad Serving Now Shifting to adCenter&summary=<p>How your Yahoo! account balance will transfer, and final tips to help ensure a smooth transition<br />
This week marks an important milestone, as we have begun to shift ad serving for Yahoo! Search Marketing over to the Microsoft Advertising adCenter platform. This means that some of your traffic from Yahoo! is now coming through your adCenter [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F13%2Fad-serving-now-shifting-to-adcenter%2F&title=Ad+Serving+Now+Shifting+to+adCenter" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F13%2Fad-serving-now-shifting-to-adcenter%2F&title=Ad+Serving+Now+Shifting+to+adCenter" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Ad+Serving+Now+Shifting+to+adCenter+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F13%2Fad-serving-now-shifting-to-adcenter%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F13%2Fad-serving-now-shifting-to-adcenter%2F&title=Ad+Serving+Now+Shifting+to+adCenter" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F13%2Fad-serving-now-shifting-to-adcenter%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F13%2Fad-serving-now-shifting-to-adcenter%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/13/ad-serving-now-shifting-to-adcenter/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/13/ad-serving-now-shifting-to-adcenter/"
    dc:title="Ad Serving Now Shifting to adCenter"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/13/ad-serving-now-shifting-to-adcenter/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 12th, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/12/get-your-transition-questions-answered-3/" title="Permalink">Get Your Transition Questions Answered</a></h1>

		<h3>Attend a free online webinar October 14 to learn more about moving your account to adCenter</h3>
<p>We&#8217;ve begun the final phase of transitioning Yahoo! Search Marketing accounts to the Microsoft Advertising adCenter platform. YSM advertisers will want to make sure that they have an adCenter account in place&#8212;complete with campaigns and budget&#8212;no later than October 25; otherwise their ads will no longer be displayed on the Yahoo! Search network. If you haven&#8217;t already, <a title="Transitions Open" href="http://www.yadvertisingblog.com/blog/2010/08/31/advertisers-begin-your-account-transitions/" target="_blank">you should initiate your transitions now</a> in the Yahoo! account interface.</p>
<p>We realize that some advertisers may have uncertainty about taking this step, or just have questions they’d like answered. If you count yourself as part of that group, <a title="Webinar registration" href="https://swrt.worktankseattle.com/webcast/5629/preview.aspx" target="_blank">we invite you to attend a free webinar</a> this Thursday, October 14, during which Yahoo! and Microsoft experts will walk you through the transition tool and show you the simple steps you&#8217;ll need to take to help ensure a smooth account transition.</p>
<p> <a href="http://www.ysmblog.com/blog/2010/10/12/get-your-transition-questions-answered-3/#more-2000" class="more-link">(more&#8230;)</a></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/12/get-your-transition-questions-answered-3/#comments" class="commentlink"  title="Comment on Get Your Transition Questions Answered">5 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/12/get-your-transition-questions-answered-3/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=Get+Your+Transition+Questions+Answered+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/12/get-your-transition-questions-answered-3/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F12%2Fget-your-transition-questions-answered-3%2F&t=Get+Your+Transition+Questions+Answered" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F12%2Fget-your-transition-questions-answered-3%2F&title=Get Your Transition Questions Answered&summary=<p>Attend a free online webinar October 14 to learn more about moving your account to adCenter<br />
We&#8217;ve begun the final phase of transitioning Yahoo! Search Marketing accounts to the Microsoft Advertising adCenter platform. YSM advertisers will want to make sure that they have an adCenter account in place&#8212;complete with campaigns and budget&#8212;no later than October 25; otherwise their ads [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F12%2Fget-your-transition-questions-answered-3%2F&title=Get+Your+Transition+Questions+Answered" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F12%2Fget-your-transition-questions-answered-3%2F&title=Get+Your+Transition+Questions+Answered" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Get+Your+Transition+Questions+Answered+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F12%2Fget-your-transition-questions-answered-3%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F12%2Fget-your-transition-questions-answered-3%2F&title=Get+Your+Transition+Questions+Answered" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F12%2Fget-your-transition-questions-answered-3%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F12%2Fget-your-transition-questions-answered-3%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/12/get-your-transition-questions-answered-3/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/12/get-your-transition-questions-answered-3/"
    dc:title="Get Your Transition Questions Answered"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/12/get-your-transition-questions-answered-3/trackback/" />
</rdf:RDF>-->

	

	
		
		<DIV class="sm">October 8th, 2010</div>
		

		<h1 class="entry-title"><a href="http://www.ysmblog.com/blog/2010/10/08/yahoo-and-microsoft-continue-ad-testing-with-adcenter/" title="Permalink">Yahoo! and Microsoft Continue Ad Testing with adCenter</a></h1>

		<h3>Expect short-term fluctuations in traffic volume in both your YSM and adCenter accounts</h3>
<p>Yesterday, we <a title="Ad serving transition" href="http://www.yadvertisingblog.com/blog/2010/10/07/important-steps-to-take-before-ad-serving-transitions-to-adcenter/" target="_blank">announced plans</a> for the upcoming transition of Yahoo! Search ad serving to Microsoft Advertising adCenter in North America. We wanted to provide you with an update on our current and upcoming testing activities, as the <a title="Ad delivery testing" href="http://www.ysmblog.com/blog/2010/07/20/new-search-alliance-transition-updates-and-tips/" target="_blank">testing of ad delivery</a> from adCenter to Yahoo! Search has been occurring for a while. The Yahoo! Search ad serving transition is set to begin as soon as October 11, and we may accelerate test volumes on adCenter for brief periods over the next couple of days.</p>
<p><strong>Effect on Yahoo! Search Marketing accounts:</strong> YSM advertisers may see traffic briefly decrease in their accounts during these testing periods. When ad serving begins to transition to adCenter, you are likely to experience a decrease in clicks and impressions in your Yahoo! Search Marketing account.</p>
<p><strong>Effect on Microsoft adCenter accounts:</strong> It’s a best practice to monitor your adCenter budgets closely during testing and be ready to increase your spend if your budget falls within 20% of being fully spent. When ad serving begins to transition to adCenter, adCenter account clicks and impressions are likely to increase.</p>
<p>Remember, the test volumes will fluctuate in the next couple of days, but don’t jump to any conclusions about long-term traffic volumes based on these tests. You can <a title="Tips during testing" href="http://www.yadvertisingblog.com/blog/2010/10/07/important-steps-to-take-before-ad-serving-transitions-to-adcenter/" target="_blank">follow the tips we provided</a> to help you continue to get valuable clicks from Yahoo! Search and Bing during the transition.</p>
<p style="text-align: right;"><em>&#8212; The Team</em></p>

		<DIV class="footer1">
		<P>Posted by Administrator</p>
        <P>[ <a href="http://www.ysmblog.com/blog/2010/10/08/yahoo-and-microsoft-continue-ad-testing-with-adcenter/#comments" class="commentlink"  title="Comment on Yahoo! and Microsoft Continue Ad Testing with adCenter">2 comments</a> | Categories:  <a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts in Search Alliance" rel="category tag">Search Alliance</a> ]<BR>

		<script type="text/javascript" charset="utf-8">
	// wait until page is loaded to call API
	BitlyClient.addPageLoadEvent(function(){
		BitlyCB.myShortenCallback = function(data) {
			// this is how to get a result of shortening a single url
			var result;
			for (var r in data.results) {
				result = data.results[r];
				result['longUrl'] = r;
				break;
			}
			var bitLink = (result['shortUrl'] != '') ? result['shortUrl'] : 'http://www.ysmblog.com/blog/2010/10/08/yahoo-and-microsoft-continue-ad-testing-with-adcenter/'; 
			document.getElementById("link_twitter").innerHTML = '<a href="http://twitter.com/home/?status=Yahoo%21+and+Microsoft+Continue+Ad+Testing+with+adCenter+'+result['shortUrl']+'" target="_blank" alt="Twitter" title="Twitter" border="0"><img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" /></a>';
		}
		BitlyClient.shorten('http://www.ysmblog.com/blog/2010/10/08/yahoo-and-microsoft-continue-ad-testing-with-adcenter/', 'BitlyCB.myShortenCallback');
	});
</script>
<br />
<ul class="links">
	<li><div id="link_twitter" style="display:inline;width="20px;height="20px;"></div></li>
    <li><a href="http://www.facebook.com/share.php?u=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F08%2Fyahoo-and-microsoft-continue-ad-testing-with-adcenter%2F&t=Yahoo%21+and+Microsoft+Continue+Ad+Testing+with+adCenter" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-facebook.gif" width="19" height="20" alt="Facebook" title="Facebook" border="0" /></a></li>
    <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F08%2Fyahoo-and-microsoft-continue-ad-testing-with-adcenter%2F&title=Yahoo! and Microsoft Continue Ad Testing with adCenter&summary=<p>Expect short-term fluctuations in traffic volume in both your YSM and adCenter accounts<br />
Yesterday, we announced plans for the upcoming transition of Yahoo! Search ad serving to Microsoft Advertising adCenter in North America. We wanted to provide you with an update on our current and upcoming testing activities, as the testing of ad delivery from adCenter [...]</p>
&source=http%3A%2F%2Fwww.ysmblog.com%2Fblog" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" width="19" height="20" alt="LinkedIn" title="LinkedIn" border="0" /></a></li>
    <li><a href="http://del.icio.us/post?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F08%2Fyahoo-and-microsoft-continue-ad-testing-with-adcenter%2F&title=Yahoo%21+and+Microsoft+Continue+Ad+Testing+with+adCenter" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-del.gif" width="19" height="20" alt="delicious" title="delicious" border="0" /></a></li>
    <li><a href="http://digg.com/submit?phase=2&url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F08%2Fyahoo-and-microsoft-continue-ad-testing-with-adcenter%2F&title=Yahoo%21+and+Microsoft+Continue+Ad+Testing+with+adCenter" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-digg.gif" width="21" height="20" alt="Digg" title="Digg" border="0" /></a></li>
	<!--a href="http://twitter.com/home/?status=Yahoo%21+and+Microsoft+Continue+Ad+Testing+with+adCenter+http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F08%2Fyahoo-and-microsoft-continue-ad-testing-with-adcenter%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-tweet.gif" width="12" height="20" alt="Twitter" title="Twitter" border="0" /></a-->
    <li><a href="http://www.stumbleupon.com/submit?url=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F08%2Fyahoo-and-microsoft-continue-ad-testing-with-adcenter%2F&title=Yahoo%21+and+Microsoft+Continue+Ad+Testing+with+adCenter" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-stumble.gif" width="19" height="20" alt="Stumble Upon" title="Stumble Upon" border="0" /></a></li>
    <li><a href="http://www.technorati.com/faves?add=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F08%2Fyahoo-and-microsoft-continue-ad-testing-with-adcenter%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-techno.gif" width="19" height="20" alt="Technorati" title="Technorati" border="0" /></a></li>
    <li><a href="http://buzz.yahoo.com/buzz?publisherurn=yahooadvertiserblog&guid=http%3A%2F%2Fwww.ysmblog.com%2Fblog%2F2010%2F10%2F08%2Fyahoo-and-microsoft-continue-ad-testing-with-adcenter%2F" target="_blank"><img src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-yahoob.gif" width="21" height="20" alt="Yahoo! Buzz" title="Yahoo! Buzz" border="0" /></a></li>
</ul>
		  </P>
		</DIV>  		   	
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
			<rdf:Description rdf:about="http://www.ysmblog.com/blog/2010/10/08/yahoo-and-microsoft-continue-ad-testing-with-adcenter/"
    dc:identifier="http://www.ysmblog.com/blog/2010/10/08/yahoo-and-microsoft-continue-ad-testing-with-adcenter/"
    dc:title="Yahoo! and Microsoft Continue Ad Testing with adCenter"
    trackback:ping="http://www.ysmblog.com/blog/2010/10/08/yahoo-and-microsoft-continue-ad-testing-with-adcenter/trackback/" />
</rdf:RDF>-->

	

	<p><!-- this is ugly -->
	<span class="next"></span>
	<span class="previous"><a href="http://www.ysmblog.com/blog/page/2/" >Previous Posts  &#187;</a></span>
	</p>



 <!-- /content -->

  </TD>

<TD class="rail">

  <DIV class="search">

          <form action="http://search.yahoo.com/search" method="get">

          <IMG src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/yahoo_search.gif" alt="Yahoo! Search" width="169" height="35">

          <TABLE border="0" align="center" cellpadding="0" cellspacing="0">

            <TR>

              <TD><INPUT name="p" id="s" type="text" class="srch"></TD>

              <TD><input type="image" value="Go!" id="searchbutton" name="searchbutton" src="http://us.i1.yimg.com/us.yimg.com/i/us/ysm/cn/blg/btn_srch.gif" class="scrhbtn"></TD>

            </TR>

            <TR>

              <TD colspan="2" valign="middle" style="padding: 5px 0;"> <INPUT name="vs" type="radio" value="" checked>

                web

                <INPUT name="vs" type="radio" value="www.ysmblog.com">

                blog</TD>

            </TR>

          </TABLE>

        <input type="hidden" name="fr" value="yscpb">

        </DIV>
        
        <TABLE width="100%" border="0" cellpadding="0" cellspacing="0">

          <TR>

            <TD><a href="http://yadvertisingblog.com/" target="_blank"><img 
            src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/yadblog_callout_190x133.jpg" width="190" height="133" border="0" 
            alt="Yahoo! Advertising Blog" title="Yahoo! Advertising Blog" /></a></TD>
         </TR>
         </TABLE>

        <TABLE width="100%" border="0" cellpadding="0" cellspacing="0">

          <TR>

            <TD class="railhd">FOLLOW US</TD>

          </TR>

          <TR>

           <TD> <div class="follow-us">

	<a href="http://www.twitter.com/YahooAdBuzz" target="_blank">

    	<div class="link-box twitter">

    		<img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/img_icon_twitter.gif" alt="" /><br /><br />Twitter

  		</div>

  	</a>

	<a href="http://www.facebook.com/pages/Yahoo-Advertising/93477348187" target="_blank">

    	<div class="link-box facebook">

    		<img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/img_icon_facebook.gif" alt="" /><br /><br />Facebook

      	</div>

  	</a>

    <a href="http://www.linkedin.com/groups?gid=2003446&trk=hb_side_g" target="_blank">

    	<div class="link-box linkedin">

    		<img border="0" height="20" width="19" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/linkedin_icon.jpg" alt="" /><br /><br />LinkedIn

  		</div>

  	</a>

    <a href="http://www.ysmblog.com/blog/content-rss" target="_blank">

    	<div class="link-box rss">

        	<img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/img_icon_rss.gif" alt="" /><br /><br />RSS

       	</div>

  	</a>

    <a href="http://add.my.yahoo.com/rss?url=http://www.ysmblog.com/blog/feed/" target="_blank">

    	<div class="link-box my-yahoo">

        	<img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/img_icon_yahoo.gif" alt="" /><br /><br />My Yahoo!

      	</div>

  	</a>  

    <a href="http://www.feedblitz.com/f/?Sub=693814" target="_blank">

    	<div class="link-box email">

        	<img border="0" src="http://www.ysmblog.com/blog/wp-content/themes/YSMBlog/images/ico-email.gif" alt="" /><br /><br />Email

      	</div>

  	</a>



</div></TD>    



  

                  <TR>

            <TD class="railhd">USEFUL LINKS</TD>

          </TR>

          <TR>

            <TD class="railbdy">

			<UL>



			  <LI><A href="https://login12.marketingsolutions.yahoo.com/adui/signin/loadSignin.do">Account Log-in</A></LI>

			  <LI><A href="http://searchmarketing.yahoo.com/arp/srchv2.php?o=US2092&cmp=Yahoo&ctv=YSM_Blog&s=YSM&s2=&b=25">Sign up for Sponsored Search</A></LI>

<LI><A href="http://help.yahoo.com/l/us/yahoo/ysm/sps/whatsnew/">What's New</A></LI>

			  <LI><A href="mailto: feedback-ysm@yahoo-inc.com">Customer Service/Feedback</A></LI>

<LI><a href="http://searchmarketing.yahoo.com/trafficquality/">Traffic Quality Center</a></LI>

			</UL></TD>

          </TR>

                    

        <TR>

            <TD class="railhd">OUR PHOTOS</TD>

</TR>

<TR>

            <TD class="railbdy">

<!-- Start of Flickr Badge -->

<style type="text/css">

.zg_div {margin:0px 5px 5px 0px; width:117px;}

.zg_div_inner { color:#000000; text-align:center; font-family:arial, helvetica; font-size:11px;}

.zg_div a, .zg_div a:hover, .zg_div a:visited {color:#000000; background:inherit !important; text-decoration:none !important;}

</style>

<script type="text/javascript">

zg_insert_badge = function() {

var zg_bg_color = 'ffffff';

var zgi_url = 'http://www.flickr.com/apps/badge/badge_iframe.gne?zg_bg_color='+zg_bg_color+'&zg_person_id=56984041%40N00';

document.write('<iframe style="background-color:#'+zg_bg_color+'; border-color:#'+zg_bg_color+'; border:none;" width="113" height="151" frameborder="0" scrolling="no" src="'+zgi_url+'" title="Flickr Badge"><\/iframe>');

if (document.getElementById) document.write('<div id="zg_whatlink"><a href="http://www.flickr.com/badge_new.gne"	style="color:#000000;" onclick="zg_toggleWhat(); return false;">what is this?<\/a><\/div>');

}

zg_toggleWhat = function() {

document.getElementById('zg_whatdiv').style.display = (document.getElementById('zg_whatdiv').style.display != 'none') ? 'none' : 'block';

document.getElementById('zg_whatlink').style.display = (document.getElementById('zg_whatdiv').style.display != 'none') ? 'none' : 'block';

return false;

}

</script>



<div class="zg_div"><div class="zg_div_inner"><a href="http://www.flickr.com">www.<strong style="color:#3993ff">flick<span style="color:#ff1c92">r</span></strong>.com</a><br>

<div id="zg_whatdiv">This is a Flickr badge showing public photos from <a href="http://www.flickr.com/photos/56984041@N00">ysmblogger</a>. Make your own badge <a href="http://www.flickr.com/badge_new.gne">here</a>.</div>

<script type="text/javascript">if (document.getElementById) document.getElementById('zg_whatdiv').style.display = 'none';</script>

</div>

</div>

<!-- End of Flickr Badge -->

            </TD>

            </TR>



         

                    <TR>

            <TD class="railhd">POSTS BY SUBJECT</TD>

          </TR>

          <TR>

            <TD class="railbdy">

			<UL>

  				<li class="cat-item cat-item-33"><a href="http://www.ysmblog.com/blog/category/adcreation/" title="View all posts filed under Ad Creation">Ad Creation</a>
</li>
	<li class="cat-item cat-item-31"><a href="http://www.ysmblog.com/blog/category/agencies/" title="View all posts filed under Agencies">Agencies</a>
</li>
	<li class="cat-item cat-item-43"><a href="http://www.ysmblog.com/blog/category/analytics/" title="View all posts filed under Analytics">Analytics</a>
</li>
	<li class="cat-item cat-item-34"><a href="http://www.ysmblog.com/blog/category/audiences/" title="View all posts filed under Audiences">Audiences</a>
</li>
	<li class="cat-item cat-item-35"><a href="http://www.ysmblog.com/blog/category/creative/" title="View all posts filed under Creative">Creative</a>
</li>
	<li class="cat-item cat-item-44"><a href="http://www.ysmblog.com/blog/category/display/" title="View all posts filed under Display">Display</a>
</li>
	<li class="cat-item cat-item-56"><a href="http://www.ysmblog.com/blog/category/events/" title="View all posts filed under Events">Events</a>
</li>
	<li class="cat-item cat-item-38"><a href="http://www.ysmblog.com/blog/category/guestposts/" title="View all posts filed under Guest Posts">Guest Posts</a>
</li>
	<li class="cat-item cat-item-46"><a href="http://www.ysmblog.com/blog/category/industrynews/" title="View all posts filed under Industry News">Industry News</a>
</li>
	<li class="cat-item cat-item-36"><a href="http://www.ysmblog.com/blog/category/insights/" title="View all posts filed under Insights">Insights</a>
</li>
	<li class="cat-item cat-item-41"><a href="http://www.ysmblog.com/blog/category/marketing/" title="View all posts filed under Marketing">Marketing</a>
</li>
	<li class="cat-item cat-item-49"><a href="http://www.ysmblog.com/blog/category/optimization/" title="View all posts filed under Optimization">Optimization</a>
</li>
	<li class="cat-item cat-item-42"><a href="http://www.ysmblog.com/blog/category/products/" title="View all posts filed under Products">Products</a>
</li>
	<li class="cat-item cat-item-32"><a href="http://www.ysmblog.com/blog/category/search/" title="View all posts filed under Search">Search</a>
</li>
	<li class="cat-item cat-item-45"><a href="http://www.ysmblog.com/blog/category/searchalliance/" title="View all posts filed under Search Alliance">Search Alliance</a>
</li>
	<li class="cat-item cat-item-40"><a href="http://www.ysmblog.com/blog/category/strategy/" title="View all posts filed under Strategy">Strategy</a>
</li>
	<li class="cat-item cat-item-47"><a href="http://www.ysmblog.com/blog/category/videoandphotos/" title="View all posts filed under Video and Photos">Video and Photos</a>
</li>
	<li class="cat-item cat-item-48"><a href="http://www.ysmblog.com/blog/category/yahoonews/" title="View all posts filed under Yahoo News">Yahoo News</a>
</li>

			</UL></TD>

          </TR>

          

          

          

          

   

          

          

          

          

          



          

          

          

         <TR>

            <TD class="railhd">BLOGROLL</TD>

          </TR>

          <TR>

            <TD class="railbdy">

			<UL>

<li><a href="http://www.lauralippay.com/blog/">Laura Lippay's Lip Service</a></li>

<li><a href="http://blog.clickz.com">ClickZ News Blog</a></li>

<li><a href="http://flyteblog.com/">FlyteBlog</a></li>

<li><a href="http://battellemedia.com/">John Battelle's Searchblog</a></li> 

<li><a href="http://www.marketingpilgrim.com/">Marketing Pilgrim</a></li>

<li><a href="http://blog.marketingprofs.com/">MarketingProfs: Daily Fix</a></li>

<li><a href="http://www.marketingvox.com/">MarketingVOX</a></li>

<li><a href="http://toprankblog.com/">Online Marketing Blog</a></li>

<li><a href="http://gesterling.wordpress.com/">Screenwerk</a></li>

<li><a href="http://www.searchenginejournal.com/">Search Engine Journal</a></li> 

<li><a href="http://www.searchengineland.com/">Search Engine Land</a></li>

<li><a href="http://www.seroundtable.com/">Search Engine Roundtable</a></li>

<li><a href="http://blog.searchenginewatch.com/blog/">Search Engine Watch</a></li>

<li><a href="http://www.searchmarketingstandard.com/">Search Marketing Standard</a></li>

<li><a href="http://www.traffick.com/">Traffick</a></li>











			</UL></TD>

          </TR>

          

        

        

                  

                <TR>

            <TD class="railhd">OTHER YAHOO! BLOGS</TD>

          </TR>

          <TR>

            <TD class="railbdy">

			<UL>

<li><a href="http://blog.del.icio.us/">del.icio.us Blog</a></li>

<li><a href="http://blog.flickr.com/">FlickrBlog</a></li>

<li><a href="http://dir.yahoo.com/thespark">The Spark Blog</a></li>

<li><a href="http://upcoming.org/news/">Upcoming.org News</a></li>

<li><a href="http://www.yadvertisingblog.com/">Yahoo! Advertising Blog</a></li>

<li><a href="http://www.yanswersblog.com/">Yahoo! Answers Blog</a></li>

<li><a href="http://buzz.yahoo.com/buzz_log/">Yahoo! Buzz Log</a></li>

<li><a href="http://developer.yahoo.net/blog/">Yahoo! Developer Network Blog</a></li>

<li><a href="http://ymailupdates.com/blog/">Yahoo! Mail Updates</a></li>

<li><a href="http://ymusicblog.com/">Yahoo! Music Blog</a></li>

<li><a href="http://ysearchblog.com/myweb/">Yahoo! My Web Blog</a></li>

<li><a href="http://www.ypnblog.com/">Yahoo! Publisher Network Blog</a></li>

<li><a href="http://ysearchblog.com/">Yahoo! Search Blog</a></li>

<li><a href="http://shopping.yahoo.com/blog">Yahoo! Shopping Blog</a></li>

<li><a href="http://360.yahoo.com/ysportsblog">Yahoo! Sports Blog</a></li>

<li><a href="http://ystoreblog.com/">Yahoo! Store Blog</a></li>

<li><a href="http://yuiblog.com">Yahoo! User Interface Blog</a></li>

<li><a href="http://www.yvideoblog.com/">Yahoo! Video Blog</a></li>

<li><a href="http://yodel.yahoo.com">Yodel Anecdotal</a></li>





     

     

     

			</UL></TD>

          </TR>

        

        

        

        

          <TR>

            <TD class="railft"><P>We encourage comments and look forward to hearing from you. Please note that Yahoo! may, in our sole discretion, remove comments if they are off topic, inappropriate, or otherwise violate our <A href="http://docs.yahoo.com/info/terms/">Terms of Service</A>.</P>

              <P style=" border-top: 1px solid #AAA; padding-top: 1em;">Powered by <A href="http://wordpress.org/">WordPress</A><BR>

		    Hosted by <A href="http://smallbusiness.yahoo.com/">Yahoo!</A></P></TD>

          </TR>

        </TABLE>

<script>
var s_pageName="Yahoo Marketing Solutions:YSMBlog:Main";
</script>
	</TD>
	</TR>
</TABLE>
</DIV>
<div align=center>
<div style="text-align: left; width: 700px; margin-top: 9px; margin-bottom: 9px; padding: 9px 6px 9px 6px; font: normal 11px Verdana, clean, sans-serif; color: #000000;">Copyright &copy; 2008 Yahoo! Inc. All Rights Reserved | <a href="http://searchmarketing.yahoo.com/legal/copyright.php">Copyright/IP Policy</a> | <a href="http://docs.yahoo.com/info/terms/">Terms of Service</a> | <a href="http://searchmarketing.yahoo.com/legal/trademarks.php">Trademarks</a> | <a href="http://searchmarketing.yahoo.com/legal/patents.php">Patents</a> | <a href="http://help.yahoo.com/help/us/index.html">Help</a><br /><span style="font-size: 10px;">NOTICE: We collect personal information on this site.  To learn more about how we use your information, see our <a href="http://docs.yahoo.com/info/privacy/us/yas.html"><strong>Privacy Policy</strong></a>.</span>
</div>
</div>
<script src="/blog/wp-content/themes/ysmblog/s_code_remote.js"></script>
<script type='text/javascript' src='http://track3.mybloglog.com/js/jsserv.php?mblID=2008030511380186'></script>
<script type="text/javascript" language="JavaScript">
//<![CDATA[
var wpdone;
function wpvisit()
{
  var z;
  z="&r="+escape(document.referrer);
  z=z+"&b="+escape(navigator.appName+" "+navigator.appVersion);
  w=parseFloat(navigator.appVersion);
  if (w > 2.0) {
    z=z+"&s="+screen.width+"x"+screen.height;
    z=z+"&o="+navigator.platform;
    v="1.2";
    if (navigator.appName != "Netscape") {
      z=z+"&c="+screen.colorDepth;
    } else {
      z=z+"&c="+screen.pixelDepth
    }
    z=z+"&j="+navigator.javaEnabled();
  } else {
    v=1.0;
  }
  z=z+"&v="+v;

  document.writeln("<img border=\"0\" src=\"http://visit.webhosting.yahoo.com/wisit.gif"+"/"+"?"+z+"\" />");
}
  wpvisit();
//]]>
</script>

<noscript><img src="http://visit.webhosting.yahoo.com/wisit.gif?1302007772" border="0" width="1" height="1" alt="visit" /></noscript><!-- Advertiser 'Yahoo!', Include user in segment '[Ymarketing] Yahoo Search Marketing Blog' - DO NOT MODIFY THIS PIXEL IN ANY WAY --> <img src="http://ads.bluelithium.com/pixel?id=555216&t=2" width="1" height="1" /> <!-- End of segment tag -->
<!-- Yahoo! Web Analytics - All rights reserved --> <script type="text/javascript" src="http://d.yimg.com/mi/ywa.js">
</script>
<script type="text/javascript">
var YWATracker = YWA.getTracker("1000623433910"); YWATracker.submit(); </script> <noscript> <div><img src="http://a.analytics.yahoo.com/p.pl?a=1000623433910&amp;js=no"
width="1" height="1" alt="" /></div> </noscript>
<!-- End of Yahoo! Web Analytics Code -->
<!-- http://geo.yahoo.com/b?s=2142538416&t=randomstring -->
</body>
</html>
<!-- Dynamic page generated in 0.469 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2011-04-05 04:49:32 -->
