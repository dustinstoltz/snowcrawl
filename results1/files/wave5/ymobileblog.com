<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Yahoo! Mobile Blog</title>

<meta name="description" content="Yahoo! Mobile" />

<meta http-equiv="imagetoolbar" content="no" />

<link rel="stylesheet" href="http://ymobileblog.com/blog/wp-content/themes/Archive/style.css" type="text/css" media="all" />

<link rel="alternate" type="application/rss+xml" title="Y! Mobile Blog RSS Feed" href="http://ymobileblog.com/blog/feed/" />

<link rel="pingback" href="http://ymobileblog.com/blog/xmlrpc.php" />


<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://ymobileblog.com/blog/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://ymobileblog.com/blog/wp-includes/wlwmanifest.xml" /> 
<link rel='index' title='Y! Mobile Blog' href='http://ymobileblog.com/blog' />
<meta name="generator" content="WordPress 2.8.4" />

<!-- All in One SEO Pack 1.6.10 by Michael Torbert of Semper Fi Web Design[-1,-1] -->
<meta name="description" content="Yahoo! Mobile Blog" />
<meta name="keywords" content="Yahoo!, Yahoo, Mobile, Blog, Internet" />
<link rel="canonical" href="http://ymobileblog.com/blog/" />
<!-- /all in one seo pack -->
<script language="javascript" type="text/javascript">
<!--
		function collapseThread( theId ) {
			var comment = document.getElementById(theId);
			if(!comment)
			{
				alert("ERROR:\nThe document structure is different\nfrom what Threaded Comments expects.\nYou are missing the element '"+theId+"'");
				return;
			}
			var theBody = findBody(comment);
			if(comment.className.indexOf("collapsed") > -1) {
				comment.className = comment.className.replace(" collapsed", "");;
			} else {
				comment.className += " collapsed";
			}
		}

		function expandThread( theId ) {
			var comment = document.getElementById(theId);
			if(!comment)
			{
				alert("ERROR:\nThe document structure is different\nfrom what Threaded Comments expects.\nYou are missing the element '"+theId+"'");
				return;
			}
			var theBody = findBody(comment);
			if(comment.className.indexOf("collapsed") > -1) {
				comment.className = comment.className.replace(" collapsed", "");;
			} 
		}
		
		function findBody(el)
		{
			var divs = el.getElementsByTagName("div");
			var ret;
			for(var i = 0; i < divs.length; ++i) {
				if(divs.item(i).className.indexOf("body") > -1)
					return divs.item(i);
			}
			return false;
		}
	
		function onAddComment() {
			//checkDocumentIntegrity();
			var el = document.getElementById("commentform");
			// Future release: Check if form is filled correctly and mark the form fields.
			el.submit();
		}
		
		function moveAddCommentBelow(theId, threadId, collapse)
		{
			expandThread( theId );
			var addComment = document.getElementById("addcomment");
			if(!addComment)
			{
			  	alert("ERROR:\nThreaded Comments can't find the 'addcomment' div.\nThis is probably because you have changed\nthe comments.php file.\nMake sure there is a tag around the form\nthat has the id 'addcomment'"); 
				return
			}
			var comment = document.getElementById(theId);
			if(collapse)
			{
				for(var i = 0; i < comment.childNodes.length; ++i) {
					var c = comment.childNodes.item(i);
					if(typeof(c.className) == "string" && c.className.indexOf("collapsed")<0) {
						c.className += " collapsed";
					}
				}
			}
			addComment.parentNode.removeChild(addComment);

			comment.appendChild(addComment);
			if(comment.className.indexOf("alt")>-1) {
				addComment.className = addComment.className.replace(" alt", "");					
			} else {
				addComment.className += " alt";
			}
		        var replyId = document.getElementById("comment_reply_ID");
			if(replyId == null)
			{
				alert("Brians Threaded Comments Error:\nThere is no hidden form field called\n'comment_reply_ID'. This is probably because you\nchanged the comments.php file and forgot\nto include the field. Please take a look\nat the original comments.php and copy the\nform field over.");
			}
			replyId.value = threadId;
			var reRootElement = document.getElementById("reroot");
			if(reRootElement == null)
			{
				alert("Brians Threaded Comments Error:\nThere is no anchor tag called 'reroot' where\nthe comment form starts.\nPlease compare your comments.php to the original\ncomments.php and copy the reroot anchor tag over.");
			}
			reRootElement.style.display = "block";
			var aTags = comment.getElementsByTagName("A");
			var anc = aTags.item(0).id;
			//document.location.href = "#"+anc;
			document.getElementById("comment").focus();
		}

		function checkDocumentIntegrity()
		{
			str = "";
			
			str += checkElement("reroot","div tag");
			str += checkElement("addcomment", "div tag");
			str += checkElement("comment_reply_ID", "hidden form field");
			str += checkElement("content", "div tag");
			str += checkElement("comment", "textfield");
			str += checkElement("addcommentanchor", "anchor tag");
			
			if(str != "")
			{
				str = "Brian's Threaded Comments are missing some of the elements that are required for it to function correctly.\nThis is probably the because you have changed the original comments.php that was included with the plugin.\n\nThese are the errors:\n" + str;
				str += "\nYou should compare your comments.php with the original comments.php and make sure the required elements have not been removed.";

				alert(str);
			}
		}
               
		function checkElement(theId, elDesc)
		{
			var el = document.getElementById(theId);
			if(!el)
			{
				if(elDesc == null)
					elDesc = "element";
				return "- The "+elDesc+" with the ID '" +theId + "' is missing\n"; 
			}
			else 
				return "";
		}
		
		function reRoot()
		{
			var addComment = document.getElementById("addcomment");			
			var reRootElement = document.getElementById("reroot");
			reRootElement.style.display = "none";
			var content = document.getElementById("content-main");
			if( !content )
				content = document.getElementById("content");
			if( content )
			{
				addComment.parentNode.removeChild(addComment);
				content.appendChild(addComment);
			}
			addComment.className = addComment.className.replace(" alt", "");
			document.location.href = "#addcommentanchor";
			document.getElementById("comment").focus();				
			document.getElementById("comment_reply_ID").value = "0";
		}			
		
		function changeCommentSize(d)
		{
			var el = document.getElementById("comment");
			var height = parseInt(el.style.height);
			if(!height && el.offsetHeight)
				height = el.offsetHeight;
			height += d;
			if(height < 20) 
				height = 20;
			el.style.height = height+"px";
		}		
-->
</script>
<style type="text/css">
.comment 
{
	position: 				relative;
	margin:					3px;
	margin-top:				6px;
/*	border: 				1px solid #666; */
	padding:				4px 4px 4px 8px;
	background-color:		#fff;
}

.odd
{
	background-color: #f8f8f8;
}

.comment div {
	position: 				relative;
}

.comment .comment img
{
	margin: 				0px;
}

.comment .collapseicon 
{
	width: 					13px;
	height: 				13px;
	overflow:				hidden;
	background-image: 		url(http://ymobileblog.com/blog/wp-content/plugins/briansthreadedcomments.php?image=subthread-open.png);
}

.collapsed .collapseicon 
{
	background-image: 		url(http://ymobileblog.com/blog/wp-content/plugins/briansthreadedcomments.php?image=subthread.png);
}


.comment .reply {
	text-align: 			right;
	font-size: 				80%;
	padding: 				0px 6px 6px 0px;
}

.comment
{
	border: 	1px solid #ddd;
	margin-top: 			10px;
}

input#subscribe
{
	width: auto;
}

.comment .body .content
{
	padding:				0px 3px 0px 3px;
	/*width: 					100%;	*/
	overflow: 				hidden; 
}

.comment .title abbr
{
	border: none;
}

.collapsed .body, .collapsed .comment
{
	display:				none;
}
 
#commentform textarea {
	width: 97%;
}

.btc_gravatar {
	float: right;
	margin: 3px 3px 4px 4px;
}
</style>
<link rel="stylesheet" type="text/css" href="http://ymobileblog.com/blog/wp-content/plugins/wp-recaptcha/recaptcha.css" /><script type="text/javascript" charset="utf-8" src="http://w.sharethis.com/widget/?wp=2.8.4&amp;publisher=213c9a3c-e73b-47dc-a101-5a4b6f2fc191"></script>
<script type="text/javascript"><!--//--><![CDATA[//><!--

sfHover = function() {

	var sfEls = document.getElementById("menu").getElementsByTagName("LI");

	for (var i=0; i<sfEls.length; i++) {

		sfEls[i].onmouseover=function() {

			this.className+=" sfhover";

		}

		sfEls[i].onmouseout=function() {

			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");

		}

	}

}

if (window.attachEvent) window.attachEvent("onload", sfHover);

try{document.execCommand("BackgroundImageCache",false,true);}catch(err){}

//--><!]]></script>

</head>

<body>

<div id="page">

	<ul class="skipLinks">

		<li><a href="#content" title="Skip to content">Skip to content</a></li>

		<li><a href="#menu" title="Skip to navigation">Skip to navigation</a></li>

		<li><a href="#footer" title="Skip to footer">Skip to footer</a></li>

	</ul>

	<div id="header">

		<h1 id="logo">

			<a href="http://ymobileblog.com/blog/">Y! Mobile Blog</a><br />

			<em>Yahoo! Mobile</em>

		</h1>

		<div class="inner">

			<ul id="menu">

				

				<li class="first current_page_item">

                    <a href="http://ymobileblog.com/blog/">Home</a></li>

                <li><a href="http://ymobileblog.com/blog/archives">Archive</a></li> 

				<li><a href="http://feeds.feedburner.com/YahooMobile">Subscribe</a></li>

				<li><a href="http://ymobileblog.com/blog/about">About</a></li>

				<li><a href="http://help.yahoo.com/l/us/yahoo/mobile/index.html">Help</a></li>

                

			</ul>

		<form method="get" id="searchform" action="http://ymobileblog.com/blog/">
			<fieldset>
				<legend>Search Form</legend>
				<input type="text" value="Search our blog..." name="s" id="s" onblur="if (this.value == '') {this.value = 'Search our blog...';}"  onfocus="if (this.value == 'Search our blog...') {this.value = '';}" />
				<input type="submit" id="searchsubmit" value="" />
			</fieldset>
		</form>
		</div>

	</div>

	<div id="wrap">

		<div id="main">

			<div id="content">

				<div class="apost">					<br><h2 id="post-1051"><a href="http://ymobileblog.com/blog/2011/04/01/busted-brackets-yahoo-has-ncaa-tournament-madness-by-the-numbers/" rel="bookmark" title="Permanent Link to Busted Brackets. Yahoo! has NCAA Tournament Madness by the Numbers">Busted Brackets. Yahoo! has NCAA Tournament Madness by the Numbers</a></h2>					<ul class="info">						<li class="time">Posted April 1st, 2011 at 11:18 am by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2011/04/01/busted-brackets-yahoo-has-ncaa-tournament-madness-by-the-numbers/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2011/04/01/busted-brackets-yahoo-has-ncaa-tournament-madness-by-the-numbers/</script>-->					<div class="pmain">						<!-- spost --><p>The 2011 NCAA Men’s Basketball Tournament has not been kind to the ‘by-the-numbers’ methodology that many people use to fill out their brackets each year. With none of the #1 or #2 seeds making it through to the Final Four for the first time ever, this year’s tournament has been chalk-full of shocking upsets, baffling fans and experts alike.</p>
<p>More than 3 million Yahoo! Tourney Pick’em brackets were filled out this year. Get every game right, and you could win $1,000,000 for a “perfect bracket.” This year, however, not one bracket was even perfect after the Round of 64, so it’s down to the $10,000 “Best Bracket” portion of the contest.</p>
<p>Experts have said this is the <em>maddest </em>NCAA Tournament ever, and Yahoo! has the data to prove it:</p>
<ul>
<li>Out of more than 3 million registered Yahoo! Tourney Pick’em brackets, none were perfect after the first round of the tournament</li>
<li>Only 3 brackets were able to even pick 31 of the 32 first round winners correctly</li>
<li>Over 80% of brackets failed to project a single Final Four team correctly</li>
<li>Only 5.7% of all brackets even had all 4 of these Final Four teams winning in the first round, let alone making it to the Final Four</li>
<li>Only 1 bracket has <a href="http://sports.yahoo.com/fantasy/blog/roto_arcade/post/School-librarian-correctly-books-improbable-Fina?urn=fantasy-wp371">predicted all 4 of the Final Four teams correctly</a></li>
<li>Pick percentages for each of the Final Four teams to make it this far were: UConn = 20.4%, Kentucky = 6.6%, Butler = 0.6%, VCU = 0.1%</li>
<li>Of the 4 teams left, the pick percentages for each to win the championship are: UConn = 3.3%, Kentucky = 1.4%, Butler = 0.1%, VCU = 0.0% (negligible, although roughly 400 people did pick them)</li>
<li>An infinitesimal 0.023% of brackets have both potential championship participants remaining</li>
<li>Despite seeing VCU play in the first 2 rounds, only 6.0% of registered brackets picked them to make the Final Four in Yahoo!’s <a href="http://tournament.fantasysports.yahoo.com/t2">Second Chance Tourney Pick’em game</a> (where people pick from the Sweet 16 on), and only 0.6% are picking them to win the national championship game</li>
<li>Out of hundreds of thousands of entries in the Second Chance game only has 7 brackets that are still “perfect”</li>
</ul>
<p>Yahoo! Sports is the #1 sports site on the Web and home to millions of fans and fantasy sports players around the world. For the most up-to-date sports news, highlights, and fantasy scores, visit <a href="http://sports.yahoo.com/">sports.yahoo.com</a>.</p>
<p>Edwin Pankau</p>
<p>Sr. Product Manager, Fantasy Sports</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2011/04/01/busted-brackets-yahoo-has-ncaa-tournament-madness-by-the-numbers/#respond" title="Comment on Busted Brackets. Yahoo! has NCAA Tournament Madness by the Numbers">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2011/04/01/busted-brackets-yahoo-has-ncaa-tournament-madness-by-the-numbers/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "Busted Brackets. Yahoo! has NCAA Tournament Madness by the Numbers", url: "http://ymobileblog.com/blog/2011/04/01/busted-brackets-yahoo-has-ncaa-tournament-madness-by-the-numbers/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-1041"><a href="http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/" rel="bookmark" title="Permanent Link to Yahoo! Fantasy Baseball ’11 &#038; the Impact of Mobile on Fantasy Sports">Yahoo! Fantasy Baseball ’11 &#038; the Impact of Mobile on Fantasy Sports</a></h2>					<ul class="info">						<li class="time">Posted March 31st, 2011 at 10:57 am by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/</script>-->					<div class="pmain">						<!-- spost --><p>With another Opening Day upon us, baseball fans around the world are given a clean slate and renewed optimism that this will be the year their team brings home the pennant. Same is true in the fantasy sports world, where the difference between celebrating a championship and finishing at the bottom of the barrel could be the result of an ill-advised draft pick or, even worse, a missed waiver wire<em> </em>pickup.</p>
<p>At Yahoo!, we understand the importance of getting timely updates on the latest news, statistics and injuries for your fantasy players. With today’s release of the Yahoo! Fantasy Baseball ‘11 iPhone app – you will soon be able to add and drop players, claim waived players, and accept or decline trade offers – directly from your mobile phone.</p>
<p><a rel="attachment wp-att-1042" href="http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/iphone-baseball/"><img class="aligncenter size-medium wp-image-1042" title="iphone-baseball" src="http://ymobileblog.com/blog/wp-content/uploads/2011/03/iphone-baseball-200x300.PNG" alt="iphone-baseball" width="200" height="300" /></a></p>
<p>Over the last few years, the exponential growth of mobile apps and shift of computing to the mobile device has greatly impacted the way we participate in fantasy sports. Today’s fantasy gamers are more active and engaged than ever before – consider these 2010 Yahoo! Fantasy Football Statistics:</p>
<ul>
<li>Yahoo! Fantasy Football mobile use tripled in 2010 with over 30% of fantasy players engaging on their mobile device (vs. 10% in 2009)</li>
<li>Mobile users consumed over 45% of the total Yahoo! Fantasy Football page views – nearly 4x the page views in 2010 vs. 2009</li>
<li>In 2011, Yahoo! expects Fantasy Football mobile user share to be close to 50%</li>
</ul>
<p>Gone are the days of scouring box scores in the Sports section on Monday mornings to check your player stats. With Yahoo! Fantasy Sports mobile apps you have fast access to your leagues, teams, matchups, player stats and real-time scores – right at your fingertips, anytime, anywhere.</p>
<p>The Yahoo! Fantasy Baseball ’11 app is now available for free from the App Store on iPhone at: http://itunes.apple.com/us/app/yahoo-fantasy-baseball-11/id355995557</p>
<p>Let’s play ball!</p>
<p>Ron Belmarch</p>
<p>Product Manager, Yahoo! Fantasy Sports</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/#respond" title="Comment on Yahoo! Fantasy Baseball ’11 &amp; the Impact of Mobile on Fantasy Sports">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "Yahoo! Fantasy Baseball ’11 &#038; the Impact of Mobile on Fantasy Sports", url: "http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-1033"><a href="http://ymobileblog.com/blog/2011/03/17/mobile-internet-%e2%80%93-delivering-on-the-promise-of-mobile-advertising/" rel="bookmark" title="Permanent Link to Mobile Internet – Delivering on the Promise of Mobile Advertising">Mobile Internet – Delivering on the Promise of Mobile Advertising</a></h2>					<ul class="info">						<li class="time">Posted March 17th, 2011 at 3:48 pm by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2011/03/17/mobile-internet-%e2%80%93-delivering-on-the-promise-of-mobile-advertising/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2011/03/17/mobile-internet-%e2%80%93-delivering-on-the-promise-of-mobile-advertising/</script>-->					<div class="pmain">						<!-- spost --><h3>Scope</h3>
<p>“Mobile Internet – Delivering on the Promise of Mobile Advertising”  represents the first in a series of white papers from Yahoo! that provides an overview of the mobile landscape, explores the potential of emerging applications and explains mobile capabilities to marketers looking to  integrate mobile into their advertising campaigns. This paper discusses  mobile and tablet usage, delivers insights about mobile advertising  effectiveness, presents real world case studies and details current best  practices in mobile advertising.</p>
<h3>Executive Summary</h3>
<p>Mobile Internet came of age in 2010, offering advertisers the scale,  tools and technology to develop effective campaigns and vie for a fair  share of major media budgets. However, mobile advertising still lags  behind the opportunity. Will brands realize the full potential of mobile  in 2011? To do so, marketers will need to approach mobile as a channel,  not a strategy, albeit a particularly effective channel due to its  ability to connect with consumers in a very personal way. If it feels  like mobile phones are everywhere, it’s because they are. More than 80%  of the U.S. population<sup>vii</sup> owns at least one mobile phone.  Unusual in the world of consumer electronics, mobile users express  intense feelings about their phones, describing the devices as a  seamless extension of their person and psyche.</p>
<p>Faster network speeds, functionrich smartphones and tablets, a  burgeoning portfolio of applications and more engaging ad formats like  screen takeovers and expandable ads enable mobile to compete with more  established media. As for mobile’s position in the media rankings, the  facts speak for themselves. Mobile attracts younger, more affluent and  more educated consumers and delivers on performance benchmarks such as  awareness, message association and purchase intent.</p>
<p>Perhaps most importantly, mobile effectively moves consumers through the marketing funnel from awareness to purchase, repeat buys, and advocacy. Half of consumers claim they purchase an item after researching it on their mobile<sup>xii</sup> and 90% of mobile owners access the web from the retail store floor.<sup>i</sup></p>
<p>Mobile functions most effectively in an integrated media environment coupled with online display. This combination amplifies budget efficiencies and achieves synergies that increase key metrics like awareness and purchase intent. Integrated mobile and personal computer (PC) campaigns resulted in a 54% increase in brand awareness, a 46% increase in brand favorability and a 42% increase in aided brand awareness.<sup>viii</sup></p>
<p>Mobile has proved itself a most adaptable medium, facilitating quotidian tasks like shopping, checking bank balances, locating restaurants or stores, navigating routes, monitoring health, paying bills, downloading music and videos, texting friends and yes— making phone calls. Consumers have embraced smartphones with open arms and mobile Internet has finally reached enough scale to offer critical opportunities for marketers.</p>
<p>Now, tablet computers have emerged as a potential contender for screen dominance. Experts credit the iPad launch with singlehandedly reinvigorating the tablet market. Penetration projections show a growth trajectory from 10.3 million tablet users in 2010 to 82.1 million by 2015. The turning point occurs in 2012 when tablet sales (36% of U.S. PC sales) should outstrip those of notebooks/mini PCs at an estimated 32%.<sup>ix</sup></p>
<h3>Content Highlights</h3>
<p><strong>What are current mobile user preferences and patterns?</strong></p>
<ul>
<li>Mobile adoption patterns mirror the early days of the Internet when email dominated usage.</li>
<li>Rather than waxing and waning with the workday and workweek, mobile user traffic remains constant throughout the week.<sup>i</sup></li>
<li>86% of mobile Internet users surf the mobile Web while watching TV.</li>
</ul>
<p><strong>Is mobile advertising effective?</strong></p>
<ul>
<li>Click-through rates on a mobile web movie campaign were 30x higher than the PC counterpart.<sup>xxvi</sup></li>
<li>Ad recall scored 15% higher among users who viewed a mobile ad than any other channel. <sup>xxvii</sup></li>
</ul>
<p><strong>How are people using tablet PCs?</strong></p>
<ul>
<li>On the advertising front, iPad users demonstrate high receptivity to advertising, especially if coupled with an interesting video (49%) or interactive features (46%).<sup>iv</sup></li>
<li>The iPad diverts time and attention away from other devices, impacting video game players the most and smartphones the least.<sup>iv</sup></li>
</ul>
<p><strong>What are some opportunities and best practices for mobile advertisers?</strong></p>
<ul>
<li>Use mobile ad buys strategically to smooth over audience troughs when traditional advertising traffic diminishes during work hours and on weekends.</li>
<li>Mobile is finally scalable enough to be an extension of an advertiser’s communication strategy and should be incorporated into all buys.</li>
</ul>
<p><a href="http://l.yimg.com/a/i/us/ayc/article/mobile_internet_whitepaper.pdf">Read more of the whitepaper.</a></p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2011/03/17/mobile-internet-%e2%80%93-delivering-on-the-promise-of-mobile-advertising/#respond" title="Comment on Mobile Internet – Delivering on the Promise of Mobile Advertising">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2011/03/17/mobile-internet-%e2%80%93-delivering-on-the-promise-of-mobile-advertising/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "Mobile Internet – Delivering on the Promise of Mobile Advertising", url: "http://ymobileblog.com/blog/2011/03/17/mobile-internet-%e2%80%93-delivering-on-the-promise-of-mobile-advertising/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-1027"><a href="http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/" rel="bookmark" title="Permanent Link to Introducing Flickr for Windows 7 and Windows Phone 7!">Introducing Flickr for Windows 7 and Windows Phone 7!</a></h2>					<ul class="info">						<li class="time">Posted March 1st, 2011 at 2:34 pm by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/</script>-->					<div class="pmain">						<!-- spost --><p>Just in case you missed it, we recently introduced Flickr for Windows Phone 7® ! With this new app, you’ll have everything you expect from Flickr (and more!) right on your Windows Phone 7 devices.</p>
<p><a rel="attachment wp-att-1028" href="http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/flickrimage002/"><img class="aligncenter size-medium wp-image-1028" title="flickrimage002" src="http://ymobileblog.com/blog/wp-content/uploads/2011/03/flickrimage002-238x300.jpg" alt="flickrimage002" width="238" height="300" /></a></p>
<p>Here are some of the features we’re most excited about:</p>
<ul>
<li>Swipe through panoramic blades to see all your recent activity, latest uploads from your contacts and all your newest uploads.</li>
<li>Fully optimized for brilliant hi-resolution display. Tap on a photo to bring up a full-screen lightbox view and zoom in to enlarge and get close up on details in a photo. You’ll be able to access your photos in their original resolution, right from your mobile device!</li>
<li>On Windows 7 Slate devices, we’re introducing a new “Context View” as a whole new way to get the story behind a photo and discover new and relevant content. Zoom out from a photo in lightbox view and get more information such as other photos in the set, group, from a contact or nearby on a map.</li>
<li>Geo-location capabilities display your photos on a full-screen color map. Also view nearby geo-tagged photos from the Flickr community on the map as well for a slightly different view of the world around you.</li>
</ul>
<p>Easily share photos to your friends from you mobile device directly to Flickr and also from Facebook, Twitter or email!</p>
<p>Check out our <a href="http://www.flickr.com/windows7">microsite</a> to get a quick demo and download them today!</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/#respond" title="Comment on Introducing Flickr for Windows 7 and Windows Phone 7!">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "Introducing Flickr for Windows 7 and Windows Phone 7!", url: "http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-1018"><a href="http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/" rel="bookmark" title="Permanent Link to Delivering A Better Mobile Finance Tracking Experience">Delivering A Better Mobile Finance Tracking Experience</a></h2>					<ul class="info">						<li class="time">Posted February 22nd, 2011 at 10:04 am by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/</script>-->					<div class="pmain">						<!-- spost --><p>The pace of change in the mobile space continues to move at break-neck speeds. It seems like every day, a new app, device or platform is announced. Simply put, the opportunity to innovate and create better consumer experiences is immeasurable, which brings us to the news of the day: the unveiling of a new Yahoo! Finance MarketDash app for the Apple<sup>®</sup> iPad, which empowers consumers to track their investments, follow personally relevant companies, and read top financial stories on the iPad like never before (<a href="http://ycorpblog.com/2011/02/22/yahoofinanceipad/">http://ycorpblog.com/2011/02/22/yahoofinanceipad/</a>).</p>
<p>Consumers will find that this new app offers an intuitive and rich design that can be used in both portrait and landscape displays on the iPad. The innovative Multi-Touch user interface makes it easy for investors to find real-time market data and editorial content and stay abreast of their investments while on the go. Some of the key features of Yahoo! Finance for iPad include:</p>
<ul>
<li><strong>Market      Data:</strong> Research and track investments and      companies using easy to read and interactive search pages, featuring      real-time quotes, and key statistics.</li>
<li><strong>Interactive      Charts:</strong> Elegant chart experiences include pinch, expand and scroll capabilities,      timeline features, and the ability compare multiple stocks on one      interactive screen.</li>
<li><strong>News      Headlines:</strong> Read up-to-the      minute editorial content from Yahoo! contributors and partners such as the      Wall Street Journal, Bloomberg, Reuters, and The Financial Times.</li>
<li><strong>Robust      Personalization:</strong> Ability to      create personally relevant watch lists, access to your portfolio stored on      Yahoo! Finance.</li>
</ul>
<p><a rel="attachment wp-att-1020" href="http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/yahoo-main-page/"><img class="aligncenter size-medium wp-image-1020" title="Yahoo Main Page" src="http://ymobileblog.com/blog/wp-content/uploads/2011/02/Yahoo-Main-Page-300x225.png" alt="Yahoo Main Page" width="300" height="225" /></a><a rel="attachment wp-att-1019" href="http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/charts/"><img class="aligncenter size-medium wp-image-1019" title="Charts" src="http://ymobileblog.com/blog/wp-content/uploads/2011/02/Charts-300x225.png" alt="Charts" width="300" height="225" /></a></p>
<p>Ultimately, the Yahoo! Finance MarketDash app is about giving people an experience that pushes the limits of what’s possible with the amazing technologies of the Web and Yahoo!. When you consider that Yahoo! Finance reaches more than 44.9 million people each month (comScore, January 2010), it’s easy to see why we invested so heavily in creating a new immersive experience that leverages the unique attributes of the fast growing tablet market.</p>
<p>Stay tuned for more innovations from Yahoo! in the future. We definitely don’t plan on stopping here.</p>
<p>Cheers,</p>
<p>David Putnam</p>
<p>Sr. Director, Product Management Yahoo! Finance</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/#respond" title="Comment on Delivering A Better Mobile Finance Tracking Experience">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "Delivering A Better Mobile Finance Tracking Experience", url: "http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-1008"><a href="http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/" rel="bookmark" title="Permanent Link to Introducing Livestand from Yahoo!">Introducing Livestand from Yahoo!</a></h2>					<ul class="info">						<li class="time">Posted February 10th, 2011 at 4:13 pm by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/feature-releases/" title="View all posts in Feature Releases" rel="category tag">Feature Releases</a>,  <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a>,  <a href="http://ymobileblog.com/blog/category/media-product/" title="View all posts in Media Product" rel="category tag">Media Product</a>,  <a href="http://ymobileblog.com/blog/category/mobile-advertising/" title="View all posts in Mobile Advertising" rel="category tag">Mobile Advertising</a>,  <a href="http://ymobileblog.com/blog/category/product/" title="View all posts in Product" rel="category tag">Product</a>,  <a href="http://ymobileblog.com/blog/category/yahoo-mobile-homepage/" title="View all posts in Yahoo! Mobile Homepage" rel="category tag">Yahoo! Mobile Homepage</a>,  <a href="http://ymobileblog.com/blog/category/ipad/" title="View all posts in iPad" rel="category tag">iPad</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/</script>-->					<div class="pmain">						<!-- spost --><p>Earlier today, we announced <a href="http://ycorpblog.com/2011/02/10/livestand/">Livestand from Yahoo!</a> – a one-of-a-kind digital newsstand that offers a wealth of ever-changing content, continuously programmed by each individual’s interests. We are super excited about it and you can learn more about Livestand on our corporate blog, Yodel Anecdotal <a href="http://ycorpblog.com/2011/02/10/livestand/">Yodel Anecdotal</a> and <a href="http://developer.yahoo.com/blogs/ydn/posts/2011/02/livestand/">YDN Blog</a>.</p>
<p><a rel="attachment wp-att-1009" href="http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/livestand/"><img class="aligncenter size-medium wp-image-1009" title="Livestand" src="http://ymobileblog.com/blog/wp-content/uploads/2011/02/Livestand-300x225.PNG" alt="Livestand" width="300" height="225" /></a></p>
<p>As we head to Mobile World Congress in Barcelona next week, we will have much more to say about Livestand. We hope to see you there and until then, stay tuned to hear more about Livestand. <span style="text-decoration: line-through;"> </span></p>
<p>Cheers,</p>
<p>Irv Henderson, VP of Product Management, Yahoo! Mobile</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/#respond" title="Comment on Introducing Livestand from Yahoo!">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "Introducing Livestand from Yahoo!", url: "http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-978"><a href="http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/" rel="bookmark" title="Permanent Link to Yahoo! Sketch-a-Search Updated with Dish Search">Yahoo! Sketch-a-Search Updated with Dish Search</a></h2>					<ul class="info">						<li class="time">Posted January 20th, 2011 at 2:11 pm by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/</script>-->					<div class="pmain">						<!-- spost --><p>Have a yen for tuna sashimi after watching Iron Chef? Are you out and about and have a sudden craving for Fish Tacos?</p>
<p>With our <a href="http://itunes.apple.com/us/app/yahoo-sketch-a-search/id361077521?mt=8">updated Yahoo! Sketch-a-Search app</a> for the iPhone, restaurants serving the dish you crave are <a href="../2010/11/18/yahoo-sketch-a-search-discover-top-restaurants/">at your fingertips</a>. We have added a Dish Filter that lets you search for restaurants serving specific menu items within a sketched location on the map &#8211; similar to the <a href="http://www.ysearchblog.com/2010/04/28/get-the-dish-that-you-wish/">feature we provide for desktop searches</a>.</p>
<p><a rel="attachment wp-att-982" href="http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/yahoosas_duw_1-2/"><img class="size-medium wp-image-982 alignnone" title="YahooSAS_DUW_1" src="http://ymobileblog.com/blog/wp-content/uploads/2011/01/YahooSAS_DUW_1-200x300.png" alt="YahooSAS_DUW_1" width="200" height="300" /></a><a rel="attachment wp-att-981" href="http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/yahoosas_duw_2-2/"><img class="alignleft size-medium wp-image-981" title="YahooSAS_DUW_2" src="http://ymobileblog.com/blog/wp-content/uploads/2011/01/YahooSAS_DUW_2-200x300.png" alt="YahooSAS_DUW_2" width="200" height="300" /></a></p>
<p>Go ahead &#8211; search and discover a great line up of restaurants!</p>
<p>Bon Appétit!</p>
<p>- Deepa Mahalingam, Sr. Product Manager, Mobile Search</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/#respond" title="Comment on Yahoo! Sketch-a-Search Updated with Dish Search">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "Yahoo! Sketch-a-Search Updated with Dish Search", url: "http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-941"><a href="http://ymobileblog.com/blog/2010/12/08/the-new-yahoo-local-for-mobile/" rel="bookmark" title="Permanent Link to The New Yahoo! Local for Mobile – With You Wherever You Are">The New Yahoo! Local for Mobile – With You Wherever You Are</a></h2>					<ul class="info">						<li class="time">Posted December 8th, 2010 at 12:28 pm by Kirk Lieb</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a>,  <a href="http://ymobileblog.com/blog/category/product/" title="View all posts in Product" rel="category tag">Product</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2010/12/08/the-new-yahoo-local-for-mobile/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2010/12/08/the-new-yahoo-local-for-mobile/</script>-->					<div class="pmain">						<!-- spost --><p>At Yahoo!, we’re all about harnessing the massive power of the web and the ever-expanding capabilities of connected devices to create engaging and personally relevant mobile experiences for our users. But we also know that sometimes, even when you have the world at your fingertips, it’s what’s right outside your door that matters to you most.</p>
<p>That’s why today we’re excited to make the new Yahoo! Local available in limited beta. With rich, hyper-local content, Yahoo! Local offers a uniquely personalized experience via the mobile web on iPhone and Android, enabling you to discover the best local news, deals, and events in your neck of the woods, all in one place.  Some of our top features include:</p>
<ul>
<li>Local News and Information – delivering relevant hyper-local neighborhood and city content created by the community, local publishers, and Yahoo! editors as well as content from the best local blogs and newspapers</li>
<li>Saving Money in Your Area – we’re integrating our recently launched Local Offers program to deliver the best locally relevant offers from nearby spas, restaurants and retailers so you save cash while you support local businesses</li>
<li>Powerful Technology – our geo-informatics and content enrichment platforms identify and geo-tag every news story that appears on every page down to the neighborhood level</li>
</ul>
<p>We’re beta testing the product in more than 30 neighborhoods and cities in the U.S., including San Francisco, Palo Alto, Mountain View, Sunnyvale, Brooklyn, NY as well as Birmingham, Ferndale, and Royal Oak in Michigan. You can access it by directing your mobile browser to: <a href="http://beta.local.yahoo.com">http://beta.local.yahoo.com</a>.</p>
<p><img class="aligncenter size-full wp-image-942" title="hyperlocal-hayes-main-mobile-screen" src="http://ymobileblog.com/blog/wp-content/uploads/2010/12/hyperlocal-hayes-main-mobile-screen.png" alt="hyperlocal-hayes-main-mobile-screen" width="377" height="785" /></p>
<p>Check out our <a href="http://ycorpblog.com/2010/12/08/hyperlocal/">Yodel Blog</a> for more details on the new Yahoo! Local and stay tuned as we expand to more neighborhoods and cities around the country to bring your community onto your phone.</p>
<p>Irv Henderson</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2010/12/08/the-new-yahoo-local-for-mobile/#respond" title="Comment on The New Yahoo! Local for Mobile – With You Wherever You Are">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2010/12/08/the-new-yahoo-local-for-mobile/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "The New Yahoo! Local for Mobile – With You Wherever You Are", url: "http://ymobileblog.com/blog/2010/12/08/the-new-yahoo-local-for-mobile/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-925"><a href="http://ymobileblog.com/blog/2010/11/30/2010-year-in-review-%e2%80%93-top-mobile-searches-of-the-year/" rel="bookmark" title="Permanent Link to 2010 Year in Review – Top Mobile Searches of the Year">2010 Year in Review – Top Mobile Searches of the Year</a></h2>					<ul class="info">						<li class="time">Posted November 30th, 2010 at 9:00 pm by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2010/11/30/2010-year-in-review-%e2%80%93-top-mobile-searches-of-the-year/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2010/11/30/2010-year-in-review-%e2%80%93-top-mobile-searches-of-the-year/</script>-->					<div class="pmain">						<!-- spost --><p>This is one of our <a href="http://yearinreview.yahoo.com/">favorite times of the year</a> at Yahoo!, because we get to take a look back at the big trends and moments in time to find out what fascinated people most, based on billions of web searches conducted across Yahoo!.  There are always surprises and 2010 was no exception.</p>
<p>This year, as connected devices continue to play a more prominent role in our lives, Yahoo! followed searchers to their mobile phones for a glimpse into what we’re searching for while we’re on-the-go.</p>
<p><strong>Top Mobile Searches on Yahoo! in 2010:</strong><a href="http://www.ysearchblog.com/blog/wp-content/uploads/JESS3_Yahoo_YIR-Graphic-v1.7-FINAL.JPG" target="_self"><img class="alignright size-medium wp-image-932" title="Year in Review" src="http://ymobileblog.com/blog/wp-content/uploads/2010/11/Year-in-Review2-191x300.jpg" alt="Year in Review" width="191" height="300" /></a></p>
<ol>
<li>NFL</li>
<li>Lady Gaga</li>
<li>Rihanna</li>
<li>Sandra Bullock</li>
<li>NBA</li>
<li>World Cup</li>
<li> Justin Bieber</li>
<li>American Idol</li>
<li>Winter Olympics</li>
<li>BP oil spill</li>
</ol>
<p>While the search topics aren’t vastly different on mobile than PC (five out of 10 were the same, but in different order), we do know that what varies is the type of content consumers want. People use their phones to search for quick, bite-size information on the most popular news of the day: NFL scores and stats, Justin Bieber lyrics, American Idol winners and the latest news on Sandra’s and Rihanna’s personal lives. It’s about getting just enough information to keep us informed on the latest trends while we are out and about.</p>
<p>In addition to sharing the top mobile search terms this year, we are also giving people a glimpse into the top searches across 16 different countries and on topics related to finances, sports, obsessions and <a href="http://www.ysearchblog.com/2010/11/30/year-in-review-2010/" target="_self">more</a>.</p>
<p>Enjoy the look back at what captivated us the most in 2010.</p>
<p>Cheers,</p>
<p>Irv Henderson</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2010/11/30/2010-year-in-review-%e2%80%93-top-mobile-searches-of-the-year/#respond" title="Comment on 2010 Year in Review – Top Mobile Searches of the Year">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2010/11/30/2010-year-in-review-%e2%80%93-top-mobile-searches-of-the-year/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "2010 Year in Review – Top Mobile Searches of the Year", url: "http://ymobileblog.com/blog/2010/11/30/2010-year-in-review-%e2%80%93-top-mobile-searches-of-the-year/" });</script></li>											</ul>				</div>				<div class="apost">					<br><h2 id="post-921"><a href="http://ymobileblog.com/blog/2010/11/22/mobile-devices-are-now-santa%e2%80%99s-and-everyone%e2%80%99s-little-helpers/" rel="bookmark" title="Permanent Link to Mobile Devices are Now Santa’s (and Everyone’s) Little Helpers">Mobile Devices are Now Santa’s (and Everyone’s) Little Helpers</a></h2>					<ul class="info">						<li class="time">Posted November 22nd, 2010 at 10:54 am by Brandon Boone</li>						<li class="categories">Categories: <a href="http://ymobileblog.com/blog/category/general/" title="View all posts in General" rel="category tag">General</a></li>					</ul><!--					<script showbranding="0" src=http://d.yimg.com/ds/badge.js badgetype="small">yahoo_mail_bl498:http://ymobileblog.com/blog/2010/11/22/mobile-devices-are-now-santa%e2%80%99s-and-everyone%e2%80%99s-little-helpers/</script>BUZZ: This on is actually fine, might replace with <script type="text/javascript"	src="http://d.yimg.com/ds/badge2.js"	badgetype="small">http://ymobileblog.com/blog/2010/11/22/mobile-devices-are-now-santa%e2%80%99s-and-everyone%e2%80%99s-little-helpers/</script>-->					<div class="pmain">						<!-- spost --><p>Every holiday season, consumers are looking for the easiest ways to go head-on with holiday shopping. The simple solution…smartphones. It is perhaps the most important and widely-used technology tool for today’s holiday shopper.</p>
<p>Yahoo! did some digging to figure out how consumers plan to use their smartphones during the upcoming holiday season that officially kicks-off this coming (black) Friday. The <em>Holiday Shopping Intentions and Attitudes Survey</em>¸ conducted with Ipsos OTX MediaCT, concludes that nearly two out of three Internet-enabled mobile phone owners will use their device while shopping this holiday season.  This is especially true (68%) among those who start their holiday shopping before and around Thanksgiving.</p>
<p>Below are some of the other more interesting points from the survey on how people will use their mobile devices while shopping:</p>
<p style="padding-left: 30px;">o   Using tools and apps, ranging from Map usage to mobile click-to-call features (44%)</p>
<p style="padding-left: 30px;">o   Communicating with others through mobile messaging (40%)</p>
<p style="padding-left: 30px;">o   Finding deals (32%)</p>
<p style="padding-left: 30px;">o   On-the-go comparison shopping for best prices (32%)</p>
<p style="padding-left: 30px;">o   Take/send pictures of potential purchases to share with others (29%)</p>
<p style="padding-left: 30px;">o   Read user reviews (20%)</p>
<p>Our <a href="http://mobile.yahoo.com/messenger" target="_self">Yahoo! Messenger</a>, <a href="http://mobile.yahoo.com/mail" target="_self">Yahoo! Mail</a> and <a href="http://mobile.yahoo.com/flickr" target="_self">Flickr</a> apps are our best holiday helpers this year. Whether you want face-to-face video calling to get your sister’s opinion on a sweater for mom, or to share photos along the way, all of these apps can be downloaded today to help ease your shopping worries later.</p>
<p>Good luck out there!</p>
<p>Cheers,</p>
<p>Ashmeed Ali</p>
<p>Strategic Research &amp; Insights</p>
						<!-- epost -->					</div>					<ul class="pfoot">						<li class="comments"><a href="http://ymobileblog.com/blog/2010/11/22/mobile-devices-are-now-santa%e2%80%99s-and-everyone%e2%80%99s-little-helpers/#respond" title="Comment on Mobile Devices are Now Santa’s (and Everyone’s) Little Helpers">0 Comment</a></li>						<li class="rss"><a href="http://ymobileblog.com/blog/feed/">Subscribe</a></li>						<li style="padding-top:2px">							<script type="text/javascript"								src="http://d.yimg.com/ds/badge2.js"								badgetype="small">http://ymobileblog.com/blog/2010/11/22/mobile-devices-are-now-santa%e2%80%99s-and-everyone%e2%80%99s-little-helpers/							</script>												</li>												<li><script type="text/javascript">SHARETHIS.addEntry({ title: "Mobile Devices are Now Santa’s (and Everyone’s) Little Helpers", url: "http://ymobileblog.com/blog/2010/11/22/mobile-devices-are-now-santa%e2%80%99s-and-everyone%e2%80%99s-little-helpers/" });</script></li>											</ul>				</div>							</div>

		</div>

		<div id="sidebarOne">


			<a href="http://mobile.yahoo.com/" class="backToYH">back to yahoo! mobile</a>

			<div class="sbColumns emailSubscribe">

				<h2>subscription options</h2>

				<div class="rss"></div>

				<div class="inner">

				<table cellspacing="10" align="center" style="text-align: center;">
				<tbody><tr>
				<td width="60px" style="text-align: center;">
				<a href="http://alerts.yahoo.com/edit_feedalert.php?url=http://ymobileblog.com/blog/feed/">
				<img src="/blog/wp-content/themes/Archive/images/sub_alerts.png"/>
				
				</a>
				</td>
				<td width="60px" style="text-align: center;">
				<a href="http://www.twitter.com/YahooMobile">
			<img src="/blog/wp-content/themes/Archive/images/sub_twitter.png"/>
			
				</a>
				</td>
				<td width="60px" style="text-align: center;">
				<a href="http://ymobileblog.com/blog/feed/">
<img src="/blog/wp-content/themes/Archive/images/sub_rss.png"/>

				</a>
				</td>
				</tr>
				<tr>
				<td style="text-align: center;">
				<a href="http://alerts.yahoo.com/edit_feedalert.php?url=http://ymobileblog.com/blog/feed/">Y! Alerts</a>
				</td>
				<td style="text-align: center;">
				<a href="http://www.twitter.com/YahooMobile">Twitter</a>
				</td>
				<td style="text-align: center;">
				<a href="http://ymobileblog.com/blog/feed/">RSS</a>
				</td>
				</tr>
				</tbody></table>

								</div>

			</div>
			
						<div class="sbColumns">

							<h2>Facebook Fans</h2>

							<div class="inner">
<iframe scrolling="no" frameborder="0" src="http://www.facebook.com/connect/connect.php?id=15358813305&connections=8&stream=0" allowtransparency="true" style="border: none; width: 269px; height: 250px;"></iframe>
							</div>

						</div> 

			<div class="sbColumns latestPosts">

				<h2>latest posts</h2>

				<div class="inner">

					<ul>



						<li>

							<h3><a href="http://ymobileblog.com/blog/2011/04/01/busted-brackets-yahoo-has-ncaa-tournament-madness-by-the-numbers/">Busted Brackets. Yahoo! has NCAA Tournament Madness by the Numbers</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2011/04/01/busted-brackets-yahoo-has-ncaa-tournament-madness-by-the-numbers/#respond" title="Comment on Busted Brackets. Yahoo! has NCAA Tournament Madness by the Numbers">0</a></li>

								<li>Posted on: April 1st, 2011</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/">Yahoo! Fantasy Baseball ’11 &#038; the Impact of Mobile on Fantasy Sports</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2011/03/31/yahoo-fantasy-baseball-%e2%80%9911-the-impact-of-mobile-on-fantasy-sports/#respond" title="Comment on Yahoo! Fantasy Baseball ’11 &amp; the Impact of Mobile on Fantasy Sports">0</a></li>

								<li>Posted on: March 31st, 2011</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2011/03/17/mobile-internet-%e2%80%93-delivering-on-the-promise-of-mobile-advertising/">Mobile Internet – Delivering on the Promise of Mobile Advertising</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2011/03/17/mobile-internet-%e2%80%93-delivering-on-the-promise-of-mobile-advertising/#respond" title="Comment on Mobile Internet – Delivering on the Promise of Mobile Advertising">0</a></li>

								<li>Posted on: March 17th, 2011</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/">Introducing Flickr for Windows 7 and Windows Phone 7!</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2011/03/01/introducing-flickr-for-windows-7-and-windows-phone-7/#respond" title="Comment on Introducing Flickr for Windows 7 and Windows Phone 7!">0</a></li>

								<li>Posted on: March 1st, 2011</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/">Delivering A Better Mobile Finance Tracking Experience</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2011/02/22/delivering-a-better-mobile-finance-tracking-experience/#respond" title="Comment on Delivering A Better Mobile Finance Tracking Experience">0</a></li>

								<li>Posted on: February 22nd, 2011</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/">Introducing Livestand from Yahoo!</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2011/02/10/introducing-livestand-from-yahoo/#respond" title="Comment on Introducing Livestand from Yahoo!">0</a></li>

								<li>Posted on: February 10th, 2011</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/">Yahoo! Sketch-a-Search Updated with Dish Search</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2011/01/20/yahoo-sketch-a-search-updated-with-dish-search/#respond" title="Comment on Yahoo! Sketch-a-Search Updated with Dish Search">0</a></li>

								<li>Posted on: January 20th, 2011</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2010/12/08/the-new-yahoo-local-for-mobile/">The New Yahoo! Local for Mobile – With You Wherever You Are</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2010/12/08/the-new-yahoo-local-for-mobile/#respond" title="Comment on The New Yahoo! Local for Mobile – With You Wherever You Are">0</a></li>

								<li>Posted on: December 8th, 2010</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2010/11/30/2010-year-in-review-%e2%80%93-top-mobile-searches-of-the-year/">2010 Year in Review – Top Mobile Searches of the Year</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2010/11/30/2010-year-in-review-%e2%80%93-top-mobile-searches-of-the-year/#respond" title="Comment on 2010 Year in Review – Top Mobile Searches of the Year">0</a></li>

								<li>Posted on: November 30th, 2010</li>

							</ul>

						</li>


						<li>

							<h3><a href="http://ymobileblog.com/blog/2010/11/22/mobile-devices-are-now-santa%e2%80%99s-and-everyone%e2%80%99s-little-helpers/">Mobile Devices are Now Santa’s (and Everyone’s) Little Helpers</a></h3>

							<ul>

								<li>Comments: <a href="http://ymobileblog.com/blog/2010/11/22/mobile-devices-are-now-santa%e2%80%99s-and-everyone%e2%80%99s-little-helpers/#respond" title="Comment on Mobile Devices are Now Santa’s (and Everyone’s) Little Helpers">0</a></li>

								<li>Posted on: November 22nd, 2010</li>

							</ul>

						</li>


					</ul>

				</div>

			</div>

			<div class="sbColumns">

				<h2>recent visitors</h2>

				<div class="inner">

				<script type="text/javascript" src="http://pub.mybloglog.com/comm2.php?mblID=2010012514413320&c_width=274&c_sn_opt=n&c_rows=3&c_img_size=f&c_heading_text=&c_color_heading_bg=FFFFFF&c_color_heading=666666&c_color_link_bg=FFFFFF&c_color_link=666666&c_color_bottom_bg=FFFFFF"></script>

					<a href="http://www.mybloglog.com/buzz/yjoin/?ref_id=2010012514413320&ref=w" class="join">JOIN OUR COMMUNITY</a>

				</div>

			</div>
			


			<div class="sbColumns">

				<h2>categories</h2>

				<div class="inner">

					<ul>

							<li class="cat-item cat-item-7"><a href="http://ymobileblog.com/blog/category/feature-releases/" title="View all posts filed under Feature Releases">Feature Releases</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://ymobileblog.com/blog/category/general/" title="View all posts filed under General">General</a>
</li>
	<li class="cat-item cat-item-10"><a href="http://ymobileblog.com/blog/category/ipad/" title="View all posts filed under iPad">iPad</a>
</li>
	<li class="cat-item cat-item-5"><a href="http://ymobileblog.com/blog/category/media-product/" title="View all posts filed under Media Product">Media Product</a>
</li>
	<li class="cat-item cat-item-4"><a href="http://ymobileblog.com/blog/category/mobile-advertising/" title="View all posts filed under Mobile Advertising">Mobile Advertising</a>
</li>
	<li class="cat-item cat-item-6"><a href="http://ymobileblog.com/blog/category/product/" title="View all posts filed under Product">Product</a>
</li>
	<li class="cat-item cat-item-9"><a href="http://ymobileblog.com/blog/category/search/" title="View all posts filed under Search">Search</a>
</li>
	<li class="cat-item cat-item-8"><a href="http://ymobileblog.com/blog/category/social-pulse/" title="View all posts filed under Social Pulse">Social Pulse</a>
</li>
	<li class="cat-item cat-item-11"><a href="http://ymobileblog.com/blog/category/yahoo-mobile-homepage/" title="View all posts filed under Yahoo! Mobile Homepage">Yahoo! Mobile Homepage</a>
</li>

					</ul>

				</div>

			</div>

			<div class="sbColumns">

				<div class="sbColumns sbColumns-21">

					<h2>archives</h2>

					<div class="inner">

						<ul>

								<li><a href='http://ymobileblog.com/blog/2011/04/' title='April 2011'>April 2011</a>&nbsp;(1)</li>
	<li><a href='http://ymobileblog.com/blog/2011/03/' title='March 2011'>March 2011</a>&nbsp;(3)</li>
	<li><a href='http://ymobileblog.com/blog/2011/02/' title='February 2011'>February 2011</a>&nbsp;(2)</li>
	<li><a href='http://ymobileblog.com/blog/2011/01/' title='January 2011'>January 2011</a>&nbsp;(1)</li>
	<li><a href='http://ymobileblog.com/blog/2010/12/' title='December 2010'>December 2010</a>&nbsp;(1)</li>
	<li><a href='http://ymobileblog.com/blog/2010/11/' title='November 2010'>November 2010</a>&nbsp;(5)</li>
	<li><a href='http://ymobileblog.com/blog/2010/10/' title='October 2010'>October 2010</a>&nbsp;(4)</li>
	<li><a href='http://ymobileblog.com/blog/2010/09/' title='September 2010'>September 2010</a>&nbsp;(2)</li>
	<li><a href='http://ymobileblog.com/blog/2010/08/' title='August 2010'>August 2010</a>&nbsp;(3)</li>
	<li><a href='http://ymobileblog.com/blog/2010/07/' title='July 2010'>July 2010</a>&nbsp;(4)</li>
	<li><a href='http://ymobileblog.com/blog/2010/06/' title='June 2010'>June 2010</a>&nbsp;(5)</li>
	<li><a href='http://ymobileblog.com/blog/2010/05/' title='May 2010'>May 2010</a>&nbsp;(4)</li>
	<li><a href='http://ymobileblog.com/blog/2010/04/' title='April 2010'>April 2010</a>&nbsp;(6)</li>
	<li><a href='http://ymobileblog.com/blog/2010/03/' title='March 2010'>March 2010</a>&nbsp;(7)</li>
	<li><a href='http://ymobileblog.com/blog/2010/02/' title='February 2010'>February 2010</a>&nbsp;(9)</li>

						</ul>

					</div>

				</div>

				<div class="sbColumns sbColumns-22">

					<h2>Yahoo! blogs</h2>

					<div class="inner">

						<ul>

							<li><a href="http://ycorpblog.com" target="_blank">Yodel Anecdotal</a></li>
<li><a href="http://upcoming.yahoo.com/news">Upcoming.org News</a></li>
<li><a href="http://yanswersblog.com" target="_blank">Y! Answers blog</a></li>
<li><a href="http://mybloglogb.typepad.com/my_weblog" target="_blank">MyBlogLog blog</a></li>
<li><a href="http://next.yahoo.net" target="_blank">Next* blog</a></li>
<li><a href="http://blog.flickr.net/en" target="_blank">flickr blog</a></li>
<li><a href="http://blog.delicious.com">del.icio.us blog</a></li>
<li><a href="http://buzz.yahoo.com/buzzlog" target="_blank">Y! Buzz log</a></li>
<li><a href="http://yfinanceblog.com" target="_blank">Y! Finance blog</a></li>
<li><a href="http://ygroupsblog.com/blog" target="_blank">Y! Groups blog</a></li>
<li><a href="http://ymailblog.com" target="_blank">Y! Mail blog</a></li>
<li><a href="http://ymailuk.com/blog1" target="_blank">Y! Mail UKIE blog</a></li>
<li><a href="http://ymessengerblog.com" target="_blank">Y! Messenger blog</a></li>
<li><a href="http://new.music.yahoo.com/blogs" target="_blank">Y! Music blogs</a></li>
<li><a href="http://dating.personals.yahoo.com/singles" target="_blank">Y! Personals blog</a></li>
<li><a href="http://yuiblog.com/blog" target="_blank">Y! UI blog</a></li>
<li><a href="http://www.yanalyticsblog.com" target="_blank">Y! Web Analytics Blog</a></li>
<li><a href="http://produce.yahoo.com/jhudson/iphone_messenger/iphone%20messenger%20video%20final.mov">messenger for iPhone II</a></li>

						</ul>

					</div>

				</div>

			</div>



		</div>

		<div class="extra"></div> <!-- jump on :hover -->

	</div>

		<div id="footer">
	<!--
	<p>&copy; 2011 Yahoo! Mobile Blog. Powered by <a href="http://www.wordpress.org">Wordpress</a> and <a href="http://www.uniqueblogdesigns.com">Blog Design</a>.</p>
	-->

	<p>Copyright &copy; 2011 Yahoo! Inc. All rights reserved. <a href="http://info.yahoo.com/privacy/us/yahoo/details.html">Privacy Policy</a> - <a href="http://info.yahoo.com/legal/us/yahoo/utos/utos-173.html">Terms of Service</a>
		<br/>
		Powered by <a href="http://wordpress.org">WordPress</a> on <a href="http://smallbusiness.yahoo.com/webhosting/">Yahoo! Web Hosting</a>.</p>

	</div>

<script type="text/javascript" language="JavaScript">
//<![CDATA[
var wpdone;
function wpvisit()
{
  var z;
  z="&r="+escape(document.referrer);
  z=z+"&b="+escape(navigator.appName+" "+navigator.appVersion);
  w=parseFloat(navigator.appVersion);
  if (w > 2.0) {
    z=z+"&s="+screen.width+"x"+screen.height;
    z=z+"&o="+navigator.platform;
    v="1.2";
    if (navigator.appName != "Netscape") {
      z=z+"&c="+screen.colorDepth;
    } else {
      z=z+"&c="+screen.pixelDepth
    }
    z=z+"&j="+navigator.javaEnabled();
  } else {
    v=1.0;
  }
  z=z+"&v="+v;

  document.writeln("<img border=\"0\" src=\"http://visit.webhosting.yahoo.com/wisit.gif"+"/"+"?"+z+"\" />");
}
  wpvisit();
//]]>
</script>

<noscript><img src="http://visit.webhosting.yahoo.com/wisit.gif?1302016343" border="0" width="1" height="1" alt="visit" /></noscript>
			<!-- tracker added by Yahoo Analytics plugin v0.1.8: http://www.rudishumpert.com/projects/ -->
			
			<!-- Yahoo! Web Analytics - All rights reserved -->
			<script type="text/javascript" src="http://d.yimg.com/mi/ywa.js"></script>
			<script type="text/javascript">
			var YWATracker = YWA.getTracker("10001655977415");
			YWATracker.setDocumentName("Blog Home");
			YWATracker.setDocumentGroup("Blog Home");
			YWATracker.setDomains("*.ymobileblog.com");
			YWATracker.setCF(1,"No");
			
			
			
			YWATracker.submit();
			</script>
			
			<noscript>
			<div><img src="http://a.analytics.yahoo.com/p.pl?a=10001655977415&amp;js=no/p.pl?a=10001655977415&amp;js=no" width="1" height="1" alt="" /></div>
			</noscript>
			
			

</div>

</body>

</html>