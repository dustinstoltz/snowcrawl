<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title> Official Yahoo! Store Blog: Tips and Tricks for Ecommerce Success </title>

<meta name="generator" content="WordPress 2.9.2" /> <!-- leave this for stats -->
<link rel="shortcut icon" href="http://ystoreblog.com/favicon.ico" />
<link rel="stylesheet" href="http://www.ystoreblog.com/blog/wp-content/themes/ystore/style.css" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="Official Yahoo! Store Blog: Tips and Tricks for Ecommerce Success RSS Feed" href="http://www.ystoreblog.com/blog/feed/" />
<link rel="pingback" href="http://www.ystoreblog.com/blog/xmlrpc.php" />

<style>
.catlistmain{
	font-family: Georgia, "Times New Roman", Times, serif;	
	color:#FFFFFF;
}
.catlistmain a{		
	text-decoration:underline;
}

.catlistmain a:hover{	
	text-decoration:none;
}
#catlist{
	position:absolute;
	visibility:hidden;
	overflow:auto;
	height:430px;
	width:280px;
	z-index:1;
	text-align:left;
	padding:2px;
	left:100px;
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size:12px;
}
#catlist li {
margin:0 0 0 2px;
padding:2px 0 2px 0px;
}
#catlist a:hover {
	text-decoration:none;
}

</style>

<link rel='stylesheet' id='wp-email-css'  href='http://www.ystoreblog.com/blog/wp-content/plugins/wp-email/email-css.css?ver=2.50' type='text/css' media='all' />
<script type='text/javascript' src='http://www.ystoreblog.com/blog/wp-includes/js/jquery/jquery.js?ver=1.3.2'></script>
<style type="text/css">img.nothumb{border:0;margin:0 0 0 2px !important;}</style>
<!-- Tweet This v1.8.1 b035 mode delta -->
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.ystoreblog.com/blog/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.ystoreblog.com/blog/wp-includes/wlwmanifest.xml" /> 
<link rel='index' title='Official Yahoo! Store Blog: Tips and Tricks for Ecommerce Success' href='http://www.ystoreblog.com/blog' />
<meta name="generator" content="WordPress 2.9.2" />
<link rel="stylesheet" href="http://www.ystoreblog.com/blog/wp-content/plugins/Contributor-PS/contributor-PS-css.css" type="text/css" media="screen" />
<script language="javascript" type="text/javascript">
<!--
		function collapseThread( theId ) {
			var comment = document.getElementById(theId);
			if(!comment)
			{
				alert("ERROR:\nThe document structure is different\nfrom what Threaded Comments expects.\nYou are missing the element '"+theId+"'");
				return;
			}
			var theBody = findBody(comment);
			if(comment.className.indexOf("collapsed") > -1) {
				comment.className = comment.className.replace(" collapsed", "");;
			} else {
				comment.className += " collapsed";
			}
		}

		function expandThread( theId ) {
			var comment = document.getElementById(theId);
			if(!comment)
			{
				alert("ERROR:\nThe document structure is different\nfrom what Threaded Comments expects.\nYou are missing the element '"+theId+"'");
				return;
			}
			var theBody = findBody(comment);
			if(comment.className.indexOf("collapsed") > -1) {
				comment.className = comment.className.replace(" collapsed", "");;
			} 
		}
		
		function findBody(el)
		{
			var divs = el.getElementsByTagName("div");
			var ret;
			for(var i = 0; i < divs.length; ++i) {
				if(divs.item(i).className.indexOf("body") > -1)
					return divs.item(i);
			}
			return false;
		}
	
		function onAddComment() {
			//checkDocumentIntegrity();
			var el = document.getElementById("commentform");
			// Future release: Check if form is filled correctly and mark the form fields.
			el.submit();
		}
		
		function moveAddCommentBelow(theId, threadId, collapse)
		{
			expandThread( theId );
			var addComment = document.getElementById("addcomment");
			if(!addComment)
			{
			  	alert("ERROR:\nThreaded Comments can't find the 'addcomment' div.\nThis is probably because you have changed\nthe comments.php file.\nMake sure there is a tag around the form\nthat has the id 'addcomment'"); 
				return
			}
			var comment = document.getElementById(theId);
			/*
			if(collapse)
			{
				for(var i = 0; i < comment.childNodes.length; ++i) {
					var c = comment.childNodes.item(i);
					if(typeof(c.className) == "string" && c.className.indexOf("collapsed")<0) {
						c.className += " collapsed";
					}
				}
			}
			addComment.parentNode.removeChild(addComment);

			comment.appendChild(addComment);
			*/
			if(comment.className.indexOf("alt")>-1) {
				addComment.className = addComment.className.replace(" alt", "");					
			} else {
				addComment.className += " alt";
			}
		        var replyId = document.getElementById("comment_reply_ID");
			if(replyId == null)
			{
				alert("Brians Threaded Comments Error:\nThere is no hidden form field called\n'comment_reply_ID'. This is probably because you\nchanged the comments.php file and forgot\nto include the field. Please take a look\nat the original comments.php and copy the\nform field over.");
			}
			replyId.value = threadId;
			var reRootElement = document.getElementById("reroot");
			if(reRootElement == null)
			{
				alert("Brians Threaded Comments Error:\nThere is no anchor tag called 'reroot' where\nthe comment form starts.\nPlease compare your comments.php to the original\ncomments.php and copy the reroot anchor tag over.");
			}
			reRootElement.style.display = "block";
			var aTags = comment.getElementsByTagName("A");
			var anc = aTags.item(0).id;
			//document.location.href = "#"+anc;
			document.getElementById("comment").focus();
		}

		function checkDocumentIntegrity()
		{
			str = "";
			
			str += checkElement("reroot","div tag");
			str += checkElement("addcomment", "div tag");
			str += checkElement("comment_reply_ID", "hidden form field");
			str += checkElement("content", "div tag");
			str += checkElement("comment", "textfield");
			str += checkElement("addcommentanchor", "anchor tag");
			
			if(str != "")
			{
				str = "Brian's Threaded Comments are missing some of the elements that are required for it to function correctly.\nThis is probably the because you have changed the original comments.php that was included with the plugin.\n\nThese are the errors:\n" + str;
				str += "\nYou should compare your comments.php with the original comments.php and make sure the required elements have not been removed.";

				alert(str);
			}
		}
               
		function checkElement(theId, elDesc)
		{
			var el = document.getElementById(theId);
			if(!el)
			{
				if(elDesc == null)
					elDesc = "element";
				return "- The "+elDesc+" with the ID '" +theId + "' is missing\n"; 
			}
			else 
				return "";
		}
		
		function reRoot()
		{
			var addComment = document.getElementById("addcomment");			
			var reRootElement = document.getElementById("reroot");
			reRootElement.style.display = "none";
			/*
			var content = document.getElementById("content");
			addComment.parentNode.removeChild(addComment);
			content.appendChild(addComment);
			addComment.className = addComment.className.replace(" alt", "");
			*/
			document.location.href = "#addcommentanchor";
			document.getElementById("comment").focus();
			document.getElementById("comment_reply_ID").value = "0";
		}			
		
		function changeCommentSize(d)
		{
			var el = document.getElementById("comment");
			var height = parseInt(el.style.height);
			if(!height && el.offsetHeight)
				height = el.offsetHeight;
			height += d;
			if(height < 20) 
				height = 20;
			el.style.height = height+"px";
		}		
-->
</script>
<style type="text/css">
.comment 
{
	position: 				relative;
	margin:					3px;
	margin-top:				6px;
/*	border: 				1px solid #666; */
	padding:				4px 4px 4px 8px;
	background-color:		#fff;
}

.odd
{
	background-color: #f8f8f8;
}

.comment div {
	position: 				relative;
}

.comment .comment img
{
	margin: 				0px;
}

.comment .collapseicon 
{
	width: 					13px;
	height: 				13px;
	overflow:				hidden;
	/* Modified by Shishir "sisir48@yahoo.com" to fix collapse/expand image in IE 6.0
	background-image: 		url(http://www.ystoreblog.com/blog/wp-content/plugins/briansthreadedcomments.php?image=subthread-open.png);
	*/
	background-image: 		url(http://www.ystoreblog.com/blog/wp-content/plugins/briansthreadedcomments/images/subthread-open.gif);
}

.collapsed .collapseicon 
{
	/* Modified by Shishir "sisir48@yahoo.com" to fix collapse/expand image in IE 6.0
	background-image: 		url(http://www.ystoreblog.com/blog/wp-content/plugins/briansthreadedcomments.php?image=subthread.png);
	*/
	background-image: 		url(http://www.ystoreblog.com/blog/wp-content/plugins/briansthreadedcomments/images/subthread.gif);
}


.comment .reply {
	text-align: 			right;
	font-size: 				80%;
	padding: 				0px 6px 6px 0px;
}

.comment
{
	border: 	1px solid #ddd;
	margin-top: 			10px;
}

input#subscribe
{
	width: auto;
}

.comment .body .content
{
	padding:				0px 3px 0px 3px;
	width: 					100%;	
	overflow: 				auto; 
}

.comment .title abbr
{
	border: none;
}

.collapsed .body, .collapsed .comment
{
	display:				none;
}
/*
#addcomment small, #addcomment div
{
	padding:				3px;
}
*/
#commentform textarea {
	width: 97%;
}
</style>
<script type="text/javascript">
  window.fbAsyncInit = function() {
    FB.init({appId: '113869198637480', status: true, cookie: true, xfbml: true});
  };
  (function() {
    var e = document.createElement('script'); e.async = true;
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    document.getElementById('fb-root').appendChild(e);
  }());
</script>

<!-- Start Of Script Generated By WP-PostRatings 1.10 -->
<script type='text/javascript' src='http://www.ystoreblog.com/blog/wp-includes/js/tw-sack.js?ver=1.6.1'></script>
<script type='text/javascript' src='http://www.ystoreblog.com/blog/wp-content/plugins/postratings/postratings-js.php?ver=1.10'></script>
<link rel="stylesheet" href="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/postratings-css.css" type="text/css" media="screen" />
<!-- End Of Script Generated By WP-PostRatings 1.10 -->
<script language="javascript" src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/js/jsfunctions.js"></script>
<script type="text/JavaScript">
function showhidetab(aDiv) { 
var Tab1 = document.getElementById('tabcontent3').style; 
var Tab2 = document.getElementById('tabcontent4').style; 
var top1 = document.getElementById('homtab1').style; 
var top2 = document.getElementById('homtab2').style; 
if(aDiv=='tabcontent3')
	{
	Tab1.display =''; 
	Tab2.display ='none'; 
	top1.display =''; 
	top2.display ='none'; 
	}
else if(aDiv=='tabcontent4')
{
	Tab2.display =''; 
	Tab1.display ='none'; 	
	top2.display =''; 
	top1.display ='none'; 
	}
}
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <table width="821" border="0" cellspacing="0" cellpadding="0" align="center" class="content_bg">
      <tr>
        <td valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="header_bg"><a href="http://www.ystoreblog.com/blog"><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/header.jpg" width="573" height="151" border="0" /></a></td>
          </tr>
          <tr>
            <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="14">&nbsp;</td>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/header_shadow.jpg" border="0" /></td>
                  </tr>


<!-- Header ends -->
                												  <tr>
                    <td>
                    <div class="post">
                    <h2 class="post_ttl"><a href="http://www.ystoreblog.com/blog/2011/03/social-media-sharing-lets-customers-spread-the-word/" rel="bookmark">Social Media Sharing Lets Customers Spread the Word</a></h2>
                    <p class="post_date">Wednesday, March 30th, 2011</p>
                    <p>Early this morning, we released a new <a href="http://help.yahoo.com/l/us/yahoo/smallbusiness/store/edit/social/" target="_blank">Social Media Sharing</a> feature that lets you add &quot;Share on Twitter&quot; and Facebook &quot;Like&quot; options to your store&#8217;s product pages. Enabling this feature allows your customers to easily spread the word about your products to their social media networks. This boost to your marketing efforts can help you reach a wider audience, which in turn can help to generate more quality traffic for your store.</p>
<p>The Social Media Sharing feature is available for all merchants using <a href="http://help.yahoo.com/l/us/yahoo/smallbusiness/store/edit/templates/templates-55.html" target="_blank">Editor 3.0 templates</a>, and can be installed by running the <a href="http://help.yahoo.com/l/us/yahoo/smallbusiness/store/edit/social/social-02.html" target="_blank">Design Wizard</a>, or by using the <a href="http://help.yahoo.com/l/us/yahoo/smallbusiness/store/edit/social/social-03.html" target="_blank">Store Editor Variables page</a> and adding CSS to your store&#8217;s css-edits file. If you&#8217;re using custom templates, you can install this feature by inserting <a href="http://help.yahoo.com/l/us/yahoo/smallbusiness/store/edit/social/social-04.html" target="_blank">a new RTML operator</a> into your store templates, and adding CSS [<a href="http://www.ystoreblog.com/blog/2011/03/social-media-sharing-lets-customers-spread-the-word">...</a>]</p>
                    <p class="post_info"><a href="http://www.ystoreblog.com/blog/2011/03/social-media-sharing-lets-customers-spread-the-word/">Read Complete Post</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.ystoreblog.com/blog/2011/03/social-media-sharing-lets-customers-spread-the-word/#respond" title="Comment on Social Media Sharing Lets Customers Spread the Word">Comments (0)</a></p>
                    </div>
                    </td>
                  </tr>
				                    <tr>
                  	<td>
                    	<table style="margin: 8px 0pt 0pt;" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody><tr>
                        <td class="lt_lt_latest" valign="top">
                        
                        <table cellpadding="0" cellspacing="0" border="0" class="categoryHolderTbl">
                        <tr>
                        <td>
                        <!-- Recent Post -->
                                                                        <div class="cat_listing">
                            <table class="ttlBlue_bg_latest" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody><tr>
                            
                                <td class="categoryTtlTxt">Recent Posts</td>
                                <td width="55">&nbsp;</td>
                            
                              </tr>
                            </tbody></table>	
                            <ul class="cat_list_latest">
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/03/social-media-sharing-lets-customers-spread-the-word/" rel="bookmark">Social Media Sharing Lets Customers Spread the Word</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/03/save-the-dates-upcoming-yahoo-small-business-events/" rel="bookmark">Save the Dates — Upcoming Yahoo! Small Business Events</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/03/seo-for-ecommerce-advice-from-rob-snell/" rel="bookmark">SEO for Ecommerce: Advice from Rob Snell</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/03/listen-to-small-business-tips-from-yahoo-small-business-tom-byun/" rel="bookmark">Listen to Small Business Tips From Yahoo! Small Business&#8217; Tom Byun</a></li>
                                                            </ul>
                        
                        </div>
                                                <!-- Recent Post -->
                        
                        <!-- News and Announcement Post -->
                                                                        <div class="cat_listing">
                            <table class="ttlBlue_bg_latest" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody><tr>
                            
                                <td class="categoryTtlTxt">News &amp; Announcements</td>
                                <td width="55" class="viewAll"><a href="http://www.ystoreblog.com/blog/?cat=6">view all</a></td>
                            
                              </tr>
                            </tbody></table>	
                            <ul class="cat_list_latest">
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/03/social-media-sharing-lets-customers-spread-the-word/" rel="bookmark">Social Media Sharing Lets Customers Spread the Word</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/02/dig-into-your-customer-data-with-customer-manager/" rel="bookmark">Dig Into Your Customer Data With Customer Manager</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2010/08/yahoo-search-marketing-conversion-tags-and-microsoft-advertising-adcenter-transition/" rel="bookmark">Yahoo! Search Marketing Conversion Tags and Microsoft Advertising adCenter Transition</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2010/08/introducing-customer-registration/" rel="bookmark">Introducing Customer Registration</a></li>
                                                            </ul>
                        
                        </div>
                                                <!-- News and Announcement Post -->
                        
                        <!-- Best Practices Post -->
                                                                        <div class="cat_listing">
                            <table class="ttlBlue_bg_latest" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody><tr>
                            
                                <td class="categoryTtlTxt">Best Practices</td>
                                <td width="55" class="viewAll"><a href="http://www.ystoreblog.com/blog/?cat=5">view all</a></td>
                            
                              </tr>
                            </tbody></table>	
                            <ul class="cat_list_latest">
                                                                <li><a href="http://www.ystoreblog.com/blog/2010/09/countdown-to-the-summit-maximizing-your-holiday-routine/" rel="bookmark">Countdown to the Summit: Maximizing Your Holiday Routine</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2010/08/one-way-to-assess-your-mobile-strategy-today/" rel="bookmark">One way to assess your mobile strategy, today</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2010/06/countdown-to-the-summit-searching-for-profits-in-the-right-place/" rel="bookmark">Countdown to the Summit: Searching for Profits in the Right Place</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2010/05/countdown-to-the-summit-formulating-your-perpetual-growth-strategy/" rel="bookmark">Countdown to the Summit: Formulating Your Perpetual Growth Strategy</a></li>
                                                            </ul>
                        
                        </div>
                                                <!-- Best Practices Post -->
                        
                        <!-- Getting Started Post -->
                                                                        <div class="cat_listing">
                            <table class="ttlBlue_bg_latest" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody><tr>
                            
                                <td class="categoryTtlTxt">Getting Started</td>
                                <td width="55" class="viewAll"><a href="http://www.ystoreblog.com/blog/?cat=1">view all</a></td>
                            
                              </tr>
                            </tbody></table>	
                            <ul class="cat_list_latest">
                                                                <li><a href="http://www.ystoreblog.com/blog/2009/08/free-webinar-weekend-how-to-bring-new-life-to-your-website/" rel="bookmark">Free Webinar Weekend: How to Bring New Life to Your Website</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2009/06/the-danger-of-relying-on-price-promotions-to-boost-conversion-rates/" rel="bookmark">The Danger of Relying on Price Promotions to Boost Conversion Rates</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2008/11/feature-highlight-customer-ratings/" rel="bookmark">Feature Highlight: Customer Ratings</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2008/10/what-can-you-do-with-yahoo-web-analytics/" rel="bookmark">What can you do with Yahoo! Web Analytics?</a></li>
                                                            </ul>
                        
                        </div>
                                                <!-- Getting Started Post -->
                        
                        <!-- SEO/Search Marketing Post -->
                                                                        <div class="cat_listing">
                            <table class="ttlBlue_bg_latest" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody><tr>
                            
                                <td class="categoryTtlTxt">SEO/Search Marketing</td>
                                <td width="55" class="viewAll"><a href="http://www.ystoreblog.com/blog/?cat=3">view all</a></td>
                            
                              </tr>
                            </tbody></table>	
                            <ul class="cat_list_latest">
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/03/seo-for-ecommerce-advice-from-rob-snell/" rel="bookmark">SEO for Ecommerce: Advice from Rob Snell</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/03/seo-friday-articles-to-kick-off-your-weekend/" rel="bookmark">SEO Friday: Articles to Kick Off Your Weekend</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/02/read-rob-snells-ecom-experts-transcript/" rel="bookmark">Read Rob Snell&#8217;s eCom Experts Transcript</a></li>
                                                                <li><a href="http://www.ystoreblog.com/blog/2011/02/rob-snell-talks-seo-on-ecom-experts/" rel="bookmark">Rob Snell Talks SEO on eCom Experts</a></li>
                                                            </ul>
                        
                        </div>
                                                <!-- SEO/Search Marketing Post -->
                        </td>
                        </tr>
                        </table>
                        
                        </td>
                        <td class="lt_rt_latest" valign="top" width="200">
                        <div class="poll_latest">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td class="pollTtl" >Yahoo! Store Blog Poll<span class="pollTtl" style="font-size: 12px;">
                                  <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="160" height="220">
                                  <param name="movie" value="http://www.ystoreblog.com/ystore-poll/poll/poll.swf?homepath=http://www.ystoreblog.com/ystore-poll/">
                                  <EMBED src="http://www.ystoreblog.com/ystore-poll/poll/poll.swf?homepath=http://www.ystoreblog.com/ystore-poll/" width="160" height="220" type="application/x-shockwave-flash" bgcolor="#F3F8FB" quality="high"></EMBED>
                                  </object>
                                </span></td>
                            </tr>
                            <tr>
                                <td valign="top">&nbsp;</td>
                            </tr>
                            </tbody>
                            </table>
                        </div>
                                                                        <div class="events_latest">Upcoming Events
                          <ul>
                                                        <li><!-- <span class="e_date"></span><br /> -->
                            <a href="http://www.ystoreblog.com/blog/2011/03/save-the-dates-upcoming-yahoo-small-business-events/" rel="bookmark">Save the Dates — Upcoming Yahoo! Small Business Events</a>
                            </li>
                                                        <li><!-- <span class="e_date"></span><br /> -->
                            <a href="http://www.ystoreblog.com/blog/2011/03/listen-to-small-business-tips-from-yahoo-small-business-tom-byun/" rel="bookmark">Listen to Small Business Tips From Yahoo! Small Business&#8217; Tom Byun</a>
                            </li>
                                                        <li class="viewAll"><a href="http://www.ystoreblog.com/blog/?cat=16">view all</a></li>
                            </ul>
                        
                        </div>
                                                </td>
                        </tr>
                        </tbody>
                        </table>

                    </td>
                  </tr>
                </table>
                </td>
              </tr>
            </table>

            </td>
          </tr>
        </table>

        </td>
        <td valign="top" width="248">
		<!-- side bar rt starts -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="rightcol_top">&nbsp;</td>
          </tr>
          <tr>
            <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="12" valign="top" class="rightcol_ltBar"><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/rightcol_ltBar.gif" /></td>
                <td class="rightcol_bg" valign="top">
                <div id="sidebar">
 <div class="sidebar_list" id="logo_yahoo">
 	<h3 class="ttl_Sidebar" id="ttl_Sidebar_130">Categories</h3>
		<ul>
			<li class="cat-item cat-item-5"><a href="http://www.ystoreblog.com/blog/category/best-practices/" title="ecommerce best practices">Best Practices</a>
</li>
	<li class="cat-item cat-item-12"><a href="http://www.ystoreblog.com/blog/category/customer-service/" title="View all posts filed under Customer Service">Customer Service</a>
</li>
	<li class="cat-item cat-item-19"><a href="http://www.ystoreblog.com/blog/category/developer-spotlight/" title="View all posts filed under Developer Spotlight">Developer Spotlight</a>
</li>
	<li class="cat-item cat-item-16 current-cat"><a href="http://www.ystoreblog.com/blog/category/events/" title="View all posts filed under Events">Events</a>
</li>
	<li class="cat-item cat-item-4"><a href="http://www.ystoreblog.com/blog/category/general/" title="Even Jeopardy needs a Potpourri category">General</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://www.ystoreblog.com/blog/category/getting-started-cs/" title="Tips to help ecommerce merchants get open and selling quicker">Getting Started</a>
</li>
	<li class="cat-item cat-item-10"><a href="http://www.ystoreblog.com/blog/category/holidays/" title="Holiday and seasonal selling tips and advice for ecommerce sites">Holidays</a>
</li>
	<li class="cat-item cat-item-8"><a href="http://www.ystoreblog.com/blog/category/marketingpromotion/" title="Tips, Tricks, Advice and Best Practices for marketing and promoting ecommerce sites">Marketing/Promotion</a>
</li>
	<li class="cat-item cat-item-7"><a href="http://www.ystoreblog.com/blog/category/merchant-questions/" title="Questions from Yahoo! merchants about ecommerce topics">Merchant Questions</a>
</li>
	<li class="cat-item cat-item-6"><a href="http://www.ystoreblog.com/blog/category/news-announcements/" title="News and Announcements for Yahoo! Merchant Solutions, ecommerce by Yahoo! Small Business">News &amp; Announcements</a>
</li>
	<li class="cat-item cat-item-14"><a href="http://www.ystoreblog.com/blog/category/resources/" title="View all posts filed under Resources">Resources</a>
</li>
	<li class="cat-item cat-item-2"><a href="http://www.ystoreblog.com/blog/category/rtml/" title="Tips and tricks for creative and efficient use of RTML in Yahoo! stores">RTML</a>
</li>
	<li class="cat-item cat-item-3"><a href="http://www.ystoreblog.com/blog/category/seosem/" title="Improve organic search rankings, better performing PPC campaigns for ecommerce sites">SEO/SEM</a>
</li>
	<li class="cat-item cat-item-26"><a href="http://www.ystoreblog.com/blog/category/social-media/" title="View all posts filed under Social Media">Social Media</a>
</li>
	<li class="cat-item cat-item-15"><a href="http://www.ystoreblog.com/blog/category/training/" title="View all posts filed under Training">Training</a>
</li>
	<li class="cat-item cat-item-11"><a href="http://www.ystoreblog.com/blog/category/tricks/" title="View all posts filed under Tricks">Tricks</a>
</li>
	<li class="cat-item cat-item-20"><a href="http://www.ystoreblog.com/blog/category/yahoo-web-analytics/" title="View all posts filed under Yahoo! Web Analytics">Yahoo! Web Analytics</a>
</li>
		</ul>                    
</div>
<div class="sidebar_list">
 	<h3 class="ttl_Sidebar"><a href="http://smallbusiness.yahoo.com/" target="_blank">Yahoo! Small Business</a></h3>
     <ul>
			<li><A href="http://smallbusiness.yahoo.com/ecommerce/" target="_blank">Sell Online</A></li>
			<li><A href="http://smallbusiness.yahoo.com/webhosting/" target="_blank">Get a Website</A></li>	
			<li><A href="http://smallbusiness.yahoo.com/domains/" target="_blank">Domain Names</A></li>
			<li><A href="http://smallbusiness.yahoo.com/email/" target="_blank">Business Email</A></li>
     </ul>
                    
     <form name="frmmarchive">
		<select name="cbarchives" class="select_archives" style="width:160px;" onchange="SubmitThisArchieve(document.frmmarchive,'http://www.ystoreblog.com/blog')">
     	<option value="">Archives</option>
                  <optgroup label="2011">
         <option value="201101" >January</option>
			<option value="201102" >February</option>
			<option value="201103" >March</option>
			<option value="201104" >April</option>
			<option value="201105" >May</option>
			<option value="201106" >June</option>

			<option value="201107" >July</option>
			<option value="201108" >August</option>
			<option value="201109" >September</option>
			<option value="201110" >October</option>
			<option value="201111" >November</option>
			<option value="201112" >December</option>                        
			</optgroup>
                  <optgroup label="2010">
         <option value="201001" >January</option>
			<option value="201002" >February</option>
			<option value="201003" >March</option>
			<option value="201004" >April</option>
			<option value="201005" >May</option>
			<option value="201006" >June</option>

			<option value="201007" >July</option>
			<option value="201008" >August</option>
			<option value="201009" >September</option>
			<option value="201010" >October</option>
			<option value="201011" >November</option>
			<option value="201012" >December</option>                        
			</optgroup>
                  <optgroup label="2009">
         <option value="200901" >January</option>
			<option value="200902" >February</option>
			<option value="200903" >March</option>
			<option value="200904" >April</option>
			<option value="200905" >May</option>
			<option value="200906" >June</option>

			<option value="200907" >July</option>
			<option value="200908" >August</option>
			<option value="200909" >September</option>
			<option value="200910" >October</option>
			<option value="200911" >November</option>
			<option value="200912" >December</option>                        
			</optgroup>
                  <optgroup label="2008">
         <option value="200801" >January</option>
			<option value="200802" >February</option>
			<option value="200803" >March</option>
			<option value="200804" >April</option>
			<option value="200805" >May</option>
			<option value="200806" >June</option>

			<option value="200807" >July</option>
			<option value="200808" >August</option>
			<option value="200809" >September</option>
			<option value="200810" >October</option>
			<option value="200811" >November</option>
			<option value="200812" >December</option>                        
			</optgroup>
                  <optgroup label="2007">
         <option value="200701" >January</option>
			<option value="200702" >February</option>
			<option value="200703" >March</option>
			<option value="200704" >April</option>
			<option value="200705" >May</option>
			<option value="200706" >June</option>

			<option value="200707" >July</option>
			<option value="200708" >August</option>
			<option value="200709" >September</option>
			<option value="200710" >October</option>
			<option value="200711" >November</option>
			<option value="200712" >December</option>                        
			</optgroup>
                  <optgroup label="2006">
         <option value="200601" >January</option>
			<option value="200602" >February</option>
			<option value="200603" >March</option>
			<option value="200604" >April</option>
			<option value="200605" >May</option>
			<option value="200606" >June</option>

			<option value="200607" >July</option>
			<option value="200608" >August</option>
			<option value="200609" >September</option>
			<option value="200610" >October</option>
			<option value="200611" >November</option>
			<option value="200612" >December</option>                        
			</optgroup>
              </select>
		</form>
 </div>
                <div class="sidebar_tab">
                	<!-- Right Tab Function-->
                    <div id="homtab1" style="display:; background-color:#FFFFFF; float:left; width:217px; border:0; margin:0; padding:0;">
                    <div id="tablis"><input type="image" src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/tab_TopTags_1.gif" width="91" height="24" border="0" onclick="showhidetab('tabcontent3'); return false;"/></div>
                    <div id="tabind"><input type="image" src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/tab_MostPopular_0.gif" width="126" height="24" border="0" onclick="showhidetab('tabcontent4'); return false;"/></div>
                    </div>

                    <div id="homtab2" style="display:none; background-color:#FFFFFF; float:left; width:217px; border:0; margin:0; padding:0;">
                    <div id="tablis2"><input type="image" src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/tab_TopTags_0.gif" width="91" height="24" border="0" onclick="showhidetab('tabcontent3'); return false;"/></div>
                    <div id="tabind2"><input type="image" src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/tab_MostPopular_1.gif" width="126" height="24" border="0" onclick="showhidetab('tabcontent4'); return false;"/></div>
                    </div>
                    <table width="217" border="0" cellspacing="0" cellpadding="0" id="tabcontent_tbl">
                      <tr>
                        <td><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/topTags_top.gif" /></td>
                      </tr>
                      <tr>
                        <td class="topTags_bg">
                        <div id="tabcontent_outer">
                        <!-- First Tab Lis Education Start-->

						<div id="tabcontent3" style="display:; float:left;">
                        																								<a href='http://www.ystoreblog.com/blog/tag/customer-registration/' class='tag-link-27' title='2 topics' style='font-size: 14.3pt;'>Customer Registration</a>
<a href='http://www.ystoreblog.com/blog/tag/data-backup/' class='tag-link-28' title='1 topic' style='font-size: 8pt;'>Data Backup</a>
<a href='http://www.ystoreblog.com/blog/tag/developer-tip/' class='tag-link-31' title='1 topic' style='font-size: 8pt;'>Developer Tip</a>
<a href='http://www.ystoreblog.com/blog/tag/ecommerce-reads/' class='tag-link-32' title='1 topic' style='font-size: 8pt;'>Ecommerce Reads</a>
<a href='http://www.ystoreblog.com/blog/tag/floating-cart/' class='tag-link-24' title='1 topic' style='font-size: 8pt;'>Floating Cart</a>
<a href='http://www.ystoreblog.com/blog/tag/merchant-summit/' class='tag-link-23' title='1 topic' style='font-size: 8pt;'>Merchant Summit</a>
<a href='http://www.ystoreblog.com/blog/tag/pubcon/' class='tag-link-29' title='1 topic' style='font-size: 8pt;'>PubCon</a>
<a href='http://www.ystoreblog.com/blog/tag/seo/' class='tag-link-30' title='4 topics' style='font-size: 22pt;'>SEO</a>
<a href='http://www.ystoreblog.com/blog/tag/social-media/' class='tag-link-26' title='1 topic' style='font-size: 8pt;'>Social Media</a>
<a href='http://www.ystoreblog.com/blog/tag/yahoo-web-analytics/' class='tag-link-20' title='1 topic' style='font-size: 8pt;'>Yahoo! Web Analytics</a>						                        </div>
                        <!-- First Tab Lis Education END-->

                        <!-- Second Tab Industry Articles Start-->
                        <div id="tabcontent4" style="display:none; float:left; font-family:Arial, Helvetica, sans-serif;">
                        						<li><a href="http://www.ystoreblog.com/blog/2011/02/rob-snell-shares-retail-smarts/">Rob Snell Shares Retail Smarts</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2011/01/developer-tip-the-case-of-the-disappearing-breadcrumbs/">Developer Tip: The Case of the Disappearing Breadcrumbs</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2011/03/yahoo-merchant-ecreamery-scoops-up-customers-with-contest/">Yahoo! Merchant eCreamery Scoops Up Customers With Contest</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2011/03/seo-friday-articles-to-kick-off-your-weekend/">SEO Friday: Articles to Kick Off Your Weekend</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2011/03/social-media-sharing-lets-customers-spread-the-word/">Social Media Sharing Lets Customers Spread the Word</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2010/04/real-world-ecommerce-rob-snells-pubcon-south-presentation/">Real World Ecommerce: Rob Snell&amp;#039;s PubCon South Presentation</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2010/03/introducing-floating-cart-manager/">Introducing Floating Cart Manager</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2010/05/yahoo-store-blog-gets-an-upgrade/">Yahoo! Store Blog Gets an Upgrade</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2008/10/introducing-yahoo-web-analytics/">Introducing Yahoo! Web Analytics</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li><li><a href="http://www.ystoreblog.com/blog/2009/06/rob-snell-gives-the-scoop-on-smx-advanced/">Rob Snell Gives the Scoop on SMX Advanced</a> <img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /><img src="http://www.ystoreblog.com/blog/wp-content/plugins/postratings/images/stars/rating_on.gif" alt="Average: 5 out of 5" title="Average: 5 out of 5" class="post-ratings-image" /> (5 out of 5)</li>						                        </div>
                        <!-- Second Tab Industry Articles END-->
                        </div>
                        </td>
                      </tr>
                      <tr>
                        <td><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/topTags_btm.gif" /></td>
                      </tr>
                    </table>
                    <!-- Right Tab Function End-->
                </div>
                <div class="sidebar_list">
                	<h3 class="ttl_Sidebar">Contributors</h3>
					<table width="185" border="0" cellspacing="0" cellpadding="0" class="tbl_contributors">
	<tr>
		<td width="46"><a href="http://www.ystoreblog.com/blog/author/jfarwell/" title="Posts by "><img src='http://www.ystoreblog.com/blog/wp-content/images/authors/8.png' alt='' title='' class='img_border' style='margin:4px 8px 0px 0px; border:1px solid #000000;' align='left' /></a></td>
		<td width="10">&nbsp;</td>
		<td><a href="http://www.ystoreblog.com/blog/author/jfarwell/">Jennifer Farwell</a></td>

	</tr>
	
	<tr>
		<td width="46"><a href="http://www.ystoreblog.com/blog/author/pboisver/" title="Posts by "><img src='http://www.ystoreblog.com/blog/wp-content/images/authors/5.png' alt='' title='' class='img_border' style='margin:4px 8px 0px 0px; border:1px solid #000000;' align='left' /></a></td>
		<td width="10">&nbsp;</td>
		<td><a href="http://www.ystoreblog.com/blog/author/pboisver/">Paul Boisvert</a></td>
	</tr>
	
	<tr>
		<td width="46"><a href="http://www.ystoreblog.com/blog/author/mober/" title="Posts by "><img src='http://www.ystoreblog.com/blog/wp-content/images/authors/17.png' alt='' title='' class='img_border' style='margin:4px 8px 0px 0px; border:1px solid #000000;' align='left' /></a></td>

		<td width="10">&nbsp;</td>
		<td><a href="http://www.ystoreblog.com/blog/author/mober/">Mike Ober</a></td>
	</tr>
	<tr>
           <td colspan="3" class="contributor_more"><a href="http://www.ystoreblog.com/blog?contributor=top">view all &raquo;</a></td>
        </tr></table>
                </div>
                <div class="sidebar_list">
                	<h3 class="ttl_Sidebar">Merchant Resources</h3>
                    <ul>
						<li><A href="http://updates.smallbusiness.yahoo.com/" target="_blank">Yahoo! Merchant Solutions System Status</A></li>
						<li><A href="http://developernetwork.store.yahoo.com/" target="_blank">Yahoo! Merchant Solutions Developer Network</A></li>
                        <li><A href="http://www.twitter.com/YSmallBusiness" target="_blank">Follow @YSmallBusiness on Twitter</A></li>
                        <li><A href="http://www.facebook.com/YahooSmallBusiness" target="_blank">Yahoo! Small Business on Facebook</A></li>
						<li><A href="http://searchmarketing.yahoo.com/index.php?cmp=yindex" target="_blank">Yahoo! Search Marketing</A></li>
						<li><A href="http://developer.yahoo.net/stores/" target="_blank">Yahoo! Developer Network</A></li>
                    </ul>
                </div>
                <div class="sidebar_list">
                	<h3 class="ttl_Sidebar">Yahoo! Blogs</h3>
                    <ul>
						<li><a href="http://ysearchblog.com/" target="_blank">Yahoo! Search Blog</a></li>
						<li><a href="http://www.ysmblog.com/" target="_blank">Yahoo! Search Marketing Blog</a></li>
						<li><a href="http://www.ypnblog.com/" target="_blank">Yahoo! Publisher Network Blog</a></li>
						<li><a href="http://developer.yahoo.net/blog/" target="_blank">Yahoo! Developer Network Blog</a></li>
						<li><a href="http://yuiblog.com" target="_blank">Yahoo! User Interface Blog</a></li>
						<li><a href="http://yodel.yahoo.com/" target="_blank">Yahoo! Yodel Anecdotal</a></li>
						<li><a href="http://blog.del.icio.us/" target="_blank">del.icio.us Blog</a></li>
						<li><a href="http://blog.flickr.com/" target="_blank">FlickrBlog</a></li>
						<li><a href="http://dir.yahoo.com/thespark" target="_blank">The Spark Blog</a></li>
						<li><a href="http://upcoming.org/news/" target="_blank">Upcoming.org News</a></li>
						<li><a href="http://yanswersblog.com/" target="_blank">Yahoo! Answers Blog</a></li>
						<li><a href="http://buzz.yahoo.com/buzz_log/" target="_blank">Yahoo! Buzz Log</a></li>
						<li><a href="http://ymailupdates.com/blog/" target="_blank">Yahoo! Mail Updates</a></li>
						<li><a href="http://blog.messenger.yahoo.com/" target="_blank">Yahoo! Messenger Blog</a></li>
						<li><a href="http://ymusicblog.com/" target="_blank">Yahoo! Music Blog</a></li>
						<li><a href="http://sports.yahoo.com/blogs" target="_blank">Yahoo! Sports Blog</a></li>
						<li><a href="http://www.yvideoblog.com/" target="_blank">Yahoo! Video Blog</a></li>
                    </ul>
                </div>
                <div class="sidebar_list">
                	<h3 class="ttl_Sidebar">Feedback</h3>
                    <!-- <p class="text"><a href="feedback-suggestions.php">E-mail us your feedback and suggestions.</a></p> -->
					<p class="text">
					<script language=javascript>
					<!--
					  var contact = "Email us your feedback";
					  var email = "store-blog-feedback";
					  var emailHost = "yahoo-inc.com";
					  document.write("<a href=" + "mail" + "to:" + email + "@" + emailHost+ ">" + contact + "</a>" + ".");
					//-->
					</script> For support inquiries, <a href="http://smallbusiness.yahoo.com/contactus/" target="_blank">contact Customer Care</a>.
					</p>
                </div>
                <div class="sidebar_list" id="borHor_red">
                	<h3 class="ttl_Sidebar">Comment Policy</h3>
                    <p class="text">We encourage comments and look forward to hearing from you. Please note that Yahoo! may, in our sole discretion, remove comments if they are off topic, inappropriate, or otherwise violate our <a href="http://docs.yahoo.com/info/terms/">Terms of Service</a>.
					</p>
                    <p class="text" style="text-align:center">
                    Copyright &copy; 2010 Yahoo! Inc.<br />
                    All rights reserved.<br />
                    <a href="http://privacy.yahoo.com/privacy/us/ysearch/" target="_blank">Privacy Policy</a> - <a href="http://docs.yahoo.com/info/terms/" target="_blank">Terms of Service</a><br />
                    <a href="http://smallbusiness.yahoo.com/ecommerce/" target="_blank"><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/ecommercebyYahoo.gif" border="0" style="margin:15px 0 5px 0;" alt="ecommerce provided by Yahoo! Small Business" /></a>
                    </p>
                </div>
                </div>
                </td>
                <td width="5" valign="top" class="rightcol_rtBar"><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/rightcol_rtBar.gif" /></td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/rightcol_btm.gif" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>		<!-- side bar rt ends -->
        </td>
      </tr>
    </table>
    </td>
  </tr>
<tr>
    <td class="footer_sdw">
    <table width="821" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td><img src="http://www.ystoreblog.com/blog/wp-content/themes/ystore/images/footer_bar.gif" /></td>
      </tr>
      <tr>
        <td class="footer_bg">
		<p>Powered by <a href="http://wordpress.org" target="_blank">WordPress</a> on <a href="http://smallbusiness.yahoo.com/webhosting/" target="_blank">Yahoo! Web Hosting</a>. Designed by <a href="http://www.exclusiveconcepts.com" target="_blank">Exclusive Concepts</a>, the <a href="http://www.exclusiveconcepts.com" target="_blank">Yahoo! Store Marketing Experts</a>.<br />
		Copyright &copy; 2010 Yahoo! Inc. All rights reserved. <a href="http://privacy.yahoo.com/privacy/us/ysearch/" target="_blank">Privacy Policy</a> - <a href="http://docs.yahoo.com/info/terms/" target="_blank">Terms of Service</a></p>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<script type="text/javascript" src="http://d.yimg.com/mi/ywa.js" ></script>
<script type="text/javascript">
YWATracker = YWA.getTracker("10001139297862");
//YWATracker.setDocumentName("PAGEID");
//YWATracker.setDocumentGroup("Blog");
YWATracker.setDomains("*.ystoreblog.com");
YWATracker.submit();
</script>
</body>
</html>
<!-- Dynamic page generated in 2.012 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2011-04-05 07:10:17 -->
