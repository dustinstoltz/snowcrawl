#Solo script
import snowcrawl

def fiveWaves(self):
	return self.current_wave > 5

if __name__ == '__main__':
	my_path = 'results1/'

	my_params = snowcrawl.Parameters(
		time_limit = 5,
		wave_size = 20,
		save_states=True, save_files=True, save_edges=True,
		prioritize_urls = True,
		process_url_function = snowcrawl.defaultProcessUrl,
		decide_terminate_function = fiveWaves
	)
	my_seed_list = ['www.whitehouse.gov']

	my_crawl = snowcrawl.SoloCrawler()
	my_crawl.runUntilDone( my_path, my_params, seed_list=my_seed_list, overwrite_existing_files=True )

